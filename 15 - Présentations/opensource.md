# Le choix de l’Open source !

Nos épices, nos recettes et notre code source sont libres d’utilisation et d’amélioration.

## L’Open Source
Une méthodologie de développement encadré juridiquement par des licences qui contribuent à un cercle vertueux d’amélioration d’une technologie. 

L’Open Source repose sur :
une vision profondément humaniste,
de l’innovation collaborative,
la reconnaissance par le partage des savoirs.

Richard Stallman, initiateur du mouvement du logiciel libre, définissait le logiciel comme suit : 

>“le logiciel est, à la manière de la connaissance scientifique, une forme de patrimoine de l’humanité, un bien commun que nous enrichissons collectivement, pour le bien être de tous”.
