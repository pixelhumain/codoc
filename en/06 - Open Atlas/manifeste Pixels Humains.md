

#Réseau social citoyen, au service du bien commun

#Manifeste

## Parce que la vie, la joie, la paix, le respect, et l'amour sont des valeurs communes à l'ensemble des êtres humains peuplant notre magnifique planète.

## Parce que nous voulons vivre des vies heureuses, où nous sommes protégés de la faim, de la soif, des conditions  climatiques, et en bonne santé.

## Parce que nous voulons vivre des vies joyeuses et paisibles, où la guerre nucléaire ne menace plus la survie même de l'Humanité, et où le respect de la vie dans son ensemble prime sur tout intérêt personnel ou lucratif.

## Parce que ces valeurs doivent être défendues et pratiquées, corps et âmes, par chacun d'entre nous, afin de conserver ce Monde Vivant, et créer les conditions favorables à l'épanouissement de toutes personnes sur Terre.

## Parce que nous savons que l'être humain ne pourra pas éternellement piller, gaspiller, détruire et polluer les ressources naturelles de notre environnement, et que seul un changement radical de notre façon de produire et de consommer pourra rendre durable la présence de 7 milliards d’êtres humains sur Terre.

## Parce que nous savons que la majorité des politiciens sont des artisans actifs de ce monde qui fonce droit dans un mur, et qu'aucun changement significatif ne pourra être initié par eux.

## Parce que nous savons que rien ne changera si nous ne décidons pas nous-même de devenir les acteurs d'une autre société. autre monde.

## Parce que nous n'avons plus confiance en la classe politique qui prétend savoir ce qui est bien pour nous nous diriger, et que nous voulons participer activement et directement à la vie citoyenne et politique de nos territoires, au niveau local, départemental, régional, et national.

## Parce que nous voulons assumer pleinement notre rôle de citoyen acteur de la société et de la démocratie.

## Parce que nous avons tous les capacités de réfléchir, analyser, débattre, proposer, et surtout : d'agir.

## Parce que nous savons que les réponses aux défis environnementaux, écologiques, sociétaux, sanitaires, éducatifs, démocratiques, etc, existent déjà, et qu'il ne tient qu'à nous de les mettre en place.

## Parce que nous ne voulons pas faire de cette magnifique planète, une poubelle que nous aurons honte de léguer à nos enfants.

## Parce que le règne du capital n'a que trop duré, et qu'il est temps de remettre l'Humain au centre de nos préoccupations.


Nous, Pixels Humains de toutes origines, et dans toute notre diversité, décidons d'unir nos forces, nos intelligences, nos compétences, nos savoirs et savoirs faire, nos ressources, nos énergies, nos aspirations, nos recherches, nos rêves, 
à travers la création, l’usage et la promotion d’outils numériques libres, coopératifs et révolutionnaires, afin d’imaginer et de co-construire ensemble un monde plus humain, qui nous ressemblera à tous, et auquel chacun pourra contribuer 
à sa mesure, localement et POUR LE BIEN COMMUN.



