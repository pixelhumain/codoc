# Gestion du Kiltir Vrac
## Informations
-   Montant minimal : 150 €
-   Référente chez _Open Atlas_ : ?
-   Référent au _Comptoir du Vrac_ : Giovani `epicerie@comptoirduvrac.re`


## Préparation du tableau
Le tableau est à envoyer aux membres du groupement d'achat la première semaine de chaque mois. Le bon de commande rempli est à envoyer au Comptoir du vrac **le DEUXIÈME VENDREDI de chaque mois**.


### 1 - Formatage
-   Accéder au [dossier “Kiltir Vrac”](https://drive.google.com/open?id=1V1H1VsUukdx3XrQ4ZcMESo3tKls9XFbe)
-   Dupliquer
-   Renommer et ouvrir
-   Dans la feuille `Gestion des produits`
    -   `Données > Vue filtrée > Aucun`
    -   Supprimer le contenu des colonnes `A à K`
-   Dans la feuille `Vue participant`
    -   `Données > Vue filtrée > Aucun`
    -   /!\ à ne pas supprimer la ligne `TOTAL` tout en bas
    -   Supprimer le contenu à partir de la colonne `I`
-   Dans la feuille `Bon de commande`
    -   `Données > Vue filtrée > Aucun`
    -   Sélectionner les colonnes ayant du contenu
    -   Clique droit > `Effacer les colonnes...`


### 2 - Copie du nouveau catalogue
-   Ouvrir le fichier du comptoir du vrac
-   Copier les colonnes `A à K`
-   Coller les produits dans `Gestion des produits`

ATTENTION : Si le bon de commande vierge n'a pas été envoyé par le Comptoir avant la première semaine du mois, ré-utiliser le bon de commande vierge envoyé le mois précédent, ils s'arrangeront.

Un nombre de produits différents entre deux commandes demandera un ajustement de l’affichage des totaux.


### 3 - Finalisation
-   Décocher les produits que l’on ne souhaite pas proposer (non éthique, doublon, …) dans la colonne `SELECTION`
-   Dans `Vue participant` : `Données > Vue filtrée > Produits selectionnés`
-   Vérifiez les erreurs (ex : prix aberrant) et simulez l’ajout d’un participant
-   Si possible : effectuer sa commande pour donner l’exemple


## Gestion des commandes
### 1 - Communication
Faire le premier appel dès que possible et un autre à J-2.
-   Affichez la vue filtrée dans `Vue participant` et copiez en l'url qui s'affiche pour la diffuser
-   Communication
    -   dans le journal de [Kiltir Vrac](https://communecter.org/#page.type.projects.id.5a004a5e40bb4eb718f5ca70)
    -   dans le chat avec `@all`


### 2 - Commander
-   Prénom et nom dans la première ligne
-   **Nombre d’unité** dans sa colonne en face du produit
-   Total en bas
-   Une fois la date limite dépassée :
    -   Dans "Gestion des produits" checker chacun des produits
    -   Si besoin modifier la "Quantité de produits commandés" si, par exemple, on sait que le colis incomplet sera de toute façon vendu.
    -   Activez la vue "Bon de commande"
    -   Copier le contenu des colonnes de A à L
    -   Dans l'onglet "Bon de commande" : `Édition > Collage spécial` : Copier uniquement les valeurs + Copier uniquement le format
    -   Remplir le bordereau en haut du fichier (PRÉNOM : Open, NOM : Atlas, N° : +262 6 93 91 85 32 (Tibor), MAIL : contact@communecter.org)
    -   Enregistrer et envoyer le fichier à Giovany : `epicerie@comptoirduvrac.re` avec en copie `comptoir-du-vrac@googlegroups.com` et `contact@communecter.org`.
        
Un produit est commandé mais est déjà disponible en stock ? On ne commande pas sauf :
- si la commande ne dépasse pas la moitié d'un colis -> on vend le stock et on modifie la commande du participant (couleur + commentaire) ![](/Images/kiltirvrac-pas-dispo.png)
- si la commande dépasse et que le fait d'en re-commander n'augmente pas le stock actuel


### 3 - Distribution
-   Le comptoir du vrac nous informe de la date de réception . On transmet l'information aux participants à la commande + un appel à l'aide pour aider à décharger (il faut 4 minimum personnes).
-   Les volontaires aident à décharger les palettes reçues au Port pour les amener au Comptoir du vrac. Dans la foulée nous vérifions et récupérons la commande du Kiltir Vrac qui se retrouve enfin au Kiltir Lab.
-   Nous lançons un appel à venir récupérer la commande **dans des contenants personnels**
-   La commande de chaque personne est préparée.
-   Le jour J la personne :
    -   récupère sa commande
    -   (optionnel) : se sert dans le surplus de produits (bien noter les quantités)
    -   paye le montant (ou promet de le faire)

Les participants ont 7 jours pour venir récupérer leur commande, sinon nous considérons que c'est du surplus.

