# Integration du SSO avec plusieurs applications

## Préalable

- Recherche l'adresse de retour de l'application. Si elle n'est pas connu, mettre une adresse générique, le sso fera un retour en json indiquant l'adresse de retour que l'application à indiqué.
- Contacter Thomas Craipeau sur chat pour lui demander un token OAuth2 avec l'adresse de retour

Données transmise après le login:
- sub: user._idd
- user_id: user._id,
- name: user.profile.pixelhumain.name,
- displayName: user.profile.pixelhumain.name,
- username: user.profile.pixelhumain.username,
- usernameLo: user.profile.pixelhumain.username.toLowerCase(),
- email: user.profile.pixelhumain.email,
- email_verified: true,
- picture: `${Meteor.settings.urlimage}${user.profile.pixelhumain.profilThumbImageUrl}`,
- apiToken: user.profile.token,

## Moodle

- suivre les etapes suivantes:
- https://docs.moodle.org/311/en/OAuth_2_services
- Supprimer la demande d'email dans la table: 
> It seems, another option (for urgent purposes) instead of code patching you can set 0 in `requireconfirmation` column of `mdl_oauth2_issuer` table in DB.
> source: https://tracker.moodle.org/browse/MDL-67556

