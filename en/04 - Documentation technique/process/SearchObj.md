# SearchObj
https://codimd.communecter.org/NFl7lijvQCSUeQ_x-jLSPQ?view

# Configuration de l'objet SearchObj - Options (filters.js) 
L'objet searchObj inclut beaucoup de méthodes allant des vues au events liés aux filtres,à l'algorithme de recherche js à l'affichage des résultats
Cet objet permet d'unifier toutes les dépences aux logiques de filtres et de recherche comme pour les espaces d'administration, les applications de recherche (search, agenda, live, dashboard, answer, etc) et les communautés
## I - Arborescence central de l'objet

|_ `filters` : _Object_ contient toutes les vues des filtres et ces fonctionnalités liées
|_ `header` : _Object_ contient toutes les vues et fonctionnalité de l'en-tête de la recherce
|_ `search` : _Object_ contient l'ingénieurie de recherche de l'initialisation des parametres, la récupération/traitement des données et fonction de callback 
|_ `results` : _Object_ contenant les méthodes de traitement de la donnée et d'affichage dans son contenu
## II - Options builder (ex : co2/views/app/filters/search)
Plusieurs options de configurations peuvent être envoyée à l'init du searchObj représenté par la variable pInit  
La liste décrite ci-jointe est non exhaustive et pourra laisser place à encore beaucoup de configuration : 

* **`urlData`** ^[string]^ definit l'appel ajax de recherche
* **`container`**  ^[string]^ dom d'affichage pour les filtres. Il vient peupler l'entrée searchObj.filters.dom
* **`header`** ^[object]^ Surchargé en extend permet de lister les boutons et leur event désiré 
    * **`dom`** ^[string]^ dom html où le header sera chargé   
    * **`options`** ^[object]^ contient les container et les boutons par container
        *  **`group`** ^[object]^ composé des vues du header (compteur de résultat, carto, pdf, csv etc)
    * **`views`** ^[object]^ ensemble de fonctions view spécifique qui peuvent être envoyé à l'init est seront rajouté à l'objet traité
    * **`event`** ^[object]^ ensemble de fonctions event (jquery) spécifique qui peuvent être envoyé à l'init est seront rajouté à l'objet traité

* **`filters`**  _[Object]_ : contient toutes les vues des filtres ainsi que les events liés et quelques actions spécifique au filtres
    * Cette entrée dans pInit contient la liste des filtres à activer ex {"text": true, price: true, etc}

    * Une entrée de cette liste peut-être aussi sous la forme d'un object (etc : "source":{view: "dropdownList", event : "exists", label:"Status", "field": "source.validated", list:{}})`
* **`results`** ^[Object]^ : contient des paramêtres de configuration des vues (peut-être largement dévelopé en terme de configuration)
    * **`dom`** ^[string]^ : dom HTML où les résultats seront affichés
    * **`renderView`** ^[string]^  nom de la fonction où la data sera envoyée pour récupérer l'affichage html (ex: directory.elementPanelHtml)
    * **`smartGrid`** ^[boolean]^ active ou non masonry qui permet l'agencement des résultats en quinconsse dynamiquement
    * **`community`** ^[string]^ : object permettant de faire du spécifique sur une communauté ( affiché les status /roles) et la gérer
        * **`links`** ^[object]^ contient les liens de l'element sur lequel on recherche
        * **`connectType`** ^[string]^ permet de voir quel links on visualise (exemple members)
        * **`edit`** ^[boolean]^ permet de définir si le rendu aura les actions d'administration dans les carte d'une communauté
    * **`map`** ^[object]^ configure le comportement de la carte avec les résultats
        * **`active`** ^[boolean]^ definit si l'app s'initialise sur la map ou sur le directory (default :false)
		* **`sameAsDirectory`** ^[boolean]^ definit si la carte est indépendante de l'affichage directory ou pas (permet de tout afficher sur la carte),
		* **`showMapWithDirectory`** ^[boolean]^ permet de dire si la map et le directory sont actifs en même temps (ex: costum deal )
* **`defaults`** ^[object]^ est l'ensemble des paramètres par défault envoyé au controlleur (voir fonction searchObj.initDefaults ) exemple: costum.views.ctenat.filter.php
* **`loadEvent`** ^[object]^ definit les espaces par fonctionnalité spécifique (live, agenda, scroll, pagination, dashboard) _[**nota bene** : cet algo peut être potentielement refactorer ou amélioré]_
    * **`default`** ^[string]^ par default il est définit sur scroll mais il peut être définit pour des applications différentes comme pour le live l'admin ou encore l'agenda qui des méthodes très différentes pour le fonctionnement des résultats 
    * **`options`** ^[object]^ vient modifier les options de chaque comportement (live/agenda/admin) du searchObj _[**nota bene** : gros potentielle de développement]_

## III - Filtres dans l'objet
> Comme vu ci-dessus, le searchObj.filters gére à partir de la liste envoyée l'affichage et les événements des filtres afin de peupler ou dépeupler searchObj.search.obj qui lui communique avec la partie métier  
> 
### 3.1 Vues dans filters

A l'initialisation de l'instance, le système va boucler sur la liste des filtres appelés pour créer leur vue.

**Exemple la liste des filtres dans ctenational #territoires**:
```
domainAction : {
    view : "dropdownList",
    type : "tags",
    name : "Domaine d'action",
    event : "tags",
    list : costum.lists.domainAction
},
cibleDD:{
    view : "dropdownList",
    type : "tags",
    name : "Objectif",
    event : "tags",
    list : costum.lists.cibleDD
},
scope : true,
scopeList : {
    name : "Région",
    params : {
        countryCode : ["FR","RE","MQ","GP","GF","YT"], 
        level : ["3"]
    }
}
```
Dans les différents objets, si l'objet à une entrée view alors il va chercher la vue en question.

**Exemple pour domainAction** il ira chercher la vue dans la function `searchObj.filters.views.dropdownList()`

On peut aussi lui donnée une entrée `dom` spécifique pour qu'il aille dans un container différent que définit à l'init avec `pInit.container`

Pareil, pour la clé `scope` qui est un boolean, il ira chercher la vue  `searchObj.filters.views.scope()`. Ainsi si l'entrée view n'est pas définit. le système recherche avec la clé directement

### 3.2 Evenement pour les filters
De nombreux événements sont disponibles avec une initialisation qui est similaires aux vues. Le système boucle sur la liste. Si les filtres présents ont une entrée `event` il va recherche la fonction `searchObj.filters.events[v.event]` sinon il recherchera à partir de la clé.
Certains evenements sont très spécifique à une entrée comme `text`, `price`, `scope` et d'autres vont être spécifiques à des manières d'envoyer la donnée pour la recherche comme `exists` ou encore `inArray
`
### 3.3 Field, key and value
Chaque filtre contient un field par défaut la clé qui définir l'attribut sur lequel on recherche en db
``` 
{
    name
    tags
    locality
    type
    filters : ^entrée fourre tout pour la recherche^
        {
            links.members.{$id} : {$exists : true}
        }
}
```
Généralement l'attribut `field` est le même l'ensemble du filtre. Or on peut maintenant pour un même liste de filtres définir des fields différents comme pour mes communautés ^[co2/assets/default/profilSocial.js]^
```
filters.status = {
    view : "dropdownList",
    type : "filters",
    name : "Statuts",
    action : "filters",
    typeList : "object",
    event : "exists",
    list :  {
        "admin" : {
            label: "Administrateur",
            field : "links.memberOf.{$idOrga}.isAdmin",
            count : 130 
            value : true
        },
        "members" : {
            label: "membre",
            field : "links.memberOf.{$idOrga}.isInviting&&links.memberOf.{$idOrga}.toBeValidated",
            value : false,
            count : 20 
        },
        "isInviting": {
            label:"Invitation non confirmée",
            field : "links.memberOf.{$idOrga}.isInviting",
            value : true,
            count : 0 
        },
        "toBeValidated": {
            label:"En attente de validation",
            field : "links.memberOf.{$idOrga}.toBeValidated",
            value : true,
            count : 197
        }
    }
}
```

### 3.4 Comportement d'activation des filtres 
Les listes de filtres peuvent être affichées en dropdown ou en liste toute visible ou encore en liste dropable. Ainsi leur comportement d'activation sera différents bien que les attributs eux utilisés dans l'algo reste les mêmes. On ajoutera l'attribut `active : true` si l'on veut que qu'il ajoute une classe active au filtre clique. Sinon il ajoutera un bouton d'activation 

Les deux fonctions centrales liées aux événements des filtres sont 
- `searchObj.filters.manage.addActive`
- `searchObj.filters.manage.removeActive`

### IV - searchObj.search la logique js de recherche

Le search contient l'algorithme qui permet de traiter la donnée afin de l'envoyer au controleur définit par `pInit.urlData` donné à l'initialisation de l'objet et de traiter la donnée afin de l'envoyer au searchObj.results.render . 

Il gére aussi les différents processus de début de fin et les condition de lancement de la recherche.

`searchObj.search.multiCols` _[object of methods]_ contient un algorithme qui permet de mixer les résultats de différentes collections en les ordonnant par nom ou par date. Cette méthode de mix ne marche qu'avec le scroll  et non la pagination.

`searchObj.search.obj` est l'objet final avec les params envoyé en requete POST. Il passe par un constructUrlAndData pour envoyer les bonnes données et pour envoyer les params dans l'url (méthode manageHistory)


### V - Les comportements du moteur de recherche 

Différents objets sont présent 
```
 [] Live 
     { objet permettant la gestion de fil d'actus}
     [modules/news/views/co/index.php]
 [] Agenda 
     {object permettant de rajouter les méthodes spécifique au fonctionnement de l'angenda type scroll}
     [modules/co2/app/filters/agenda.php]
 [] Admin 
      {objet contenant les méthodes spécifuques au espace admin il permet de rediriger sur les vues tableau et les actions liées à l'objet adminDirectory dans co2/assets/admin/directory.js}
 [] Scroll 
     { objet spécifique au fonctionnement d'affichage et de chargement des résultats en scrollant}
 [] Pagination 
     {contient le process permetant la recherche par page}
      
```
Toutes ces méthodes définies par le paramètre loadEvent permettent de surcharger ou de modifier le comportement globale du searchObj notament pour l'affichage des résultats ou la manière dont on met en route ou on finit les processus de recherche 

Cette partie du code pourrait être cleanée/unifier afin de rendre ces méthode surchargeable dans les options d'initialisation 
```
exemple pInit.search.successComplete : function();
         pInit.search.initProcess : function();
          pInit.search.prepData : function();
           pInit.search.callback : function();
            pInit.results.render : function();
             pInit.results.beforeRender : function();
              pInit.results.afterRender : function();
```

Voici une liste de fonctions qui pourrait être interprétées comme méthode dans l'objet d'initialisation 
Ces méthodes sont aujourd'hui interprétées (pas avec les mêmes noms) par le système via le loadEvent utilsé
exemple searchObj.live.successComplete();

# exemple 
## filtre sur une page dashboard 
https://cte.ecologique-solidaire.gouv.fr/#dashboard
dans un fichier php en render
l'url qui sera appelé a chaque changement de filtre sera 
baseUrl+"/costum/ctenat/dashgraph"
et dans filter.js il ne se passe encore pas grand chose 
```
<div id="filters-dash" class="col-xs-12 searchObjCSS">
    
</div>

<div id="dashboardContainer">

</div>

<script type="text/javascript">
    alert("dashboardFilters");
    var paramsSearch = {
        container : "#filters-dash",
        urlData : baseUrl+"/costum/ctenat/dashgraph",
        loadEvent: {
            default : "dashboard",
            eventType: "scroll",
            eventTarget: null,  
        },
        dashboard : {
            options:{}
        }, 
        results : {
            dom : "#dashboardContainer"
        },
        filters : {
            text : true,
            scope : true,
            themes : {
                lists : <?php echo json_encode(CO2::getContextList("filliaireCategories")) ?>
            }
                    
        }
    };

    var filterSearch={};

    jQuery(document).ready(function() {

        mylog.log("render","/modules/costum/views/custom/ctenat/dashboardFilters.php");
        filterSearch = searchObj.init(paramsSearch);
        filterSearch.search.init(filterSearch);
    });

    
</script>
```

dans filter.js
```
dashboard : {
		options : {},
		successComplete : function(fObj, data){
			//alert(fObj.search.loadEvent.default);
			fObj.search.currentlyLoading = true;
			$(fObj.results.dom).find(".loader").remove();
    		$(fObj.results.dom+" .processingLoader").remove();
        	$(fObj.results.dom).append(data);
        	
		}
	},
```













