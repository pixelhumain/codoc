

# export PDF
ExportController
    'pdfelement'    => 'citizenToolKit.controllers.export.PdfElementAction',
    'csvelement'    => 'citizenToolKit.controllers.export.CsvElementAction',
    'csv'   => 'citizenToolKit.controllers.export.CsvAction',
costum/views/custom/ctenat/pdf/ 
- answers
- copil
to Debug comment sections



# EXPORT CSV

#### JS
C'est json2csvParser qui se chargera de convertire la liste des élements en csv avec l'ordre des colonne passer en parametre. Voir le code dans co2/filters.js

`Faire attention au caractere # dans les string car cela bloque le parser`



### Answers

#### PHP 
L'action qui récupere et traite la données ce fait ici : costum.controllers.actions.ctenat.api.AnswerscsvAction 

La donnée est retourner en JSON sous se format 

``` {
    "results" => $newList, // Liste des éléments après traitement
    "fields"=> $sortOrder, // L'ordre des colonnes pour le csv
    "allPath"=>$allPath // l'ensemble des chemins vers la données.
};
```



### Territoire

#### EXPORT CSV
##### PHP 
L'action qui récupere et traite la données ce fait ici : citizenToolKit.export.csvAction 

C'est une fonction générique qui peut etre réutiliser sur n'importe quel element lier au systeme de recherche.

Si besoin de faire un traitement spéciale, voir le meme procèder que Answer et faire une action spécifique.

La donnée est retourner en JSON sous se format 

``` {
    "results" => $newList, // Liste des éléments après traitement
    "fields"=> $sortOrder, // L'ordre des colonnes pour le csv
    "allPath"=>$allPath // l'ensemble des chemins vers la données.
};
```

