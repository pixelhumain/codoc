# Référencer des initiatives
Référencer permet d'œuvrer pour la libre diffusion de la connaissance de nos territoires. Remerciez-vous 👍
Le référencement consiste à **ajouter de nouveaux éléments dans Communecter** (par exemple, ajouter une organisation ou un évènement).


## Récolter
Le but d'une récolte est d'avoir sous la main (en format papier ou numérique) les données indispensables au référencement. Par exemple, pour une organisation, il faut au minimum avoir : son nom, son type (asso, entreprise, groupe informel, ...) et sa ville. Il existe plusieurs manières de récolter des données. Si vous ne savez pas par où commencer, vous pouvez utilisez votre moteur de recherche favori et des mots clés comme, par exemple, `liste des associations + le nom de votre ville` ou `coopératives + votre département`. Avec un peu de chance vous tomberez sur [une page comme celle-ci](http://www.mairie-saintpaul.re/campaigns/listing-des-associations/) listant toutes les associations de votre ville.

Vous pouvez également faire une demande auprès de votre mairie.
Si vous avez déjà fait ce genre de demandes, ce serait chouette de nous dire comment ça s'est passé en commentaire.
La dernière méthode consiste à sortir de chez soi pour aller directement sur le lieu que vous souhaitez ajouter à Communecter. Le plus convivial est de le faire à plusieurs en [organisant une cartopartie](/5 - Déployer/Tutoriels et ressources/cartopartie.md).


## Inviter
L’alternative à la récolte est de faire en sorte que les structures se référencent elles-mêmes. Voici un exemple de mail que vous pouvez envoyer pour les y inviter.

Exemples de mail

> Je me permets de vous contacter concernant votre groupement pour l'insertion des personnes handicapées. Je travaille avec l'association Open Atlas qui souhaite valoriser des initiatives humaines et éthiques telles que les vôtres et qui sont à l'origine du réseau sociétal Communecter. En effet j'ai tenté de référencer votre association dans notre annuaire collaboratif mais il me manque votre adresse postale. Dans ce sens, je vous invite à bien vouloir vous ajouter sur le réseau en cliquant ici : [communecter.org](http://communecter.org/). Cela vous permettra de donner encore plus de visibilité à votre association. Si vous ne pouvez pas le faire, il suffit de m'envoyer votre adresse postale par retour de ce présent mail. Je reste à votre écoute et vous remercie d'avance. Cordialement,


## Compléter
Cette étape n'est pas obligatoire mais permet une meilleure qualité des informations que vous souhaitez faire apparaître dans Communecter. Il faut reprendre chaque élément un à un pour y associer des informations additionnelles comme les horaires, une description, des images, ... C'est également le moment de vérifier si votre élément est toujours d'actualité.

Est-ce utile d'ajouter cette association qui n'a plus d'activité depuis 5 ans ?


## Ajouter
Deux solutions existent pour importer vos données dans Communecter :
-   créer un à un chacun des éléments via votre compte utilisateur
-   utiliser [le module d'importation](/2 - Utiliser l'outil/Fonctionnalités/importer.md)

Le module d'import est à privilégier pour ajouter beaucoup d'organisations (15 ou +), surtout si vos données sont sous forme de tableau.


## Données identifiées
-   [Grainothèques](http://umap.openstreetmap.fr/fr/map/carte-des-grainotheques_5922#6/45.429/2.659) (uMap)
-   [Répertoire National des Associations](https://www.data.gouv.fr/fr/datasets/repertoire-national-des-associations/)
-   [DataAsso](http://www.dataasso.fr/)
