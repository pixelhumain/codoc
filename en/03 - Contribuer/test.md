# Tester Communecter
Rapporter un bug est une manière simple et efficace de faire avancer ce bien commun 👍 .
La communauté de testeurs se trouve ici : [communecter.org/#@cobaye](https://www.communecter.org/#@cobaye)


## Tester l'outil
Nous lançons régulièrement des appels dans la messagerie #cobaye où nous expliquons le processus. En dehors de ces phases de test vous pouvez contribuer à la chasse aux bugs de cette manière :
1.  Choisissez un ticket ayant le label `to test` ou testez une fonctionnalité quelconque ([liste](/2 - Utiliser l'outil/Fonctionnalités/liste.md)) : [lien direct](https://gitlab.adullact.net/pixelhumain/co2/issues)
2.  Connectez vous : **[communecter.org](http://communecter.org/)**
3.  Testez la manipulation décrite sur le ticket
4.  Commentez le ticket (nécessite un compte GitLab) ou faites nous part de vos remarques sur le [chat de test](https://www.communecter.org/#page.type.organizations.id.574db1e540bb4e1e0d2762e6).


Quelques conseils lorsque vous trouvez un bug :
-   Faites des screenshots, si possible animé (voir ci dessous).
-   Ouvrez la console (Ctrl+Maj+K)
-   Découpez un maximum vos bugs (1 message = 1 bug)
-   Faites des tests en changeant un ou deux paramètres
    -   Utilisez le mode sans échec de votre navigateur ([firefox](https://support.mozilla.org/fr/kb/resoudre-problemes-firefox-mode-sans-echec))
    -   Désactivez des extensions (ex : les bloqueurs de pubs et de cookies)
    -   Réalisez votre action dans un autre élément
    -   Avec un autre navigateur


## Rapporter un bug
Plusieurs solutions s'offrent à vous pour faire part d'un bug aux développeurs (ci-dessous par ordre de préférence).
-   **Créez une "issue"** [**sur GitLab**](https://gitlab.adullact.net/pixelhumain/co2/issues)
-   **Ou laissez un message [ici](https://www.communecter.org/#@cobugs)**
-   Ou laisser un message sur [le chat](https://chat.communecter.org/channel/cobugs)
-   Ou envoyer un [mail](mailto:contact@communecter.org)
-   Ou envoyer un [courrier](http://open-atlas.org/#contacts) 🤓

Pour se connecter sur Gitlab il faut d'abord se créer un compte et se connecter sur adullact.net : [formulaire](https://adullact.net/account/register.php)


## Enregistrer une capture d'écran
### Capture simple
Tutoriels : [Windows](https://fr.wikihow.com/faire-une-capture-d%27%C3%A9cran-sous-Windows), [Linux](https://fr.wikihow.com/prendre-une-capture-d%E2%80%99%C3%A9cran-sous-Linux) et [Mac](https://support.apple.com/fr-fr/HT201361).

Firefox intègre désormais un outil de capture d'écran avec quelques options de modification : [Firefox Screenshot](https://support.mozilla.org/fr/kb/firefox-screenshots-capture-ecran)


### Capture vidéo
-   Gif : [Peek (linux)](https://angristan.fr/peek-enregistrer-gif-linux/), [ScreenToGif (windows)](https://www.screentogif.com/?l=fr_fr)
-   Vidéo : [Screencast-O-Matic](https://screencast-o-matic.com/)
