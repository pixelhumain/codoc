Au sein de communecter, notre méthode de travail est basée sur l'action : on favorise plus l'action à la discussion.

Le schéma classique utilisé par la plupart des organisation est : on prend une décision si une action est à faire ou non, et on fait l'action (parfois même on continue à faire une action qui ne fonctionne pas parce qu'on en a pris la décision). Cela peut prendre du temps en délibération et parfois les personnes effectuant l'action ne sont pas d'accord avec la décisions prises.

Le schéma que nous avons adopté est différent : on fait l'action, si elle marche (c'est à dire que des personnes continuent de faire l'action), l'action continue, sinon, elle s'arrête. Ainsi, la plupart du temps, pas besoin de prendre de décision, on favorise l'initiative individuelle.

## Qu'est ce qu'une action ?

### Définition

A chaque fois que l'on fait quelque chose, c'est une action : prendre un objet, manger, regarder un arbre...

Au sein du commun, quand on parle d'action, on fait référence aux actions qui rentrent dans l'objectif de ce dernier: "créer la meilleure plateforme internet pour permettre à chaque personne de savoir ce qu'il se passe au sein de sa localité, et de pouvoir facilement contribuer aux projets et organisations de ce territoire."

**Pour être plus rigoureux** : Une action est une mise en mouvement d'une personne pour répondre à un but bien précis. Elle est facilement faisable et compréhensible, et peu coûteuse en temps. Elle est liée à des compétences bien spécifiques et parfois à un nombre de participants.

### Caractéristiques

Une action c'est :

-   Un objectif
-   Des ressources :
	-   qu'on va utiliser
	-   qu'on va créer
-   Des compétences
-   Un nombre de participants
-   Court dans le temps

### Exemples

Voici quelques exemples d'actions :

-   coder une fonction
-   publier un document
-   écrire une page dans le wiki
-   ...

## Qu'est ce qu'une mission ?

### Définition

Parfois l'échelle d'une action est trop petite pour organiser son travail. On préfèrera alors parler de mission. Une mission est composée de plusieurs actions qui répondent à un objectif. La mission invoque, comme l'action, des compétences spécifiques et parfois aussi un nombre de contributeurs bien précis. A la différence de l'action, une mission est longue dans le temps.

### Caractéristiques

Une mission c'est :

-   Un objectif
-   Des ressources :
-   -   qu'on va utiliser
-   -   qu'on va créer
-   Des compétences
-   Un nombre de contributeurs
-   Composé de plusieurs actions
-   Long dans le temps

### Exemples

Voici quelques exemples de mission :

-   Implémenter un nouveau module au sein de Communecter
-   Faire une campagne de communication
-   Partir à la recherche de partenaires
-   ...

## En résumé

### Caractéristiques d'un commun

On peut considérer que la mission est un petit commun, et que l'action est une petite mission. Les caractéristiques de ces 3 entités sont très similaires. Voici ce qu'est un commun :

-   Un objectif
-   Des ressources :
-   -   qu'on va utiliser et rendre pérenne
-   -   qu'on va créer et rendre pérenne
-   Des compétences
-   Un nombre de contributeurs
-   Composé de plusieurs actions et missions
-   Très long voire illimité dans le temps

### 3 différentes échelles

Dans un commun, vous avez 3 niveaux d'abstraction :

-   Le commun qui va réunir toutes les personnes qui veulent œuvrer pour son objectif.
-   Les missions qui permettent de mettre en mouvement beaucoup de monde pour une durée longue et remplir un objectif ambitieux. Cet objectif est inclut dans l'objectif du commun.
-   Les actions qui soit font partie d'une mission, soit sont indépendantes.

L'échelle d'une mission et d'une action ne sont pas les mêmes, mais leurs règles et la manière d'aborder leur documentation est exactement pareil.

## Règles d'une action / mission

_Pour plus de lisibilité, on utilisera le mot action pour parler du duo action/mission._

Pour faciliter la contribution extérieure, votre action / mission doit respecter un certain nombre de règles, elle doit être :

### Caractérisée

Pour qu'on puisse facilement savoir si l'on peut contribuer ou non à une action, il est préférable de la taguer. Ces tags sont avant tout là pour voir quelles sont les thématiques en lien avec l'action.

Il peut aussi s'il le faut en définir les compétences, le nombre de contributeur/ices, et leur rôle s'ils en ont.

### Compréhensible en 30 secondes

L'action doit être facilement compréhensible pour que chacun puisse savoir s'il a l'envie ou les compétence d'aider à accomplir cette action. C'est à dire :

-   que le nom de l'action doit être le plus court et le plus clair possible
-   que dans la description de l'action le premier paragraphe puisse être lut en moins de 15 secondes
-   que ce paragraphe comporte très clairement l'objectif de l'action.

On pourra ensuite dans les paragraphes suivant expliquer plus en profondeur comment on va effectuer l'action.

### Open source

Votre action doit respecter les valeurs de l'open source, c'est à dire qu'elle doit être facile à copier, libre d'accès et historisée.

#### Facile à copier

Pour faciliter sa réutilisation par des tiers personnes, il faut documenter tout ce que l'on fait :

-   Comment faites vous l'action ?
-   Quels outils utilisez vous ?
-   Combien de temps cela vous prend-il ?
-   Combien de personnes êtes vous pour faire cette action ?
-   A-t-elle fonctionné ?
-   ...

Globalement toutes les richesses créées pendant la réalisation de l'action (savoirs acquis, documents graphiques créés, objets fabriqués, ...) doivent être mis en commun de la manière la plus pertinente pour que chacun puissent les réutiliser.

#### Libre d'accès

Il ne doit pas y avoir de barrières fortes à la contribution de l'action. Les règles et barrières doivent être visibles très facilement dans la documentation de l'action.

#### Historisée

Pour pouvoir facilement revenir en arrière, il faut historiser les tâches effectuées durant l'action. Historiser permettra toujours de revenir à une version antérieure de l'action qui fonctionnait.

### Planifiée

Pour qu'on puisse facilement pouvoir situé l'action dans le temps, voici ce qu'il faut faire :

-   Toujours montrer l'état d'avancement (à faire, en cours et finie).
-   Si besoin est, rendre la date d'échéance visible pour savoir jusqu'à quand la tâche est valide.
-   Si la date d'échéance est dépassée, accepter d'abandonner l'action.

## Conflit

Si deux actions rentrent en collision, il est important de rappeler l'objectif principal pour trouver un terrain d'entente, afin de trouver un terrain d'entente qui n'est pas de faire son action, mais bien de parvenir à l'objectif final.

L'impossibilité de se mettre d'accord sur deux actions concurrentes doit amener à annuler les deux actions ou d'en créer une troisième qui s'inspire des actions concurrentes.

## En résumé

### Légitimité d'une action

La **légitimité** d'une action dépend de sa **réutilisation** et non des décisions de personnes non impliquées dans l'action. Alors **faites**, puis **montrez** et enfin **peaufinez** !!!

### Droit à l'erreur : N'ayez pas peur d'expérimenter !

Il est important de ne pas avoir peur de faire des erreurs. L'historisation est là pour pouvoir revenir en arrière si besoin, et l'erreur est un **processus important** et nécessaire d'apprentissage par la pratique !

## Outils numériques

Pour lister vos actions, vous pouvez utiliser [l'application open source wekan](https://wekan.communecter.org). Une action = une carte / Une mission = une étiquette.
