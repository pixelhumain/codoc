# Histoire de Communecter

Communecter est un projet actuellement porté par l’association Open Atlas mais dont les racines se trouvent dans une quinzaine d’années d'expérimentations, d’essais, de pertes de motivation, d’espoirs retrouvés, de rencontres, d’amitiés et d'innovation permanente pour l’ensemble des acteurs qui ont rejoint peu à peu l’aventure jusqu’à aujourd’hui.

Depuis sa création en 2008, l'association Open Atlas travaille sur des projets liés aux biens communs, à la cartographie et à la démocratie participative. C’est une association locale qui oeuvre au développement territorial à La Réunion.

![](/Images/histoire.png)

**2002** - Application et Site web Open Atlas - projet open source de Service Géolocalisé réalisé par 3 développeurs à Limoges dans le cadre d’un incubateur.

**2008** - Association Open Atlas - création de l’association loi 1901 à La Réunion (suite au déménagement du porteur de projet). Son objectif initial était de tester l’outil Open Atlas sur le territoire réunionnais selon un modèle pensé pour être distribué. Malgré un bon démarrage avec quelques milliers d’utilisateurs, le projet s’est essoufflé et l’association s’est tournée vers d’autres activités.

**2008** - Worldsouk - projet de E-commerce d’artisanat équitable avec redistribution du profit pour construire des puits à Madagascar.

**2012** - Pixel Humain - Présentation du projet au Start up week-end. Création d'une communauté de concepteurs orientés vers l’innovation sociétale, l'amélioration continue, efficience d’un territoire.

**2013** - Ateliers citoyens sur toute l’île et lancement d'un financement participatif ([trailer](https://vimeo.com/74212373)). Expérimentation sur le terrain. Création du Tool Kit Citoyen (boite à outils pour construire des applications Citoyennes). Le projet est incubé à la région Réunion.

**2014** - FabLab.re - communauté de Fablabeurs dans le sud de la Réunion dont le 1er projet est un réseau Smart Citizen sur le territoire réunionnais, en collaboration avec le FabLab de Barcelone. Mise en ligne d'une première version de pixelhumain.org.

**2015** - Fin de l'incubation. Organisation du Forum des Communs : journée dédiée aux rencontres et présentations d’acteurs et de projets œuvrant pour le bien commun à la Réunion.

**2015-2019** - Communecter - Réseau Social Citoyen Libre pour créer un territoire connecté d’acteurs et d’activité locale ([vidéo](https://vimeo.com/133636468)). Participation à de nombreux évènements grâce aux [ambassadeurs](https://www.communecter.org/#@coambassadeurs.view.directory.dir.members) à travers le monde.

**2018-2019** - Projet de SCIC - Le projet Communecter s’apprête à prendre son envol dans le cadre d’une SCIC. L’objectif est de le sortir du cadre de l’association pour en faire un bien commun.

## Évolution du design de la plateforme
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTwd3uR9yc-nJT9wqsnR2onoQ9bJF3OKEmTHhmU2pCFrn_c7XaLEIiyn5g-9RbPC95k_-qK5PqSQSXX/embed?start=false&amp;loop=false&amp;delayms=3000" width="840" height="501" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
