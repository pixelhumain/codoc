Voici les outils numériques que nous utilisons dans ce commun

### Chat

Notre chat est sur [rocket chat](https://chat.lescommuns.org/channel/communecter_accueil). Il nous permet de communiquer vraiment facilement entre nous.

### Kanban

Notre tableau des actions est sur [wekan](https://wekan.indie.host/b/CBcoopTTHYETvRX5E/communecter). Il permet de savoir qui fait quoi à tout moment !

### Sonder les avis de tout le monde

Nous sommes actuellement sur [loomio](https://www.loomio.org/invitations/d72bfb3b9ee79717ce68) pour prendre la température par rapport à des actions spécifiques.

Les développeurs ont un groupe spécial sur le [loomio](https://www.loomio.org/invitations/a03c4725eb63aa98aa8b)

### Wiki

Vous êtes actuellement sur le wiki que nous utilisons pour documenter toutes nos actions.