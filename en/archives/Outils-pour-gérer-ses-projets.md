Voici le lien du google doc pour pouvoir contribuer plus facilement à ce document : https://docs.google.com/document/d/1OtnSuMg4xxzadAW_gJ162ahB5UpBfSWAumksUrwjzlg/edit?usp=sharing
 
*Le but principal de ce document est d'avoir une réflexion collective sur cette nouvelle fonctionnalité.*
 
# Intro
 
## But du document
 
Je voulais mettre par écrit ma vision d'une telle fonctionnalité au sein de communecter. Je pense que ma vision n'est pas la même que la votre, mais je vais tout de même l'exposer ici. Je vais essayer d'être le plus précis possible. Je vais décrire ici ce qui pour moi serait **le top du top** pour engager une discussion avec vous et partir ensuite sur des bases solides pour l'implémentation de cette fonctionnalité.
 
Ce document sert avant tout à lister ce qu'on aimerait dans le meilleur des cas. Ce n'est pas le document pour dire **comment** on va le faire, mais **qu'est-ce** qu'on veut.
 
## Syntaxe du document
 
J'ai utilisé une syntaxe assez simple pour s'y retrouver avec les termes dans communecter :
* Une ENTITE dans communecter est soit : une ORGANISATION, un PROJET, un CITOYEN, ou un LIEU.
* Quand un mot est en majuscule, c'est que c'est qu'on parle d'un élément de communecter. Par exemple : un projet est un amas d'actions qui répondent à des objectifs. Un PROJET, c'est notre PROJET, celui de communecter, c'est le type PROJET.
 
## Prenons notre temps
 
La fonctionnalité étant totalement indépendante de communecter, nous pouvons vraiment prendre notre temps et faire quelque chose de génial qui défiera toutes les lois de l’internet et qui deviendra bien entendu une référence dans la gestion de projets de n’importe quelle entité.
 
# Valeurs
 
## Pour un internet décentralisé et indépendant
 
Ce qui est bien les outils libres c'est qu'ils peuvent hébergé par n'importe qui. Mais ça c'est dans la théorie, parce que dans la pratique, c'est plus compliqué que ça.
 
### Il faut avoir les compétences
 
Tout le monde ne peut pas héberger des outils libres parce qu'il faut se former à ça, ça prend du temps et c'est pas forcément accessible à tout le monde de prime abord.
 
### Notre projet est alors isolé
 
Un des gros freins est le fait de se couper des autres. Quand on héberge des outils libres, ces outils libres ne sont pas connectés avec le reste du monde.
 
Pour illustrer ça je vais prendre l'exemple de loomio :
* Sur loomio il y a une fonction de recherche des groupes publiques. Cette fonction permet de trouver des projets intéressants dans lesquels on va potentiellement pouvoir contribuer.
* La personne qui a son instance de loomio sur son serveur ne sera pas connecté avec le serveur principal de loomio, donc invisible pour les personnes recherchant un groupe dans loomio.
 
### A MOINS QUE
 
On puisse connecter le serveur de la personne avec un autre serveur qui serait là avant tout pour centraliser l'information.
 
Ce qui nous permet d'avoir plusieurs serveurs qui communiquent entre eux, sans avoir un serveur maître.
 
![Voici une image qui illustre bien mes propos](https://sandstorm.io/news/images/decentralize.png)
 
### POURQUOI ?
 
Cela permet plusieurs choses.
 
#### Pour la personne qui s'auto-héberge
 
Elle ne dépend pas d'une tierce organisation, elle est donc indépendante. Le rapport de domination entre l'organisation qui a développé l'outil et la personne n'existe pas. En plus de cela elle est libre de pouvoir filtrer les données qu'elle souhaite, utiliser les protocoles qu'elle a envie, etc... Libre et indépendante, c'est tout de même sympa :D
 
#### Pour les sites avec un gros besoin d'hébergement (type communecter)
 
Si de plus en plus de personnes s'auto-hébergent ou si de plus en plus d'hébergeurs indépendants émergent (du type [CHATONS](https://framablog.org/2016/02/09/chatons-le-collectif-anti-gafam/) ), alors ça fait moins de charge pour les sites avec un gros besoin d'hébergement qui n'ont alors plus besoin de gérer trop de serveur et peuvent se focaliser sur le développement de leur logiciel.
 
De plus une communauté très active défend ardemment ces nouvelles formes d'hébergements, si communecter rentre dans cette communauté, cela veut dire potentiellement :
* Plus de contributeurs
* Plus de communication autour du site
* Une plus grande communauté
 
En donnant la possibilité à sa communauté de s'auto-héberger, un site comme communecter pourrait faire des économies, mais aussi renforcer les liens de toutes les personnes qui gravitent autour de la ressource.
 
## Les liens entre les entités renforcent les projets de tou-te-s
 
Des liens se créent au fur et à mesure que les projets avancent, évoluent, mutent, fusionnent, etc... De cette mise en lien né plus de possibilité, plus d'idées et d'actions pour constamment améliorer les ressources d'un projet. Si mettre en lien les gens est si puissant, il faut créer des méthodes et des outils pour permettre cela.
 
### Écosystème des outils open source
 
Créer un écosystème résilients des outils open source avec une bonne interopérabilité entre eux, avec la possibilité de modifier le contenu d'une application avec une autre, permettra de réellement concurrencer les gros pontes du marché. Cela permettra alors de sensibiliser plus de gens à ce type de pratique et donc augmenter la communauté des commoners numériques. #cerclevertueux
 
### Écosystème des créateurs de lien
 
Créer un écosystème résilients des créateurs de lien, avec de la documentation en commun, savoir qui fait quoi, où, comment, qui a besoin de quoi, etc... Permettrait d'être plus performant dans notre manière d'aborder le travail collaboratif et notre manière d'appréhender notre société. C'est un moteur considérable pour l'évolution de notre système vers un autre, plus collaboratif.
 
## Communecter dans tout ça ?
 
Le but est que communecter s'intègre parfaitement dans les valeurs citées plus haut et qu'elle devienne la plateforme de référence pour tous les projets ouverts de notre siècle !
 
# Cahier des charges de la nouvelle fonctionnalité
 
## Les applis ça sert à qui ? Qui peut en créer ?
 
Toutes les ENTITÉS de communecter, aussi bien une ORGANISATION, qu'un CITOYEN, qu'un PROJET, qu'un LIEU peuvent créer des applis.
 
## Des applis connectées entre elles
 
Toutes les applis sont connectées entre elles.
 
C'est à dire que si par exemple une ORGANISATION crée une action dans un des ses PROJET dans l'application wekan et qu'elle précise que ce PROJET se passe dans un LIEU gérée par des CITOYENS. Alors on pourra accéder à l'action de par le wekan de toutes les ENTITÉS citées.
 
Cela veut aussi dire qu'on peut ajouter/enlever du contenu dans une appli alors qu'on est dans une autre (comme par exemple le lien entre trello et slack).
 
Ce type de feature doit être déterminé en fonction de nos besoins et listés pour faire des demandes aux développeurs de chaque appli
 
## Comment accéder à une appli, comment naviguer entre elles ?
 
### Une gestion des applis à la Google
 
L'une des choses qu'on peut reprocher à sandstorm c'est d'avoir une barre sur la gauche qui met constamment les outils qu'on utilise. Alors c'est pratique quand tu veux facilement changer d'outils, mais par contre ça t'obstrue toute une partie de l'écran. Et si tu travailles sur un PC portable, ce n'est pas gérable.
 
Google utilise une barre horizontale pour naviguer entre ces applis et gérer des options pour spécifique à chaque appli. Dans certaines applis (par exemple Google doc), nous n’avons pas accès aux autres applis.
 
Copions les ! Créons une icone “appli” en haut à droite qui liste toutes les applis auxquelles on a accès.
 
Remarquons que chaque appli (rocketchat, loomio, wekan, mediawiki) a déjà une barre horizontale qui reste. quand on scroll. Il suffira alors d’ajouter l'icône applis.
 
Si on veut vite passer d'une appli à une autre, alors il suffit que de les laisser dans ses onglets actifs.
 
### Lien de l'appli aussi au sein de l'ENTITE dans communecter
 
Je pourrais aussi accéder à une appli dans une ENTITÉ quelconque, que ce soit un PROJET, une ORGANISATION, un LIEU, ou un CITOYEN. Elle sera présente dans la partie "application" de la page de présentation de l'ENTITE.
 
### Lien de toutes les applis auxquelles le CITOYEN est attaché
 
On pourra aussi accéder à l'appli souhaitée en allant sur la page "toutes mes applis" dans communecter. Cette page centralise toutes les applis auxquelles le CITOYEN contribue, elles peuvent être classées de différentes manière :
* par ENTITÉ : On verrait les applis disponibles pour chaque ENTITES pour lesquelles je contribue.
* par applis : On verrait les ENTITES disponible en fonction de chaque appli.
 
Mais ce n'est pas tout, on peut passer d'un projet à l'autre dans l'appli elle-même.
 
### Dans chaque appli on a accès à toutes nos ENTITÉS
 
Quand on est dans une application, on peut facilement passer d'une ENTITÉ à une autre. Par exemple, je suis dans le wekan d'un PROJET quelconque, je peux facilement naviguer dans le wekan pour voir les autres PROJETS auxquels je contribue.
 
## Comment créer une appli au sein de communecter ?
 
### A la création de votre ENTITÉ
 
Quand vous créez une ENTITÉ, vous pouvez cocher les applis que vous utiliserez pour la gérer.
 
### A n'importe quel moment
 
En modifiant votre ENTITÉ, vous pouvez facilement cocher de nouvelles applis.
 
Vous pouvez aussi créer de nouveaux tableaux dans wekan, channel dans rocketchat, etc...
 
### Comment ça se passe ?
 
Quand vous créez une appli, toutes les personnes faisant partie de l'ENTITE dans laquelle vous l'avez créée, vont recevoir dans leurs notifications la possibilité d'être ajoutées à cette nouvelle appli. On demande aux personnes rejoignant l'ENTITE en cours de route si elles veulent être ajoutées aux applis de cette ENTITÉ.
 
## Quelles sont les particularités pour chaque appli
 
### Wekan
 
De base dans wekan on a un tableau personnel nommé "Perso". Ce dernier regroupe toutes les actions auxquelles on contribue.
 
Quand on ouvre une nouvelle instance de wekan, cela crée un nouveau tableau dans l'application.
 
Soit cette instance vient d'une ENTITE parente (ORGANISATION ou LIEU par exemple). Dans ce cas cela va ouvrir un tableau dont le nom sera le même que le nom de l'ENTITE.
 
Soit cette instance vient d'une ENTITE enfant (PROJET par exemple). Dans ce cas on va créer un nouveau dossier dans le wekan du nom de l'ENTITE parente avec dans ce dossier deux tableaux : "General" qui est le tableau qui regroupe toutes les actions de tous les tableaux du dossier, et "NomDeLEntiteEnfant" qui est le tableau de l'ENTITE enfant. Quand on ajoute une action dans le tableau de l'ENTITE enfant, l'action est aussi visible dans le tableau général.
 
##
 
## Quels liens entre les différentes applis ?
 
Le plus important est d'avoir de réels liens entre les différentes applis et de pouvoir interagir facilement les unes avec les autres. Par exemple on pourrait ajouter une action dans un wekan en mettant +add nomdelaction dans le rocket chat. Comme ça tout le monde est au courant de l'ajout qui a été fait et on pourrait faire en sorte que tout le monde puisse aussi s'ajouter à cette action en cliquant sur le plus qui est mis sur le message.
 
### Lister ici tous les liens qu'on aimerait avoir entre les applis
 
* 1
* 2
* ...
 
## Exemple concret
 
### Création d'une ORGANISATION
 
Je suis nouveau sur la plateforme et je fais partie de l'association Livin'Coop, organisation qui a pour but de créer du lien entre les différents acteur/ices de la métropole. J'ajoute donc les infos nécessaires pour créer mon ORGANISATION, à cela j'ajoute que j'aimerais utiliser 4 outils : le wekan, le loomio, le wiki et le rocketchat. J'ai maintenant une ORGANISATION et des outils à disposition pour bien gérer cette organisation.
 
### Ajout d'un PROJET
 
#### Nouvelle décision dans le loomio pour décider de la pertinence du projet
 
J'aimerais maintenant ajouter un PROJET : "Fête des commoners", le but du projet est de faire un événement qui va rassembler tous les commoners et potentiels commoners de la métropole lilloise pour qu'ils échangent sur leurs pratiques et qu'ils contribuent ensemble à l'amélioration de l'écosystème des communs. Ça sera aussi l'occasion de communecter tout le monde et de faire des ateliers sur des sujets divers.
 
Chez Livin'Coop, pour ajouter un PROJET, il faut que tous les CITOYENS concernées par ce PROJET soit consultés. Je dois alors créer une "nouvelle décision" dans le loomio de l'ORGANISATION. Pour ce faire, je dois accéder au groupe Loomio de Livin'Coop pour créer ma "nouvelle décision", voilà comment je peux m'y prendre :
* Je peux cliquer sur l’icône en haut à droite des applis, et je choisis loomio. Dans loomio je choisis le groupe "Livin'Coop".
* Je peux aller dans l'ORGANISATION Livin'coop, puis cliquer sur "loomio" dans la partie application de l'ORGANISATION.
* Je vais sur ma page "Toutes mes applis" dans communecter et sélectionne loomio dans l'ORGANISATION "Livin'Coop".
 
#### Ajout du PROJET
 
Tout le monde est d'accord et s'enjaille de cette idée ! Pour créer le PROJET, je vais dans l'ORGANISATION "Livin'Coop" et je fais "Ajouter un nouveau projet". Je mets les informations nécessaires et oh magie, les précédentes applis sont déjà cochées. Je veux bien utiliser les mêmes applications pour ce projet. Elles sont donc automatiquement créées de la manière suivante :
* Wekan : un nouveau tableau est créé, il est le fils du tableau de Livin'Coop, c'est à dire que quand une action est mise dans le tableau de la "Fête des commoners", l'action est aussi mise dans le tableau de Livin'Coop, il est étiqueté comme "Fête des commoners". Aussi, maintenant quand je vais dans wekan dans "Livin'Coop", deux tableaux s'offrent à moi : "Général" et "Fête des commoners". "Général étant le tableau général de l'ORGANISATION Livin'Coop.
* Loomio : Un sous groupe est créé.
* Wiki : Une sous-section est créé, c'est à dire aura une URL comme suis : https://wiki.communecter.org/wiki/Livin'Coop/Projet/Fete_des_commoners avec les informations basiques données par le créateur du PROJET.
* RocketChat : Un sous canal est créé portant un nom du type : livincoop_fete.des.commoners_lille.
 
### Ajout d'une action dans un PROJET
 
Je veux ajouter une action dans le tableau wekan de mon PROJET "Fête des commoners". Cette action consiste à faire une liste de tous les commoners de la métropole pour leur parler du projet. Cette action ne concernant directement personne de l'ORGANISATION, je peux directement créer mon action. Je préfère tout de même laisser un message sur le chat, sur le channel #livincoop_fete.des.commoners pour informer tout le monde ce que je m'apprête à faire. Ceci étant fait, je dois donc accéder à mon tableau. A moi s'offre un large panel de choix :
* Je peux cliquer sur l’icône en haut à droite des applis, et je choisis wekan. Dans wekan je choisis "Fête des commoners".
* Je peux aller dans l'ORGANISATION Livin'coop, puis
* * cliquer sur "wekan dans la partie application de l'ORGANISATION puis je peux choisir le tableau "Fête des commoners".
* * dans le PROJET "Fête des commoners" et cliquer sur "wekan" dans la partie "application".
* Je vais sur ma page "Toutes mes applis" dans communecter et sélectionne wekan dans le PROJET "Fête des commoners".
 
***
En gros tout est interconnecté donc il est très facile d'accéder à ce que l'on veut. La plupart du temps on utilisera le bouton appli et on choisira l'application que l'on veut. Et on naviguera dans les différentes applis en les laissant dans nos onglets actifs.
 
# Comment mettre en place une telle fonctionnalité
 
## Tel google : nomappli.communecter.org
 
Chaque appli sera disponible séparément, par exemple il suffira de marquer l'url d'un channel de chat pour pouvoir y accéder.
 
### Login
 
Un login commun pour toutes les applis sera requis. On pourra se connecter depuis n'importe quelle appli. Le mieux étant de faire comme facebook/google : se connecter automatiquement.
 
### Création d'une ou plusieurs applis
 
Quand on crée une ou plusieurs appli dans communecter, le serveur va automatiquement créer le groupe si c'est un loomio, le channel si c'est un rocketchat, le tableau si c'est un wekan, l'article si c'est un wiki, etc... Pour ce faire il va utiliser l'API de chacune de ces applications.
 
### Les applis peuvent très bien marcher toute seule.
 
On peut par exemple utiliser communecter que pour son wekan.
 
## Liens entre les différentes applis
 
Toutes les applis marcheront en synergie les unes avec les autres via leur API. Elle s'échangeront constamment des données et se mettront à jour automatiquement en fonction des informations qu'elles échangeront.
 
# Voir à long terme
 
Nous pouvons aussi voir à long terme et voir vers où on veut aller avec cette nouvelle fonctionnalité.
 
## Permettre aux auto-hébergeurs d'héberger leur propre communecter
 
N'importe qui pourrait télécharger communecter et les applis qui lui sont associées pour les installer sur son propre serveur. Son serveur s'il est sur internet pourra interagir avec les autres serveurs communecter (dont le notre) pour échanger toutes les datas nécessaires. Il aura les mêmes fonctionnalités qu'un utilisateur lambda, sauf qu'il l'hébergera sur un autre serveur.
 
On pourrait aussi par exemple travailler en local sans avoir besoin d'internet, puis ensuite push ses datas sur un serveur lié à internet.
 
## Regrouper toutes les données de tous les serveurs qui font tourner les applis
 
Le problème aujourd'hui c'est qu'on ne peut pas avoir une information sur "qu'est ce qui se passe dans ce monde et où est ce que je peux contribuer ?". Et je ne pense pas (enfin je n'espère pas) que le but de communecter c'est de rassembler tout le monde derrière une seule plateforme (la notre + nos serveurs à nous).
 
On peut effectuer un travail à long terme d'intégration d'autres serveurs dans la plateforme communecter.
 
Par exemple, on peut aller voir un serveur wekan et leur dire : "Eh ! C'est sympa ce que vous faites, ce qu'on peut faire pour vous c'est recenser tous vos tableau dans notre plateforme et on peut vous donner la possibilité d'ajouter des outils à la votre". Ce qui fait qu'ils pourraient sélectionner les outils dont leur communauté aurait besoin (et donc sûrement communecter parce que c'est de la boulette) et comme ça on élargit notre cercle, sans utiliser aucune ressource et en laissant une communauté là où elle est. C'est ça aussi la puissance de la décentralisation et de la création de lien, agrandir sa communauté sans abîmer celle des autres ;) .
 
On pourrait même imaginer des serveurs différents pour chaque appli qui échangeraient entre elles les infos nécessaires. Le but c'est de trouver la meilleure manière de coder ça pour rendre le tout plus souple. Dans l'idée je pourrais très bien être sur les serveurs de loomio pour mon appli loomio, les serveur indie.io pour mon wekan, mon serveur perso pour le wiki et le serveur d'un hébergeur indé pour communecter, et que tout marche ensemble via les APIs !
 
Bref, j'espère très sincèrement qu'on se dirige par là, parce que je ne veux pas imposer communecter à tou-te-s, je veux juste leur dire, si tu veux c'est disponible et en plus ça marche avec ce que tu fais déjà !
 
# Questions
 
## Pourquoi utiliser l'API des applications plutôt que les intégrer entièrement à communecter ?
 
En faisant cela, cela offre une plus grande liberté à l'écosystème des applis open source. Cela permet
* de communiquer entre applis même si on fait pas partie du même serveur ce qui permettra par exemple à plus long terme d'avoir des serveurs décentralisés qui communique entre eux.
* que les applis restent indépendantes et peuvent continuer à être améliorées par leur communauté
* facilement d'ajouter et de retirer des applications à communecter
* d'utiliser des applis même si ce n'est pas du php
* créer des liens avec l'écosystème des libristes en améliorant leurs applis
* pleins d'autres choses auxquelles on ne pense pas maintenant
 
## Et si l'API d'une appli n'est pas assez fournie
 
### Rentrer en contact avec les développeurs et leur faire les yeux doux
 
Soit ils nous aident et on est content, soit ils ne peuvent pas, ce qui nous amène à :
 
### Se dépatouiller nous même
 
A ce moment là, on aura plus qu'à forker leur projet et push de nouvelles fonctionnalités. Comme ça on améliore notre projet tout en améliorant celui des autres. Et on crée cette écosystème dont on parlait dans les valeurs.
