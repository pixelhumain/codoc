# Philosophie du projet
Nous nous efforçons de rendre la contribution la plus simple possible. Pour nous aider dans cette démarche ça se passe [ici](https://chat.lescommuns.org/channel/co_holoptisme).
**Si vous ne savez pas par où commencer n'hésitez pas à poser votre question sur le chat du projet. [Le channel "Accueil"](https://chat.lescommuns.org/channel/co_accueil) est là pour ça.**

# Contribuer au code
N'ayez pas peur ! Le code est écrit de manière accessible afin que vous puissiez y contribuer facilement.
Si vous êtes à l'aise avec les méthodes plus "standard" dans l'industrie, il est possible de coder de votre côté et de communiquer via l'API.
### Ressources
* [Feuille de route des développeurs](https://raw.githubusercontent.com/pixelhumain/communecter/master/docs/roadmap.org)
* [Application mobile](https://play.google.com/store/apps/details?id=org.communevent.meteor.pixelhumain&ah=lVN3mXqHKQjIOg3qHn0YzhiUebc&hl=fr)
* [API](https://github.com/pixelhumain/api)
* [Fonctionnement PHP](https://docs.google.com/drawings/d/1nSlaLy6ce7CWxvkXUbwIEYNVTeL6f35qm0CqoBjlvR0/edit?usp=sharing)
* [Chat de tous les développeurs](https://chat.lescommuns.org/channel/co_dev_open)

# Contribuer à l'amélioration de la plateforme
Nous essayons d'améliorer l'holoptisme du commun (le fait d'avoir accès à tout ce qu'il se passe en rapport à la création de communecter) dans le but de permettre à tout le monde de s'impliquer dans ce dernier et d'en faire réellement sa plateforme.
Voici quelques liens qui vous permettront de nous aider sur ce sujet : 
* [La page wiki sur l'organisation interne du commun](https://github.com/pixelhumain/communecter/wiki/Organisation-interne)
* [La page wiki des idées](https://github.com/pixelhumain/communecter/wiki/Id%C3%A9es)

# Contribuer au wiki
Il manque une page sur ce wiki : créez-en une. 
Vous avez identifié une faute de français : modifiez la page :).
...

### Astuces
* Si vous créez une page n'oubliez pas de l'intégrer au menu du wiki.
* Si vous modifiez le nom d'une page, pensez à modifier le lien du menu pointant vers cette page.


# Contribuer autrement (divers projets / événements, communication …)
Communecter, en tant que bien commun, se revendique être une communauté ouverte qui tâche de faciliter au maximum les contributions de chacun, quelles qu'elles soient, en fonction de ses envies et de ses compétences. Pour cela, il convient de donner à voir les projets en cours, leur état d'avancement, les prochaines étapes et les besoins associés.


## Liste et caractéristiques des projets transversaux :

###Projet Tutoriels :
Brève description : Ecrire des scripts et tourner des videos de 3 minutes à disséminer dans Communecter pour faciliter son utilisation

Equipe : Julien Lecaille, Rémi Bocquet

Référent : Julien Lecaille

Liens de diffusions d’informations : 
* [chat de discussion](https://chat.lescommuns.org/channel/co_tuto)
* [scripts des tutos](https://docs.google.com/document/d/1PpS3M3qC1nQhqRP71ccgD9F7LubNDNjiI1kzbo0T_pA/edit)
* [Page Communecter du projet](https://www.communecter.org/#element.detail.type.projects.id.5861e50840bb4e2d75947c28)

Historique / avancement du projet : début

Prochaine étape du projet : NC

Prochaine rencontre / fréquence : NC



###Projet Traduction
Brève description : Traduire la version française actuelle dans un maximum de langue pour exporter Communecter.

Equipe : NC

Référent : NC

Liens de diffusions d’informations : NC

Historique / avancement du projet : début

Prochaine étape du projet : NC

Prochaine rencontre / fréquence : NC

***



> # Évolution

> En 2013 le projet a commencé comme une API sans FrontEnd ... avec l'interface, l'architecture a évolué
> Dans les versions futures, se posera à un moment la question globale de la qualité du code

> # Pixel humain 
> la communauté de développeurs/contributeurs autour de Communecter
> Readme de pixelhumain :: https://github.com/pixelhumain/pixelhumain
> Yii dans Pixel Humain -> http://www.yiiframework.com/

> Pour ceux qui n'ont jamais fait de web, Yii va cadrer, avec une vraie modélisation MVC (modele/vue/controller): c'est le rôle d'un framework

> Pas encore eu le temps de passer à Yii2
> _Proposition rajouter dans la doc des contrôleurs un lien vers la doc de Yii_


> # Communecter version web
> http://www.communecter.org

> * php riche en javascript
> * mongodb 

> readme de l'installation sur le github
> https://github.com/pixelhumain/communecter
> Basé sur Citizen Tool Kit (CTK : tout est orienté API et Restful)

> Voir le MVC : https://docs.google.com/drawings/d/1nSlaLy6ce7CWxvkXUbwIEYNVTeL6f35qm0CqoBjlvR0/edit?usp=sharing

> Pour l'installation docker : https://github.com/pixelhumain/docker
>  
> # Communecter version mobile

> Application mobile CommunEvent sur un stack meteor/NodeJS
> https://play.google.com/store/apps/details?id=org.communevent.meteor.pixelhumain&ah=lVN3mXqHKQjIOg3qHn0YzhiUebc&hl=fr

> * meteor  (javascript pur)
> * mongodb pour mobile

> Readme :
> https://github.com/pixelhumain/communEvent
> _Cherche un dev meteor pour backuper Thomas_



> ***
> A ce stade on essaie de définir différents Points d'Entrée dans le projet
> L'idée est de bien segmenter toutes les entrées possibles (et rappeler cette liste dans chaque repo)
> #  Point d'Entrée à Communecter sur la documentation

> contribuer aux explications
> https://github.com/pixelhumain/communecter/blob/master/views/default/explainPanels.php
> Idée d'évolution : avoir des fichiers Markdown sur le wiki, qui seraient récupérés automatiquement

> # Point d'Entrée à Communecter comme intégrateur Front End web
> Front End (jquery)
> Différents langages sur une même page : choix assumé d'avoir des fichiers facilement lisibles pour des développeurs débutants

> # Point d'Entrée à Communecter sur le stack meteor
> _Cherche un dev meteor pour backuper Thomas_

> # Point d'Entrée à Communecter sur un module indépendant
> voir : https://github.com/pixelhumain/api
> API --> fait son module indépendant

> exemple : Vincent a un projet d'agrégateur de news/feeds intégrant Diaspora et Communecter (deux réseaux complémentaires)

> Host ++ API

> API Json dans Postman

> https://www.getpostman.com/docs/importing_swagger
> https://github.com/josephpconley/swagger2postman

> # Point d'Entrée à Communecter côté serveur
> Nécessité d'expérimenter des méthodes de serveur non centralisées (mais capable de gérer une montée en charge)

