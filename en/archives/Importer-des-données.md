# Présentation
Le module d'importation de données permet de créer des dizaines organisations de manière automatisée. C'est un outil efficace si vous avez, par exemple, un listing d'associations que vous souhaitez référencer dans Communecter.

# Introduction
La première étape est d'organiser les données en votre possession (par exemple le listing des associations de votre commune) pour qu'elles soient interprétables par le module d'import.

# Format des adresses
Pour une adresse en France, Communecter va vérifier l'existance de l'adresse dans l'ordre suivant: [DataGouv](https://adresse.data.gouv.fr/), [Nominatim](https://nominatim.openstreetmap.org/), puis [Googlemaps](https://maps.google.fr).

Si avec la combinaison rue+CP+Ville+Pays, l'adresse ne donne pas de résultat via les 3 géocodeur, on refait une recherche avec seulement CP+Ville+Pays, si toujours pas de résultat l'import remonte une erreur.

# Convertisseur de données

Le modules d'import permet la conversion de données en ontologie PH décrit ci-dessous :
 
## Onthologie PH
```
{
    "\_id" : ObjectId("STRING"),
    "name" : "STRING",
    "email" : "STRING",
    "type" : "STRING",
    "address" : {
        "@type": "PostalAddress",
        "postalCode" : "STRING",
        "addressLocality" : "STRING",
        "codeInsee" : "STRING",
        "streetAddress" : "STRING",
        "addressCountry" : "STRING",
    "regionName": "Nord-Pas-de-Calais-Picardie",
    "depName": "NORD"
    },
    addresses : [
        "address"
        ],
    "geo" : {
        "latitude" : "FLOAT",
        "longitude" : "FLOAT",
        "@type": "GeoCoordinates",
    },
    geoPosition": {
        "type": "Point",
        "coordinates": [ 
           "latitude -> FLOAT",
           "longitude -> FLOAT"
            ]
},
    "description" : "STRING",
    "shortDescription" : "STRING",
    "url" : "STRING",
    "image" : "STRING",
    "urlFacebook" : "STRING",
    "urlTwitter" : "STRING",
    "source" : {
        "id" : "STRING",
        "url" : "STRING",
        "key" : "STRING",
        keys": [ 
            "STRING"
    ],
"insertOrign": "STRING",
        "update" : "DATETIME"
    },
    "tags" : [ 
        "STRING"
    ],
    "telephone" : {
        "fixe" : [ 
            "STRING"
        ],
        "mobile" : [ 
            "STRING"
        ],
        "fax" : [ 
            "STRING"
        ]
    },
    "contacts" : [ 
        {
            "name" : "STRING",
            "role" : "STRING",
            "telephone" : [ 
                "STRING"
            ],
            "email" : "STRING"
        }
    ],
     "preferences" : {
        "isOpenData" : "BOOLEAN",
        "isOpenEdition" : "BOOLEAN",
    },
    "state" : "STRING",
    "profilImageUrl" : "STRING",
    "profilThumbImageUrl" : "STRING",
    "profilMarkerImageUrl" : "STRING",
    "profilMediumImageUrl" : "STRING",
    "modified" : "ISODate",
    "updated" :"INT",
    "links" : {
        "members" : {
            },
        "events" : {
            },
        "projects" : {
            },
         "memberOf" : {
            },
            "needs" : {
            }, 
            "follows" : {
            },
    },
    "modifiedByBatch" : [ 
        {
            "NameBatch" : "ISODate"
        }
    ]
    "creator" : "STRING",
    "created" : "INT"
    
}
```

## Etape 1 : Choix des données à convertir 

Sélections obligatoires:

Éléments disponibles : 
* Organisation
* Citoyens
* Evenement 
* Projet

Types de source disponibles : 
* URL
* FILE

Sélection optionnelle : 
* Le lien (le mapping)

Type de fichier disponible pour une URL 
* JSON

Types de fichier disponibles pour un fichier
* JSON
* CSV

Remplir le champ "Path to Elements" qui indique où se trouve la données pertinentes 

Exemple : 

Pour un fichier JSON suivant, la donnée pertinente se trouve dans le tableau "features" : 

```json
{
	"type": "FeatureCollection",
	"features": [{
		"type": "Feature",
		"geometry": {
			"type": "Point",
			"coordinates": [55.433699, -21.259772]
		},
		"properties": {
			"name": "Damien Grondin",
			"username": "Femu",
			"img": "http:\/\/127.0.0.1\/ph\/upload\/communecter\/citoyens\/5880b24a8fe7a1a65b8b456b\/1489667898_linkpkm.jpg"
		}
	}]
}
```
Dans ce cas de figure, il faut donc mettre "features" dans le champ "Path to Element"

## Etape 2 : Création du mapping

Dans cette étape, on va réaliser ce qu'on appelle un mapping. C'est à dire renseigné à quel champ de l'onthologie PH correspond tel ou tel champ des données du fichier source.

Dans la colonne de gauche : "Source", on peut sélectionner toutes les branches du fichier source.
Dans la colonne de droite : "Communecter", on peut sélectionner le champ de l'onthologie PH auquel on veut associer la branche du fichier source de la colonne de gauche.  

On ajoute une ligne simplement en cliquant sur la bouton "Ajouter" dans la troisième colonne.
On supprime une ligne en cliquant sur la croix rouge qui apparaît dans la troisième colonne lors d'un ajout de ligne.

Lors d'un nouveau mapping, on peut à tout moment sauvegarder le mapping actuel en cliquant sur le bouton "Save Mapping". ( Si lors de l'étape 1, on avait sélectionné un mapping enregistré, les boutons "Update Mapping" et "Delete Mapping" apparaissent. Bouton qui permettent respectivement de modifier ou de supprimer un mapping existant ).

Remarque : si on clique sur le bouton étape précédente, le mapping reste enregistré jusqu'au nouveau rafraîchissement de la page.

Avant de pouvoir passer à l'étape suivante, il est impératif de : 
* faire au moins une assignation dans le mapping 
* avoir renseigné la "key" dans le champ adéquat

Champ du mapping obligatoire pour la conversion d'organisations : 
* name
* type

Champ du mapping obligatoire pour la convention de citoyens : 
* name 
* username 
* birthdate

Champ du mapping obligatoire pour la conversion d'évènements : 
* name

Champ du mapping obligatoire pour la conversion de projets : 
* name 

La checkbox "Test" permet de tester la conversion et donc de visualiser les données qui sont bien converties ainsi que les données qui sont rejetée avec le message d'erreur qui décrit le pourquoi du rejet.

### Etape 3 : Sauvegarder des données converties et/ou des données rejetées

Lors de la troisième étape, on peut enregistré au format JSON, les données du fichier source qui ont été converties avec succès au format PH. 

On peut également enregistré les données qui ont été rejetés au format JSON.

# Injecter des données 

Le module d'import permet d'injecter par la suite ses données (préalablement convertie au format PH grâce aux convertisseur de données du module d'import) dans communecter.

Type de fichiers autorisés : 
* CSV 
* JSON

Règles : 
* Les doublons sont INTERDIT : Une organisation ne doit apparaître qu'une seule fois dans le fichier
* Avant de mettre un organisme, vérifier qu'il n'existe pas sur communecter.org
* NE pas mettre formations/projets/ qui peuvent être regroupés dans une seule organisation.

Informations obligatoires : 
* Nom de l'organisme
* Code Postal
* Ville
* Code Pays (exemple : FR)
* Type : (Association, Groupe, Entreprise ou Groupe Gouvernemental)

Informations optionnelles : 
* Téléphones
         * Format recommandé : "+33 6 00 00 00 00")
         * Si plusieurs numéros, mettre un seul numéro par colonne)
         * On peut en ajouter autant qu'on le souhaite
         * Différents types de téléphone : Mobile, Fixe et Fax
* URL
* Description
* Numéro et nom de la voie
* Email
* Tags ( Si plusieurs tags, mettre un seul tags par colonne)
* Site Web
* Latitude / Longitude
* Contact ? : 
         * Nom et Prénom du contact
         * Rôle du contact au sein de l'organisme
         * Téléphone du contact ( Si plusieurs numéros, mettre un seul numéro par colonne)
         * Émail du contact