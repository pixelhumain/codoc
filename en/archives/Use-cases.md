**Cette page wiki vise à recenser les différentes customisations faites de communecter que ce soit avec le module network ou en marque blanche.** 

N'hésitez à en recenser d'autres et à rajouter celles qui sont faites au fur et mesure.

### Mini-cartos avec le module network

- https://www.communecter.org/?network=BretagneTelecom
- https://www.communecter.org/?network=tierslieuxlille

Avec JSON externe:
- http://paris.zerowastefrance.org/la-carte
- https://sites.google.com/site/colibrisdeversailles/dechets#TOC-Carte-Z-ro-D-chets-Versailles

### Marques blanches

- http://www.granddir.re/granddir/organization/dashboardMember/id/54eed95ea1aa143e020041c8
- https://www.azotlive.com/

### Usages temporaires

- Utilisé jusqu'au 1er Mars 2017 : Apéros citoyens pour une coalitions de la gauche
http://frama.link/1maispas3