Pages anciennes

Version 0.15 (Août 2016)

# Objectif 

Refonte partielle de Communecter au 31 Août

Meilleure ergonomie de l'interface pour être plus appropriable par Nuit Debout

Repenser les fonctionnalités sur la base du cahier des charges de Nuit Debout : http://www.flipsnack.com/PierreLalu/communecter-02.html

Développement intégrant les discussions avec Nuit Debout :  https://mensuel.framapad.org/p/suGyB9MbCS

# Calendrier


Fait : Redesign de l'interface (version 2) dès le 5 Août

Réunion de formation des dev avec Oceatoon ( prise de date : https://framadate.org/LeDevuY6eZCob2Bf )

JMV+Raphael : documentation de l'API

Répartition des tâches : https://raw.githubusercontent.com/pixelhumain/communecter/master/docs/roadmap.org

Discussions : https://github.com/pixelhumain/communecter/issues

Deadline au 31 Août mais éventuellement 15j de délai si les retours utilisateurs ne sont pas assez convaincants

# Coordination du projet 

Sur le tchat de Nuit Debout : https://chat.nuitdebout.fr/channel/communecter

# Besoins
* Dev Web : PHP ou Meteor, Javascript lourd , MongoDB, API REST
* sys Admin : déploiement automatisé et amélioration , installation divers
* Designer et Ergonome : faire des propositions améliorantes
* Testeur : écrire des test UX avec Selenium 
* Documentaliste : nous aider a améliorer la documentation 


# Descriptif de l'équipe

##  Développeurs

* JMV
* ale
* Zool (Claire) : https://github.com/clairezed
* Vincent.dev.lille
* faridb59


### Core Dev 

* Oceatoon (Tibor) : https://github.com/oceatoon
* Tango : https://github.com/Kgneo
* Raphael Rivière : https://github.com/RaphaelRIVIERE


##  Testeurs

Rejoindre l'équipe de testeurs sur Communecter : https://www.communecter.org/communecter#organization.directory.id.574db1e540bb4e1e0d2762e6?tpl=directory2

* Caroline
* Julien Lecaille : https://github.com/JulienLecaille
* Mathieu Coste
* Michel Bertrand (michelbertrand123654789@gmail.com) 

### Stress test (trolls ? utilisateurs ?)

* Vote Blanc (François)