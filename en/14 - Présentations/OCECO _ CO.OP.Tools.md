# OCECO : CO.OP.Tools
Open & Collaborative Tools for cooperation

- Organization Management simplified and collaborative
- Inclusive Project Management 
- Interactive Project Proposals 
- Open Gouvernance and Decisions
- Multiple Financing options
- Task Tracking 
- Organization and Project Observatory
- Holoptism and Stigmergy

[Opal Process ](https://docs.google.com/drawings/d/1bgxTe3j5LWcC_T6zZ-FlK_ypNaws3IG3UJUpOwjYbTc/edit)

[PPT CO.OP.TOOLS](https://docs.google.com/presentation/d/1sgDu7mya0yq9PgDOj0KuKEFc77j3aevuuuyzO7qSLZE/edit#slide=id.g87ac77bfdb_0_164)

[DESIGN](https://docs.google.com/drawings/d/12VPzz5JL9KnWisny02o45vskjETNUSbqgmBCH70_rdc/edit)