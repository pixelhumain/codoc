# Open Atlas

## Une coopérative d’entrepreneurs 
unis pour créer des solutions humaines et locales
spécialisée dans la communication et le numérique appliqués au fonctionnement de la société depuis 2007

## Nos devises
“1 + 1 = 3 | L'intelligence collective au service du citoyen”
“La réflexion individuelle au service de L'intelligence collective”

## Notre intention 
L’open source ! Des épices, des recettes et du code source  libre pour créé des communs numériques libres d’utilisation et d’amélioration.

