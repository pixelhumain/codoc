Nous proposons de partager notre savoir faire, nos réseaux et nos outils numériques. Nous proposons donc de co-construire avec le comité pour la science ouverte les outils et méthodes pour faire de la recherche scientifique un bien commun, une science ouverte dynamique et tendre vers une science partagée avec le plus grand nombre afin de faire face aux enjeux de notre temps (changement climatique, transition énergétique, épidémies, etc.), maximiser son impact sociétal, y compris en dehors du champ scientifique.

--- 
# Actions concrètes en 3 CO (phases)

*COmmunauté* : Le réseau des acteurs de la science ouverte locaux, nationaux et internationaux et du tiers-secteur de la recherche

*COnnecter* : Interopérer avec le maximum de plateformes de science ouverte 

*COconstruire* : Communauté d’acteurs motivés pour créer un processus éditorial collaboratif et participatif

---
# 5 outils pour une COmmuanuté COopérative et Actions concrètes  
- AIDER à *STRUCTURER* une COMMUNAUTÉ avec CO
- *CONNAISSANCE* : RÉCOLTER avec *COFORMs* (link)[COForm.md]
- *OBSERVATOIRE* : Structurer la DATA *CObsrevatory* (link)[CObservtory.md]
- *DESIGNER VISUALISER* *COSTUM* : Rendre résultats visuel ouvert et connectable (link)[COstum.md]
- *ACTIONS EN COMMUNS* : Outils ouverts et Collaboratifs pour la  cooperation, *OCECO* (link)[OCECO : CO.OP.Tools.md]

(presentation)[https://docs.google.com/presentation/d/1sgDu7mya0yq9PgDOj0KuKEFc77j3aevuuuyzO7qSLZE/edit#slide=id.g87ac77bfdb_0_270]

--- 
# COmmunity
People & Organizations together around things in common

- Answers automatically create communities 
- Create relations between participants
- Automatic caracterization
- Can create complexe profiles 
- Can produce community actions automatically 
- Vizualisations create links through answers


