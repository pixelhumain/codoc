- Co-construire en intelligence collective le contenu dynamique d’une ligne éditoriale coopérée et composer une boîte à outils logicielle libre d’édition, de curation pour les sciences ouvertes
- Créer un espace de valorisation de la recherche ouverte et un potentiel pour créer du lien, animer cet écosystème entre infrastructures de recherche, entre chercheurs mais surtout entre tout acteur de la science. C’est un travail interdisciplinaire incluant une mise en relation entre publications, individus et terrains d’action.
- Mise en réseau des infrastructures de recherche, plateformes, auteurs, acteurs et données des sciences ouvertes
- Capitalisation, valorisation et réutilisation des publications, contenus éditoriaux, services, biens
- Doter les sciences ouvertes d’une plateforme open-source, véritable relais entre la science et le grand public pour une expérience ludique des sciences
- Outiller la science ouverte d’un observatoire* réflexif coopéré sur ses propres pratiques
