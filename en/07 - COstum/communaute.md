# Plateforme pour une communauté locale

## Objectifs
- Faciliter les échanges (marchands, non-marchands, bourses d’emploi, …) au sein d’une communauté
- Satisfaire une demande en l’associant à une offre
- Modéliser une place de marché et les “monnaies” d’échanges


## Principe
A travers l’offre COstum, nous sommes à même de proposer un outil d’animation de réseaux et communautés dont l’activité principale est d’animer et faciliter les échanges entre ses membres. Nous modélisons des “places de marché” (modèle offres/demandes) selon les spécificités de chaque communauté d’échanges. Nous facilitons la mutualisation de ressources non-marchandes ou la vente/location de biens et services qui peuvent reposer ou non sur des devises diverses (devises bancaires, monnaies locales, monnaies libres, cryptomonnaie). En nous basant sur notre module “annonces”, nous tâchons de faciliter les interactions pour faire correspondre au mieux une proposition (offre) et un besoin (demande).
Nous associons bien souvent, en réponse aux besoins de nos partenaires, la place de marché avec d’autres modules annexes tels que l’agenda mutualisé pour connaître les activités en lien avec la communauté d’échanges, l’annuaire des membres de cette communauté, …


## Exemples
### [Co-mains](http://co-mains.csconnectes.eu/#welcome)
Co-mains est le réseau de mutualisation de ressources non-marchandes entre centre sociaux du Sud de la ville de Lille. Il a pour but de donner la possibilité aux acteurs du quartier d’échanger plus facilement du matériel ou d’autres ressources non marchandes. Il permet aux acteurs associatifs de proximité d’avoir une alternative au recours systématique à des prestataires dans l’organisation d’événements ou leurs activités quotidiennes.
