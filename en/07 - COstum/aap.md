# Plateforme d'appel à projets

## Objectifs
- Créer un formulaire d’appel à projets personnalisés
- Réaliser un appel à projets dynamiquement connectés aux acteurs du territoires
- Faciliter les échanges avec les parties prenantes (porteurs de projets, financeurs, partenaires, …)
- Faciliter le financement de filières en fédérant les candidats autour d’une vision commune
- Proposer un réel outil support des procédures d’instruction (animation des comités de pilotage, génération de comptes rendus sur les aspects budgétaires, suivi du statut des candidatures [refus, complet, incomplet, à l’étude, signé, financement alloué, bilan reçu, …)


## Principe
Permet à un financeur privé ou publics (fondations, mécènes, collectivités, services de l’état, … ) d’établir un appel à projets et de le lancer directement à partir de sa plate-forme personnalisée (Costum). L’ensemble des champs récoltés : porteurs de projets, communauté des projets, actions finançables, budgets prévisionnels, … sont directement intégrés et interprétables au sein de la plate-forme. Les financeurs et organismes en charge de l’instruction des dossiers pourront statuer sur l’éligibilité des projets de manière facilitée. Les actions financées pourront également être suivies, les porteurs de projet étant invités à y renseigner l’avancée de leurs projets fiancièrement, qualitativement et quantitativement selon des critères d’évaluation préalablement définis. En somme, Communecter propose un système d’appels à projets complets de la définition du cahier des charges de l’APP jusqu’à la réalisation des projets financés permettant d’évaluer les impacts réels sur un territoire donné, en cohérence et en interaction avec les acteurs en présence.


## Exemples
### [Contrat de Transition Ecologique](http://cte.ecologique-solidaire.gouv.fr/) (CTE) développée pour le ministère de la Transition écologique et solidaire

Les EPCI, dans un processus de labellisation CTE, invitent les acteurs (associations, entreprises, services publics, …) à proposer les actions qui vont dans le sens des objectifs de transition écologique qu’ils se sont fixés. Toutes les actions sont renseignées par leurs porteurs à partir du formulaire de l’appel à projets. Deux niveaux d’administration (avec des rôles définis qui donnent accès ou non à l’informations en écriture ou en lecture) sont alors présents allant du local au national :

- Les EPCI qui récoltent les projets d’acteurs locaux mettent en lien, rendent cohérents les projets entre eux, instruisent et suivent les actions.
- Le ministère et financeurs/partenaires nationaux et régionaux (ADEME, DEAL, Banque des territoires, …) évaluent et trouvent des solutions de financements et d’accomapgnement pour la réalisation des projets locaux.

Un observatoire national de l’impact des CTE selon des critères écologiques définis est alors établi et ne cessent d’être alimenté par le suivi des projets à travers la plate-forme. De réels outils de valorisation des résultats et d’essaimage de la démarche sont ainsi mobilisables et adaptables à un contexte donné.
