# Se communecter
_English version_

Il est devenu une habitude pour un grand nombre de personnes de se connecter chaque jour à un réseau social pour prendre des nouvelles de ses proches et du monde. Communecter est un réseau social, mais à la différence de ceux que nous utilisons depuis 2008, le code source est libre (vous pouvez contribuer à son amélioration, savoir comment il fonctionne). Par ailleurs, les données qui sont visibles sur le site sont en open data, ce qui veut dire qu'il est possible de les réutiliser sur d'autres plateformes.

L'intérêt de COmmunecter réside dans le référencement des initiatives locales d'un territoire, et la facilitation des échanges entre acteurs locaux (associations, citoyen·ne·s, groupes informels, entreprises locales, ...)

## Quelques astuces pour démarrer
1.  Créer un compte via la [page d'accueil](https://www.communecter.org/)
2.  Compléter votre profil en cliquant sur votre pseudo en haut à droite puis sur "À propos"
3.  Suivez les personnes et organisations que vous aimez
4.  Rejoignez les organisations dont vous faites partie, demandez à en être administrateur si vous y êtes impliqué

## Aller plus loin (faire venir sa communauté)
Communecter vous sera particulièrerement utile si votre écosystème y est présent. La communauté dans laquelle vous êtes impliquée n'est pas encore sur Communecter ? Voici une proposition de plan d'action :
1.  Créez et compléter le profil de votre groupe
2.  Invitez par mail les membres les plus actifs
3.  Commencez à référencer vos projets et/ou évènements à venir si vous en avez
4.  Postez un petit message d'arrivée sur le Live 😃
5.  Expérimentez le [Chat de discussions](/2 - Utiliser l'outil/Fonctionnalités/chat.md) et l' [Espace coopératif](/2 - Utiliser l'outil/Fonctionnalités/espaceco.md)

## Questions fréquentes

**Puis-je créer un compte pour mon organisation ?** Étant donné que toutes les initiatives d'un territoire partent des individus il n'y a pas de compte spécial pour les organisations. Une fois connecté, n'importe quel citoyen peut créer une organisation, un évènement ou un projet.

**Mon projet existe déjà sur la plateforme, comment puis-je modifier sa fiche ?** Par défaut toutes les fiches sont en édition libre. Si c'est le cas de votre projet, vous avez juste besoin d'être connecté pour accéder aux boutons d'édition. Si l'édition libre a été désactivé seul les membres peuvent modifier les informations. En savoir + : [Rôles](/2 - Utiliser l'outil/Fonctionnalités/Concepts/roles.md).

**Je ne veux pas que n'importe qui puisse modifier les informations de mon projet. Comment limiter cet accès ?** Rejoignez l'évènement, le projet ou l'organisation en tant qu'administrateur puis cliquez sur Paramètres > Paramètres de confidentialités.

**Autorisez vous le pseudonymat ?** Oui, il est tout à fait possible d'utiliser un pseudo à la place de son nom et prénom.
