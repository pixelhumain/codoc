# Galerie

> - Hey Roger ! T'as mis où l'ordre du jour de la réunion de demain ?
> - Dans la galerie d'Communecter pardi !

La galerie permet de retrouver tous vos photos, documents et liens important dans un même espace : compte-rendu de réunion, organigramme, statuts, ...

Pour accéder à la galerie de votre organisation :
-   Rendez-vous sur la fiche de l'organisation
-   Cliquez sur "Galerie"

Retrouvez votre galerie personnelle en allant sur votre propre profil.

![](/Images/galerie-rejoindre.png)

Pour des raisons de moyens nous limitons la taille du fichier à 2Mb. Les formats acceptés sont : pdf, xls, xlsx, doc, docx, ppt, pptx, odt, ods, odp.
