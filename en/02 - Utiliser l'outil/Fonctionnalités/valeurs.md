# Valeurs

## Présentation
Ces formulaires (accessibles sur **Nos valeurs**) permettent de donner un aperçu de la gestion et de la vie de votre organisation.
Si la fiche est en Open Édition tout le monde peut procéder à l'évaluation.


## Évaluer votre organisation comme un bien commun
Définir une organisation comme un commun signifie qu'elle gère une ou plusieurs ressources de manière ouverte et transparente sans se l'approprier. [En savoir +](https://fr.wikipedia.org/wiki/Bien_commun).

Si vous n'êtes pas sûr que votre organisation soit un commun, ou que vous ne connaissez pas ce terme, ce formulaire vous permettra d'appliquer facilement ce concept à votre groupe.


## Libre
Ce formulaire vous permet de mettre en avant n'importe quel aspect de votre organisation.
Vous pouvez supprimer les rubriques par défaut et en créer autant que vous voulez.
