# LISTE DES FONCTIONNALITÉS

## Applications
-   **[MOTEUR DE RECHERCHE](/2 - Utiliser l'outil/Fonctionnalités/recherche.md)** : retrouver les acteurs d'un territoire
-   **ACTUALITÉ** : calendrier des actions locales
-   **[ÉVÈNEMENTS](/2 - Utiliser l'outil/Fonctionnalités/agenda.md)** : actualité localisée
-   **[ANNONCES](/2 - Utiliser l'outil/Fonctionnalités/annonces.md)** : vendez des biens et des services, proposez et recherchez des ressources humaines ou matérielles
-   **AGORA** : votes et consultations citoyennes


## Outils
-   **[ESPACE COOPÉRATIF](/2 - Utiliser l'outil/Fonctionnalités/espaceco.md)** : outil de gouvernance pour les projets et les organisations
-   **[MESSAGERIE](/2 - Utiliser l'outil/Fonctionnalités/chat.md)** : outil de discussions instantanées
-   **[GESTIONNAIRE DE FICHIERS ET D'URL](/2 - Utiliser l'outil/Fonctionnalités/galerie.md)** : stockage de documents ou d'images et partage de liens


## Fonctionnalités supplémentaires
-   **[Cartographie interne](/2 - Utiliser l'outil/Fonctionnalités/cartes.md)** : tous les éléments étant géolocalisés on peut afficher une cartographie interactive des données de la page affichée (résultat de recherche, agenda d'une ville, liste des projets d'une organisation, ...)
-   **[Sondages](/2 - Utiliser l'outil/Fonctionnalités/sondages.md)** : questionnaire à choix multiple diffusable dans le live
-   **Tags** : permet de classer les éléments par thème
-   **Alertes** : soyez notifié d'une nouvelle publication d'annonce
-   **S'abonner** : pour retrouver l'actualité de n'importe élément dans l'accueil
-   **Inviter** : pour ajouter des membres sur un élément, possibilité d'inviter par mail et de créer des rôles (ex : organisateur, partenaires, ...)
-   **Publier** : pour afficher un message sur le journal d'un élément, possibilité de choisir l'audience de la publication et de mentionner des citoyens et organisations
-   **Internationalisation** : pour changer de pays (langue de l'interface et lieux proposés)
-   **Espaces commentaires**
-   **Exporter** : récupérer son profil en JSON
-   **Visualisation en graphes** : un graphe interactif des écosystèmes présents dans Communecter


## Architecture Modulariser et Personnalisable
-   **[COSTUM](/6 - COstum/costum.md)** : plateforme sociale personnalisé ré-utilisant toutes ou partie des fonctionnalités de communecter.org
-   **[NETWORK](/2 - Utiliser l'outil/Fonctionnalités/network.md)** : création de cartes personnalisées
-   **FORMULAIRE** : récoltez et analysez de l'information par questionnaire
