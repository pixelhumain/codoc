## À partir de communecter.org

Les organisations affichées sur un [network](https://doc.co.tools/books/2---utiliser-loutil/page/cr%C3%A9er-un-network) (carte indépendante) sont modifiables via COmmunecter.

Il faut être connecté pour modifier des information.

1.  Utilisez le [moteur de recherche de COmmunecter](https://communecter.org/#search) pour **trouver l'élément** à modifier (association, évènement, groupe, projet, ...)
2.  Cliquez sur "**Éditer les informations**" (1)
3.  Dans la partie "Informations générales" cliquez sur "**Éditer**" (2)
4.  Modifiez et sauvegardez

[![editertag.png](https://doc.co.tools/uploads/images/gallery/2019-01-Jan/scaled-840-0/screenshot-communecter.org-2019.01.10-10-26-55.png)](https://doc.co.tools/uploads/images/gallery/2019-01-Jan/screenshot-communecter.org-2019.01.10-10-26-55.png)
