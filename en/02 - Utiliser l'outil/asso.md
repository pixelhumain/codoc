# Communecter mon organisation
English version

## Présentation
En tant que réseau dédié aux acteurs et initiatives locales les fonctionnalités de COmmunecter sont particulièrement adaptées aux pratiques des associations.

_[Ouvrir ce diaporama dans un onglet](https://pixelhumain.github.io/reveal.js/slideshows/Associations.html#)_
<iframe title="Communecter mon association (diaporama)" src="https://pixelhumain.github.io/reveal.js/slideshows/Associations.html#/" width="830" height="600"></iframe>

## Par où commencer ?
Quelques minutes suffisent pour commencer à échanger via COmmunecter.

#### 1 - Préparer le terrain
Commencez par créer les gros éléments de votre communauté. Pas besoin d'y passer des heures, l'objectif est de montrer ce qu'il est possible de faire sur COmmunecter et de **rendre la contribution facile**.

> Cool, mon groupe de travail est déjà créé. Hop ! Je lui rajouter une image de profil et j'annonce la date de la prochaine réunion.

1.  Créez votre organisation grâce au bouton en bas à gauche
2.  Complétez votre fiche via les différents onglets
3.  Publiez un message de bienvenue

![](/Images/creeelement.md)

![](/Images/membreelement.png)

#### 2 - Inviter les membres actifs
Ils n'auront qu'a cliquer sur le lien reçu par mail pour rejoindre le groupe. ![](/Images/inviter.png)

#### 3 - Lancez une conversation
En cliquant sur "Messagerie" vous accédez au [chat interactif](/2 - Utiliser l'outil/Fonctionnalités/chat.md) (disponible sur Android et iOS) de votre communauté.

![](/Images/discussionchat.png)

#### 4 - Aller plus loin
-   Référencez les organisations avec qui vous êtes en lien
-   [Ajoutez les documents important dans le gestionnaire de fichiers et d'URL](/2 - Utiliser l'outil/Fonctionnalités/galerie.md)
-   [Lancez un vote dans l'espace collaboratif](/2 - Utiliser l'outil/Fonctionnalités/espaceco.md)
-   Créez des tâches et répartissez les entre vous
-   [Créez une dynamique inter-associative locale](/5 - Déployer/kit.md)

![](/Images/resultatsvotes.png)
