# Communecter son média
_English version_

Les médias locaux sont des acteurs très important de la vie de la cité. Ils recueillent et diffusent beaucoup d'informations liées à l'activité locale. Les médias libres portent également des valeurs fondamentales auquel les pixels humains sont fortement attachés.

![](/Images/radiolibre.jpeg)

Sur Communecter il est possible de :
-   partager du contenu (podcast, articles, ...) via des [publications territorialisées](/2 - Utiliser l'outil/Fonctionnalités/publications.md)
-   interagir avec les autres organisations locales grâce aux mentions et commentaires
-   créer et diffuser des [cartes](/2 - Utiliser l'outil/Fonctionnalités/cartes.md) (partenaires, membre de la communauté, ...)
-   présenter les projets et évènements portés le média
-   animer sa communauté de journalistes ([chat](/2 - Utiliser l'outil/Fonctionnalités/chat.md), [agenda interne](/2 - Utiliser l'outil/Fonctionnalités/agenda.md), [espace collaboratif](/2 - Utiliser l'outil/Fonctionnalités/espaceco.md), ...)
-   participer au réseau d'entraide local
