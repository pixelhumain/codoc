# Fete de l'art évolution 

:::danger 
ce document est **en cours de création**, si tu es là alors n'hésite pas à contribuer
:::

:::success
### PARTICIPER
- [En feedback sur le chat](https://go.rocket.chat/invite?host=chat.communecter.org&path=invite%2FgnXaJt)
- Rejoignez le réseau de ceux qui font de l'art évolution et de la culture Libre avec son  [Réseau Social Citoyen Libre](https://www.communecter.org/#@lartEvolution)
:::

# Description
> Tout les artistes mobilisés et en action pour une contre fete de la musique , une réappropriation de l'espace commun culturel 
> un appel à tous les artistes à venir faire leur art avec du sens , celui du message du monde dont on reve. Un moment de partage populaire

> Un appel de désobéisance à ne pas la fete le jour où ils l'ont instituer mais NOUS les artistes décidons du moment 

> un déploiement spontané, ne jamais organiser en rassemblemnt , c'est l'art qui sera le rassemblement 

> Tout les types d'arts, et sutout l'art citoyen ouvert à tous. 

:::info
# Détails
- hashTag **#lartevolution**
- évennement aura lieu la **journée du 5juillet** (premiere pleine lune aprés la nouvelle lune du 21 juin)
- la communication commencera **lundi 15 juin** (premiere pleine lune aprés la nouvelle lune du 21 juin)
- **à decider** : **heure fixe** 13h => 22h en mode on/off
- Sans Nom , Sans étiquette juste **fete l'art évolution** 
- qu'avec des outils libres 
- **l'ubiquité** , mouvement se passe partout au meme moment
- chacun porte le message qui lui correspond le plus
    - Message Libre 
    - pas de marques , que du soutien, 
    - exemple :
        - Je suis ........
        - Je suis charlie
        - Je suis Floyd
        - Je suis Adama
        - je suis la Foret 
        - je suis L'océan 
        - Je suis 100 NOMS
:::

:::warning
# idée d'action culture
- conteur 
- chanteur 
- musicien
- sculpteur 
- l'art citoyen
    - drap avec un slogan 
    - crier un texte à la fenetre ou la place 
    - danser dans la rue 
    - répéter les beaux messages des anciens
> important TOUJOURS porter le message 
signé **fete l'art évolution**
:::

:::warning
# Slogans
- Le coup de clairon 
- Faites l'art évolution 
- L'Art envahit les rues, et Nos rêves sont trop grands pour vos cases
- célébrer les changements de saison 
- la liberté de penser, de créer
- Désobeissance artistique
- Nos rêves sont trop grands pour vos cases
- La terre native sur une terre nette
- C'est juste , c'est  neutre , il faut juste oser 
- 4 fete de la culture, plutot qu'une de la musique
- 
:::

:::danger
# En cours 
- creation d'une image logo 
- partage au 1er petit cercle 
- recolte de feed back, améliorer le
:::

:::warning
# Phasage du projet 
- un page projet dédié
    - calendrier des évennements
    - actualité et activité des créations
    - costum dédié
- une vidéo 
    - TODO : scenario de la video 
- communication / diffusion 
    - finir le texte pour qu'il soit clair 
        - relecture
    - texte envoyé au 1er niveau du reseau 
![](https://codimd.communecter.org/uploads/upload_da24c24fd25fb229641941715a06ff61.png)
        - c le COEUR
            - Chaque acteur s'inscrit sur https://www.communecter.org/#@lartEvolution
            un reseau Libre 
        - le COEUR comprend l'interet , se l'approprie et sait le communiquer 
        - sans interaction (sans reunion, sans telephone) c'est le resultat du texte initial
    - tous les acteurs du COEUR devront expliquer le projet en vidéo, story, IGTV avec leur mot
    - texte envoyé au 2eme niveau du reseau 
        - le COEUR transmet a son tour le projet à son réseau 
        c'est le 2eme niveau de COEUR (le COEUR grossit)
    - et ainsi de suite sans limite 
- il y aura d'autres actions au sein de ce réseau de l'ART EVOLUTION
:::
![](https://codimd.communecter.org/uploads/upload_376da26bef343bd9f4c61d44e3d9d6a4.png)

:::info
# Communication
## Chat 
>  Hello 
>  Je te présente  un projet à te partager, lancer par un ami artiste qui en marre et veut des actions positive : 
>  [FETES L'ART ÉVOLUTION](https://codimd.communecter.org/s/fyPGkRuyx)
>  document en cours n'hésite pas à contribuer 

> on va hacker la fete de la musique
on va boycoter la fete de la musique institutionnelLe de la nouvelle lune
et instaurer la fete citoyenne à la pleine lune du 5 juillet 
puisque le gouv ne comprends pas, n'écoute pas et est entrain d'essayer de tout museller , controller, ce sera récurrent à chaque changement des 4 saisons, il y aura 4 fete de la culture, plutot qu'une de la musique et surtout un coup de clairon de tout les artistes vivant et tout ceux qui dorment 
> vient là que ca s'organise #lartEvolution
> [Voir ou rejoindre les discussions](https://go.rocket.chat/invite?host=chat.communecter.org&path=invite%2FgnXaJt)
## Email
## Réseau Sociaux
:::

:::success
# Resultat
- enregistrer l'action avec le message mis au premier niveau 
- se retrouver en réseau pour s'unir et etre positif 
:::