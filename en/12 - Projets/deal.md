# projet DEAL 

# requete mongo
```
db.forms.find({$or:[{"id":/deal/},{"subForms":/deal/}]});
db.organizations.find( {slug:/dealAH/} ) 
```

# Liste des dossiers 
http://127.0.0.1/costum/co/index/slug/dealAH#dossiers 

# Configurer le coform
http://127.0.0.1/costum/co/index/slug/dealAH#form.edit.id.5eaa97e463824ff677db090d

# Lire un dossier
http://127.0.0.1/costum/co/index/slug/dealAH#answer.index.id.5eb1ba7b539f22b5738b4569.mode.r 

# Editer un dossier
http://127.0.0.1/costum/co/index/slug/dealAH#answer.index.id.5eb1ba7b539f22b5738b4569.mode.w

# Observatoire
http://127.0.0.1/costum/co/index/slug/dealAH#dashboard

# JS Objects

```
var answerId
var answerObj

_id: Object { "$id": "5eb1ba7b539f22b5738b4569" }
​answers: Object { deal1: {…}, deal3: {…} }
​created: 1588705915
​form: "5eaa97e463824ff677db090d"
​formId: "deal1|deal2|deal3|deal4|deal5"
​formList: 5
​links: Object { answered: (1) […], updated: {…}, operators: {…}, … }
​source: Object { key: "deal", keys: (1) […], insertOrign: "costum", … }
​step: "deal5"
​updated: 1589867931
​user: "5534fd9da1aa14201b0041cb"
​validation: Object { deal3: {…}, deal4: {…} }
```

```
var form

_id: Object { "$id": "5eaa97e463824ff677db090d" }
​active: true
​hasStepValidations: 1
​id: "dealForm"
​mapping: Object { name: "answers.deal1.deal111", address: "answers.deal1.deal114", description: "answers.deal1.deal112", … }
​params: Object { elementdeal13: {…}, deal24: {…}, budget: {…}, … }
​parent: Object { 5e5f4157539f226271b7b4a7: {…} }
​rules: Object { deal2: {…}, deal3: {…}, deal4: {…} }
​source: Object { key: "deal", keys: (1) […], insertOrign: "costum" }
​subForms: Array(5) [ "deal1", "deal2", "deal3", … ]
​updated: 1588789273
```

```
formInputs
{…}
​deal1: Object { deal11: {…}, deal111: {…}, deal111b: {…}, … }
​deal2: Object { deal2regle: {…}, deal24: {…}, deal24x: {…}, … }
​deal3: Object { deal312: {…}, budget: {…}, deal3stepValidation: {…} }
​deal4: Object { financement: {…}, stepValidation: {…} }
​deal5: Object { deal43: {…}, deal5stepValidation: {…} }
```

```
formsData
{…}
​deal1: Object { id: "deal1", type: "openForm", name: "Dossier", … }
​deal2: Object { id: "deal2", type: "openForm", name: "Instruire", … }
​deal3: Object { id: "deal3", type: "openForm", name: "Dépense", … }
​deal4: Object { id: "deal4", type: "openForm", name: "Finances", … }
​deal5: Object { id: "deal5", type: "openForm", name: "Travaux", … }
```

```
sectionDyf
{…}
​deal111bParams: Object { jsonSchema: {…} }
​deal111bParamsData: Object { options: [] }
​deal24Params: Object { jsonSchema: {…} }
​deal24ParamsData: Object { options: (2) […] }
​financementFromBudgetfinancement: Object { jsonSchema: {…} }
​financementFromBudgetfinancementParams: Object { jsonSchema: {…} }
​financementFromBudgetfinancementParamsData: Object { financerTypeList: {…}, limitRoles: (1) […], openFinancing: true }
​stepValidationdeal3stepValidationParams: Object { jsonSchema: {…} }
​stepValidationdeal3stepValidationParamsData: Object { canValidRoles: (1) […], canRequestRoles: (1) […] }
​stepValidationdeal5stepValidationParams: Object { jsonSchema: {…} }
​stepValidationdeal5stepValidationParamsData: Object { canValidRoles: (1) […], canRequestRoles: (1) […] }
​stepValidationstepValidationParams: Object { jsonSchema: {…} }
​stepValidationstepValidationParamsData: Object { canValidRoles: (1) […], canRequestRoles: (1) […] }
​suiviFromBudgetdeal43: Object { jsonSchema: {…} }
​suiviFromBudgetdeal43Params: Object { jsonSchema: {…} }
​suiviFromBudgetdeal43ParamsData: Object { limitRoles: (2) […] }
```