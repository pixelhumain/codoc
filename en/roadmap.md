
# roadmap 2020.1 

- Mongo 3.6 
- PHP7
- CDN OVH
- CO FORMS Générique 
- CObserv
- New Design
- Dashboard Personnel
- new Filter refactor
- Costum Generique template 

# roadmap 2020.2

- passage Yii2
- refonte APi
- refactor js conforme avec JSLint
- refactor SEO 
- CI : integration continue
- new design et Homepage 2020
- system Maintenance
- Collabathon trimestriel
- costum Ecoute à projet 
- Fediverse
- COP2P : COPI et COPIPI
- Costum Light
- interop RSS
- interop WEKAN
- interop Loomio
- Costum GogoCarto
- Costum Transiscope
