# Science ouvert 
06/2020
(DOC)[https://docs.google.com/document/d/1JtxXpTbQXATHni3Fz96TE6tr216uxNdTJlQz16kbNCA/edit?ts=5ecd5639#)
(PPT)[https://docs.google.com/presentation/d/17e5hN7xUAqzmCXo4joUMdlFhbJ4gNKiyrzmWYGDZfnU/edit?ts=5ecebca7#slide=id.g7f2715dafa_5_0]

# FACT
06/2020
(DOC)[]
(PPT)[https://docs.google.com/presentation/d/1di2j3etdu2CeUJvk6Rq7J2RbO3Qr2evvaKKm9xDWZG8/edit#slide=id.g7f2715dafa_5_0]
(PPT FACT)[https://docs.google.com/presentation/d/1ZesGOCO15vuIaW1MSpO852PhSGEbQ6NWlrV2kAWH7eM/edit#slide=id.g7f2715dafa_5_0]

# Mozilla 
06/2020
(DOC)[https://docs.google.com/document/d/1DBUcq-pzfSSQHB2_082SAFYiVjddczw14uUVnKQ1afU/edit]

# SMARTERRE
11/2019
(Methodo)[https://docs.google.com/presentation/d/1eyq2KHWZhPTmTvBajskaeFs4-jm10hBlAePEgWn1ux4/edit#slide=id.g5e5279c140_4_273]
(Synthese PPT)[https://docs.google.com/presentation/d/1NZr1DMFeS3I-JYuXrGvecijvgWW4zLaVvUyaevK-jHM/edit#slide=id.p]

# OPEN ATLAS
(DIAPO PPT)[https://docs.google.com/presentation/d/1Y7-aU4CgTAFXcsQ7EmLhD9vBUmS8c3-y8VGTanXRW5A/edit#slide=id.g6dd64bd4d7_0_30]
## Propositions
(PPT TOUT CO OPAL SMARTERRE (Short))[https://docs.google.com/presentation/d/1pnmib1Wpq3YFiDHgFFBQna3ugkP25qQGgJIjzrZk95E/edit#slide=id.g6d7662a366_0_0]
