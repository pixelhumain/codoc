## Présentation
Un network est une carte exploitant la base de donnée de Communecter. Il permet d'afficher tous les points correspondant à un tag (par exemple : #AMAP, #Culture, #Energie, ...) sur un périmètre défini (par exemple : plusieurs régions ou une seule ville).

Voici quelques exemples de network : [Tiers-Lieux des Hauts de France](http://hauts.tiers-lieux.org/mel/), [Assemblée des Communs de Lille](http://lille.lescommuns.org/cartographier-les-communs/), [Restaurants vegan de la Réunion](https://restovegan.re/), [Zéro Déchet Île de France](http://paris.zerowastefrance.org/la-carte), [Bretagne Telecom](https://www.communecter.org/?network=BretagneTelecom).

> Voir la page : [utilisation des cartes](https://github.com/pixelhumain/communecter/wiki/Utilisation-des-cartes) pour des tutoriels utilisateurs.

> Voir le canal de discussion : [marque blanche](https://chat.lescommuns.org/channel/marqueblanche) si vous avez besoin d'un coup de main.

## Introduction
> Ce tuto existe aussi en vidéo : https://youtu.be/OOnEnxjhr-I

L’adresse d’une carte commence par `https://www.communecter.org/communecter?network=` suivi d'un lien vers un fichier JSON contenant les différents paramètres de la carte (nom, filtres, limites géographiques, etc.). Ce JSON peut être hébergé sur votre site ou sur un site tierce (pour ce tuto nous utiliserons [Gist](https://gist.github.com/)).

Exemple de lien complet : `https://www.communecter.org/communecter?network=https://gist.githubusercontent.com/Simroubriff/3e14264620d9e59e874d44d306314679/raw/f8f013ded329d9e07e3e44666a548f974a7606c0/ZD-Paris`

> ⚠️ Le format JSON n’autorise pas l’ajout de commentaire dans le code. Le nom de votre fichier ne doit pas contenir d'espaces.

Pour commencer, le plus simple est de copier une configuration existante puis de modifier les paramètres un à un pour éviter les erreurs.

## Créer une carte personnalisée
1. Copiez l'exemple de configuration ci-dessous dans un nouveau Gist : http://gist.github.com/
1. Cliquez sur Raw (en haut à droite de votre code)
1. Ouvrez un nouvel onglet pour ouvrir l'URL suivante : `https://www.communecter.org/communecter?network=LienDuRAW`

## Intégrer la carte
En ajoutant le code suivant dans une page de vos pages, vos utilisateurs pourront utiliser la carte sans quitter votre site.
```HTML
<iframe src="https://www.communecter.org/communecter?network=LienDuRAW" width="100%" height="700px" frameborder="0"></iframe>
```


***


## Exemple de configuration

```json
{
    "name" : "Communs",
    "mode" : "server",
    "skin" : {
        "logo" : false,
        "title" : "Communs dans la région Hauts de France",
        "shortDescription" : "false",
        "displayScope" : false,
        "docs":true,
        "displayCommunexion" : true,
        "displayNotifications" : true,
        "profilBar" : false,
        "breadcrum" : true,
        "displayButtonGridList" : false,
        "class" : {
            "mainContainer" : false
        },
        "iconeAdd" : false,
        "iconeSearchPlus" : false,
        "loginTitle":"",
        "front" : {
	        "organization" : false,
	        "project" : true,
	        "event" : true,
	        "community" : true,
	        "dda" : false,
	        "live" : false,
	        "search" : false,
	        "need" : true,
	        "poi" : true
	    },
	    "menu" : {
		    "aroundMe":false,
		    "connectLink":false,
		    "add":true,
		    "detail":true,
		    "news":true,
		    "directory":true,
		    "gallery":true
	    }
    },
 "filter" : {
        "types" : false,
        "linksTag" : {
            "Type de tiers-lieu" : {
                "tagParent" : "Type",
                "background-color" : "#f5f5f5",
                "image" : "Travail.png",
                "tags" : {
                  "Télétravail" : "Télétravail",
                  "Coworking" : "Coworking",
                  "Médiation Numérique" : "Médiation numérique",
                  "FabLab" : "Fablab",
                  "Accompagnement de projets" : "Accompagnement de projets",
                  "Atelier" : "Atelier",
                  "Salle de réunion" : "Salle de réunion",
                  "Communauté Catalyst" : "Catalyst",
                  "Espace à louer" : "Espace à louer"
                }
            },
            "Services complémentaires" : {
                "tagParent" : "Service",
                "background-color" : "#f5f5f5",
                "image" : "Service.png",
                "tags" : {
                    "Espace détente" : "Espace détente",
                    "Cuisine" : "Cuisine",
                    "Achat groupé" : "Achat groupé",
                    "Bar participatif" : "Bar participatif",
                    "Cuisine participative" : "Cuisine participative",
                    "Epicerie participative" : "Epicerie participative",
                    "Jardiné" : "Jardin",
                    "Domiciliation" : "Domiciliation",
                    "Atelier participatif" : "Atelier participatif",
                    "Restaurant" : "Restaurant",
                    "Bar":"Bar"
                }
            },
            "Modèle de gestion" : {
                "tagParent" : "Modele",
                "background-color" : "#f5f5f5",
                "image" : "Loisir.png",
                "tags" : {
                  "Commun" : "Commun",
                  "Association" : "Association",
                  "Business" : "Business",
                  "Public" : "Public"
                }
            },
            "État du projet" : {
                "tagParent" : "Etat",
                "background-color" : "#f5f5f5",
                "image" : "Reparation.png",
                "tags" : {
                  "En projet" : "En projet",
                  "En démarrage" : "En démarrage",
                  "En fonctionnement" : "En fonctionnement"
                }
            },
            "Taille de l'espace" : {
                "tagParent" : "Surface",
                "background-color" : "#f5f5f5",
                "image" : "Information.png",
                "tags" : {
                  "Moins de 60m2" : "Moins de 60m2",
                  "Entre 60 et 200m2" : "Entre 60 et 200m2",
                  "Plus de 200m2" : "Plus de 200m2"
                }
            }
          }
    },
    "add" : {
        "organization" : true,
        "person" : true
    },
    "result" : {
        "displayImage" : true,
        "displayType" : false,
        "fullLocality" : true,
        "datesEvent" : false,
        "displayShortDescription" : true
    },
    "request" : {
        "pagination" : 1000,
        "searchType" : [
            "organizations",
	    "person",
	    "poi"
        ],
           "searchTag" : [
            "communs",
            "commun",
            "Commun",
            "Communs"
        ],

	"searchLocalityREGION": [
		"Nord-Pas-de-Calais-Picardie"
	],

	"mainTag" : ["commun"]
	}
}
```

***

## Paramétrage

    Vous cherchez une info sur un tag ? Utilisez le moteur de recherche de votre navigateur : Ctrl + F.

### `name`
Titre affiché dans la bannière du haut.

### `mode`
?

### `skin`
Paramètres pour afficher des fonctionnalités sur la carte.

#### `logo`
Affiche un logo affiché dans la bannière. Valeurs possibles : `false` ou lien vers un fichier `jpeg` ou `png`.

#### `title`
Titre affiché dans la bannière.

#### `shortDescription`
Description.

#### `displayScope`
#### `docs`
#### `displayCommunexion`
#### `displayNotifications`
#### `profilBar`
#### `breadcrum`
#### `displayButtonGridList`
#### `class`
`mainContainer` : ...
#### `iconeAdd`
#### `iconeSearchPlus`
#### `loginTitle`
#### `front`
* `organization` : ...
* `project` : ...
* `event` : ...
* `community` : ...
* `dda` : ...
* `live` : ...
* `search` : ...
* `need` : ...
* `poi` : ...
#### `menu`
* `aroundMe"` : ...
* `connectLink` : ...
* `add` : ...
* `detail` : ...
* `filter` : ...
* `news` : ...
* `directory` : ...
* `gallery` : ...

### `filter`
Permet de créer des filtres. Exemple de filtre :
```json
"filter" : {
    "types" : false,
    "tags" : true, 
    "paramsFiltre" : {
        "conditionBlock" : "or",
        "conditionTagsInBlock" : "and"
    },
    "linksTag" : {
        "Se nourrir" : {
            "tagParent" : "Alimentation",
            "background-color" : "#f5f5f5",
            "image" : "Alimentation.png",
            "tags" : {
                "Amap" : "Amap",
                "magasin de producteurs" : "MagasinDeProducteurs",
                "Groupement d'achat" : "GroupementDAchat",
            }
        }
    }
}
```

#### `types`
Filtre par type. Valeurs possibles : `false`, `organizations` ou `events`.

#### `tags`

#### `paramsFiltre`
Par défaut si on ne met pas de paramsFiltres , les tags dans un même block agissent comme des "OU" et ceux entre les blocks comme des "ET".

* `conditionBlock` : permet de définir comment agissent les tags entre les blocks
* `conditionTagsInBlock` : permet de définir comment agissent les tags dans un block

#### `linksTag`
* `tagParent` : ...
* `background-color` : couleur de fond de ...
* `image` : logo du filtre
* `tags` : ...

### `add`
#### `organization`
#### `person`
### `result`
#### `displayImage`
#### `displayType`
#### `fullLocality`
#### `datesEvent`
#### `displayShortDescription`

### `request`
#### `sourcekey`
Clé fournit par Communecter pour accéder aux données (MANDATORY).
 Requête transmise à Communecter pour afficher des résultats lors de l'ouverture de la carte.
#### `pagination`
Nombre de points remonté par requête.

#### `searchType`
Type de recherche dans la base de donnée communecter (MANDATORY).
Liste des valeurs possibles : 'organizations', 'person', 'poi', 'projects', 'events'.

#### `searchTag` 
Permet de filtrer la liste des points selon un ou plusieurs tags.
Exemple de valeurs :
```json
"searchTag" : [ "communs", "commun", "Commun", "Communs" ]
```

#### `mainTag`
Tag principal des points remontés. Les points créés via la carte auront le tag mentionné.

#### `searchLocalityREGION`
Limite la liste des points remontés à une ou plusieurs régions. Liste des valeurs acceptées :
```
"Île-de-France"
"NOUVELLE-CALEDONIE"
"POLYNESIE"
"Bourgogne-Franche-Comté"
"Aquitaine-Limousin-Poitou-Charentes"
"Alsace-Champagne-Ardenne-Lorraine"
"Normandie"
"Mayotte"
"Nord-Pas-de-Calais-Picardie"
"Languedoc-Roussillon-Midi-Pyrénées"
"Provence-Alpes-Côte d'Azur"
"Pays de la Loire"
"Auvergne-Rhône-Alpes"
"Guadeloupe"
"Martinique"
"Bretagne"
"Centre-Val de Loire"
"Corse"
"Guyane"
"La Réunion"
```

#### `searchLocalityDEPARTEMENT`
Limite la liste des points remontés à un ou plusieurs département. Liste des valeurs acceptées :
```
"ESSONNE"
"YONNE"
"TERRITOIRE DE BELFORT"
"HAUTE-VIENNE"
"VOSGES"
"VAL-D'OISE"
"SEINE-MARITIME"
"MAYOTTE"
"SEINE-ET-MARNE"
"YVELINES"
"DEUX-SEVRES"
"SOMME"
"TARN"
"TARN-ET-GARONNE"
"VAR"
"VAUCLUSE"
"VENDEE"
"VIENNE"
"PAS-DE-CALAIS"
"PUY-DE-DOME"
"PYRENEES-ATLANTIQUES"
"HAUTES-PYRENEES"
"PYRENEES-ORIENTALES"
"BAS-RHIN"
"HAUT-RHIN"
"RHONE"
"HAUTE-SAONE"
"SAONE-ET-LOIRE"
"SARTHE"
"SAVOIE"
"ISERE"
"HAUTE-SAVOIE"
"PARIS"
"GUADELOUPE"
"MARTINIQUE"
"LOT-ET-GARONNE"
"LOZERE"
"ARDECHE"
"MAINE-ET-LOIRE"
"LOIRE-ATLANTIQUE"
"MANCHE"
"MARNE"
"HAUTE-MARNE"
"MAYENNE"
"MEURTHE-ET-MOSELLE"
"MEUSE"
"MORBIHAN"
"MOSELLE"
"NIEVRE"
"NORD"
"OISE"
"ORNE"
"COTES-D'ARMOR"
"CREUSE"
"DORDOGNE"
"DOUBS"
"DROME"
"EURE"
"EURE-ET-LOIR"
"FINISTERE"
"GARD"
"HAUTE-GARONNE"
"GERS"
"GIRONDE"
"HERAULT"
"ILLE-ET-VILAINE"
"INDRE"
"INDRE-ET-LOIRE"
"JURA"
"LANDES"
"LOIR-ET-CHER"
"LOIRE"
"ALLIER"
"HAUTE-LOIRE"
"CANTAL"
"LOIRET"
"LOT"
"AIN"
"AISNE"
"ALPES-DE-HAUTE-PROVENCE"
"HAUTES-ALPES"
"ALPES-MARITIMES"
"ARDENNES"
"ARIEGE"
"AUBE"
"AUDE"
"AVEYRON"
"BOUCHES-DU-RHONE"
"CALVADOS"
"CHARENTE"
"CHARENTE-MARITIME"
"CHER"
"CORREZE"
"CORSE-DU-SUD"
"HAUTE-CORSE"
"COTE-D'OR"
"HAUTS-DE-SEINE"
"SEINE-SAINT-DENIS"
"VAL-DE-MARNE"
"GUYANE"
"REUNION"
```

#### `searchLocalityNAME`
Limite la liste des points remontés à une ou plusieurs villes.
La liste des valeurs autorisée est disponible ici : [https://www.data.gouv.fr/fr/datasets/base-officielle-des-codes-postaux/](https://www.data.gouv.fr/fr/datasets/base-officielle-des-codes-postaux/)