* [Accueil](https://chat.lescommuns.org/channel/co_accueil) : Le chat général pour communecter
* [API](https://chat.lescommuns.org/channel/co_api_semantique) : Spécialisé dans l'API et l'utilisation de cette dernière
* [Bus](https://chat.lescommuns.org/channel/co_bus) : Détail du projet de Bus Communecter (AFNIC)
* [Datatools](https://chat.lescommuns.org/channel/co_datatools) : Comment gérer les datas d'autres plateformes
* [Développeurs](https://chat.lescommuns.org/channel/co_dev_open) : Chat des développeurs, allez y si vous voulez savoir ce que vous pouvez coder.
* [Documentation](https://chat.lescommuns.org/channel/co_documentation) : Comment mettre en place une bonne documentation pour communecter
* [Holoptisme](https://chat.lescommuns.org/channel/co_holoptisme) : Améliorer l'holoptisme de la plateforme est une nécessité si l'on veut qu'une communauté prennent en main cette dernière
* [Juridique](https://chat.lescommuns.org/channel/co_juridique) : Comment faire en sorte que la plateforme et tout ce qui gravite autour soit juridiquement viable.
* [Nuit Debout](https://chat.lescommuns.org/channel/co_nuitdebout) : Créer un lien entre communecter et nuit debout
* [Référencement](https://chat.lescommuns.org/channel/co_referencement) : Ici on discute de choses à référencer sur la plateforme
* [Scrum](https://chat.lescommuns.org/channel/co_scrum) : version écrite pour les devs qui ne peuvent assister au Scrum (réunion visio) quotiden
* [Subventions](https://chat.lescommuns.org/channel/co_subventions) : Ici, en plus de parler des différentes subventions qui impliquent communecter, on définit une méthode pour pouvoir financer la plateforme plus facilement.
* [Tests](https://chat.lescommuns.org/channel/co_tests) : Pour tout ce qui est d'améliorer la plateforme via de nouvelles idées ou de rapport de bug, c'est ici !
* [Tuto](https://chat.lescommuns.org/channel/co_doc_tuto) : Création de tuto pour améliorer la compréhension des utilisateurs.