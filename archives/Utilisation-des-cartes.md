> Pour la création de carte, [voir la page Network](https://github.com/pixelhumain/communecter/wiki/Network)

### Découverte de l’interface de navigation
*En vidéo : https://youtu.be/XcuYgWg5zj0*
![Présentation de l'interface](http://animateurmultimedia.fr/partage/doccarto.png)

Vous pouvez accéder à une carte Communecter via le site communecter.org, un site tierce intégrant la carte dans sa page ou une adresse internet dédiée. Les critères et la zone géographiques des points affichés dans la carte sont définis par l’éditeur. Sur la droite vous pouvez zoomer plus ou moins sur une zone géographique. Sur la gauche, les filtres vous permettent d’affiner votre recherche selon des critères définis par l’éditeur. Sur la droite, vous trouverez un moteur de recherche pour affiner votre recherche selon vos critères. En haut à droite, vous pouvez passer en mode liste. En cliquant sur en savoir plus, vous accéder à la fiche complète du point d’intérêt.

### Créer un compte Communecter via le module network
*En vidéo : https://youtu.be/hK5JaYYj0KA*
![S'inscrire](http://animateurmultimedia.fr/partage/inscrirecarto.png)

Comme pour wikipédia, un compte est indispensable pour modifier la fiche d’une organisation présente sur la carte. Nous pouvons s’inscrire sur Communecter en cliquant sur le bandeau en haut de la carte, puis “S’inscrire”. Entrer les informations puis cliquez sur je m’inscris.

### Ajouter une organisation
*En vidéo : https://youtu.be/DTuzy7bWo-0*
![Ajouter un lieu](http://animateurmultimedia.fr/partage/ajoutcarto.png)

Une fois connecté vous trouverez en bas à gauche le bouton vous permettant d’ajouter un élément. Le type d’élément proposé (organisation, événement, … ) a été choisi par la personne ayant créer la carte.
Si vous souhaitez compléter la fiche créée, il faudra passer par le site communecter.org.

### Modifier une fiche
*En vidéo : https://youtu.be/fqt_fkV0Mfo*
![Accès fiche](http://animateurmultimedia.fr/partage/accesfiche.png)
![Modifier fiche](http://animateurmultimedia.fr/partage/modifierfiche.png)

Connectez-vous avec votre compte communecter.org. Choisissez un point d'intérêt, puis cliquez sur "en savoir +".  Modifiez la fiche et n'oubliez pas de sauvegarder.
L'onglet "historique" vous permet de connaître la date, l'auteur et la teneur exacte de chaque modification.
