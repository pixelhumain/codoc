
--- 
OLD 2019
---


# COFORMS : Collaborative Open Forms
![](https://codimd.communecter.org/uploads/upload_eeace7a51697831f6d96f276ed6098c0.png)

[PPT](https://docs.google.com/presentation/d/12E7zhh-Tp7J0fRcEVRIuxpXPlbPBVBP7GB3JnycPNm8/edit#slide=id.g737fb454ef_0_7)
[CODMID](https://codimd.communecter.org/mlc2qxtGStCNb5MnUJwfiA#)

# C'est quoi
:::danger
 TOOOODDDOOOOOOOO
:::

# Slogans
- L’intelligence collective structurer
- Open Forms sont des semence libres , des graines qui peuvent faire pousser des communautés, du savoir, du partage, de la collaboration
- DIUKW : Data > Information > Understanding > Knowledge > Wisdom

# Cas d'usages
- Créer une communauté
- Un observatoire autour d’une étude ou d’une thématique
- Sondage
    - Journalismes citoyens 
    - des mémoires dynamiques étudiant
- Appel à projet 
- Collaboratif
    - Brainstorming
    - Compte rendu, Taches, Agendas
- Parametrer un Costum 
- Usage Complexe 
    - management de projet
    - gestion de groupement d’achat
- Virer Google Forms 

![](https://codimd.communecter.org/uploads/upload_3d2ba1bad1052ce1de487f6fbe3ebf96.png)

# Export un schema
http://127.0.0.1/survey/form/schema/id/ctenatForm

# Lire un dossier
http://127.0.0.1/costum/co/index/slug/dealAH#answer.index.id.5eb1ba7b539f22b5738b4569.mode.r 

# New Answer
http://127.0.0.1/survey/answer/index/id/new/form/5f3e5d62275640c499c2e11a/contextId/5cebf50a40bb4e283c46ba57/contextType/projects

# Lire un dossier
http://127.0.0.1/costum/co/index/slug/dealAH#answer.index.id.5eb1ba7b539f22b5738b4569.mode.r 

# Editer un dossier
http://127.0.0.1/costum/co/index/slug/dealAH#answer.index.id.5eb1ba7b539f22b5738b4569.mode.w

# Observatoire
http://127.0.0.1/costum/co/index/slug/dealAH#dashboard

## building your own forms
- on element detail page 
- click form button 
- add a new form 

## input templates

## complexe inputs

# Export answers
http://127.0.0.1/survey/???


# Coding Notes

## Open Forms
[X] documentation [[~d/modules/costum/views/tpls/forms/README.md]]
[ ] schema [[https://docs.google.com/presentation/d/1jwi3pYEzL_r8IH1AEpzlJe4jTt2kG7BZS5Hyq1kZHeE/edit#slide=id.p]]


## Project manager
[[projectManager.org]]

### element costum params
    costum.form.hasStepValidations : answer step active le bon stepper
      on the answer step attrib gives 

## FEATURES

### Session params
hasOpenForm
isMultiForms

### global vars
  sectionDyf : contains all dynForm definitions for any cplx inputs
  tplCtx : contains savable datasets
  initValues : contains any dyfinputs initial data 
  answerObj : contains answer data for the current form
#### to edit costum page pieces
  var configDynForm =   <?php echo json_encode(Yii::app()->session['costum']['dynForm']); ?>;

#### information and structure of the form in this page
  var formInputs = <?php echo json_encode($form['inputs']); ?>;

## url
  [ ] repaire @xxx.co

## admin

[ ] new form editor and interface save to forms
  [-] if no Form ID found on element 
    [X] if forms exist by slug 
    [ ] if /form/idxxx then load given Form
    [X] else generate Form::generateOpenForm( $el["slug"] )
  [ ] check costum.form exists
[X] connect form to element costum.form
[-] add questions 
    new questions are added into the {{formTpl}}
    [X] add input question 
        open dynform with slug, label, placeholder , info, position
    [ ] add design options 
      [[https://getbootstrap.com/docs/4.0/components/forms/]]
    [-] edit question
        [X] open dynform for this input
        [ ] be carefull !! shared and reused forms when edited have impacts for others 
        [ ] needs a collaborative editing process 
            [ ] minimal : notify other form admins
            [ ] better : modification validation process
    [ ] add tpl question
    [X] array answers tpl builder > use dynform for each line 
[X] delete a question 
[X] build open forms dynamically
[X] add stepper
[X] order questions   
  [X] questions can be ordered by drag n drop
[ ] mode lecture if userId != user


## save to answers
    [ ] add configs inside the edit panel and remove de config params stuff
      [ ] first choose tplForm
      [ ] open Config
    [-] find a way to have cplx forms directly even without an existing answer
      [X] btn particpate creates the answer entry
      [X] open form directly 
      [ ] open and click on PARTICPATE btn
    [X] all in one block serialize form default to costum.form.saveAllAtOnce
    [X] one by one costum.form.saveOnInputBlur
    ** if form contains just one cplx input then all saves are made oneByone 
### BUG save
[ ] tags
[ ] reload answers seperataly

[-] reusible and tpls forms sections
  [X] author view pod 
  [X] add element
    [X] create new element 
    [X] limit to fixed number of elements
    [X] connect existing one
    [X] connect with the answer in an afterSave 
    [X] multiple element block in page with different types 
  [X] text [[~d/modules/costum/views/tpls/forms/text.php]] 
  [X] select [[~d/modules/costum/views/tpls/forms/select.php]]
    * TODO : dynform add key value question for list / or value list 
  [X] tags  [[~d/modules/costum/views/tpls/forms/text.php]]
    * BUG design
    * TODO : dynform add key value question for list / or value list 
  [X] calendar :oneByone: SAve [[~d/modules/costum/views/tpls/forms/cplx/calendar.php]]
  * BUG cannot have 2 calendars in the same time
  [X] multi checkbox  : multicheckbox
  [ ] finder 
    [ ] communityList 
  * link to openForm and connect to answer
  [ ] open select 
    * new options can be added to the list , 
    * other peoples answers become options, 
    * or my old options become answers
  [ ] wysiwyg
  [ ] markdown
  [ ] locality
  [ ] image
  [ ] tagList
  [ ] postalcode
  [ ] arrayForm
  [ ] array
  [ ] properties
  [ ] fichier pour des conditions personnalisé
  [ ] fichier input complexe
          [ ] text libre : sectiontitle

[ ] link + preview
[ ] lists 
[ ] searchInvite
[ ] formLocality
[X] partner [[~d/modules/costum/views/tpls/forms/cplx/partner.php]]
[X] budget previsionnel [[~d/modules/costum/views/tpls/forms/cplx/budgte.php]]
  [ ] ajouter un graph feature montrant l'evolution du budget
[X] financement [[~d/modules/costum/views/tpls/forms/cplx/financement.php]]
  costum.form.params.financement.limitRoles = "Financer"
  costum.form.params.financement.tpl = tpls.forms.equibudget
    [[~d/modules/costum/views/tpls/forms/cplx/equibudget.php]]
[X] indicators [[~d/modules/costum/views/tpls/forms/cplx/indicateurs.php]]
* BUG empty indicators
[X] stepValidation by role 
  inside a stepper wizard adds a validation process between steps
    
[X] edit answers 
[X] delete answer
[X] show answers
    [X] table
    [ ] reveal ppt
    [ ] export CSV
    [ ] export JSON
    [ ] statistics
      [ ] admin has to set à crtieria et a graph type

[ ] export answers multi format 

# USE CASES

### appel a projet
### questionnaire : google Forms
[ ] connected to CO elements
#### votre idée de costum
Description
upload document d'information

### qcm
### collaborative :
#### thinking Brainstorming 
      [ ] todo add vote feature
#### tasking , listing things todo
    bookmarking veille
      [ ] ex : list of urls classified by tags
      [ ] add link and publish to chat or journal 
        onelink > many publish process 
        add different publish processes like 
        one post push to many 
    benchmarking 
      [ ] compare collectively what things do what 
#### COCR
      who 
      said what 
#### journalisme 
#### template openForm [TONY]
      a template is defined by a json map to build it's content
      the json map corresponds to a openForm
      [X] use element.costum.cms map to define template block .editBtn
        element.costum.cms.title
        element.costum.cms.subtitle
        element.costum.cms.wizard1
        element.costum.cms.text1
      [X] show template using default data 
      [X] open Edit form
      [X] show template using element data
#### collab.slides
      uses the template open Form but with answers to allow many proposals
      create a slide html 
      each section is editble as a proposal editBtn 
      everyone proposes content, answers can be viewed in the template 
      and everyone can vot<e their favorite proposal , appart for theirs 
      [ ] using open Forms like editBtn for saving conect here 
      <a href="/costum/co/config/slug/<?php echo $slug ?>" class="btn btn-danger"><i class="fa fa-pencil"></i> here </a>
      [ ] show template using answers data
#### Excel and calculator
      [ ] add content  manuelly or copy paste 
      [ ] columns can be sum and simple excel macros ,or predefined complex macros 
###### examples
        - previsionel dynamic 
        - liste d'achat 
#### comparateur de prix 
### calcul de baricentre
    [ ] ex ask where do you live 
    [ ] ex who particpates 
      show the best place to meet 
### documenter les evenements
### votes 
  [ ] proposals with check boxes 
      [ ] calculate best result
  - faire une assemblée des communs
  - faire une carto party
  - petition https://onestla.tech/

### contract de pixel funding 
### builder 
    [ ] community
    [ ] page 
      what is it 
      who's is in it
      what do you do 
    [ ] filiere
      qui inviterais tu à la filiere 
      fait une proposition (orjet, ressouce, action) à la filiere 
    * fork 
    [ ] costum
    - instead of adding questions add sections
    - each seaction has config form
### config dashboard
### formulaire collectivité
  [[https://mail.google.com/mail/u/0/#inbox/FMfcgxwGDDrvJHWGNHKbPsNJCnGhxcpj]]
### DEAL
    [ ] wizard stepper for 4 steps 
    [ ] seperate open Forms for each steps
    [ ] ajax load each step on demand
    [ ] save to proper location answer.wizard.0.idxxx
### reecrire la constitution
  voir avec Etienne 
### choix des produits du vrac

### MA France (tps réél)
une france connecté,bien evidemment dont on ne profite pas mais qui agit interactivement, 
qui peut répondre quasi instantanément ou en asynchrone aux questions des uns et des autres 
ex : etes vous malade ?
Les questions passe par des process de publication si ca vient du citoyen, de la collectivité et peuvent meme etre rejeter par les participants
C'est un peu le retour de l'idée de la Cozette initiale mais grandeur natianal
qui peut bien sur s'appliquer par niveau geographqiue , regional, agglo, communale ...
  Ma Region 
  Ma commune 
ou par thématique , ca rejoint la vision smarterre 
  Ma santé 
  Mon océan 
  Mon Co
