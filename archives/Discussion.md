La discussion est essentielle au sein du projet, et elle doit être un maximum public et historisée !

Il existe plusieurs manières de discuter, et pour rester efficace, il convient de savoir à quel outil correspond tel niveau de discussion

# Discussion courte et spontanée sur une nouvelle activité

Pour ce genre de discussion il est mieux d'utiliser le [chat](https://chat.lescommuns.org/channel/communecter_accueil). Vous pouvez rechercher les différents channels au sein de ce dernier (ce seront tous ceux commençant par "co_") ou regarder ici : [[Liste de tous les channels]]

Vous pouvez si vous le souhaitez en créer de nouveaux, la syntaxe à suivre est : co_"nomdevotregroupe".

# Discussion longue

Dans le but d'être rapidement rejointe ou comprise par d'autres personnes, il convient de résumer régulièrement une discussion trop longue. Ce résumé peut être préfixé par le hashtag #résumé pour plus de lisibilité

# Proposition

Les propositions sont la base d'une action de groupe, mais elles doivent rester orientées action et non décision. Même s'il est rassurant de se mettre d'accord sur des points théoriques, rien ne vaut la pratique !

Toutefois, si une décision semble nécessaire, il convient qu'elle reste rapide et pas source de réunions qui peuvent décourager l'action et favoriser l'entre-soi, en réduisant par la même occasion la diversité des opinions et tendent vers un modèle coopératif où une minorité de personnes influentes vont décider pour tous en utilisant la pression de groupe !

Le DDA est une pièce maitresse pour satisfaire ces décisions rapides et constructives. Chacun peut proposer ou participer à des micro-débats à échéance, en votant par oui ou non. Une fois l'échéance passée, les oui et les non pourront se réunir pour faire chacun de leur côté les [[actions|Action]] issuent des micro-débats.

# Annonce

Dans le cas d'une annonce large et à question fermée, il convient d'utiliser la mailing liste.

Les mails doivent contenir un sujet clair et des mots clefs importants pour faciliter leur recherche ultérieure

# Réunion

Une réunion permet de fédérer l'énergie et les objectifs de chacun vis à vis du projet global afin de favoriser le travail de chacun au cours de la réunion de travail ou des actions à distance

Une réunion doit successivement présenter les sujets suivant dans l'ordre proposé

1. l'organisation : le projet n'avancera pas sans cadre et sans objectif global commun
2. les outils : se mettre d'accord sur les moyens techniques pour avancer ensemble en dehors de la réunion
3. les actions : il est important de profiter de la réunion pour mettre à jour les avancements des actions (mettre à la poubelle les actions qui n'avancent plus, pousser à terminer les actions presque terminées, etc.)
4. les sujets de la réunion

Et utiliser un scribe pour retranscrire la réunion (traces, traces, traces) !

Le modèle stigmergique poussant à l'autonomie, les sujets de la réunion sont moins importants que la prise de connaissance de l'organisation et des outils, car ils pourront toujours être travaillés et approfondis entre deux réunions, à l'aide d'une maitrise de l'organisation et des outils.

> Il est important d'avoir des individus un maximum autonomes dans un collectif peu structuré, sinon, le projet va tendre vers un noyau dur d'individus qui vont accaparer la connaissance de par leur implication disproportionnée par rapport à d'autres personnes qui se retrouveront rapidement découragées. Et au final, ce noyau va doucement s'épuiser ou rejoindre un modèle décisionnel et structuré en hiérarchie ou en coopération, pour reproduire les mêmes schémas de société actuel, qui sont déjà progressivement dépassés par l'open-source.