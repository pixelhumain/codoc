*Page parente : [[Rôles]]*

* [Liste des créateurices de lien](https://github.com/pixelhumain/co2/wiki/créateurice-de-liens#liste-des-créateurice-de-liens)
* [Liste des channels](https://github.com/pixelhumain/co2/wiki/créateurice-de-liens#liste-des-channels-utiles)
* [Missions spécifiques](https://github.com/pixelhumain/co2/wiki/créateurice-de-liens#missions-spécifiques)
* * [Recherche de nouveaux partenaires](https://github.com/pixelhumain/co2/wiki/créateurice-de-liens#Recherche-de-nouveaux-partenaires)
* * [NomMission](https://github.com/pixelhumain/co2/wiki/créateurice-de-liens#NomMission)

# Description du rôle

## WHY

L'objectif des créateurices de liens va bien au delà de la plateforme communecter. Leur but est de créer un écosystème résilient des projets libres, ouverts, connectés et décentralisés qui ont pour but d'améliorer notre démocratie et notre rapport à l'autre. Ceci pour 2 objectifs : 
* Offrir la meilleure expérience utilisateur aux personnes qui vont utiliser ces applications.
* Rendre accessible les solutions éthiques (en accord avec [[Nos valeurs]]) au plus grand nombre.

*Je n'ai pas mis de nom sur cet écosystème (qui pourrait être les pixels actifs), pour être plus inclusif envers les personnes des autres projets.*

## WHAT

Créer des outils numériques pour penser la transition vers une société : 
* respectant son environnement : planète, végétaux, animaux (les humains faisant partie des animaux)
* portée sur le locale : relocalisons notre pouvoir d'agir !
* sans rapport de domination : un pouvoir distribué à tou-te-s, respect de la différence de l'autre : "ce qui est différent me rend intelligent."
* d'acteurices : Plus de personnes qui agissent et moins de personnes qui demandent.

## HOW

* Définir une méthode de travail à tout le réseau pour pouvoir travailler tou-te-s ensemble de manière décentralisée.

# Liste des créateurices de liens
*N'hésitez pas à vous ajouter*

* Draft
* 

# Outils numériques utilisés

## Chat

* [#co_linkers](https://chat.lescommuns.org/channel/co_linkers)
* [#co_contribution](https://chat.lescommuns.org/channel/co_contribution)
* [#synergie-numerique](https://chat.lescommuns.org/channel/synergie-numerique)

## Wiki

* [[Créateurices de liens : Organisations à contacter]]
* [[Créateurices de liens : Lien avec les autres organisations]]

# Missions spécifiques

Si vous faites ce genre de choses, c'est que vous êtes un-e créateurice de lien :

## Recherche de nouveaux partenaires

### WHY

Permettre de nouveaux horizons avec de nouvelles personnes au sein de la communauté

### WHAT

Partir à la pêche aux projets open source avec qui on pourrait travailler.

### HOW

* Faire une liste des organisations à contacter puis les contacter.
* Faire un message type pour contacter les organisations.

### Ressources utilisées

* Channel [#co_linkers](https://chat.lescommuns.org/channel/co_linkers) : Channel des pixels actifs qui créent du lien

### Ressources créées ou à créer

* [[Créateurices de liens : Lien avec les autres organisations]]

## Améliorer l'expérience contributeur

### WHY

Augmenter le nombre de contributions envers Communecter

### WHAT

Facilitée la contribution des pixels humains (aussi bien les actifs que les inactifs)

### HOW

* Créer les règles sur le couple contribution/rétribution.
* Créer/Mettre à jour la documentation dans le wiki en rapport à la contribution.

### Ressources utilisées

* Le channel [#co_contribution](https://chat.lescommuns.org/channel/co_contribution)) : endroit où l'on discute/réfléchit/pose des questions à propos de la contribution envers le commun "COmmunecter"
* Un hébergeur de vidéo : En choisir un : Exemple : Youtube / Vimeo

Ressources à créer : 

* [[Comment contribuer ?]]
* [[Contribution/Rétribution]]
* [[Guide de la contribution]] : Page user friendly qui explique comment contribuer. Elle agrège toutes les connaissances en rapport à la contribution de ce wiki. Plus tard, on pourra présenter cela sous forme de livre interactif.
* Vidéo s'inspirant du [[Guide de la contribution]]
