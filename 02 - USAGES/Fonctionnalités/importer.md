# Importer
Le module d'importation de données permet de créer des dizaines d'éléments (organisation, évènement, ...) de manière automatisée. C'est un outil efficace si vous avez, par exemple, un listing d'associations que vous souhaitez référencer dans Communecter.

Pour effectuer toutes ces actions il vous faudra le statut d'Admin Public. Demandez le sur [le canal #codev_open du chat](https://chat.communecter.org/channel/codev_open).


## Résumé
1.  Télécharger le [fichier type](https://cloud.co.tools/index.php/s/SxrCtgAZ7aP7jKy) (onglets = types de données)
2.  Remplir et "Enregistrer sous" en .csv votre tableau
3.  Utilisez le [convertisseur](https://www.communecter.org/#admin.view.createfile) pour obtenir votre json
4.  Laissez vous guider


## Méthodologie détaillée
### 1 - Agréger les données
La première étape consiste à organiser les données en votre possession (par exemple la liste des associations de votre commune) pour qu'elles soient interprétables par le module d'import.

Pour cela, vous pouvez les mettre dans un tableau ou remplir un fichier json. Dans tous les cas on ne peut importer qu'un seul type d'[élément](/2 - Utiliser l'outil/Fonctionnalités/Concepts/elements.md) à la fois (organisations, citoyens, événements, ou projets).

Pour les organisations il faudra au minima le nom, le type (ex: entreprise), la ville, le code postal et le pays de chacune des organisations.

#### Tutoriel
1.  Téléchargez le [fichier type](https://cloud.co.tools/index.php/s/SxrCtgAZ7aP7jKy)
2.  Choisissez l'onglet correspondant au type de données (évènements, orga, ...)
3.  Remplissez : une ligne = un élément
4.  "Enregistrer sous" en .csv votre tableau


##### Remarques
-   Nous vous conseillons d'utiliser [LibreOffice](https://fr.libreoffice.org/)
-   Sauvegardez toujours votre fichier en .csv
-   Privilégier les points virgules et les guillemets comme séparateurs (Enregistrer sous / Éditer les paramètres du filtre)
-   Si vous avez plus de 500 entrées il est préférable de découper le fichier (ex : [commande split sur Linux](https://www.commentcamarche.net/faq/953-linux-decouper-un-fichier-en-plusieurs-parties))
-   Vérifier bien que vous n'avez pas mis de saut de ligne dans les cellules (Ctrl-H / Autres options -> Expressions régulières / Rechercher `\n` et Remplacer par un espace / Tout remplacer). Exemple dans Google Sheets :![](/Images/import-espace.png)


### 2 - Convertir
1.  Accédez à l'interface "Admin Public" de Communecter en haut à droite.
2.  Cliquez sur "Convertisseur"
3.  Choisissez le type d'_élément_ que vous souhaitez importer
4.  Choisissez la _source_
5.  `URL` : lien vers un fichier JSON disponible en ligne
6.  `FILE` : fichier CSV ou JSON présent sur votre ordinateur
7.  [OPTIONNEL] Choisissez le bon _lien_ (le mapping) _Utile si le fichier est déjà formaté convenablement avec les colonnes dans le bon ordre, correspondant aux types d'éléments présents dans le fichier type._
8.  Cliquez sur "Étape suivante"

#### Remarques
Pour les fichiers JSON, le champ "Path to Elements" indique où se trouve la données pertinentes.

Pour le code JSON suivant, la donnée pertinente se trouve dans le tableau "features". Dans ce cas de figure, il faut donc mettre "features" dans le champ "Path to Element".

```JSON
{
   "type":"FeatureCollection",
   "features":[
      {
         "type":"Feature",
         "geometry":{
            "type":"Point",
            "coordinates":[
               55.433699,
               -21.259772
            ]
         },
         "properties":{
            "name":"Damien Grondin",
            "username":"Femu",
            "img":"http:\/\/127.0.0.1\/ph\/upload\/communecter\/citoyens\/5880b24a8fe7a1a65b8b456b\/1489667898_linkpkm.jpg"
         }
      }
   ]
}

```

### 3 - Création du mapping
Si vous avez utilisé le fichier type pas besoin de lire cette partie, il suffit de sélectionner le mapping "Organisation (standard V2)".

Le mapping consiste à associer les champs de votre tableau avec les termes que communecter utilise pour définir les informations (cf. ontologie PH).
-   [OPTIONNEL si vous avez utiliser le fichier type] : associez le nom de votre colonne au terme utilisé dans communecter
-   Réalisez un `test` avec 5 éléments pour vérifier qu'il n'y a pas d'erreurs.
-   Si c'est OK, retournez au tableau et faites glisser le bouton `Test` pour lancer la conversion
-   `Sauvegardez` le json et cliquez sur `Page d'ajout des données`

#### Remarques
-   `key` permet d'identifier vos éléments ce qui permettra, si besoin, de [créer une carte](/2 - Utiliser l'outil/Fonctionnalités/network.md) interactive contenant tous vos éléments.
-   `Mettre à jour à mon mapping` est utile si vous avez un fichier type qui est propre à votre projet

### 4 - Importer les données
Le module d'import permet d'injecter vos données préalablement convertie dans communecter. Seul le format JSON est autorisé.
-   Extrayez les fichiers de l'archive `.zip` créée par le convertisseur
-   [OPTIONNEL si `SaveCities.json` contient quelque chose] Choisissez Ville et sélectionnez le fichier
-   Sélectionnez ensuite `importfileXXX.json`
-   [OPTIONNEL si vous souhaitez que les éléments créés soient affichées comme faisant partie d'une organisation] Faites glisser `Lier les entités` sur Oui et cherchez l'élément qui englobera celles que vous importez.


#### Remarques
-   Une organisation ne doit apparaître qu'une seule fois dans le fichier
-   Avant de mettre un organisme, vérifier qu'il n'existe pas sur communecter.org
-   Ne pas mettre formations/projets/ qui peuvent être regroupés dans une seule organisation.
-   Pour les téléphones
    -   On peut en ajouter autant qu'on le souhaite
    -   Format recommandé : "+33 6 00 00 00 00"*
    -   Si plusieurs numéros, mettre un seul numéro par colonne
    -   Différents types de téléphone : Mobile, Fixe et Fax
-   Pour les contacts
    -   Nom et Prénom du contact
    -   Rôle du contact au sein de l'organisme
    -   Téléphone du contact ( Si plusieurs numéros, mettre un seul numéro par colonne)
    -   Email du contact


## Annexes
### Format des adresses
Liste des codes pays (FR, EN, RE, ...) : [liste complète](https://www.iso.org/obp/ui/fr/#search/code/)

Le module d'importation réalise les étapes suivantes pour géocoder une adresse :
-   Interrogation de ces bases de données : d'abord [DataGouv](https://adresse.data.gouv.fr/), ensuite [Nominatim](https://nominatim.openstreetmap.org/), et [Google Maps](https://maps.google.fr) en dernier recours.
-   Si ces géocodeurs ne donne aucun résultat avec l'adresse complète (rue, CP, ville et pays) une nouvelle recherche est effectuée sans la rue
-   S'il n'y a toujours pas de résultat un message d'erreur apparaît


## Ontologie Pixel Humain
Dans le module Recherche de Communecter il y a des **thématiques** (ex : déchets, économie, citoyenneté, ...) regroupant tous les éléments ayant un **tag** parmi la liste de tags associé à la thématique en question. Par exemple quand on clique sur "Biodiversité" on y trouve les éléments ayant pour tag #biodiversite bien sûr, mais aussi #environnement ou #ecologie.

Cette liste est aujourd'hui définie par les développeurs.

```JSON
{
    "\_id" : ObjectId("STRING"),
    "name" : "STRING",
    "email" : "STRING",
    "type" : "STRING",
    "address" : {
        "@type": "PostalAddress",
        "postalCode" : "STRING",
        "addressLocality" : "STRING",
        "codeInsee" : "STRING",
        "streetAddress" : "STRING",
        "addressCountry" : "STRING",
    "regionName": "STRING",
    "depName": "STRING"
    },
    "addresses" : [
        "address"
        ],
    "geo" : {
        "latitude" : "FLOAT",
        "longitude" : "FLOAT",
        "@type": "GeoCoordinates",
    },
    "geoPosition": {
        "type": "Point",
        "coordinates": [ 
           "latitude -> FLOAT",
           "longitude -> FLOAT"
            ]
},
    "description" : "STRING",
    "shortDescription" : "STRING",
    "url" : "STRING",
    "image" : "STRING",
    "urlFacebook" : "STRING",
    "urlTwitter" : "STRING",
    "source" : {
        "id" : "STRING",
        "url" : "STRING",
        "key" : "STRING",
        "keys": [ 
            "STRING"
    ],
"insertOrign": "STRING",
        "update" : "DATETIME"
    },
    "tags" : [ 
        "STRING"
    ],
    "telephone" : {
        "fixe" : [ 
            "STRING"
        ],
        "mobile" : [ 
            "STRING"
        ],
        "fax" : [ 
            "STRING"
        ]
    },
    "contacts" : [ 
        {
            "name" : "STRING",
            "role" : "STRING",
            "telephone" : [ 
                "STRING"
            ],
            "email" : "STRING"
        }
    ],
     "preferences" : {
        "isOpenData" : "BOOLEAN",
        "isOpenEdition" : "BOOLEAN",
    },
    "state" : "STRING",
    "profilImageUrl" : "STRING",
    "profilThumbImageUrl" : "STRING",
    "profilMarkerImageUrl" : "STRING",
    "profilMediumImageUrl" : "STRING",
    "modified" : "ISODate",
    "updated" :"INT",
    "links" : {
        "members" : {
            },
        "events" : {
            },
        "projects" : {
            },
         "memberOf" : {
            },
            "needs" : {
            }, 
            "follows" : {
            },
    },
    "modifiedByBatch" : [ 
        {
            "NameBatch" : "ISODate"
        }
    ],
    "creator" : "STRING",
    "created" : "INT"
}

```


### Codes erreur

```JSON
    "001" => "L'entité n'a pas de nom",
    // Partie concernant l'adresse
    "100" => "L'entité n'a aucune informations l'adresse.",
    "101" => "L'entité n'a pas de code postal.",
    "102" => "L'entité n'a pas de code INSEE.",
    "103" => "L'entité n'a pas de commune.",
    "104" => "L'entité n'a pas de pays.",
    "105" => "L'entité n'a pas de nom rue.",
    "106" => "Ce code postal n'existe pas dans notre base de données.",
    "110" => "Nous n'avons pas trouver la commune : Vérifier si le code postal et le nom de la commune soient bonnes",
    "111" => "Nous n'avons pas réussi a récupérer le nom de la commune car l'INSEE et le code postal ne sont pas compatibles. Vérifier l'adresse.",
    "112" => "Nous n'avons pas réussi récupérer le code INSEE. Vérifier l'adresse.",

```
