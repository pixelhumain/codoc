# Publications

## Créer une publication
Positionnez-vous sur un élément (votre profil, une association dont vous êtes membres, ...) et cliquez dans la zone de texte.

En bas à gauche vous pouvez choisir la portée de votre publication et à droite l'auteur.

![](/Images/publication.png)
