# Les sondages dans Communecter

Un sondage permet de recueillir l'avis d'individus. Il est publié de la même manière qu'un message lambda :
-   n'importe quel citoyen, organisation, évènement ou projet peut créer un sondage
-   possibilité de définir un territoire (ex : Bretagne, ville de Montpellier, ...)
-   possibilité de définir une portée (sondage privé, seulement pour ma communauté ou public)

Il est complémentaire à l' [Espace coopératif](/2 - Utiliser l'outil/Fonctionnalités/espaceco.md) qui permet de prendre des décisions au sein de son organisation.
![](/Images/sondages.png)


## Créer un sondage
Le formulaire de création est disponible au niveau de la publication d'un message.

![](/Images/sondages-creer.png)
![](/Images/sondages-formulaires.png)


## Partager et accéder aux sondages
La liste des sondages d'un élément est visualisable via le menu.

![](/Images/sondages-liste.png)
