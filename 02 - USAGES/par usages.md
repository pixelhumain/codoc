## Présentation de CO par cas d'usage précis

------------------------------------------------------------------------

## Valorisation des acteurs d'un territoire (ex : tiers lieux co)
- Contexte et objectif

- Outils ou module utilisé
- Resultats (voir avec Beapotier )
attendus atteints | non atteints 
non attendu atteint | non atteint
- Exemples

------------------------------------------------------------------------

## Systeme d'appels à projet
### Objectifs
* Créer un formulaire d'appel à projets personnalisés
* Réaliser un appel à projets dynamiquement connectés aux acteurs du territoires
* Faciliter les échanges avec les parties prenantes (porteurs de projets, financeurs, partenaires, ...)
* Faciliter le financement de filières en fédérant les candidats autour d'une vision commune
* Proposer un réel outil support des procédures d'instruction (animation des comités de pilotage, génération de comptes rendus sur les aspects budgétaires, suivi du statut des candidatures [refus, complet, incomplet, à l'étude, signé, financement alloué, bilan reçu, ...]) 
### Principe
Permet à un financeur privé ou publics (fondations, mécènes, collectivités, services de l'état, ... ) d'établir un appel à projets et de le lancer directement à partir de sa plate-forme personnalisée (Costum). L'ensemble des champs récoltés : porteurs de projets, communauté des projets, actions finançables, budgets prévisionnels, ... sont directement intégrés et interprétables au sein de la plate-forme. Les financeurs et organismes en charge de l'instruction des dossiers pourront statuer sur l'éligibilité des projets de manière facilitée. Les actions financées pourront également être suivies, les porteurs de projet étant invités à y renseigner l'avancée de leurs projets fiancièrement, qualitativement et quantitativement selon des critères d'évaluation préalablement définis. En somme, Communecter propose un système d'appels à projets complets de la définition du cahier des charges de l'APP jusqu'à la réalisation des projets financés permettant d'évaluer les impacts réels sur un territoire donné, en cohérence et en interaction avec les acteurs en présence.

### Exemple : La [plate-forme du Contrat de Transition Ecologique](http://cte.ecologique-solidaire.gouv.fr/) (CTE) développée pour le ministère de la Transition écologique et solidaire
Les EPCI, dans un processus de labellisation CTE, invitent les acteurs (associations, entreprises, services publics, ...) à proposer les actions qui vont dans le sens des objectifs de transition écologique qu'ils se sont fixés. Toutes les actions sont renseignées par leurs porteurs à partir du formulaire de l'appel à projets. Deux niveaux d'administration (avec des rôles définis qui donnent accès ou non à l'informations en écriture ou en lecture) sont alors présents allant du local au national :
* Les EPCI qui récoltent les projets d'acteurs locaux  mettent en lien, rendent cohérents les projets entre eux, instruisent et suivent les actions. 
* Le ministère et financeurs/partenaires nationaux et  régionaux (ADEME, DEAL, Banque des territoires, ...) évaluent et trouvent des solutions de financements et d'accomapgnement pour la réalisation des projets locaux.

Un observatoire national de l'impact des CTE selon des critères écologiques définis est alors établi et ne cessent d'être alimenté par le suivi des projets à travers la plate-forme. De réels outils de valorisation des résultats et d'essaimage de la démarche sont ainsi mobilisables et adaptables à un contexte donné.

------------------------------------------------------------------------

## Observatoire de secteur

### Objectifs
* Evaluer et valoriser les dynamiques sectorielles
* Définir des critères d'évaluation représentatifs et en visualiser les résulats
* Faciliter la lecture d'une analyse sectorielle
* Mesurer l'impact d'un dispositif incitatif (animation de réseau, financement, réglementation, ...)
### Principe
Nous pouvons valoriser l'ensemble de l'activité d'une thématique , d'une compétences de collectivité territoiriales, d'une filière économique, à différentes échelles géographiques, et en extraire des grilles d'analyses pertinentes pour en mesurer les externalités positives ou négatives. A partir de la connaissance et de la caractérisation  des acteurs, de leurs activités, de leurs interactions et de leurs impacts sur leur territoire, nous pouvons réaliser un véritable observatoire qui tient compte des évolutions et dynamiques relatives à des thématiques variées (transition écologique, mouvements citoyens et démocratiques, politiques publiques, ...) en temps réel. Nous nous appuyons sur notre méthodologie pour élaborer et modéliser avec les partenaires l'ensemble des projets qui font partie du secteur ciblé, les interactions entre acteurs, leurs actions, les parties prenantes en présence pour mettre en exergue les effets sur leur territoire.
### Exemple
[L'observatoire national des  CTE](https://cte.ecologique-solidaire.gouv.fr/#dashboard) propose une grille d'analyse quantitive (nombre de CTE signés, d'actions mises place, montant des financements publics et privées, quantité de ressouces naturelles préservées, ...) et qualitative (la chronologie du programme, les domaines d'actions, les objectifs visés et atteintes, les acteurs en présence et leurs rôles, ...) qui permet de mesurer l'impact réel du programme du ministère de la Transition écologique et solidaire.

------------------------------------------------------------------------

## Action Citoyenne
[TOM] Lien avec [communecter ma fédé](https://doc.co.tools/books/2---utiliser-loutil/page/communecter-ma-f%C3%A9d%C3%A9ration) et [communecter mon asso](https://doc.co.tools/books/2---utiliser-loutil/page/communecter-mon-association) ?

### Objectifs
Nous permettons aux membres d'une communauté (et aux citoyens curieux) d'avoir une vision d'ensemble de ce qu'il se passe dans un réseau : où sont les membres ? que proposent-ils sur leurs territoires respectifs ? agenda commun ? ...

L'objectif est également de développer l'entraide entre les membres d'une même communauté : discuter via des messageries communes, interagir dans le fil d'actualité, s'échanger du matériel et des compétences, ...

Enfin l'intérêt d'avoir une plateforme dédiée à sa communauté est de pouvoir communiquer à l'extérieur, et de transmettre des informations à ces membres.


### Principes
Le COstum "Action Citoyenne" référence tous les membres de la communauté. Ensuite libre à chacun de ces membres de créer des publications, partager des évènements, déclarer ces compétences, ...

Le visiteur lambda pourra ainsi accéder à ces informations : 
- La carte et le moteur de recherche permettent de retrouver les membres.
- L'agenda partagé affiche les évènements du réseau.
- L'agora permet de voir les prises de décision.
- Le gestionnaire de fichiers et d'URL met en partage les documents importants.
- Le fil d'actualité affiche les publications des membres.

_Bien sûr le COstum peut ne pas afficher certaines de ces fonctionnalités._


### Exemple : [Pacte pour la Transition](https://www.pacte-transition.org/)
Le Collectif pour une Transition Citoyenne réunis 27 mouvements partageant la même vision d’une transition écologique, sociale et humaine. Pour les municipales ils ont créé le Pacte pour la Transition (32 mesures concrètes pour des communes plus écologiques et plus justes). Le COstum https://www.pacte-transition.org affiche : 
- une carte des groupes locaux que l'on peut rejoindre
- la liste des mesures choisie via l'agora
- un espace "Ressources"

------------------------------------------------------------------------

## Gestion de communauté valorisation de ressource
[FLO]

### Objectifs
* Faciliter les échanges (marchands, non-marchands, bourses d'emploi, ...) au sein d'une communauté
* Satisfaire une demande en l'associant à une offre
* Modéliser une place de marché et les "monnaies" d'échanges

### Principe
A travers l'offre COstum, nous sommes à même de proposer un outil d'animation de réseaux et communautés dont l'activité principale est d'animer et faciliter les échanges entre ses membres. Nous modélisons des "places de marché" (modèle offres/demandes) selon les spécificités de chaque communauté d'échanges. Nous facilitons la mutualisation de ressources non-marchandes ou la vente/location de biens et services qui peuvent reposer ou non sur des devises diverses (devises bancaires, monnaies locales, monnaies libres, cryptomonnaie). En nous basant sur notre module "annonces", nous tâchons de faciliter les interactions pour faire correspondre au mieux une proposition (offre) et un besoin (demande). 
Nous associons bien souvent, en réponse aux besoins de nos partenaires, la place de marché avec d'autres modules annexes tels que l'agenda mutualisé pour connaître les activités en lien avec la communauté d'échanges, l'annuaire des membres de cette communauté, ...

### Exemple
[Co-mains](http://co-mains.csconnectes.eu/#welcome) est réseau de mutualisation de ressources non-marchandes entre centre sociaux du Sud de la ville de Lille. Il a pour but de donner la possibilité aux acteurs du quartier d’échanger plus facilement du matériel ou d’autres ressources non marchandes. Il permet aux acteurs associatifs de proximité d’avoir une alternative au recours systématique à des prestataires dans l'organisation d'événements ou leurs activités quotidiennes.

------------------------------------------------------------------------

## Site et reseau social personnalisé (COstum)
[TODO] À relier avec la présentation dans [la doc](https://doc.co.tools/books/2---utiliser-loutil/page/costum)

### Objectifs
COstum vous permet de créer un site web collaboratif clé en main.

### Principes
Vous bénéficiez de toutes les nouveautés développées pour COmmunecter.org, tout en ayant la main sur de nombreux paramètres permettant de personnaliser votre plateforme :
- thème du réseau (biodiversité, social, déchets, emploi, ...)
- territoire (ville·s, département, région ou pays)
- fonctionnalités proposées (agenda, moteur de recherche, messagerie, ...) -> - liste complète
- charte graphique (logo, couleurs, ...)
- lien personnalisé

Le but n'est pas de créer un "silo" mais, au contraire, d'avoir un site web interconnecté et contributif : 
- Vous pouvez afficher tout ou partie des évènements, projets et organisations présents sur communecter.org sur votre plateforme.
- Le référencement effectué sur votre site améliore automatiquement le contenu disponible sur communecter.org

### Exemple
Les possibilités sont nombreuses. Voici des exemples de réseaux qu’il est possible de créer :
-   Réseau social local et thématique : **[Emploi à la Ville du Port](https://www.communecter.org/custom?el=city.97407)**, **[Numérique à La Réunion](https://www.communecter.org/costum/co/index/id/coeurNumerique), [Haute Vallée de l'Aude](https://www.portailhva.org/)**
-   Plateforme de consultation citoyenne : **[Pacte pour la Transition](https://www.communecter.org/custom?el=p.pactePourLaTransition)**
-   Moteur de recherche et cartographie de filière : **[Tiers-Lieux du Nord](https://www.communecter.org/costum/co/index/id/laCompagnieDesTierslieux), [Meuse Campagnes](https://www.communecter.org/costum/co/index/id/meuseCampagne)**
-   Plateforme d'appel à projets et de valorisation des acteurs pour un dispositif déployable dans plusieurs territoires : **[Contrat de Transition Écologique](https://cte.ecologique-solidaire.gouv.fr/)**
-   Liste pour les municipales : **[Mayenne Demain](https://www.mayenne-demain.fr/)**, [**Tepoz en Comùn**](https://www.communecter.org/costum/co/index/id/tepozencomun)
-   Jeu de contribution **: [Chtitube](https://www.chtitube.fr/)**
-   Tiers-lieu : **[Raffinerie](https://www.communecter.org/costum/co/index/id/laRaffinerie3)**
-   Journal : **[Insoumis de Chambéry](https://www.journal-insoumis-chambery.com)**

------------------------------------------------------------------------

## Recolte de connaissance (COForms formulaire personnalisé)
### Objectifs
* Mener une étude des acteurs d'un secteur, d'une filière, d'un territoire à travers un formulaire dynamique
* Relier un formulaire avec votre plate-forme personnalisée qui modélisent les dynamiques sociales et sociétales de votre commaunuté
* Gagner du temps sur le traitement des données issues d'un formulair et leurs mises à jour
* Disposer d'une base de connaissance actualisée interactive et évolutive
* Donner la possibilité de valoriser cette connaissance à travers des outils d'analyse en temps réel (obsevatoire)
* Décloisonner la connaissance et la rendre accessible à d'autres systèmes d'informations


### Principes
Nous proposons un module de formulaire totalement personnalisé qui est directement liés aux acteurs et à leurs activités au sein de votre COstum : chaque champ renseigné de votre formulaire peut être interprété. Vous pouvez ainsi récolter ou mettre à jour dynamiquement l'ensemble des informations relatives aux membres de votre communauté (changement d'adresse/horaires, évolution de l'offre de services, ...), faire remonter le bilan (quantitatif, qualitatif, financier) d'une action pour évaluer son impact ([observatoire](#observatoire)).

### Exemple

------------------------------------------------------------------------

## Gestion du temps bénévolat et Banque de temps (OCECO)
### Objectifs
### Principes
### Exemple


# Evolution docs des fonctionnalités
À faire : 
- Liens avec les costums (ex de cas d'usage)
- Modèle de documentation
- MÀJ et compléter


 FLO :
 MOTEUR DE RECHERCHE : retrouver les acteurs d'un territoire
   ACTUALITÉ : calendrier des actions locales
   ÉVÈNEMENTS : actualité localisée
   ANNONCES : vendez des biens et des services, proposez et recherchez des ressources humaines ou matérielles
   AGORA : votes et consultations citoyennes
   
 TOM  :  
    [ESPACE COOPÉRATIF](https://doc.co.tools/books/2---utiliser-loutil/page/espace-collaboratif) : outil de gouvernance pour les projets et les organisations
   [MESSAGERIE](https://doc.co.tools/books/2---utiliser-loutil/page/chat-de-discussions) : outil de discussions instantanées
   [GESTIONNAIRE DE FICHIERS ET D'URL](https://doc.co.tools/books/2---utiliser-loutil/page/galerie) : agréger de la connaissance et partage de listes de liens


- Cas d'usage
- Cible
- Carnet de développement

------------------------------------------------------------------------

## Récolte de connaissance et Observatoire Dynamique
### création de formulaire personnalisé
### création d'observatoire personnalisé

------------------------------------------------------------------------

## Gestion de réseau de Tiers Lieux
### La Réunion des Tiers Lieux
https://www.communecter.org/costum/co/index/slug/LaReunionDesTiersLieux#

### RFFLabs
https://www.communecter.org/costum/co/index/slug/rfflabs#observatory

### La Rosée
https://www.communecter.org/costum/co/index/slug/LaRosee
### La compagnie des Tiers Lieux
### RELIEF : réseau AURA des Tiers Lieux

------------------------------------------------------------------------

## Actions Citoyennes organisés
### Pacte de transition
https://www.pacte-transition.org/

------------------------------------------------------------------------

## Crowdfunding
### adopte un commun
https://adopteuncommun.communecter.org