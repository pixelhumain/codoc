# Communecter sa fédération

## Présentation
Communecter est un outil très efficace pour les communautés structurées en groupes locaux. Ce réseau sociétal est utile à toutes les échelles de votre réseau :

-   Au sein d'un groupe local Communecter permet de **s'organiser** grâce à ces différents outils de gouvernance ouverte.
-   Communecter rend plus efficace les **échanges entre les groupes locaux**. Grâce à l'actualité fédérée, vos membres et sympathisants restent informées de ce qui se passe au sein du réseau.
-   Globalement, vous positionnez votre fédération comme un acteur de la thématique pour laquelle elle est engagée.

Communecter permet de retrouver sa communauté locale pour s'organiser, discuter et partager l'information _en interne_, tout en ayant un regard sur ce qui se fait ailleurs. C'est un outil [glocal](https://fr.wiktionary.org/wiki/glocal).

Certaines Assemblées des communs et Collectifs locaux [Zéro Déchets](/1 - Le Projet/Nos amis/zerodechet.md) utilisent déjà Communecter. Cette documentation fait état de ce que les [pixels humains](https://communecter.org/#@pixelhumain.view.directory.dir.members) (collectif portant ce projet) ont constatés au sein de réseaux auxquels ils et elles participent.

Nous serions ravi d'échanger avec vous de votre expérience vis-à-vis des outils que vous utilisez actuellement et de votre expérience avec Communecter : [contact@communecter.org](mailto:contact@communecter.org)

## Comment ?
Quelques exemples concrets :

-   [Identifier rapidement qui sont les autres acteurs locaux](/2 - Utiliser l'outil/Fonctionnalités/recherche.md)
-   Mutualiser de l'information avec d'autres collectifs (par ex. Alternatiba Strasbourg y a référencé tous les [lieux qui acceptent des affiches](https://communecter.org/#search?text=#AccepteAffiche) venant d'associations grâce au tag #AccepteAffiche)
-   [Partager une liste de liens](/2 - Utiliser l'outil/Fonctionnalités/galerie.md)
-   Échanger du matériel avec d'autres collectifs locaux grâce au [module d'entraide](https://communecter.org/#annonces?searchSType=ressources)
-   [Organiser un vote](/2 - Utiliser l'outil/Fonctionnalités/espaceco.md)
-   [Ouvrir un canal de discussions instantanée](/2 - Utiliser l'outil/Fonctionnalités/chat.md)
-   Visualiser sa communauté
-   Afficher la programmation de son festival

[Liste complète des fonctionnalités](/2 - Utiliser l'outil/Fonctionnalités/liste.md)

## Comment sont gérées les améliorations ?
S'il y a un besoin récurrent dans plusieurs communautés et que la fonctionnalité manquante correspondant à l'esprit du projet nous sommes prêt à la développer. Nous n'en avons pas toujours les moyens, mais il arrive régulièrement qu'il y ai suffisamment de personnes prêtes à participer à une petite partie du financement pour que ça puisse se faire. Concrètement, si vos groupes locaux ont des besoins similaire nous pouvons co-organiser une collecte au sein de votre communauté pour réunir la somme nécessaire au développement d'une fonctionnalité.

[En savoir + sur notre modèle économique](/1 - Le Projet/modeleeconomique.md)

## Par où commencer ?
-   Je suis membre d'un groupe local : [lancer une dynamique locale](/5 - Déployer/Lancer une dynamique locale)
-   Je suis un coordinateur national : [lancer un costum](/6 - COstum/costum.md)
