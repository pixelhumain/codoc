# Calendrier d'action locale

Cette technique d'animation s'inspire du travail des crieurs publics. Elle a deux objectifs :
-   Regrouper les événements et inciter les associations à communiquer ensemble
-   Faire connaître les événements aux personnes qui s’intéressent depuis peu à la cause

## Facilitation
-   une personne qui guide sur le calendrier, invite les gens à garder la date
-   une personne qui alpague les gens et leur demande quels types d’activités peut les tenter, les freins qu’il peut y avoir à agir => toucher des gens qui ne sont pas dans la boucle

## Version numérique
-   ajouter les événements sur une plateforme d’événements ([strasbourg.curieux.net](http://strasbourg.curieux.net), [communecter.org](http://communecter.org))
