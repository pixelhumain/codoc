# Communecter à Strasbourg

Le groupe local (porté initialement par Jibé et Tom) s'est structuré autour d'une démarche nommée [CoStrasbourg](https://libreto.sans-nuage.fr/costrasbourg). Au fur et à mesure de notre avancée nous avons définis plusieurs objectifs :
-   Développer l'entraide entre nos différents collectifs dans la perspective de les faire changer d'échelle et soutenir toutes les démarches allant dans ce sens.
-   Sensibiliser à l'intérêt du réseau, de l'entraide et de la mutualisation
-   Porter des évènements pour se rencontrer, s'organiser, s'auto-former et développer la contribution à nos projets respectifs.
-   Encourager la réutilisation de données ouvertes et mutualisées.
-   Produire des ressources favorisant la création de projets citoyens.

## Historique
### Origine
Je (Tom) suis arrivé à Strasbourg en juin 2018. Je suis d'abord aller à la rencontré des collectifs proches des thématiques de COmmunecter. J'ai participé à l'organisation de deux évènements qui m'ont permis de découvrir une grande diversité d'acteurs locaux : les [Rencontres Mondiales du Logiciel Libre](https://2018.rmll.info/) et l'[accueil du Tour Alternatiba](https://mailchi.mp/43b2b9baba0e/le-10-aot-ntait-quun-dbut).

Je suis rapidement tombé sur Jibé qui a la même envie que moi : travailler sur la coopération inter-associative. C'est avec lui que j'ai lancé CoStrasbourg.

Nos contacts respectifs et notre connaissance du réseau associatif local nous permet d'avoir une capacité à mobiliser un nombre non-négligeable de membres actifs (coordinateurs, salariés, ...).


### Stratégie
Pour développer une culture de la contribution et du partage à Strasbourg nous faisons vivre de petites expériences concrète : agréger des évènements dans un groupe facebook, lister les salles de réunions, référencer des initiatives locales, ...

Ces petites choses sont des "produits d'appels" pour emmener les personnes impliquées vers ce qui nous préoccupe fondamentalement : sortir des GAFAM, avoir un espace commun décloisonné, la transparence, ... Le fait de vivre une expérience de mutualisation permet également de prendre conscience de la puissance de cette pratique.


### Évènements
-   Présentation aux RMLL
-   Présentation de la démarche à différents collectifs
-   Les assos invitent les assos
-   Ateliers de contribution
-   Mutualisons

### [Projets](https://libreto.sans-nuage.fr/costrasbourg)
