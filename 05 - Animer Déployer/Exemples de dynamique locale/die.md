# Communecter à Die

## Contexte
**Die** est une commune de 4500 habitants du département de la [Drôme](https://fr.wikipedia.org/wiki/Dr%C3%B4me_(d%C3%A9partement) "Drôme (département)") en région [Auvergne-Rhône-Alpes](https://fr.wikipedia.org/wiki/Auvergne-Rh%C3%B4ne-Alpes "Auvergne-Rhône-Alpes").

![](/Images/die-carte.png)

Yoann est ambassadeur local dans cette commune. Il a conscience que la mise en réseau des acteurs locaux a un fort potentiel à Die, notamment parce qu'il y a une densité de collectifs bien supérieur à d'autres communes de cette taille.

## Usages de COmmunecter
[Yoann](https://communecter.org/#page.type.citoyens.id.574ea82f40bb4e5a2b2762ea) a testé beaucoup d'approches pour développer l'usage de COmmunecter à Die :
- Rencontre avec des associations locales
- Présentation
- Formation

Malgré ces efforts l'usage de CO n'augmente pas :
- [Ensemble ici](http://www.ensembleici.fr/) (proto-COmmunecter local) a obtenu un financement pour développer leur réseau et ne veut pas changer de cap.
- La fracture numérique
- Les libertés numérique mobilisent peu
- Sous-estimation des effets positifs de la mise en réseau
- Le manque de temps (et de moyens) pour aller plus loin dans son rôle d'ambassadeur

Des pistes pour tenter de débloquer la situation :
- Constituer un dossier sur la base de l'[offre commune](https://docs.google.com/document/d/1sObvNaL_xnYeRwzoARfRMhUw0AUzOXTl3rx2PGsWpgA/edit?usp=sharing) pour se rapprocher des collectivités
- Formaliser le statut d'ambassadeur avec éventuellement une rétribution à la clé (ce n'est pas son idée mais ça me paraît logique, au moins en Ğ1)
