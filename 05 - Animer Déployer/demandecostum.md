# Demander la réalisation d'un COstum

Vous souhaitez créer un nouveau design [COstum](/6 - COstum/costum.md) à votre image mais vous n’avez pas la notion du développement ? Vous pouvez alors demandez à notre équipe de le réaliser pour vous.
Pour cela vous devrez créer votre la fiche élément de votre futur COstum (cf Choisir son COstum générique et le configurer > Crée un élément).
Cliquez sur le bouton “Créer la fiche projet COstum”.

![](/Images/costum-idee.png)

_Le bouton n’était pas encore mise à jour au moment du screenshot_
-   Nom de votre Costum : Le nom de votre future COstum, si votre COstum aura le même que votre fiche élément, vous pouvez mettre le même nom
-   Descrivez votre Costum : Détaillez-nous votre future COstum en étant le plus clair possible
-   Document associé : Si vous avez des documents à nous transmettre (logo, bannière, photo, document technique…) vous pouvez le transmettre ici, vos documents doivent faire au maximum 5 mo, format de vos fichiers `pdf,xls,xlsx,doc,docx,ppt,pptx,odt,ods,odp,csv,png,jpg,jpeg,gif,eps`
-   Ajouter quelques mots clés (tags) : Si vous souhaitez mettre des tags à votre future COstum

Vous pouvez modifier à tout moment vos informations ou bien supprimer si vous souhaitez recommencer.

![](/Images/costum-fiche.png)

Vous pouvez toujours tester nos COstums génériques.

Une fois que vous êtes sûre de votre proposition de COstum, veuillez nous contacter en cliquant sur le lien “Contactez-nous”, celle-ci va vous rediriger vers la page du tchat [#custom](https://chat.communecter.org/channel/custom), il vous restera plus qu’a nous envoyé le lien et un membre de nos équipes prendra le temps de vous répondre et de proposer un devis rapidement.
