# Lancer une communauté locale

## Un terrain propice ?
L'appropriation d'un outil numérique n'est pas seulement lié aux compétences technique de vos voisins. Il existe plusieurs indices vous permettant de connaître la difficulté de la tâche dans laquelle vous vous lancez :

La facilité des acteurs d'un territoire à s'approprier une innovation telle que COmmunecter dépends énormément de leurs **volontés de faire réseau**. Une simple envie de de gagner en efficacité est déjà un très bon début. Si les associations locales essaient d'avoir le monopole de leur thématique, ce n'est pas bon signe.

Le deuxième point est la capacité des acteurs locaux (citoyens compris) à **se fédérer localement** autour d'objectifs commun. Ça se matérialise souvent par un réseau associatif capable de mettre sur pied de nombreux projets.

Dans le cas où une plateforme d'information et/ou de démocratie locale existe déjà, nous vous invitons à rentrer rapidement en contact avec les porteurs de ce projet. Il ne s'agit pas d'y voir une forme de concurrence mais une opportunité, par exemple, de partager nos données. Malheureusement ce genre d'outils sont souvent propriétaires et développés dans le cadre d'appels à projets, ce qui complique la mutualisation...

Enfin, **la grandeur de votre réseau** et votre influence sur celui-ci va bien sûr jouer un rôle.

COmmunecter est porté par des ambassadeur·ice·s avec des profils très différent :

- Autonome : citoyen sensibilisé
- Communauté : vous participez à un ou plusieurs collectifs (bénévolat, ...)
- Décideur : vous avez des responsabilités (élu, ...)


## Présenter Communecter
Si vous souhaitez des supports de communication (carte de visite, plaquettes, goodies, ...), n'hésitez pas à en faire part sur le chat des ambassadeurs.
- Clarifier son rôle d'ambassadeur
- Adapter son discours
    - Modularisation le permet
    - Identifiez les besoins
        - À faire remonter aux devs (peut potentiellement aboutir sur un devis)
- Rassurer sur les peurs
    - Édition libre
    - Modèle économique
- Montrer des exemples
- Faites en direct
    - Inscrire les gens (s'ils sont d'accord)

## Participer à des évènements thématiques  
Exemples d'évènements : Alternatiba (Alternatives),
C'est une façon très efficace de faire découvrir COmmunecter :
- On y rencontre un nombre important de personnes qui :
    - n'auraient pas forcément fait le déplacement pour discuter "Réseau social libre"
    - s'intéressent à la thématique de l'évènement. Elles voient donc l'intérêt d'avoir un outil pour retrouver et discuter facilement avec les organisations présentes proches de chez elles.
- Les acteurs présents sont souvent frustrés de ne pas pouvoir discuter entre eux. COmmunecter réponds à ce besoin de mise en réseau des filières locales.

Quelques conseils :
- Référencez l'évènement, puis prenez contact avec les organisateur·ice·s pour les inviter à ajouter eux-mêmes le détail du programme tout en leur présentant le projet.
- Recherchez les citoyens déjà inscrit prochent de l'évènement et invitez les. Demandez un coup de mains aux autres ambassadeur·ice·s de la région.
- Proposez autre chose qu'un simple stand (démonstration, cartopartie, ...)

## Initier des évènements
- Objectif : donner du sens à nos actions
- rencontres Thématiques (Filière, Open Gov, Emploi)
- Cf. cartopartie
