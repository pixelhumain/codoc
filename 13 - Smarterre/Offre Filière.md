# Offre “FILIÈRE” : la mise en valeur des atouts territoriaux


#connaissance, #intelligence collective, #collaboration, #inclusion, #co-construire, #innovation, #expérimentation, #évaluation, #plateforme numérique, #réseau sociétal,  #filières,  #participation citoyenne, #animation #développement durable



# PRÉSENTATION DE L’OFFRE “FILIÈRE”

Crises sanitaires avec la pandémie du COVID 19, crises financières et économiques telles celles des Subprimes en 2007-2008, crises sociales avec les Gilets Jaunes et enfin urgence climatique, ou la simple amélioration naturelle de la société, ce sont tant de défis à relever pour nos sociétés. 
Dans un monde globalisé, un contexte économique hautement concurrentiel, compétitif et de crises sanitaires, l’importance de développer et de mettre en place des stratégies de résilience comme **la mise en valeur des atouts territoriaux et de ses ressources** permet de répondre dans la durée et de façon cohérente aux enjeux du triptyque du développement durable – environnement, société et économie – tout en gardant un caractère adaptable et inclusif. 

Il appartient aux territoires d'engager une réflexion sur la conception même des territoires en matière d’organisation du travail, de mobilité et de consommation – production, distribution, commerce -, de la gestion de crises mais aussi et surtout aux stratégies de résilience.

L’offre “Filière”, développée par OPEN-ATLAS, est une plateforme numérique collaborative et open source pour faciliter la co-construction et l’animation d’une filière à l’échelle locale autour d’une thématique donnée. Il s'agit d'augmenter les compétences territoriales et de tous les habitants ou bénéficiaires. 
.
Le dispositif “Filière” propose une approche sociétale innovante, basée sur l’humain​, l’inclusivité et les flux propres au territoire où le fruit de la collaboration des acteurs, leur intelligence collective et les différentes initiatives entreprises sont mises en valeur pour tendre vers la structuration d’une filière dynamique, pérenne et autonome.
S’appuyer sur les savoirs-faire locaux permet de se différencier et d’exceller dans les activités promues. Soutenir ces particularités localement est important pour les filières afin qu’elles puissent se développer et continuer à produire et proposer localement leurs activités et services. 


Faire Filère c’est rassembler tous les acteurs d’une thématique donnée sur un territoire défini, autour d’une plateforme numérique et d’une synergie commune afin de structurer et développer cette filière pour créer de l’emploi, favoriser des retombées économiques locales mais aussi faire face à des enjeux et crises à venir, qu’elles soient sanitaires, sociales, économiques mais également environnementales. 

Le dispositif “Filière” comprend : 
une plateforme numérique centralisée de la filière avec toutes les fonctionnalités pour la structurer, créer des liens et la développer pour en faire un réel instrument de promotion des atouts territoriaux. 
une méthodologie “COEUR” ouverte et souple à l’écoute du territoire et accessible à tous pour l’animation de communautés, thématiques locales, et tendre vers l’autonomie de la filière avec un écosystème complet rassemblant les institutions publiques, acteurs de la filière, organismes de formation, financeurs,  têtes de réseau et grand public.
Un comité de pilotage qui assure l’animation de la filière. 



une présentation 
https://docs.google.com/presentation/d/144-pyUfuNEbLxlX7SIkkPqAqUYjQWKC4mvVY_HQ_MvA/edit#slide=id.g38418d7f5a_0_378


https://docs.google.com/presentation/d/e/2PACX-1vS8uPgkFxze9z9x4vDMvcBjYu-i9NrVP6L1RurG-5mcPoIJW6jPmNatMOGbqNkmdELuFRRs11oDP0_c/pub?start=false&loop=false&delayms=3000&slide=id.g5e5279c140_0_0

https://docs.google.com/presentation/d/1eyq2KHWZhPTmTvBajskaeFs4-jm10hBlAePEgWn1ux4/edit#slide=id.g5e5279c140_4_81

https://docs.google.com/presentation/d/1DidolE2nREJIdezfhF3o-3NIF6lWz6jDdDfYpGwzZPk/edit#slide=id.g809938a175_1_65

https://docs.google.com/presentation/d/1NZr1DMFeS3I-JYuXrGvecijvgWW4zLaVvUyaevK-jHM/edit#slide=id.g7e37893dea_0_0



Cette plateforme permet de remettre continuellement en question les normes d’utilisation du digital afin de mieux satisfaire les usagers grâce à l’amélioration continue d’une collection d’outils et de processus facilitant les dynamiques du référencement, de la connaissance, des besoins et du partage. Certaines des fonctionnalités innovantes de la plateforme ont fait l’objet d’une incubation à la Technopole de la Réunion, permettant de mobiliser la collaboration des acteurs des territoires.

“Filière” accompagne les acteurs d’une filière dans son développement : la diffusion, la réplication et la massification. Elle s’appuie sur différents outils  Open Source et plateformes numériques pour créer du lien, accélérer les processus, être plus EFFICACES ET PERFORMANTS. 

La plateforme numérique qui véhicule des valeurs partagées par tous les acteurs va servir de socle commun et centralisé pour partir à la connaissance des atouts de son territoire, de ses acteurs, de ses compétences, de ses savoir-faire, de ses savoir-être… où l’information sera crowdsourcée (alimentée, vérifiée et validée) par les acteurs du territoire puis  structurée, géolocalisée, partagée et diffusée au plus grand nombre pour devenir un écosystème sociétal multiplicateur de valeurs.  

Une fois le système d’information mis en place, la dynamique de la filière peut s’enclencher autour des outils disponibles sur la plateforme. 
Module de recherche territorial pour rechercher simplement et rapidement toute information relative à la filière. 
Structuration et animation avec mise en réseau des acteurs autour d’une même thématique ou filière avec les projets, les initiatives, les événements,  les espaces collaboratifs de discussions, d’échanges, d’interactions, de partage de ressources…
Mise en œuvre d’intelligence collective pour répondre aux enjeux de la filière,  résoudre des problématiques projets, pour valider des méthodes….
Interconnecter des acteurs entre eux, de territoires et domaines différents…
Répliquer sur son territoire les projets, événements, initiatives…qui fonctionnent sur d’autres territoires. 


“Filière” va ainsi permettre de définir une proposition de valeur partagée au sein de la filière, de créer une base de connaissance dynamique et interactive, de structurer/organiser la remontée d’informations et de connaissance pour s’adapter et capitaliser via l’observatoire, évaluer les besoins des acteurs de la filière, permettre la mutualisation des ressources, valoriser l'activité et la dynamique du territoire, contribuer à la réussite de l’écosystème et proposer des outils et des méthodologies pour tendre vers une filière dynamique, pérenne et autonome. 


MÉTHODOLOGIE “COEUR”
Afin de maximiser les chances de rendre une filière autonome et durable sur un territoire, nous proposons d’utiliser une méthode appelée “COEUR”  basée sur des valeurs de co-construction, de commun et de collaboration qui permettent au plus grand nombre de participer et de s’approprier ce commun (la filière). Une autre méthodologie peut, bien sûr, être choisie pour lancer une filière. 

Donner du sens à l’action de “Faire Filière” constitue la base de la démarche pour une meilleure appropriation  : 
Le territoire doit prendre en compte ses spécificités et l’interprétation de chacun. 
Définir des valeurs communes au sein de la filière. En ce sens, dans notre approche et les outils proposés nous mettons l’accent sur les notions suivantes : 
Considérer la filière à structurer et développer comme un “Commun” : des ressources, gérées collectivement, entre communautés, qui établissent des règles et une gouvernance dans le but de préserver et pérenniser ces ressources.
Une gouvernance la plus ouverte et horizontale possible afin de garantir une participation accrue et la prise en compte de l’expression de tous. 
Intelligence collective ou méthodologie Ariane 
Se baser sur une plateforme Open-Source contribue à l’éthique de la démarche 

Une approche pragmatique et agile permettant à tous de collaborer et de suivre l’évolution de la filière. 


Pragmatique car 
“Colle” aux questions et problématiques du terrain
L’animation est co-construite avec les acteurs
Le collectif est guidé pour trouver par lui-même ses propres solutions


Agile car
Le processus est régulièrement évalué pour s’ajuster et se transformer
Les informations issues du terrain sont prises en compte pour la construction de l'efficience et de la cohérence des actions et des projets
L’animation est appuyée sur un pilotage en mode “évaluation/régulation” qui permet de se requestionner et d’ajuster son action au fil de l’eau

Une filière se co-construit dans le temps,  pas à pas, en se basant sur un noyau solide d’acteurs actifs dès le départ. Le groupe d’acteurs s’agrandit avec un rezotage à rebonds pour mailler l’écosystème. 





COMITÉ DE PILOTAGE 

Un comité de pilotage collaboratif, à l’image de la méthode choisie par le territoire, qui accompagne le lancement et l’animation de la filière. 


Les différents axes de travail et actions du comité de pilotage : 
définir un état des lieux initial de la filière : 
analyse de la filière et comparaison avec les autres territoires
mise en place des indicateurs de la filière au sein de la plateforme 

Composer le comité de pilotage et d’animation : 
Identifier un noyau dur sélectionné selon des critères de motivation et de capacité proactive. 
des représentants de chaque catégorie de bénéficiaires pour garantir une neutralité et transparence

Définir et partager la stratégie et méthodologie de mise en oeuvre de la filière 
Un comité à l’écoute de la filière qui, au travers de la plateforme  : 


Administre la plateforme
Accompagne
Anime
Alimente les informations relatives à la filière (événements, mur d’actualité, veille technologique…) 

Structure, au besoin, les données de la plateforme pour établir une base de connaissance dynamique de la filière : gestion de la sémantique de mots clés.

Restitue toutes les informations relatives à la filière sur la plateforme centralisée.   
Accompagne, au besoin, la cartographie des acteurs de la filière ainsi que leurs offres de services, compétences et besoins… Chaque utilisateur bénéficie d'un espace personnalisé pour détailler ses activités, ses besoins etc.. (textes, images, vidéos..). L’import des informations peut se faire en masse, crowdsourcées par les utilisateurs, par l’organisation d'événements  de cartoparties (cartographie participative) et au besoin avec un accompagnement du comité de pilotage. 

Met en avant les aides publiques et financières à disposition de la filière.


Anime et dynamise la filière, au travers de la plateforme, d’événements et d’ateliers collaboratifs d’intelligence collective, pour créer du lien, faire émerger les besoins, trouver des solutions aux problématiques remontées

Propose des actions pour le développement et les moyens d’attraction de la filière

Favorise un développement de la filière (emplois et retombées économiques)
Evaluation des données de l’observatoire et réorienter les stratégies







ou 





UTILISATEURS ET BÉNÉFICIAIRES 

    Les acteurs de la filière (entreprise, association, entrepreneur, artisan…) qui vont trouver dans Filère une opportunité de mettre en vitrine leurs savoir-faire, leurs compétences, leurs spécificités mais aussi de trouver des collaborateurs, de partager des ressources, trouver des compétences et savoir-faire complémentaires.


    Les collectivités (Commune, Ville, EPCI, Département, Région) qui disposent d’une base de connaissance étendue des atouts de leur territoire. Chacune des collectivités à son échelle et selon ses prérogatives peut insuffler des initiatives correspondant à sa stratégie territoriale. 


Les structures de formation qui bénéficieront d’un outil “prédictif” des besoins de formation de la filière. 

Associés très tôt dans le dispositif, les différents guichets de financements publics sont répertoriés et visibles. 

Les têtes de réseau qui pourront utiliser le dispositif dans le cadre de leur activité, pourront en faire bénéficier leurs adhérents et seront parties prenantes directes de l’animation et du pilotage. Les acteurs de l’animation des réseaux locaux (industriels, artisanat, économie circulaire, numérique, ESS, énergie, innovation, recherche…) utilisent “Filière” pour diffuser et déployer leurs initiatives, coordonner leurs réseaux, valoriser leurs actions et collecter l’information.
 Les usagers du territoire (et d’ailleurs) qui trouveront sur la plateforme une information centralisée et structurée pour consommer localement, donner leur avis et collaborer sur les projets, initiatives, événements… se les approprier et profiter des retombées territoriales.

“Filière” peut représenter une opportunité de fédérer dans un contexte unique l’ensemble des acteurs d’une filière (institutions, financeurs, acteurs de la filière et le grand public fonctionnant habituellement en silo) dans un engagement collectif par et pour le territoire. 




POINTS FORTS

Les collectivités consentent d’importants investissements et des dépenses de plus en plus élevées pour aménager leur territoire et y maintenir un haut niveau de service et de qualité de vie. 
Les processus de conception et gestion des territoires sont historiquement pensés dans un « temps long » et une « segmentation par corps de métier » très peu favorables à l’innovation.
Les collectivités sont ainsi confrontées à un écart grandissant entre les besoins des usagers et une offre de services « technocentrée », complexe à maintenir et en risque d’obsolescence rapide (uberisation).

De plus, nous vivons actuellement une période d'ouverture et d'accélération des calendriers. Les initiatives, la collaboration, l'intelligence collective, et la technologie massivement distribuée réinventent tous les secteurs de la société et toutes les filières économiques (énergie, écologie, éducation, santé, transport, déchet, économie...).
Les retours d’expériences internationaux démontrent que les territoires connectés sont intelligents mais que les territoires interactifs le sont encore plus. Dans ce contexte, il s'agit donc de maximiser le potentiel du territoire et de se donner les moyens d’un développement significativement plus rapide et intense.

“Filière” n’est pas un nouveau dispositif destiné à en supplanter d’autres existants. 
Il propose d’offrir une infrastructure innovante à tous les acteurs d’une filière. Il vise à apporter des outils complémentaires à ces acteurs, mais aussi à terme de nouveaux débouchés, de nouveaux usagers, de nouvelles opportunités, de nouvelles sources de financement.

Ce réseau sociétal est vecteur d'une intelligence et d’une valeur que l'on commence à peine à entrevoir. Les liens et opportunités innombrables, riches de connaissances, de partage et d'économie représentent un potentiel considérable.

Toutes les créations de “Filière” sont génériques et peuvent être dupliquées sur tous les territoires, s’appliquer à toutes les thématiques et s’interconnecter entre elles.

RÉFÉRENCES SIGNIFICATIVES

Le programme “Filière” et son déploiement avec COmmunecter lui a valu le prix smart city décerné par le Monde parmi 880 projets européens. 

Outil du Territoire de la Côte Ouest de la Réunion dans le cadre de la mise en place du Contrat de Transition Écologique sur la partie Appel à Projet, financement, suivi et évaluation des projets. L’utilisation d’une telle plateforme numérique a permis de collecter, documenter et commencer à mettre en relation en quelques semaines plus de 170 acteurs et 70 projets de toutes tailles à l’échelle de l’agglomération. 25 projets ont été retenus après suivi et évaluation. 

autres ??? ou mettre autres projets dans d’autres domaines 


PRIX

Concernant la participation de notre équipe à ce projet, nous mettrons les ressources suivantes à disposition du projet : 
Un chef de projet
Un animateur
Un formateur
Une équipe de développeurs informatique pour le développement , paramétrage et maintenance / debugs


Échéancier de paiements avec un délai de paiement à 30 jours : 
Acompte de 30% à la commande avec un engagement de livraison de la plateforme à 2  mois
Paiement intermédiaire 50% à la livraison de la plateforme MOM et Vérification Aptitude. 
Solde de 20% à la VSR au bout d’un délai de 1 mois. 
40K€ TTC sans AO 

COMMANDE OFFRE "FILIÈRE"



GESTION DE PROJET 

Notre équipe est habituée, dans les différents projets qu’elle mène, à accompagner ses
clients aussi bien sur l’aspect technique que méthodologique.

Nos habitudes de travail avec nos clients se déroulent généralement en agilité permanente et amélioration continue. Nos moyens de communication privilégiés sont les visioconférences et en présentiel si nécessaire. 



A  MÉTHODOLOGIE DE GESTION DE PROJET : 


Création d’une fiche projet sur notre outil de gestion de projet OCECO 
liste de tous les contacts du projet 
Agenda OCECO avec tous les jalons importants du projet (dates clés) avec détail des tâches assignées et différents états d’avancements 
Nb : La messagerie est connectée avec notre outil de suivi de tâches OCECO qui vous informera de toute l’activité en direct. 

Ouverture de canaux de discussion sur rocket chat (messagerie instantanée) en fonction des thématiques et des groupes d’interlocuteurs pour suivre le développement du projet et faciliter les itérations successives en mode agile. 
canal de gestion de projet entre les chefs de projet, 
autres canaux thématiques au besoin.

Réunions régulières en visioconférence de suivi de projet et de validation des différentes étapes en format webinar et/ou atelier.

Suivi mailing pour tous les points d’étapes et de gestion de projet.




B MÉTHODOLOGIE DE DÉVELOPPEMENT : 


Nous pratiquons des réunions (scrum) en permanence avec prise de parole rapide pour transmettre les avancements de chacun
Présenter ce qu’on a fait depuis le dernier scrum,
Ce qu’on va faire pour la suite, 
Et si on rencontre des problèmes ou des blocages.

Nous utilisons Git comme système de synchronisation du code source. Les demandes de modifications sont directement postées dans gitlab.

Nous utilisons un canal de chat développement/tests/qualification/validation dédié à chaque projet. 

Une document de présentation en ligne (type PPT) vous sera fourni pour faire les remontées de bugs et remarques, qui devront être accompagnés de captures d’écran et d’url bien ciblées.

Déploiement : nous avons 3 instances : 
DEV : Mise à jour à tout moment sur la branche development
QA : Qualité Assurance, tests sur la branche master avant la MEP 
PROD : Production, sur la branche master




DÉROULÉ TYPE D’UN DÉPLOIEMENT 








FORMATIONS

X jours sont compris dans l’offre pour former vos équipes pour l’appropriation de la plateforme “Filière” 

Ces journées de formation sont principalement dédiées à l’usage de l’outil et à destination des différents usagers impactés par la mise en place de la plateforme. Nous pourrons aussi aborder des questions d’amélioration (pour une version ultérieure) et de mise en œuvre de pratiques adaptées à la prise en main de l’outil. 

Cette formation est à destination de neufs collaborateurs dont 2 administrateurs. Elle permettra à ces agents de déployer par la suite le dispositif auprès d’autres usagers de la plateforme.

Nous prévoyons 3 demi-journées de  formation  pour ~9 collaborateurs et 3 demi-journées d’accompagnement à l’animation de ces outils auprès d’autres agents. 

Les formations seront dispensées à distance en visioconférence. Prévoir du côté des personnes à former des locaux que vous leur mettrez à disposition. Chacun d’eux devra disposer d’un terminal (un ordinateur, une tablette ou un smartphone), dans la mesure du possible celui utilisé dans le cadre de leur mission, connecté à une connexion internet haut débit que vous fournirez. 

ACCOMPAGNEMENT ET ANIMATION


X jours sont compris dans l’offre pour accompagner vos équipes pour la mise en œuvre de la plateforme “Filière”. 

Nous considérons que notre mission d’accompagnement ne s'arrête pas à la dispensation des formations nécessaires à l’appropriation des fonctionnalités.  Nous devrons avancer pas à pas et ajuster notre accompagnement pour faciliter les avancées des équipes et mesurer les conséquences de l’usage de l’outil “Filière” pour le comité de pilotage. 

En fonction de vos besoins nous pouvons vous accompagner sur les actions suivantes : 
Administration de la plateforme 
Méthodes d'accompagnement des acteurs de la filière 
Animation de la filière : 
Ateliers collaboratifs d’intelligence collective 
Cartoparties
Organisation de recueil d’information avec les outils COForms
Autres besoins


Détailler ensuite les formes d’animation 
Nous pouvons vous proposer en option une animation Ariane : animation de collectifs orientée résultats et favorisant l’efficience du collectif. 

Améliorer des dynamiques collectives de travail via une nouvelle technologie numérique d’aide à la décision et au management des hommes et de l’organisation.
Expérimenter un processus favorisant l’intrapreneuriat, la clarté des rôles et la gestion de l’activité à travers un processus simplifié appuyé sur une méthodologie solide.
Favoriser l’implication des collaborateurs et le changement de posture
Rendre la méthodologie et les outils disponibles dans le but d’essaimer le projet et la démarche.
Faire évoluer les activités vers de nouvelles formes de collaboration et d’organisation du travail plus agiles et innovantes : Une approche qui fonctionne en intelligence de situation, favorise la résolution de problèmes et crée les conditions de l’autonomie. Les animateurs orientent leurs actions sur le processus de l’accompagnement et sur le sens des décisions. 
Elle permet de construire des modèles sur mesure appropriés aux situations et de les paramétrer avec les acteurs. 
Elle s’appuie sur des outils supports permettant de traiter en profondeur toute la complexité de nos liens et de nos environnements d’aujourd’hui et de demain. 



Nous pouvons également vous mettre à disposition des fonctionnalités directement issues de la plateforme COmmunecter : 
Canaux de discussion
Agendas partagés 
Outils de prise de décision
Mur d’expression
Bookmarking Social pour la veille 
GED (Gestion Electronique de Document) simplifiée de partage de documents.

Enfin nous vous proposons tout un panel d’outils libres

Éditeurs de texte collaboratif (Framapad, HackMD, CryptPad)
Gestionnaire de fichiers (Nextcloud)
Gestionnaire de tâches (Wekan)
Système de votes (Loomio)
Messagerie instantanée (Rocket.chat)
Plateforme de débat (Dialoguea)
Documentation participative (Wiki)
Hébergement de code open source (GitHub)
Réseaux sociaux libres (Mastodon, Diaspora*)
Système d’exploitation (Ubuntu)
Visioconférences (Appaer.in, Framatalk)



LIVRABLES
    

Plateforme “Filière”
Documents et tutoriels de formation explicatifs du fonctionnement de la plateforme pour les différentes catégories d’utilisateurs
Comptes rendus de gestion de projet

MAINTENANCE ET SUPPORT 

La première année glissante de maintenance est offerte. 
Le coût de la maintenance annuelle est de 10% du prix TTC
La durée de la maintenance est modulable et prévue dans le contrat. 
Notre équipe reste disponible et mobilisée, post projet, via les différents canaux de communication mis en place pour : 
répondre à vos problématiques,
former au besoin les nouveaux utilisateurs,
accompagner des référents dans le déploiement, l’administration et la maintenance des outils. 

Nous travaillons en dynamique agile basée sur une méthode de remontée des dysfonctionnements / bugs détaillés et interactifs. 
Un dysfonctionnement / bug bloquant est pris en compte dans les 24h et réglé dans les plus brefs délais sous 48h ouvrés (corrigé ou solution de contournement proposée).  
Un dysfonctionnement / bug non bloquant est pris en compte dans un délai de 1 jour ouvré avec correction dans un délai maximum de trois (3) mois calendaires. 

Concernant les possibles besoins d’évolutions, nous récoltons, au cours des différents échanges, les idées d'innovations et d'améliorations qui peuvent émerger, ce qui produit un document d’évolution qui pourra alors, au besoin, être évalué et traduit en équivalent H/J de travail pour tout ce qui est spécifique. Pour tout ce qui n’est pas spécifique, et qui représente la majorité des évolutions, la mise à disposition des nouvelles fonctionnalités faisant partie du commun de “Filière” est gratuite.  

Modalités du service d’assistance  : 
Contact mail, canaux et téléphone,
L’assistance au bon fonctionnement des logiciels s’effectue  du lundi au vendredi, de 08h00 à 19h00 (GMT+4h),
Intervention assurée à distance (téléphone,  télémaintenance) ou sur site. 




INTEROPÉRABILITÉ 

 “Filière” s'attache à être interopérable avec d'autres systèmes d'informations. C'est en ce sens que l'interopérabilité, permise grâce à notre API standardisée, avec le logiciel Mediawiki a été réalisée pour être en capacité de valoriser, d'intégrer et de rendre dynamique au sein de nos outils collaboratifs, la connaissance agrégée par ces plateformes (Movilab, Fabrique des mobilités, ...). D'autres projets d’interopérabilité ont vu le jour :
Wikidata
Wikipédia
OpenStreetMap
OpenDataSoft (la base SIRENE)
Data.gouv
Datanova (les enseignes La Poste)
Pôle Emploi
SCANR



DESCRIPTIF FONCTIONNEL 

La plateforme “Filière” est construite sur la base du moteur originel de COmmunecter. 

“COmmunecter la connaissance individuelle au service de l’intelligence collective et de la connaissance territoriale”


Site 
Description
communecter.org
https://doc.co.tools/books/1---le-projet/page/pr%C3%A9sentation 

https://cloud.co.tools/index.php/s/EBjN4q2Ar4YjPFg#pdfviewer 
Présentation vidéo
Fonctionnalités de COmmunecter
vimeo.com/133636468
https://doc.co.tools/books/2---utiliser-loutil/chapter/fonctionnalit%C3%A9s 


Notre équipe travaille depuis 2012 sur le développement d’une plateforme collaborative COmmunecter, projet d’innovation sociétale et collaborative, ouvert à tous, libre et disponible sur internet et applications mobiles. Elle permet de réunir les acteurs d’un territoire sur un même espace participatif et collaboratif permettant de créer une synergie de territoire au travers d’un large panel de fonctionnalités :
dédiées à la connaissance et au partage :  cartographie, géolocalisation, base de connaissances territoriale, la définition des besoins des uns, des offres des autres...
dédiées à la collaboration et à l’animation : outils collaboratifs, module de recherche territorial, réseau sociétal, la création de lien, événements, projets….
        


Notre plateforme d’innovation sociétale, déclinée en COSTUM, plateformes modélisées sur mesure en fonction de vos besoins, est particulièrement adaptée pour accompagner les projets complexes multi acteurs, multi thématiques, multi territoires : 
Capacité à répondre aux défis des transformations de la société
Construire des solutions transversales pour répondre notamment aux enjeux économiques et environnementaux de tous les acteurs du territoire.
Favoriser la participation et la collaboration des acteurs
Le caractère innovant et open-source de la plateforme sont également des atouts indéniables d’attraction pour un plus large public sensible à l’éthique d’une démarche. 

L’utilisation d’une plateforme de territoire comme support à une structuration et synergie de territoire est un réel avantage pour l’ensemble des acteurs :

#Numérique
#Filière 
#Centralisé
#Innovation
#Participation  
#Collaboration 
#Co-construction
#Retombées économiques
#Simplicité 
#libre d’utilisation
#Création de lien
#Dynamique
#Interaction
#Gain de temps
#Efficacité
#Crowdsourcing 



COSTUM

COstum est conçu pour vous permettre de créer une  “Filière” spécifique à vos besoins.
Le module COstum permet de personnaliser totalement  “Filière” en marque blanche en choisissant les fonctionnalités nécessaires en fonction des projets et des contextes.
Vous bénéficiez de toutes les nouveautés développées par Open-Atlas, tout en ayant la main sur de nombreux paramètres permettant de personnaliser votre plateforme :
thème de la plateforme (biodiversité, social, déchets, emploi, ...)
territoire (ville·s, département, région ou pays)
fonctionnalités proposées peuvent être activées à tout moment. 
charte graphique (logo, couleurs, ...)
lien personnalisé
Les possibilités sont nombreuses. Voici des exemples de réseaux qu’il est possible de créer :
Plateforme d'appel à projets et de valorisation des acteurs pour un dispositif déployable dans plusieurs territoires : Contrat de Transition Écologique
Plateforme de consultation citoyenne : Pacte pour la Transition
Réseau social local et thématique : RIES (réseau italien de l’ESS)
Moteur de recherche et cartographie de filière :
France Tiers Lieux
Tiers-Lieux du Nord, Meuse Campagnes
Tiers-lieu : Raffinerie

Moteur de recherche territorial 



Annuaire

Un annuaire navigable selon de nombreux filtres associés à la donnée notamment les thématiques et les grandes familles de l’innovation, ces informations seront récoltées par des formulaires (cf Remplissage des données par formulaires). Celui ci listera :
Organisations
Projets
Financeurs
Acteurs au service de l’innovation 



Filtres, Familles , Thématiques, Secteurs 

Agenda

Un Agenda partagé de l'innovation permettra à toutes les entités de la plateforme de publier des événements selon les Familles. L’agenda permettra de centraliser toute l’activité de l’innovation trans-thématique. Les utilisateurs pourront rejoindre les événements mais aussi capitaliser sur les événements passés sans forcément y avoir assisté. 

Journal

Toutes les entités auront un journal d’activités et pourront partager des nouveautés avec la communauté de l’innovation. C’est aussi une centrale de partage pour les Familles créant un endroit commun pour partager l’activité de l’innovation.

Visualisation
La donnée de la plateforme est toujours localisée on propose donc une Cartographie dynamique et filtrable de la filière

Les Graphes sont aussi de bons supports pour structurer et partager l’information.
Avec beaucoup de ressemblance avec le mapping actuel , nous vous proposons un graphe de circle packing


L’objectif des graphes relationnels est de visualiser les liens entre les acteurs


On pourra aussi naviguer dans la donnée dans une visualisation et organisation de la donnée en mindmap


Nous proposerons une visualisation en Parcours et Timeline projet pour avoir une vue d’ensemble de l’évolution des projets, des actions menées et de leur résultat avec les dates et étapes clefs d’un projet accompagnées d’une brève description.


La mise en réseau 

Un point fort de COmmunecter est sa capacité à montrer à quoi ressemble une filière, les liens entre les acteurs, quels sont leurs projets, quels projets utilisant telles ressources, etc. Nous proposons un large éventail d’outils et de méthodologies pour faire réseau au sein d’une communauté et créer du lien et de la dynamique entre les acteurs.
De nombreux collectifs utilisent quotidiennement les outils comme la structuration de communauté, le chat, les formulaires pour rester informé , être impliqué et agir au quotidien. Nous partagerons avec vous cette expérience acquise avec et pour de nombreux domaines.


Fiches métiers / acteurs 

Chaque acteur (5 familles et porteurs de projet) ayant rempli les formulaires auront leur fiche acteurs associée présentant l'acteur, son activité , ses projets , son parcours et éventuellement les éléments pouvant servir d'inspiration à d'autres porteurs de projet pour capitaliser sur ces expériences vécues.

voir fiche metier en ex :  

Fonctionnalités et caractéristiques d'une fiche acteur
Détails et Présentation
Dénomination
Famille de métier(s)
Financement
Accompagnement - Support
Mise en réseau
Tiers Lieux
Formation
Type(s) de financement(s)
Modalités
Statut juridique
Responsable
Adresse Téléphone
Mél
Lien
Objectif(s)
Lien(s) dispositif(s) financier(s)
Mèl dispositif financier
Partenaire(s) financier(s)
Montant maximal
Public cible
Points forts
Points faibles
Conditions d’éligibilité
Référencements
Points de contact
Communauté
Agenda
Le ou les projets portés
chaque projet aura aussi une fiche projet associée
Lien ou partenariats avec d'autres projets ou organisations 
Le parcours projet de l'idée à la réalité (date et étapes clefs)
export en version pdf
Chat : communication directe






Grandes sections
Page d'accueil
L'actualité
L'Annuaire
Les fiches Familles
Les fiches métiers
L'agenda
Outils de mise en réseau 
L'observatoire

Grandes rubriques et pages principales

Des pages pour présenter les possibilités et les acteurs : 
L'innovation
Les thématiques de l'innovation
Un territoire
La Recherche
Le Financement
La Formation
L'accompagnement, conseil, stratégie
La Communication    
Le Growth
Les retours d'expériences
FAQ : Questions Fréquentes



Back Office Administration

Le Costum MEIR vient avec une interface pour les administrateurs du site, leur permettant de gérer les différents aspects de la plateforme : 
La communauté (ajout/modification/suppression)
Les utilisateurs
les acteurs par famille 
les visiteurs
les organisations
L’import de donnée
Les tags de caractérisation
Le contenu des fiches 
Créer de nouvelles fiches projets
Les statuts des projets déposés 
Les statistiques
etc.




DESCRIPTIF TECHNIQUE 

Plateforme Open-Source 

Faire le choix d’une plateforme libre ou fermée est une décision stratégique. 
Dans une plateforme fermée d’une entreprise privée, toute évolution sera facturée. 
Faire le choix d’une plateforme libre et open-source comme l’est notre solution basée sur COmmunecter apporte non seulement une vision, une éthique mais également favorise la participation et contribue au commun. 
Dans nos principes de base, toute évolution de nos solutions issues de notre service R&D bascule dans le commun que chaque client peut s’approprier en fonction de ses besoins. Les fonctionnalités déjà présentes sur COmmunecter représentent une valeur dépassant les 900 000 euros qui bénéficient à tous nos clients. Seules les spécificités liées à l’utilisation de ces nouveaux outils ou services sont facturées si ces conditions ne sont pas déjà précisées dans le cadre d’un contrat. 
Ce qui facilite grandement la capacité d’évolution technique et fonctionnelle de nos clients pour une gestion de site ouverte et pérenne.  


Partie technique du projet

PHP
MONGO DB
JQUERY
BOOTSTRAP
METEOR
NODEJS
Au centre de “Filière” nous avons une base de données MongoDB. 
“Filière” met à disposition une API sécurisée et Tokenisée pour ouvrir et rendre disponibles les contenus open data à d’autres usages. Nous avons 2 applications principales : la version WEB, en production depuis 2016, très dynamique et en évolution permanente. Celle-ci est en PHP, HTML5, CSS3, basée sur le framework Yii et utilise fortement du javascript. 
La version Mobile utilise la même base de données mais tourne sur Meteor un framework NodeJs.
L’architecture modulaire de la version Web lui donne beaucoup de souplesse et de flexibilité sans limite dans la création de nouveaux modules en fonction des nouveaux besoins et objectifs demandés. 
Gestion complexe multi structures 

Auparavant les projets fonctionnaient en silo. Désormais de  plus en plus de projets nécessitent de pouvoir collaborer entre structures, de cofinancer des actions etc... Peu de solutions, totalement intégrées, permettent cette approche plus ouverte et collaborative. 
Les instances de  “Filière” que nous vous proposons de mettre en œuvre pour votre projet comportent nativement ces fonctionnalités et répondent aux besoins de collaboration multi-structures, multi acteurs...

Remontée d’informations 

Notre solution permet de gérer des flux d’informations en circuit fermé pour par exemple la gestion et l’organisation internes du comité d’animation ainsi que des flux d’informations ouverts au grand public comme ce sera le cas par exemple pour la plateforme front office. 

De plus toutes nos informations peuvent être caractérisées (avec un #) et ainsi créer des métadonnées qui structurent, géolocalisent et catégorisent la donnée. Dès lors, la puissance de cette information couplée à du partage d’information en est décuplée pour maximiser son accès, sa visualisation, son exploitation et sa valorisation. 

Enfin, nous disposons d’un moteur de recherche puissant basé sur la sémantique pour tout rechercher et trouver sur la plateforme rapidement, géographiquement et par filtres. 


Temps d’affichage des données 

La plateforme utilise des méthodes de packaging d’assets et de ressources diminuant le nombre de requêtes globales pour charger l’architecture globale, le reste du temps nous ne faisons que des requêtes ciblées en javascript. De ce fait nous ne chargeons que ce qui est nécessaire à la demande et permet une navigation extrêmement fluide et frugale permettant notamment des temps d'affichage rapides en haut débit mais également la navigation sur du bas débit 


Sécurité des données 

Nos sites sont certifiés et cryptés en https, 
Les comptes utilisateurs sont enregistrés et le mot de passe encodé en sha256,
Les mesures anti phishing et XSS assurent la qualités des données enregistrées,
Les logs apaches permettent d’avoir l’historique des machines se connectant à l’outil,  
Nos serveurs sont fréquemment mis à jour pour assurer au maximum l’intégrité de nos instances et nos serveurs. 
Tous les accès en terminal aux instances sont sécurisés en ssh, sftp,
Les données concernant la plateforme sont exportables en json à tout moment.

Compatibilité multi navigateurs


Notre solution est compatible multi-navigateurs bureau et terminaux mobiles  avec les dernières versions d'Internet Explorer, de Mozilla Firefox, de Google Chrome, de Safari, d'Opera et systèmes Android, IOS et Windows


Respect des normes W3C et des principes d’accessibilité (RGAA)

Nous utilisons l'architecture HTML qui respecte les normes W3C. Celle-ci est structurée pour les moteurs de recherche et répond à plusieurs critères d'accessibilité.  Nous venons de recevoir une demande de mise en conformité pour la plateforme du CTE qui sera opérationnelle prochainement pour tous nos clients. 

Hébergement

Votre solution pourra être hébergée sur des serveurs OVH. Nous pouvons également héberger votre solution sur d’autres serveurs de votre choix mais cela induit un coût supplémentaire non comptabilisé dans ce marché. 

La solution, accessible 7 jours sur 7, 24h sur 24, est répliquée sur plusieurs serveurs avec des redondances, plusieurs fois, par jour pour la base de données MongoDB.

Sites « responsive design » 
Nous utilisons bootstrap comme framework de design assurant la responsivité selon les dimensions des écrans et toutes nos pages sont testées pour être fonctionnelles sur des écrans mobiles ainsi que sur ordinateur.
Graphisme
Lors le la phase de création des modèles de pages HTML, l'interface graphique est découpée en éléments graphiques distincts. Le graphisme s'intègre au fur et à mesure de la construction du modèle HTML (Lazyloading) . 
Pour garantir un temps de chargement optimal, nous optimisons systématiquement le poids des images, optimisé et enregistré suivant les principaux formats d'images pour le web (GIF, JPEG, PNG). Nous privilégions le format JPEG pour sa bonne qualité d'image et un faible poids. Nous utilisons du PNG optimisé pour les éléments nécessitant de la transparence.
Charte graphique

Une charte graphique dédiée à la plateforme (Costum) sera établie pour correspondre à vos exigences. La souplesse de notre architecture permet d'intégrer finement toute spécificité en matière de charte graphique.
Nos graphistes ont produit plusieurs modèles qui pourront servir de base de travail ou d’inspiration pour la charte qui sera co-construite.

L'interface de saisie

L'interface de saisie des contenus est de type wysiwyg, elle permet de télécharger plusieurs contenus (images, PDF...). Les fichiers insérés sont classés dans une médiathèque. Nous offrons la possibilité de mise en page complexe au travers de blockCMS éditables.
Tous les contenus sont structurés par un titre, du contenu principal, image(s), lien(s), documents(s), géolocalisation permettant une forte géolocalisation et un meilleur référencement par les moteurs de recherche.

Indicateurs et compteurs 

Nous avons une page de statistiques disponible dans la partie dédiée à l’administration, visible également sur la partie front office, il est possible de poser des indicateurs sur plusieurs types de contenu, des requêtes, des pages,etc...permettant de comptabiliser les visites et l’impact de nouveaux contenus.

Réglementations de la CNIL

La plateforme respecte les préconisations RGPD de la CNIL
