# Présentation de Smarterre

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSOTSgijMFNPHrxmWnmMvAdrYA6Vb3x0v8E6lQ_Lf8-ThL6ATCtFoWQBhb33U99kQ/embed?start=false&amp;loop=false&amp;delayms=3000" width="830" height="495" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Smarterre est une démarche globale déployable sur n’importe quel territoire pour favoriser l’innovation collaborative et la transition écologique.

[Communauté Smarterre](https://www.communecter.org/#page.type.projects.id.5979951740bb4e293fe836cb)

![](/Images/intelligence-collective.png)

La crise écologique et climatique couplée au déséquilibre social auquel est confronté notre société a fait émerger dans l’esprit de nombreux citoyens **la nécessité de participer activement au développement de leur territoire**. 

Des projets de toutes sortes ont fleuri au fil du temps, certains avec l’espoir de corriger des erreurs du passé, d’autres avec l’intention d’inciter chacun à reconsidérer ses usages quotidiens. Des méthodes innovantes ou parfois ancestrales (et remises au goût du jour) ont démontré qu’il était possible de **créer une société davantage responsable, inclusive, solidaire et raisonnable, prenant en compte la finitude des ressources**.

![](/Images/construire-ensemble.png)

C’est face à cette effervescence de cheminements ingénieux que le projet Smarterre s’est construit. Des citoyens et des citoyennes engagés se sont réunis pour **faire converger des visions variées et engagées favorisant le bien-être humain et la protection des biens communs**. Smarterre doit évoluer comme il a commencé, il est et doit rester à la croisée des représentations positives. Il est, pourrait-on dire, un souhait collectif qui s’exprime avec la logique de **répondre à des enjeux concrets**. Après validation, il prend vie dans l’ensemble du territoire dans lequel les citoyens souhaitent qu’il s’applique. Pour ce faire, **Smarterre propose des outils, des concepts, des projets déjà existants ou possiblement reproductibles**. Il se veut ouvert et collaboratif, il se veut culturel et local. Smarterre, c’est écrire ensemble l’histoire de notre territoire, celle qui est, qui pourrait être ou que l’on souhaite perpétuer.

Dans le but de **rendre intelligibles leurs aspirations**, les citoyens et les citoyennes ayant participé au projet Smarterre ont alimenté différentes thématiques avec des idées en tout genre, parfois sous forme de conseils simples à appliquer au quotidien, parfois en terme d’infrastructures ou d’outils à mettre en place et à dupliquer, certains se sont même vu imaginer de nouvelles méthodes à appliquer au travail contre la pollution, d’autres à l’école pour rendre les cadres d’apprentissage moins conditionnants et plus épanouissants. **Education, Economie, Transport & Énergie, Alimentation, Économie, Communs, Aménagement & Construction, Citoyenneté, Déchet**.. Autant de thématiques qui serviront de canevas à une société qui se transforme et qui doit s’adapter aux besoins de ses citoyens.

# smarterre plateforme
plateforme de documentation des meilleurs pratiques pour atteindre et mettre en action un smarterritoire

# smarterritoire plateforme
associé à un territoire , region, agglomération, commune ou meme un quartier, une plateforme smarterritoire existe au travers de l'activité des filières locales structurés de maniere ouverte, transparente, pour valoriser l'activté, augmenter les échanges entre les acteurs d'une filière, et mettre ne place des projets de mutualisation au sein d'une filière. 
Une filiere ou secteur thématique se définie à partir d'un acteur locale. Peu importe son activité agriculture, numérique, education, artisanat, Culture....
Au final l'objectif est de valoriser localement et si possible connecter les smaretrritoire entre eux pour augmenter encore plus les échanges et les dynamiques.

[Méthode](https://docs.google.com/presentation/d/e/2PACX-1vS8uPgkFxze9z9x4vDMvcBjYu-i9NrVP6L1RurG-5mcPoIJW6jPmNatMOGbqNkmdELuFRRs11oDP0_c/pub?start=false&loop=false&delayms=3000)


# Démarer une démarche Smarterre
- il suffit de trouver rejoindre ou créer une communauté locale ([COEUR](COEUR.md))
- rassemler la connaissance 
    + compétence 
        + le niveau 
    + projet 
    + evennement
- créer un ou des projets au sein du territoire
- faire filière 
- lien avec d'autre communauté

# Qui peut porter une démarche Smarterre
- un collectif citoyen
- une collectivité
- une association
- une entreprise 