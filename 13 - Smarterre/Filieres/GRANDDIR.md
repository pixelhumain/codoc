GRANDDIR gouvernance digitale
===

###### tags : `communauté` `gouvernance` `structuration`
###### codimd : https://codimd.communecter.org/u3LXDo9mSsCRx-SyTIqYYA?edit#

### COstum
- [Communecter personnalisable](https://www.communecter.org/costum/co/index/slug/granddir#)
- [L'Annuaire](https://www.communecter.org/costum/co/index/slug/granddir#annuaire)
- [Les projets de Granddir](https://www.communecter.org/#@granddir.view.directory.dir.projects)
- [L'agenda partagé de Granddir](https://www.communecter.org/#@granddir.view.directory.dir.events)

### Proposition / Decision
on propose d'utiliser OCECO , un outil d'action et de gouvernance horizontale et transparente
l'instance de Granddir est [ici](https://www.communecter.org/costum/co/index/slug/proceco#welcome.slug.granddir.formid.626657738c3dbe280d2ce43f.page.list)

### Drive
https://conextcloud.communecter.org/s/6PoDFA8M4xbngo6
vous pouvez installer un petit programme pour avoir accés au drive directement dans votre ordinateur 
https://nextcloud.com/fr_FR/clients/
clicker
- se connecter à NextCloud
- serveur https://conextcloud.communecter.org

### Email
créer un alias mail pour tout les membres qui voudrait etre actif 
condition : etre présent au moins une fois au visio de gestion de granddir 

auj : 
Kazz
Severine
Patrice
Sebastien
Sylvain 
Tibor 
Greg 

### DNS (nom de domaine)
pour bloquer granddir.re je l'ai acheté 

### visio conf
https://meet.jit.si/granddir

---

### EVENTS
CR GRANDDAPERO 4/07/2022 10h-11h30

### ANNUAIRE D'ACTEURS
Diagnostique d'engagmenet à GRANDDIR

#### POSER une LEGITIMITÉ
qui agit sur quel sujet montrer l'energie en action 

#### SESSION granddir : kOSANOUFÉ ? KILELA ?
Caractérisation des acteurs
- DECHET : zero déchet
- COMPOST : Réseau compost Réunion  
- BIODIV : SEOR, SREPEN, ONF, Parc 
- EAU : 
- EDUC POP : GRANDDIR , DRAJES, CEMEA
- AGRICULTURE : GAB, Uprobio
- OCEAN : CEDTM, KELONIA, OMAR 
- NRJ : EDF, Akuo
- TIERS LIEUX DD : RTL , RAFFinerie, RECUPR
- TRANSITION ECOLOGIE : DEP, REGION, DEAL
- SCIENCE : science Reunion, IRD,  Les Pti Deb, Université, CIROI, 
- Accompagnement : Qualitropic, Technopole, Seeds, Open Atlas  
- outil et Reseau : Open Atlas , Cetanou

### ANNUAIRES DES ACTIONS(PROJETS)
Caractérisation des projets

### Les Chiffres
- aller au journalisme de donnée 
- chercher les gisements de data
    - par territoire (commune, agglo, region, departement,DEAL,ADEME)

### ACTIONS CONCRETES 
#### action GRANDDIR
- qui travail concrétement sur quoi ?
- le réseau de partenaire 
    - Diagnostique d'engagmenet à GRANDDIR
        - [html]Présenation : 
            - Pourquoi le diagnostique
            - presentation de l'observatoire (court)
            - Btn je particpe > repondre 
        - Email 
        - Nom Prénom
        - l'entité represntater 
        - souhaitez vous participez au projet d'Observatoire de la transition écologique de la Reunion porté par GranDDir 
        - décrivez vos attentes de GranDDir
        - sur quelles thématiques
        - [evaluation] Implication ( un peu / bcp / passionement / pas du tout ) 
            - je peux donner du temps pour ma thematique 
            - je peux proposer des service de formation 
            - je peux participer à l'organisation 
            - je peux participer à la gouvernance du reseau 
- Financement 
    - faire une rencontre dédié avec tout les financeurs
        - agglo, region, departement, DEAL, ADEME

#### étape de l'observatoire
- listing d'acteurs
    - partenariat avec les plateformes existantes ( [la rondavelle](https://www.larondavelle.re/structures), cetanou, DEAL... )
    - classification par thématiques
- faire un formulaire "acteur"
     - mise à jour des contacts
     - quels changements
     - les nouveautés / les projets 
     - les services / compétence 
     - les besoins 
     - ressources
- un formulaire d'impacte des actions
    - par collectivités
        - les chiffres par thématique 
        - valorisation de la data existante 
    - par acteur 
        - les chiffres d'impacte de leur action 

### OBJECTIFS
- Structurer le reseau de la transition ecologique Reunionaise
- Créer 
    - [ ] diagnostique d'engagmenet
    - [ ] un annuaire 
    - [ ] un observatoire 
    - [ ] un centre de ressources partagés