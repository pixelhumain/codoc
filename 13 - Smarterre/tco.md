# Smarterre TCO : les débuts d'un projet expérimental territorial

![](/Images/smarterre-tco.png)

_**En avril 2019, le TCO (Territoire de la Côte Ouest de la Réunion) signe le Contrat de Transition Écologique, contrat également lancé dans dix-sept autres territoires français par le Ministère de la Transition Écologique et Solidaire. C’est dans ce cadre ouvert aux projets d’innovation territoriale que Smarterre a été sélectionné et financé pour être développé. Il sera piloté par le TCO et accompagné par l’association Open Atlas via l’outil Communecter et par l’Institut Efficacity.**_

## PRÉSENTATION DE L’ÉQUIPE SMARTERRE, “AU SERVICE DE L’INTELLIGENCE TERRITORIALE”

![](/Images/smarterre-tco.png) [Open Atlas](http://www.open-atlas.org/), porteuse historique du projet Smarterre, est une association de loi 1901 à but non-lucratif et dont l’un des objectifs est de valoriser l’intelligence collective pour une meilleure gestion des biens communs. Pour ce faire, les membres de son équipe développent le logiciel **Communecter** que l’on peut retrouver sur le web sous forme de “réseau sociétal”. Ce dernier propose des fonctions qui pourront être exploitées par les acteurs du territoire dans le cadre du projet Smarterre :
- une base de connaissance, que les utilisateurs peuvent agrémenter en informations diverses concernant leur territoire. Ces informations seront visibles par l’ensemble des acteurs connectés.

Au sein de l’équipe Smarterre, son rôle est double : il se propose d’accompagner les expérimentations dans le territoire ainsi que les projets en faveur de la transition écologique, notamment les projets de R&D (1) mutualisés. Il cherchera à structurer les phases d’évaluation en proposant des outils adaptés. Propre à son domaine d’activité, il entreprend de participer activement à l’animation de la filière énergie.

_(1) R&D : recherche et développement_

## COMPRENDRE LES FILIÈRES DU TERRITOIRE & CONTRIBUER À LEUR STRUCTURATION
_Des recherches seront menées pour mettre en lumière le fonctionnement et les spécificités de chaque filière. Une fois cet état des lieux établi, les acteurs des filières pourront disposer d’outils spécifiquement élaborés pour faciliter la conception de solutions face aux problématiques qu’ils rencontrent; et pour et la mise en application d’action de transformation._

## OFFRE DE VISIBILITÉ POUR LES FILIÈRES
Une filière, pour se structurer, nécessite d’avoir une compréhension globale de son propre fonctionnement, une connaissance des acteurs qui la composent et des ressources dont elle dispose • Afin de faciliter la structuration des filières du territoire, le projet Smarterre propose des méthodes d’animation adaptées, des outils qui permettront à ses acteurs de se caractériser et de se cartographier.

## DISTRIBUTION DE L’INFORMATION
L’utilité d’une filière peut se mesurer selon la pertinence des services qu’elle propose. Il est donc naturel de se poser les questions suivantes : La filière met-elle suffisamment en réseau les entités qui la compose ? Permet-elle l’accès à des connaissances à jour et adéquates à son identité ? Propose-t-elle des opportunités de projet ? • Afin de mobiliser correctement les acteurs d’une filière, le projet Smarterre propose d’établir un cadre d’animation opérationnel adapté aux spécificités de chaque filière et de fournir des outils pour mettre à jour, rassembler et diffuser dynamiquement l’information au sein de la filière, et cela en fonction de ses besoins.

## Création d'une plateforme numérique ESS-TCO
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRwYAtGvXexbxqF6Gd3SDUrRqXj4EgeYbJAsOls96wCZRKSXj62NZ5ES9oZT6gn41zMVFLc4mwNeEfhPTNIXI4/embed?start=false&amp;loop=false&amp;delayms=3000" width="830" height="495" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

## Rencontre avec le PRMA (17/09/19)
PRMA : Pôle Régional des Musique Actuelles de la Réunion
| PARTICIPANTS     | ORGANISME                | CONTACT                                      |
|------------------|--------------------------|----------------------------------------------|
| Guillaume Samson | PRMA                     |  observation@prma-reunion.fr 06 92 02 81 76  |
| Sylvette Anibal  | Animation/porteuse du projet "Coeur de filière"  |  06 92 59 18 48      |
| Tibor Katelbach  | Open Atlas               | tibor@pixelhumain.com                        |
| Béatrice Potier  | Open Atlas / Ariane      | beatrice.potier974@gmail.com                 |


Contexte / présentation CO et Smarterre
Cartographie de la culture
Naturellement horizontal
Valoriser les filières et travailler avec les acteurs stratégiques en place - le coeur de cette filière
Le coeur qui impulse les rencontres

Présentation contexte général d’Opal et dans quel contexte s’inscrit cette démarche - Smarterre

Animation : Dvper les relations entre les acteurs, animer le réseau, création de lien entre les
acteurs, mieux se connaitre, mutualiser les ressources, faire émerger des projets, des relations
Objectif à terme est de créer de l’autonomie dans le réseau et du soutien.

Contexte PRMA : service d’observation - un peu de cartographie restreinte lié au données
collectées à travers les dispositifs d’aides (aide à l’embauche dans les cafés concert, aux
déplacement aériens, et vidéo/clip) - traitement de données administratives
Etudes ponctuelles diverses
Réponses à marché d’étude

Relance de la dynamique de cartopraphie et impulser ou soutenir des dynamiques issues du
terrain
OK pour faire en synergie avec des projets existants.

Enjeux sur la filière avec des gens qui se sentent légitimes - ensemble de dynamique qui existe - animation d’un contrat de filière - grande consultation premier semestre des acteurs locaux - sur consultation des mêmes acteurs (états généraux et consultations publiques - questionnaire en ligne…)Dialogue pour valoriser l’existant et le transformer en outils…

Prma - 100 adhérents - environ
Pblématique des dynamiques d’acteurs pour les mobiliser
Mobiliser les acteurs sur des marges d’observation partagée qu&#39;ils doivent s’approprier

La filière musicale « dite professionnalisée »
le yoma - marché qui invite les acteurs - outils au service de la filière
Les tropicales musicales
Les rencontres dans le cadre de la consultation
Rencontres initiées par le prma

Connexion avec d’autres réseaux :
- Collette - collectif de spectacles vivants - s’ouvrir à l’ensemble des acteurs de la filière

Les moteurs de la filière sont les lieux subventionnés donc du temps et légitimité pour mettre en oeuvre des dynamiques collaboratives.

Enjeux : articuler l’observation au prma et l’articulation avec Colette
Déjà filières en silot - la musique actuelle (populaires et clivées) et le reste - autres clivages entre les salles subventionnées et les artistes privés.

Enjeux d’articuler Collette, prma, accord cadre pour le dvpt et emploi dans le spectacle vivant….
Ensemble de chantier en cours qui s’articule ou pas en fonction de la capacité de dialogue des acteurs.
Relations dans le secteur à adoucir :
pb de métier - individualité et projet spécifiques -
pb de positionnement de structure - questions philosophies qui sont différentes (orientation de plus en plus business - yoma - économie du « business » -Collette autres veines
Des questions de professionnalisation ou pas du secteur - niche formelle et informelle (séga/raga) relativement étranger au spectacle institutionnel - autre monde

Champ musical et morcelé et le champs de la culture en rajoute.

Annuaire - descendant - qu’est ce que les acteurs ont à y gagner en collectif - il passe par lesoutils - trouver l’intérêt à faire filière.
Outils de fan club - « Croda » - réseau social ++ de l’artiste avec sa fan base.

Vrai chantier de réflexion en amont - Nous - Colette - Prma - Yoma - =&gt; synergie et intelligence collective entre acteurs - favoriser l’intelligence entre acteur grâce à la neutralité

Dynamique d’observation partagée et participative - enjeux à relever

Observation - traite des données de première main qu’on veut avoir ou traite de la donnée de
deuxième main - être attentif aux questions d’usage des données.
-> trouver les axes pour que les acteurs soient intéressés et trouve intérêt à la plateforme

Enjeux la propriété des outils - à poser comme préalable
Temps nécessaire entre la maturation d’une ingénierie collective et d’un outil d’observation en
ligne.

Ce n’est pas l’outil - quels sont les objectifs qu’on se donne en commun

TCO - Ecole en réseau - payer des jetons de présence - quand cadre défini : formatage fait car ils n’arrivent pas à trouver leur positionnement dans la collaboration - mentalité de travailler sur un territoire - sociologique - de même ils sortent - enseignements artistIques avec la notion de l’économie qui parasite le système
Ceux qui sont encore là et travaille encore - prestation de service qui porte des valeurs

Quid de la question des valeurs - trouver notre place et la faire connaitre - comment être
complémentaire - articuler entre nous

Projet de réflexion et de co-participation - convention de partenariat - outils d’affichage transversal - info spécifique aux disciplines et autres transversales - difficulté à mettre en place - existence de filière entre elle qui ont leur propres enjeux - outils pour la jonction entre des travaux propres pour des réseaux particulier par discipline et outil d’affichage global au secteur du spectacle vivant.
Le réseau lui même est porteur de son observation - attention à ne pas déshabiller les activités de chacun

Notre positionnement OPAL - transversalité et le dialogue entre les têtes de réseau

enjeux technique pour prma - outils d’enquête existants déjà

Colette
Nicolas Laurent - Grand marché Saint Denis - CDOI - 0693 10 20 60
Nathalie Soler - L’indic production - 0692 85 41 55
L’alambic -
Sylvette Anibal - 06 92 59 18 48 - TCO

Guillaume Samson : observation@prma-reunion.fr
06 92 02 81 76
