# Intégration des quartiers

Actuellement on a la collection cities pour les villes et zones qui pour les pays, régions département ou pour tout ce qui a un niveau superieur à cities.

Pour integrer les quartiers, il y a 2 choix possibles soit les rajouter dans zones avec un champs `"quartierDe" : "Id de la ville"` ou alors crée une nouvelle collection pour les quartiers.

Dans les deux cas il faudra voir si il y a une utilité a rajouté les champs suivante : level1 level2 level3 et level4 pour faire le lien entre les différents zones.


## Comment ajouter un quartier

Choix possible :

- Importer les données et faire le lien avec la villes associers. Si on arrive a associer une ville, on pourra rajouter les différentes zones.

- Créer un formulaire. Le formulaire contiendrait les champs suivant: nom, cp(si besoin), la ville. ( On peut rajouter d'autre champs au besoin )


