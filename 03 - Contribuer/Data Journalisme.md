Data Journaliste (DJ) et le DJ Tech (DJT)

# Objectifs
un groupe de DJ
- Elaborer des méthodes de [COJournalisme](https://docs.google.com/presentation/d/1HNNDSfn-tDetuc5O1iGB3iMWxmK2jB7XeKhx1o4MWEQ/edit#slide=id.g737fb454ef_0_7) 
- faire un travail collaboratif et contributif de veille sur une thématique
- amélioration de la data open edition dans CO 
	- si qlqun ajoute juste une url , le dataJ prend la suite et assure un minimum de qualité de l'information
- si qlqun fournit un directory ou un listing , le transforme 
- préparation et veille sur la structuration de filière 
	- numérique 
	- déchet
	- agriculture 
	- petite enfance et activité sur la Réunion
- aiderait aussi à remonter les bugs et à améliorer
- un DJ peut etre soliciter pour faire une etude precise 
	+ presta
- faire le lien avec Wikipedia ou OSM 
- DJ Tech saura utiliser l'outil d'import 
	+ remonter les besoins et améliorerations

un DJ utiliserait co pour remplir de la data sur la zone et invité des devs, des groupes d'artistes , d'artisan, l'education, les cooperative agricoles, tourisme ... etc ... ?
on pourrait le faire comme un exercice sur tout mada 
on peut se le faire financer et créer un groupe local actif

# Slogans

## COzette
https://drive.google.com/drive/folders/1OsBGuITEDfJP4U2Txut8-EITmSnaWycY
https://docs.google.com/document/d/1qSmuBB8LJPO5FxkFFhE179tX2HaEN_agPbb1scQRU0I/edit

## Journalisme Connecté Communecté Collaboratif
## Let our memorise serve our future 
## L’information est partout et partout on peut créer une mémoire des événements
## Veille Collaborative et Contributive

# Cas d’usages

- COzette 
- COScience 
- Ouvrir un sujet personnel et partageable ex : psoriasis
- Hashtag driven Costum générique
	+ https://docs.google.com/presentation/d/1c7cYfbDz_dfT_yTVm63IOw62hYxuWlcUwRbo2unEV30/edit#slide=id.g737fb454ef_0_7
- Sentinels of public service 
	- Citoyens qui vont réellement surveillé l'activité de toutes les publications et du contenu des 24 communes , région , département , agglo , DEAL, Préfecture et on va passer tout ca au crible
	- Noter tous les contrats passés et avec qui et demander les résultats et les retours de réalisation pour avoir un rapport , subvention > creation
	- Ils veulent nous fliquer ... on va les fliquer à notre tour

# METHODE COEUR ( Collaboration Organisation Evénements Unir Ressources )
https://docs.google.com/presentation/d/1NZr1DMFeS3I-JYuXrGvecijvgWW4zLaVvUyaevK-jHM/edit#slide=id.p
## C'EST QUOI
Un collectif de citoyen qui ont eu un rêve fou : inventer leur utopie sociétale….

Entre rêve et réalité, idéal et utopie, est né : 

Un projet oeuvrant pour les biens Communs pour
expérimenter/découvrir de nouvelles manières de favoriser  le développement durable et la croissance locale : Smarterre.

Appuyé sur une méthodologie innovante qui associe une plateforme numérique Open source, une animation de rencontres “ humaines” en intelligence de situation et des outils modélisés, co-construits et créés avec les acteurs.

Porté par une vision globale pour transformer localement et créer des territoires connectés

et découvrir de nouvelles manières de déployer les projets et de développer le territoire

Un modèle d’accompagnement des territoires, une Approche Collaborative

### 1. Collaboration
- Réunions 
- Gestion du COEUR
- Amorçage filière
- Stratégie Détaillée

### 2. Organisation
- Programme
- Questionnaire
- Visualisation 
- Amélioration continue

### 3. Evénements
- Événement
- Animation
- Atelier

### 4. Unir
- Restituer
- Lier
- Comprendre
- Participer
- Distribuer
- Agrandir
- Choisir
- Contribuer

### 5. Ressources

Stratégie
Résultat
Indicateurs
Analyses
Partages

## Une Approche par COEUR pour faire filière

### DES RENCONTRES
> de tous les acteurs, contributeurs,bénéficiaires…
autour d’un contexte donné

### DU PARTAGE ET DES CONNECTIONS
> d’acteurs moteurs et impliqués 

### DES PETITS CERCLES ACTIFS
> pour créer du lien, de la collaboration
Faire émerger les besoins, les services, les compétences...et les projets porteurs

### DE LA CONNAISSANCE ET DE LA LISIBILITÉ
> la construction d’outils, de méthodes collaboratives et d’une base de connaissance dynamique du territoire
### DES ACTEURS ET UN TERRITOIRE CONNECTÉS
> un territoire et des projets valorisés et une dynamique de synergie des acteurs locaux

