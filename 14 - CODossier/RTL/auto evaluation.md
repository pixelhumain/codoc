# auto evaluation

https://nuage.tierslieux.re/s/Zwt7synpc4bcHE3

Les 10 sujets
1.  Lien social : rencontre, convivialité, entraide
2.  Impact sur son territoire 
3.  Autonomisation des individus
4.  Impact environnemental
5.  Inclusion & diversité
6.  Gouvernance horizontale et transparente
7.  Coopération interne et externe
8.  Hybridation des activités (décloisonnement)
9.  Lieu physique partagé où tout converge
10. Création de commun

* niveau
– Étape 0 : Ce sujet ne m’intéresse pas
– Étape 1 : Je découvre, ça m’intéresse, je souhaite m’engager
– Étape 2 : Je fais des essais, des expérimentations
– Étape 3 : J’intègre au quotidien, j’ai les moyens de faire
– Étape 4 : J’ai de l’expérience, j’analyse, je conçois, je suis exemplaire
– Étape 5 : Je transmets, j’accompagne, je suis leader 