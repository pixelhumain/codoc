[MD](dealbudget.md)

# DEAL Amélioration Habitat doc (40K€) 

Portage : **Open Atlas**

### Poste de dépense 
- Dev
- Gestion de projet

### Réalisation 
- Prospect Montage : 5% Tib
- Coordination de projet : 5% Tib 
- Features :
    - Formulaire Tib Bouboule Raph
        - Formulaire presta 15K€
            - Regle
            - Validation ( export pdf )
            - financement ByBudgezt
            - suiviByBudget
            - socleDesPieces 
            - Encrypt 
            - Address
        - openForm commun 15K€
- Costum 4K Tib Rapha Bouboule
    - Observatoire Territoire commune 
- Chiffres en mode CTE
- Pages Statiques
- Admin 5K Bouboule
    Email d’étapes spécifiques
- Communauté 
    - Financeur
    - Opérateur

### Partage du budget

Gestion de projet : 4K€
Commun : 50% , 20K€
- coForm commun 50K€ ( 20K€ dev - 30K€ innov )
- mail par etape ciblé
- nouveau système d'addresse 5K€()
    - Objet js + recherche d'addresse via datagouv et nominatim (3K€)
    - Creation des cities et zones (1K€)
    - Maj des cities et zones (1K€)
    - Futur amélioration : 1K€
        - refactor dynform 1K€
- nouveau système de filtre - moteur de recherche js (6K€ dev - 7K€ innov)
- export PDF (1,5K)
- export CSV (2K)

#### Dev Presta : 26K€
- observatoire 4K€ (3K€ spé et 1K€ commun)
- inputs specific (5K€)
- specificité validation d'etape (5K€)
- Gestion des roles (4K€)



#### Commun : 
- inno 
- dev
#### Innovation : 
#### Presta : 
- dev
- gestion de projet 2K
#### Test - Maintenance 
- restant : 8K€
