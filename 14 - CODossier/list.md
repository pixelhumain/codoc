# Departement de la Réunion : Budget PArticipatif
Devis

# CIVIS : plateforme pour le territoire
Devis



# ANCT : Système d'appel à projet générique
03/2021 : Remporté
(DOC)[https://codimd.communecter.org/SZ4HewbWRc-V4bNIxaitaA?view#]
(PROPOSITION INITIALE)[https://codimd.communecter.org/EgRB7UXaRJuMJl9C1NEjbA#]

# COsindni smarterritoire : Appel à projet pour l'AFTB
11/2021
(DOC)[https://docs.google.com/document/d/1E6JX7KsliPIBkntsU3QFhuc0fF188d3sfgLrPaRPIJ8/edit)
(PPT)[https://docs.google.com/presentation/d/1qWjFI0x4jWXb-D9Ky2s14ZnjJIxArId9SCF9xZThKbI/edit#slide=id.gc26d11499d_0_0]

# MEIR : filiere innovation Reunion
Sep/2021
(DOC)[https://docs.google.com/document/d/1DmJrAg2Jf8Dz89IlQ3ZpcbayTI8cwtGUl9JGYxP8gaM/edit#)
(PPT)[https://docs.google.com/presentation/d/1iGj8Sun020Yu84B3y28PJ6X0xMVl6JBoJgqwcHk8Di0/edit#slide=id.g3802b17b5_110]


# SGAR Tiers Lieux Reunion
06/2021
(PPT)[https://docs.google.com/presentation/d/1RthbdQw4ksZA_ogab_UoFmW-q_lrtjEJ_uMhqw_Brdo/edit#slide=id.g7f2715dafa_5_55]
(PPT Avancement )[https://docs.google.com/presentation/d/1ieWQHmF_XRotvzts_OAKH1OqoSutIEm9zhF9AghCRR0/edit#slide=id.gecc0d68bc6_0_114]

# Science ouvert
06/2020
(DOC)[https://docs.google.com/document/d/1JtxXpTbQXATHni3Fz96TE6tr216uxNdTJlQz16kbNCA/edit?ts=5ecd5639#)
(PPT)[https://docs.google.com/presentation/d/17e5hN7xUAqzmCXo4joUMdlFhbJ4gNKiyrzmWYGDZfnU/edit?ts=5ecebca7#slide=id.g7f2715dafa_5_0]

# FACT
06/2020
(DOC)[]
(PPT)[https://docs.google.com/presentation/d/1di2j3etdu2CeUJvk6Rq7J2RbO3Qr2evvaKKm9xDWZG8/edit#slide=id.g7f2715dafa_5_0]
(PPT FACT)[https://docs.google.com/presentation/d/1ZesGOCO15vuIaW1MSpO852PhSGEbQ6NWlrV2kAWH7eM/edit#slide=id.g7f2715dafa_5_0]

# Mozilla
06/2020
(DOC)[https://docs.google.com/document/d/1DBUcq-pzfSSQHB2_082SAFYiVjddczw14uUVnKQ1afU/edit]

# SMARTERRE
11/2019
(Methodo)[https://docs.google.com/presentation/d/1eyq2KHWZhPTmTvBajskaeFs4-jm10hBlAePEgWn1ux4/edit#slide=id.g5e5279c140_4_273]
(Synthese PPT)[https://docs.google.com/presentation/d/1NZr1DMFeS3I-JYuXrGvecijvgWW4zLaVvUyaevK-jHM/edit#slide=id.p]

# OPEN ATLAS
(DIAPO PPT)[https://docs.google.com/presentation/d/1Y7-aU4CgTAFXcsQ7EmLhD9vBUmS8c3-y8VGTanXRW5A/edit#slide=id.g6dd64bd4d7_0_30]
## Propositions
(PPT TOUT CO OPAL SMARTERRE (Short))[https://docs.google.com/presentation/d/1pnmib1Wpq3YFiDHgFFBQna3ugkP25qQGgJIjzrZk95E/edit#slide=id.g6d7662a366_0_0]
