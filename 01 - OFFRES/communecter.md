# Communceter

## Un réseau sociétal libre et ouvert,
un commun numérique et logiciel libre

« COmmunecter » : se connecter à sa commune, est une boîte à outil citoyenne collaborative, ouverte à tous, un réseau sociétal innovant, open source et gratuit, de développement de territoire avec une approche locale (quartier, arrondissement, commune, ville...). 
Un bien commun numérique, disposant de nombreuses fonctionnalités, pouvant être activées en fonction des besoins sur des COstums personnalisés permettant de mettre en oeuvre l’intelligence collective d’une organisation et d’acteurs autour d’une thématique spécifique d’intérêt général comme la santé, l’agriculture, l’éducation, l’environnement, l’emploi ou encore la science.

