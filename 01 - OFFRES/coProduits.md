---
type: slide
slideOptions:
  theme: white
  transition: 'fade'
  
---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_ea9e6ca433e28f797888b9048c14a9b0.png" -->

# CO PRODUITS
![](https://codimd.communecter.org/uploads/upload_72548b7aa2219f23e821d2add85228f0.png)

---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_ea9e6ca433e28f797888b9048c14a9b0.png" -->

![](https://codimd.communecter.org/uploads/upload_64979c71935d7c706c83aaf20da74493.png)

#### Une coopérative d’acteurs unis pour créer des solutions humaines et locales
<div style='text-align:left;font-size:25px;'>
Dans le but de préserver les communs qu’elle construit, la communauté des acteurs partageant l’usage et la gouvernance de ces communs ce sont réuni autour d’une coopérative d’intérêt collectif, un  laboratoire et une cellule active de recherche et de développement.
</div>

---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_ea9e6ca433e28f797888b9048c14a9b0.png" -->

![](https://codimd.communecter.org/uploads/upload_fae9055fb81ead6ddd3ee15a2f893a7e.png)

---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_ea9e6ca433e28f797888b9048c14a9b0.png" -->

![](https://codimd.communecter.org/uploads/upload_54fc424505027d537c9f85650e9d1f2c.png)

---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_ea9e6ca433e28f797888b9048c14a9b0.png" -->

![](https://codimd.communecter.org/uploads/upload_64979c71935d7c706c83aaf20da74493.png)
### CO-créateurs de logiciels libres
### social . sociétal . territorial
<span style='color:#E5374F;'>CO</span> . <span style='color:#E5374F;'>COSTUM</span> . <span style='color:#E5374F;'>COFORM</span> . <span style='color:#E5374F;'>COBSERV</span> . <span style='color:#E5374F;'>COCITY</span> . <span style='color:#E5374F;'>CO.TOOLS</span> . <span style='color:#E5374F;'>SMARTERRE</span> . <span style='color:#E5374F;'>OCECO</a>

---

![](https://codimd.communecter.org/uploads/upload_2adc2ca335a4cfc50945d7f395daf81c.png)


#### Un réseau social libre et ouvert, <br/>un commun numérique et logiciel libre
<div style='text-align:left;font-size:25px;'>
« COmmunecter » est une boîte à outil citoyenne collaborative, ouverte à tous, un réseau sociétal innovant, open source et gratuit, de développement de territoire avec une approche locale (quartier, arrondissement, commune, ville...). Un COmmun numérique disponible sur des applications mobiles et internet permettant de participer à la vie de son territoire avec pour objectif de CO-construire les territoires de demain ! 
</div>

---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_48200b8936a7d76ebbff9442430aba42.png" -->

![](https://codimd.communecter.org/uploads/upload_7b0d40830f7d258739afc8413e808cd2.png)
#### Votre COmmunecter personnalisé, Réseau social sur mesure
<div style='text-align:left;font-size:25px;'>
Conçu pour vous permettre de créer un COmmunecter spécifique à vos besoins.
Vous bénéficiez de toutes les nouveautés développées pour Communecter.org, tout en ayant la main sur de nombreux paramètres permettant de personnaliser votre plateforme

<a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://chat.communecter.org/channel/custom'>CHAT</a> . <a style='border-bottom:1px solid #E5374F;color:#E5374F;' target='_blank' href='https://docs.google.com/presentation/d/1AzYrk3YPSp4pcw5IQnSyARcX-e8pgiqhmD6mvALhMAs/edit#slide=id.g737fb454ef_0_7'>COSTUM DES COSTUMS</a>    
</div>


---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_2403193e688fd9eabaef5861c21a79ee.png" -->

![](https://codimd.communecter.org/uploads/upload_230a5c648a68cad7fb8e6383e920d441.png)
### COFORM
#### Créer des formulaires librement et connectés à toute la boîte à outils de communecter 
<div style='text-align:left;font-size:25px;'>
Tout le monde utilise Google Forms, il fallait une solution open source modifiable et surtout totalement connectée aux objectifs que se donnent les différents acteurs, institutions et territoires tout en respectant la sécurisation de la donnée récoltée. C’est pourquoi nous vous proposons les COForms pour faire des études, des appels à projet, des questionnaires…
et plus si affinité

<a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://chat.communecter.org/channel/coform'>CHAT</a> . <a style='border-bottom:1px solid #E5374F;color:#E5374F;' target='_blank' href='https://docs.google.com/presentation/d/12E7zhh-Tp7J0fRcEVRIuxpXPlbPBVBP7GB3JnycPNm8/edit#slide=id.g737fb454ef_0_7'>PPT</a>  

</div>

---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_a844445d1f1451a30839d99e4bbfd43b.png" -->

![](https://codimd.communecter.org/uploads/upload_d2a34f0151948ec6fb5de5368ede3c04.png)

### COBSERVATOIRE

#### La donnée doit être visualisée, manipulée, interconnectée pour l’utiliser à sa juste valeur 
<div style='text-align:left;font-size:25px;'>
L’observatoire est dynamique puisqu’il est directement connecté avec la récolte d’information provenant potentiellement de différentes sources. 
L’information est alors plus facile à accepter et comprendre quand on a le même niveau d’information et lorsque celle-ci  est organisée. 
C’est aussi une manière de voir tout ce qui se passe sur un territoire avec une approche sectorielle possible.

<a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://docs.google.com/presentation/d/1tfHvnmCJXg63M7ZzKNPjQYRFPVissDQGdxScV75xVFM/edit#slide=id.g737fb454ef_0_7'>PPT</a> . <a style='border-bottom:1px solid #E5374F;color:#E5374F;' target='_blank' href='https://docs.google.com/drawings/d/1Byt9oDGDWGI6Mf4DECSh6Ozr6qOR0ATHyf7-x8GiwsE/edit'>PPT</a>  
    
</div>

---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_2d04cc14c8ba72e6800b87172dc3e41d.png" -->

![](https://codimd.communecter.org/uploads/upload_9b73d3e2b30ff7bdd2a9c213629dd82f.png)

#### Une vision globale et systémique, connectée et connectant des territoires et des communautés

<div style='text-align:left;font-size:20px;'>
Une communauté, Un collectif de citoyen avec un rêve fou : inventer leur utopie sociétale…. 

Applicable à tout territoire avec la volonté d’agir pour  le développement durable et la croissance locale. Appuyée sur une méthodologie innovante associant animation dans le réel et une plateforme numérique Libre et Open source, modélisée, co-construite et créée avec les acteurs portés par une vision glocale pour créer des territoires connectés grâce à des projets développer le territoire
    
<a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://docs.google.com/presentation/d/1aMUoqF2g2xu-VUWROjHqEUdq0DA4UxozHVBk82yrjxA/edit#slide=id.g5e5279c140_0_0'>SYNTHESE</a> .<a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://docs.google.com/document/d/1J7b3Qi3ZjR28A8mzqbkP21iwjyJOfd0AwHw9qQ3qiEc/edit'>FICHE PROJET </a> . <a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://docs.google.com/presentation/d/144-pyUfuNEbLxlX7SIkkPqAqUYjQWKC4mvVY_HQ_MvA/edit#slide=id.p'>SMARTERRE FILIERE</a> . <a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://docs.google.com/presentation/d/1SUb0CzKl23wQShwu1ynPu7HPDcT0y346Wjl9D76oXaU/edit#slide=id.p'>coup de coeur smarterre</a> . <a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://docs.google.com/presentation/d/1NZr1DMFeS3I-JYuXrGvecijvgWW4zLaVvUyaevK-jHM/edit#slide=id.g7689fc6c79_3_16'>1ere synthese methode COEUR</a>  
    
</div>

---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_53975cb3373f1d2fbf426cbc8d85152f.png" -->

![](https://codimd.communecter.org/uploads/upload_79090d57f0dc0824210b0c3b496bc5ee.png)

#### Concret et Pratique pour un territoire efficient, connecté, intelligent et apprenant

<div style='text-align:left;font-size:25px;'>
Un territoire où l’information est fluide et transparente, toutes les actions qui se développent sont transmises et partagées en permanence. Des outils en communs se développent et s'améliorent pour créer un territoire aussi ouvert que wikipedia, aussi connecté que facebook, avec une activité oeuvrant toujours pour les biens communs. Mise en application sur un territoire efficient, connecté, intelligent et apprenant
 connectés grâce à des projets développer le territoire

<a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href=' https://docs.google.com/presentation/d/1eyq2KHWZhPTmTvBajskaeFs4-jm10hBlAePEgWn1ux4/edit#slide=id.g5e5279c140_5_90'> SMARTERRE TCO </a> . <a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://docs.google.com/presentation/d/1gm0X7yhkUcElxKs5xN56kJYQ9ZIeMZohzIwsS7DyyPc/edit#slide=id.g468f6e2600_0_92'>smart region</a> 
</div>

---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_9a39aa6fc8a94e53fdff088690725c9d.png" -->

![](https://codimd.communecter.org/uploads/upload_05dcb55097115f5c6fb94758231fd314.png)

#### Une commune, des citoyens, des organisations connectés pour construire 

<div style='text-align:left;font-size:20px;'>
Des outils pour mieux décider et construire ensemble des communes qui nous ressemblent et nous rassemblent, dans lesquelles les acteurs ont accès à la connaissance, et au sein desquelles les questions sont posées avec une dynamique citoyenne interactive. 
    
Des outils pour mieux COnnaitre, COmprendre et COllaborer 

Valoriser tout le travail et l’information existante pour ensuite la compléter et l’animer sur différents axes en fonction des besoins et des objectifs de la commune. Associé aux COForms et CObservatoire, il est possible d’imaginer de nombreuses façons de présenter les résultats en fonction de ces mêmes objectifs.

    
<a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://docs.google.com/presentation/d/1e-ozTx2dV3QXTiqwWk83xnZqNdHSm_N4quUAUj6DEtk/edit#slide=id.p'>SMARTERRE 1st PPT en image</a> .

</div>

---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_abdd0863e27c3f8efe9c24b404c59a41.png" -->

![](https://codimd.communecter.org/uploads/upload_9a4f55443ce0504c8b1939592429bf74.png)

#### Nouveaux outils et méthodes de travail Collaboratif d’Économie COopérative

<div style='text-align:left;font-size:20px;'>
Améliorer des dynamiques collectives de travail via une nouvelle technologie numérique d’aide à la décision et au management des hommes et de l’organisation.
Expérimenter un processus favorisant l’intrapreneuriat, la clarté des rôles et la gestion de l’activité à travers un processus simplifié appuyé sur une méthodologie solide.
Favoriser l’implication des collaborateurs et le changement de posture
Rendre la méthodologie et les outils disponibles dans le but d’essaimer le projet et la démarche.
Faire évoluer les activités vers de nouvelles formes de collaboration et d’organisation du travail plus agiles et innovantes.
    
<a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://docs.google.com/presentation/d/1tXM5WJK2PsWIvBNCpCwNJiTKwij40X3_LFnvg_8Hxn8/edit#slide=id.gb44acf559e_3_0'>PPT</a> . <a style='border-bottom:1px solid #E5374F;color:#E5374F;'  target='_blank' href='https://docs.google.com/presentation/d/1sgDu7mya0yq9PgDOj0KuKEFc77j3aevuuuyzO7qSLZE/edit#slide=id.g87ac77bfdb_0_0'>CO.OP TOOLS</a> .

</div>

---



## CO TOOLS
https://docs.google.com/presentation/d/1Vhp1F-a4vP42M-DhJ9QgRX8xltKQFtrOQUhDwsPRHCw/edit#slide=id.g7863567aa2_0_227

---

# DEVELOPPEZ COMMUNECTER XX

---

# USAGES

---

## APPEL à PROJET 
- https://docs.google.com/presentation/d/17kT4wzcz0BEc4qwbCBXNsURu9IIxMs7IKLalSj2Fjzc/edit#slide=id.g60be068e42_0_26
#### COSINDNI 
https://docs.google.com/presentation/d/1qWjFI0x4jWXb-D9Ky2s14ZnjJIxArId9SCF9xZThKbI/edit#slide=id.g9e45ea9246_0_68 
#### CTE

---

## MONDE ASSOCIATIF 
#### NOTRAGORA
#### ANNUAIRE ASSO
https://docs.google.com/presentation/d/1P3T7PwIH5omlgVxwjQnrNORP3mg98EEs58ibztVcfc8/edit#slide=id.g503d440c51_0_0

---

## DEVELOPPEMENT TERRITORIAL
#### LOGEMENT DEAL 
https://docs.google.com/presentation/d/1bSJkGhn4jrlS2ZyRZIzmQz7-XZ3XXJWXjG4uTwVYaT4/edit#slide=id.g75771d5559_0_124

---

## OBSERVATOIRE TERRITORIAL ET THéMATIQUE
#### CTE
#### SOMMOM
#### SMARTERRE 

---

## CITOYENS : Actions & Participation
#### PACTE TRANSITION
#### COCITY 

---

## GOUVERNANCE et MANAGEMENT
#### FACT OCECO
#### COCITY
#### SMARTERRITOIRE

---

## CONNAISSANCE
#### SOMMOM
#### CTE
#### OCECO

---

## SCIENCE : Participative et Standardisation
#### SOMMOM

---

## OBJECTIF ZERO GAFAM
#### COTOOLS

---

# OPEN ATLAS OPAL 
- TOUT CO OPAL SMARTERRE https://docs.google.com/presentation/d/157Sj4k3j_L_eJODjSla4Gm5ODJ-8e-ysvp0OgxEYnd4/edit#slide=id.g6d5083dd8a_0_731
- PPT Presentation Open Atlas - Opal - CO https://docs.google.com/presentation/d/1hk2-RymfV2zeLA0hpzapgc3tl25xgu4n/edit#slide=id.p15
- WHY CO LIBRE ET OUVERT https://docs.google.com/presentation/d/1nc8OKa2ZECBWvzAxLO3xRBKP6LQ8V5vY2LlYKHnF9sI/edit#slide=id.g47b9860e3e_0_79

---

# documenter [chaque brique](https://docs.google.com/presentation/d/1P3T7PwIH5omlgVxwjQnrNORP3mg98EEs58ibztVcfc8/edit#slide=id.ga02923e6b3_0_400) 

---

# ParCOurs en costum
Comprendre et apprendre Communecter 
- objectif bullet points 
- communauté expérimente
- projet exemple 
   

---

# par fonctionalités

---
### Co Prez + feature presentation  
### Costum Prez + feature presentation  
### Smarterritoire Prez + feature presentation  
### Coform Prez + feature presentation  
### Cobservatoire Prez + feature presentation  
### Oceco  Prez + feature presentation  

---

<!-- .slide: data-background="https://codimd.communecter.org/uploads/upload_ea9e6ca433e28f797888b9048c14a9b0.png" -->

# Nos metiers    

---

# `Developpement spécifique et innovant`
Notre métier principal est de répondre aux demandes de sites personnalisés et sur mesure basés sur COmmunecter sur des temps courts pour expérimenté rapidement grace à tout les modules  construits jusqu'a maintenant (COstum, Coform, Oceco, Cobservatoire... )

---

# `Une Boite à outils citoyennes`

---

# `Développement social et territorial`

---

# `Accompagnement au montage de projet des collectivités`
### session de brainstorm
### veille d'outillage 
### conseil en innovation 
### proposition d'animation avec un catalogue d'acteurs locaux 
### outillage générique ou sur mesure 

---

# `Accompagnement Tiers Lieux`
### bouquet d'animation avec un reseau d'animateur locaux 
### evennements itinérants
### modele de financement 
### dynamique de communication
### outils de fonctionnement

---

# `Accompagnement Filière`
### Developpement de la connaissance thématique et sectorielle
### Structurer des Animations ciblés
### Diagnostique mutliple de tout genre (filière, personnel, organisationnel, )

---

# `Formation`
### Formation en autonomie de CO
### Formation au developpement de CO

---

# `Structuration`
### coConstruire et outiller les organisatoins et les collectivités
- reseau sociaux personnalisé 
- formuliare et recolte d'information 
- observatoire thématique  

---

# `Développement territorial`
### Créer du lien sur les territoires
### Une intelligence collective par filière
### Un modèle de commune autogéré et participatif 
### donner de la visibilité des projets locaux 
### Innover l'action social, sociétal, territorial
### Développement d'observatoire territorial et thématique 

---

# `Vous êtes`
## Générique et systémique, le projet correspond au fonctionnement de nombreux contexte

---

# `Vous êtes citoyen`

### vous voulez experimentez une commune sans élus
### construisez l'annuaire local
### vous maitrisez dans un domaine 
### vous souhaitez expérimentez autrement le fonctionnement local
### vous gérer ou etes membres d'une organisation 


---

# `Vous êtes une organisation, un projet`
association, coopérative, groupe libre, entreprise
### profitez d'outil libre et ouvert (non Gafam)
### structurer 
### faite vous connaitre
### créer du lien localement 

---

# `Vous êtes filière`
communauté, réseau, secteur, thématique
### produisez de la connaissance 
### organisation et gouvernance horizontale
### transparence
### visibilité et partage

---

# TODO
- convertir les Google Slide
    - [smarterre](https://docs.google.com/presentation/d/1e-ozTx2dV3QXTiqwWk83xnZqNdHSm_N4quUAUj6DEtk/edit#slide=id.g4829d1c67b_1_0)
    - [why co](https://docs.google.com/presentation/d/1KdqpJ1T9Edb1RYQyhdJvtevfjA-FAU6xJmG8L7DS-04/edit#slide=id.g9930ec6667_3_52)
    - [architecture](https://docs.google.com/presentation/d/1YpjSqWYd285QyikzY7-CcqfQDibhu1XxGD8IClRoew8/edit#slide=id.g503e9e3bd2_0_355)    

---

# PRESENTATION / DEVIS

---

# HASHTAG DRIVEN GENERIQUE 
https://docs.google.com/presentation/d/1c7cYfbDz_dfT_yTVm63IOw62hYxuWlcUwRbo2unEV30/edit#slide=id.g77077d4657_0_17

---

# COSCIENCE 
- https://docs.google.com/presentation/d/1BF3rBnyfMHKjTjn53ShyT_tvEFe6D0iUD3BhqJ1irtI/edit#slide=id.g737fb454ef_0_7
- PPT https://docs.google.com/presentation/d/17e5hN7xUAqzmCXo4joUMdlFhbJ4gNKiyrzmWYGDZfnU/edit#slide=id.g7f2715dafa_5_0
- DOC https://docs.google.com/document/d/1JtxXpTbQXATHni3Fz96TE6tr216uxNdTJlQz16kbNCA/edit?usp=drive_web&ouid=111559540869540305126

---

# COSEARCH 
https://docs.google.com/presentation/d/1aqPZmz2aI1XyK6iubAnyyu4DANJ7kP5Mml1OWL1qLzw/edit#slide=id.p

---

# COEUR
- ESS 
    - https://docs.google.com/presentation/d/1P3T7PwIH5omlgVxwjQnrNORP3mg98EEs58ibztVcfc8/edit#slide=id.g503d440c51_0_0
- CULTURE 
    - KILTIR https://docs.google.com/presentation/d/1TE8v2oicBqdDQb-ppZgeQiq_D9Z_t1A1GEhqGz7uQnA/edit#slide=id.g3802b17b5_110
    - PRRA https://docs.google.com/presentation/d/1TE8v2oicBqdDQb-ppZgeQiq_D9Z_t1A1GEhqGz7uQnA/edit#slide=id.g4f648fe76e_0_624
- INNOVATION https://docs.google.com/presentation/d/1iGj8Sun020Yu84B3y28PJ6X0xMVl6JBoJgqwcHk8Di0/edit#slide=id.g4f648fe76e_0_624
- COEUR NUM 
    - PPT https://docs.google.com/presentation/d/1F13Wtt-f5w8hAVIyXwE8Ritq7HtoQsAQEf_qmL24q7g/edit#slide=id.g3841fbb692_0_15
    - DOC https://docs.google.com/document/d/1pALaL8QUURWV8MI7Atr5xvl1YVZKBJGF2bE3L4jn1bM/edit
- OCEANO
    - https://docs.google.com/document/d/1pALaL8QUURWV8MI7Atr5xvl1YVZKBJGF2bE3L4jn1bM/edit

---

# COEVENT 
https://docs.google.com/presentation/d/1rZpyxlMoPlhRtCDEfXjsbhImjfPa-9wJ8qT7TxZ5zbw/edit#slide=id.g737fb454ef_0_7

---

# CO JOURNALISME 
https://docs.google.com/presentation/d/1HNNDSfn-tDetuc5O1iGB3iMWxmK2jB7XeKhx1o4MWEQ/edit#slide=id.g737fb454ef_0_7

---

# KILTIR LAB 
https://docs.google.com/presentation/d/1JYVjxaXOS5lYfIK_oBYR7NOg2k6GIZywJqWxqZhSa9Q/edit#slide=id.g2394dc2e60_0_15

---

# REssources 
- COPIPI https://docs.google.com/drawings/d/1hvDIRN3u4kvuo6-N7twFrjQ8uidEmXuSzm3dli_JTmg/edit
- REBESECO https://docs.google.com/drawings/d/17L1sZ0so3BHoV8fNclsCio9HflaAVkKP_Q1XhuUBhn4/edit

---

# COPI TIERS LIEUX 
https://docs.google.com/presentation/d/1efQiAdOt54_XoxJaYZPazxCK0T83jdgdmer-le9NwDY/edit#slide=id.gdd654f576_0_6

---

# MEDNUM 
- PPT https://docs.google.com/presentation/d/1bhTJvYOMbf9ypWfD9MKg4UvF5YSsdUh-qzYfMLTdtv8/edit

---

# COstum des communs 
https://docs.google.com/document/d/1mFBmLjBlk6Iyvs_-nZtIxvS0Jf_XeJhaW6g_Vs7z9zA/edit

---

# SMARTERRE
- TCO 
    - INSTITUTION https://docs.google.com/presentation/d/1tA4YSTR4EGENGj5L0i6RdDGhy0NZk5ttSqAO_xB_vJg/edit#slide=id.p1
    - https://drive.google.com/drive/search?q=smarterre%20TCO

---

# Dossier Type
https://drive.google.com/drive/u/0/folders/1-B_H4e56b6n9fREUEOjjrXTQZ_5NtIY3

# First stepper
[Schema](https://docs.google.com/drawings/d/1cf8B9g5e-uqckUCYx0CmA3I9T-k8iXhDu5jN0uiPTUI/edit)
