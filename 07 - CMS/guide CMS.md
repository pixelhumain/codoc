# Guideline CMS Costum

## I - Configurer son costum
![](https://)![](https://codimd.communecter.org/uploads/upload_020eb376dabac238e160f2f6a7a0e87f.png)


### A - Les infos générales de son costum ![](https://codimd.communecter.org/uploads/upload_1e304cf99087d99201aec330723cca3a.png)

Un costum nait d'une organisation, d'un projet ou d'un événement qui souhaite développer son propre site qu'on appelle aussi CMS.
Par défault, le costum a les informations de cette éléments : Logo, titre, infos pour les méta de référencement 

### B - Construction générale du site
![](https://codimd.communecter.org/uploads/upload_c000e34babb07c64eab494d0d9ef0fdd.png)

- [ ] Pages : Définition et gestion de toutes les pages du site de l'accueil aux mentions légales

- [ ]Menus : vue compléte de la maquette du site et de ses menus
Pour la construction d'un menu, allez directement à la section II "construction d'un menu" 

- [ ] Element Ajoutable : Tous les elements à activer et définir que vous voudrez ajouter sur votre costum : les projets, les événements, les acteurs d'un territoire et bien d'autres encore...

### C - Design global du site [ manda nicos]
![](https://codimd.communecter.org/uploads/upload_04af114b5ce5c42865ca35dcce16a6c3.png)
- [ ] Habillage : style et configuration des loader, progress, menu(en haut ou à gauche), couleur general du site, police et contenu (titre,text,lien) du site
- [ ] Loader : les types d'animation pendant le chargement de la page
- [ ] Style css personnalisé : code css specifique du site 

### D - Réglages de la communauté


## II - Création d'un menu [manda / nicoss]


## III - Création d'une page
Sumurry : 
Créer une page et l'ajouter au menu, 
Afin d'y ajouter vos blocs, 
Et même utiliser un template de page qui génére un ensemble de bloc 
### A - Initialisation d'une page
#### A1 - Créer une page
#### A2 - Editer une page
#### A3 - Ajouter la page au menu 
#### A4 - Restreindre l'accès à une page de mon costum 
L'accès à une page peut être restreinte à ma communauté suivant le lien de l'utilisateur à mon organisation ou mon projet.
On peut rendre accessible une page uniquement aux :
[ ] membres de la communauté  
[ ] administrateurs de la communauté
[ ] Ou encore aux membres qui ont un rôle spécifique
![](https://codimd.communecter.org/uploads/upload_1cd420cac36e491ef5894f2cf381e1a0.png)


### B - Construire la page avec des blocs
#### B1 Créer une section 
Pour utiliser les elements blocs CMS. Il faut:
- Cliquer sur le bouton plus ![](https://codimd.communecter.org/uploads/upload_4b39ec56d4bc1353b3e15125e86c099b.png) sur une section ou sur le bouton "**Ajouter un bloc**" qui situé à gauche.
- Choisissez et faites gliser  vers la page. 
- Cliquez sur un élément pour ouvrir sa menu de configuration,
- Configurez selon vos besoin.

Voici une exemple d'une section qui contien de titre et de texte avec une photo de fond. 
![](https://codimd.communecter.org/uploads/upload_c72d3a1788d2e78bc38c117895aebcf4.png)
 
[Voir sur un video tutoriel](https://peertube.communecter.org/videos/watch/57668e80-a642-40c9-bc20-65c655891cff)
### B2 - Choisir la structure de la section.
Sur un section, il y a la liste des structures : 
![](https://codimd.communecter.org/uploads/upload_93f34568c47a09ecc14537eddd49ad57.png)

- Choisir et cliquer pour obtenir la colonne structuré,
- Remplir les selon vos besoin.
#### B3 - Effectuer du Copier/coller de bloc en bloc.
- En cas de contenu répétitif,
- Cliquez sur bouton **Copier** en bas des éléments en question,
![](https://codimd.communecter.org/uploads/upload_c2b44da6392e23de4447391baf263403.png)

- Allez vers l'endroit où vous voulez le coller,
- Cliquez sur bouton  **Coller** en bas des éléments où vous voulez placer.

Voici une exemple d'une section qui est créée à l'aide des 4 colonnes avec du texte. 
![](https://codimd.communecter.org/uploads/upload_b3f1ce493678664ef5e4c71863940f0d.png)

[Voir sur un video tutoriel](https://peertube.communecter.org/videos/watch/534ea756-3757-4b24-8f6e-de86fc88f1e8)
#### B4 - Comment personnaliser sa section à l'aide d'un texte et d'une image?
Pour commencer :
- Ajouter une section,
- Choisir la structure du section, 
- Glisser le texte et l'image vers la colonne. Selon le design que vous voulez,
- Cliquez sur un élément pour ouvrir sa menu de configuration,
- Configurez selon vos besoin.

Voici une exemple d'une section a 2 colonnes qui contien du texte et de titre avec couleur de fond à gauche et une image rond avec du bordure à droite.
![](https://codimd.communecter.org/uploads/upload_465df9419bd79029606b1531c7a20a94.png)

[Voir sur un video tutoriel](https://peertube.communecter.org/videos/watch/55b6420b-8b9b-4036-9451-9bda4a963a02)

#### B5 - Ajouter un bouton dans une section
Deux types de boutons le simple et le bouton avec les actions préfigurer (popup) - to describe 


## IV - Editer un block

### A - Sélectionner un bloc a édité
#### A1 - Via un click sur le bloc en question
- Au survol d'un bloc il aura une bordure en bleu, il faut juste un click pour passer à son édition. 
![](https://codimd.communecter.org/uploads/upload_814a5fbc158ca332e6f9528059a69c8f.png)
#### A2 - Via un click l'utilisation de la couche des blocs
- Ça se fait en trois étapes :
        - Cliquer sur le bouton "Ajouter un bloc" pour ouvrir le panel de gauche.
        - Accéder à l'onglet couche.
        - Sélectionner le bloc a modifié dans la couche.


![](https://codimd.communecter.org/uploads/upload_6d3a2940a977e19dd935c17eba4b79ae.png)

- Vous pouvez aussi appuyer sur la touche "Tab" ![](https://codimd.communecter.org/uploads/upload_522152a5aa2950f55eff1ce90a79bc23.png) pour accéder directement à l'onglet couche


#### A2 - Le bouton sélectionner un parent aussi permet d'accéder à l'édition du bloc parent.
![](https://codimd.communecter.org/uploads/upload_c1d08a6809f1ed9de4acc50a49d50139.png)

### B - Le panel d'édition d'un bloc

- Après avoir sélectionné un bloc a édité, le panel de droite va s'ouvrir, elle contient toutes les paramètres existant pour le bloc en question.

    ![](https://codimd.communecter.org/uploads/upload_1a489bf8158aba2c320559549221ddb2.png)
    
### C - Les options possible pour une bloc

- Après avoir sélectionné un bloc a édité, il y a des manipulations possibles à faire sur un bloc.

![](https://codimd.communecter.org/uploads/upload_25f070bb985ed20382c95286c47768f9.png)

![](https://codimd.communecter.org/uploads/upload_8ea71d242a4b7f0c74d8469cf834f1d3.png) - Deplacer vers le haut  : Pour déplacer un bloc vers le haut

![](https://codimd.communecter.org/uploads/upload_b5ce8dfe1d00e6de2380621b0eea135d.png) - Deplacer vers le bas : Pour déplacer un bloc vers le bas

![](https://codimd.communecter.org/uploads/upload_f244747bff72f7bd9546ec9a129bcb8c.png) - Éditer : Pour accéder à l'édition du bloc

![](https://codimd.communecter.org/uploads/upload_c228a5648a3389d20388c54109af5aeb.png) - Sélectionner le parent : Pour sélectionner le parent du bloc

![](https://codimd.communecter.org/uploads/upload_13eaddc276dc5927771dee5ce1ef1828.png) - Dupliquer : Pour dupliquer un bloc

![](https://codimd.communecter.org/uploads/upload_984a0b6df4a2b27fb272e6e2ba9800af.png) - Copier : Pour copier un bloc

![](https://codimd.communecter.org/uploads/upload_defed7075f0464d3c63b390761d6e5f4.png) - Coller : Pour dupliquer un bloc copier 

![](https://codimd.communecter.org/uploads/upload_4bdc540aab8a411f138e8ba47325d512.png) - Ajouter une section : Pour insérer une nouvelle section en bas de la section

![](https://codimd.communecter.org/uploads/upload_b7ad915cfe72a4c838a1065584cdeb74.png) - Enregistrer : Pour enregistrer une section en tant que blocCms réutilisable 

![](https://codimd.communecter.org/uploads/upload_eddf417ed6dab47eeb93e24f4c6b0268.png) - Supprimer : Pour supprimer un bloc

![](https://codimd.communecter.org/uploads/upload_dd68eb7e4013e6c1a2bcdd186ec30e20.png) - Désélectionner : Pour désélectionner un bloc et sortir de son mode édition 



## V - Responsive Edition
Il est possible d'avoir des configurations responsive pour certains config d'un block 
[Voir sur un video tutoriel](https://peertube.communecter.org/videos/watch/6024b1aa-ceea-4875-81a5-eb37c9330912)

Pour cela il faut : 
- Changer de mode de vue et faire les modifications  : 
    Pour changer de mode de vue il existe deux façons : 
        - En utilisant le bouton de changement de vue à côté de chaque propriété Css possédant une configuration responsive

    ![](https://codimd.communecter.org/uploads/upload_b4b95aaaa8dd84f248228d1632313e99.png)
        - Ou en utilisant le bouton en haut 
    ![](https://codimd.communecter.org/uploads/upload_9ab3dd04c5ddf6330e1bc8f1c641cbef.png)


