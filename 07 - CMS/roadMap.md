# ROADMAP

## NOUVEAUTÉ ÉDITEUR cms : Janvier 2024

### Process d'accompagnement à la création de votre COstum (First Stepper)
On vous accompagne étape par étape à la création de votre nouveau costum.
Déroulez Question par question tout ce qu'il faut pour démarré votre COstum avec le plus simplement possible

### Internationalisation et Traduction Automatique DeepL
Ca y'est nos Costums sont internationalisable vous pouvez traduire manuellement ou...
en un simple click grace à DeepL (pour les texte seulement)

Voici les blocks concernés
- Selectionnez les langues que vous voulez traduire et afficher ( liste de toutes les langue de DeepL )
- Block Titre (trad auto/trad manuel)
- Block Bouton (trad auto/trad manuel)
- Block Bouton Open Modal (trad auto/trad manuel)
- Block superText (trad auto/trad manuel)
- Block video/image (trad manuel)
- Changement de langue (Information Générale ou ajouter un bouton langues dans le menu) et vérifier si les trad correspondent à la langue sélectionner
- Traduction des menus (les labels des menus changent en fonction des langues)
- Traduction automatisé avec DeepL

### Versionez vos COstums
On nous demandait souvent comment revenir à une ancienne version ? 
C'est possible maintenant avec un système de versionnage.
Vous pourrez : 
- Enregistrer une version de costum
- Restauration d'une ancienne version

### Widgetisation des blocks (coinputs)
Pour rendre plus ergonomique l'édition des paramètres des blocks
ils ont tous été reprit pour pouvoir etre édité dans le menu droite à la place des dynforms

### Responsivité a l'édition des blocs
L'éditeur CMS fonctionne partout à présent 
- Desktop
- Tablette
- Mobile

### Raccourcis clavier
Pour les amoureux du clavier, on commence à ouvrir la porte de la navigation clavier 
Voici les 1ers raccourcis :
- `Effacer` (DELETE), Suppression d'un bloc sélectionné
- `ESC` , Quitter l'édition d'un bloc et fermer tous les menus d'édition
- `TAB` Accéder directement à l'onglet couche des blocs
- `flèche HAUT` , Déplacer un bloc sélectionné vers le haut
- `flèche BAS` ,Déplacer un bloc sélectionné vers le bas

### Selecteur de Layout d'une nouvelle Section
A la création d'un nouvelle section, on vous propose pour choisir une mise en page en un click et accélerer la mise en place de votre page 

### Nouveau Catalogue des Template et des BlockCMS
Choisir un Template pour votre costum 
ou selectionner un BlockCMS pour une page n'a jamais été aussi simple et agréable 
Venez découvrir le nouveau catalogue avec son nouveau Look and Feel. 
