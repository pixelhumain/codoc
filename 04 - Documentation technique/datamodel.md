ctk # data Model

## elements structures

### Person
description : le modèle Person pour décrire une personne et fait le lien avec toute les connection et activité 
ctk model : /modules/citizenToolKit/models/Person.php
collection : citoyens 
json document : 
```
{
	"@context" : {
        "@vocab" : "http://schema.org",
        "ph" : "http://pixelhumain.com/ph/ontology/"
    },
    "_id" : ObjectId("xxxx"),
    "modified" : ISODate("2023-03-27T13:36:48.000Z"),
    "name" : "<name of Person>",
    "username" : ""<unique slug connected to slugs collections>"",
    "slug" : "<unique slug connected to slugs collections>",
    "tags" : [ 
        "Transports", 
        "Environnement", 
        "Déchets",
        "xxxx"
    ],
    "socialNetwork" : {},
    "address" : {
        "codeInsee" : "97423",
        "addressCountry" : "RE",
        "postalCode" : "97426",
        "addressLocality" : "LES TROIS BASSINS",
        "streetAddress" : "45C Chemin des xxxx",
        "@type" : "PostalAddress",
        "level1" : "58be4af494ef47df1d0ddbcc",
        "level1Name" : "Réunion",
        "level3" : "58be4af494ef47df1d0ddbcc",
        "level3Name" : "Réunion",
        "level4" : "58be4af494ef47df1d0ddbcc",
        "level4Name" : "Réunion",
        "localityId" : "54c0965cf6b95c141800a526"
    },
    "phoneNumber" : "",
    "preferences" : {
        "sendMail" : {
            "source" : [ 
                "communecter"
            ]
        },
        "directoryView" : "list"
    },
    "profilThumbImageUrl" : "",
    "invitationDate" : [ 
        NumberLong(xxxx), 
        NumberLong(xxxxx)
    ],
    "updated" : <timestamp of update>,
    "pwd" : "<hashed pwd>",
    "collections" : { 
        "favorites" : {
            "events" : {
                "<id>" : <date>
            },
            "organizations" : {
                "<id>" : <date>
            }
        }
    },
    "actions" : {
        "proposals" : {
            "<id>" : {
                "comment" : "comment"
            }
        },
        "surveys" : {
            "<id>" : {
                "comment" : "comment",
                "voteUp" : "voteUp"
            }
        },
        "comments" : {
            "<id>" : {
                "voteUp" : "voteUp"
            }
        },
        "news" : {
            "<id>" : {
                "voteUp" : "voteUp"
            }
        },
        "actionRooms" : {
            "<id>" : {
                "comment" : "comment"
            }
        },
        "needs" : {
            "<id>" : {
                "comment" : "comment"
            }
        },
        "answers" : {
            "<id>" : {
                "comment" : "comment"
            }
        }
    },
    "skype" : "",
    
    "links" : {
    	// links are described here modules/citizenToolKit/models/Link.php
    	//person2personfollowers
        "followers" : {
            "<id>" : {
                "type" : "citoyens"
            },
            
        },
        //person2personfollows
        "memberOf" : {
            "<id>" : {
                "type" : "organizations",
                "roles" : [ 
                    "commoner"
                ]
            },
            
        },
        //person2events
        "events" : {
            "<id>" : {
                "isAdmin" : false,
                "type" : "events"
            },
            
        },
        //person2projects
        "projects" : {
            "<id>" : {
                "type" : "projects"
            },
            "<id>" : {
                "type" : "projects",
                "invitorId" : null,
                "invitorName" : null,
                "isInviting" : true
            },
            
            "<id>" : {
                "type" : "projects",
                "isAdmin" : true
            },
            
        },
        //person2personfollows
        "follows" : {
            "<id>" : {
                "type" : "organizations"
            },
        },
        //<links to projects Person contributes to>
        //person2person
        "friends" : {
            "<id>" : {
                "invitorId" : "<id>",
                "invitorName" : "<name>",
                "type" : "citoyens"
            },
        },
        //<links to projects Person contributes to>
        //project2element
        "contributors" : {
            "<id>" : {
                "type" : "answers"
            }
        }
    },
    "ph:created" : NumberLong(1425985772),
    //<links to projects Person contributes to>
    "gamification" : {
        "created" : ISODate("2022-06-02T12:54:56.000Z"),
        "total" : 1019.9,
        "actions" : {
            "total" : 14.4,
            "voteDown" : 0.4,
            "voteUp" : 13.8,
            "reportAbuse" : 0.2
        },
        "links" : {
            "total" : 1005.5
        }
    },
    
    "geoPosition" : {
        "type" : "Point",
        "coordinates" : [ 
            55.270053, 
            -21.125263
        ]
    },
    "email" : "<email of Person>",
    "geo" : {
        "latitude" : "-21.125263",
        "longitude" : "55.270053",
        "@type" : "GeoCoordinates"
    },
    "roles" : {
        "standalonePageAccess" : true
    },
    "numberOfInvit" : NumberLong(9),
    "badges" : [ 
        {
            "name" : "crowdfunder",
            "date" : ISODate("2016-06-15T11:09:48.000Z")
        }, 
        {
            "name" : "opendata",
            "date" : ISODate("2016-08-24T10:09:12.000Z")
        }
    ],
    "profilMediumImageUrl" : "<filepath>",
    "positions" : "",
    "profilMarkerImageUrl" : "<filepath>",
    "lastLoginDate" : <timestamp>,
    "birth" : "<date>",
    "url" : "<other url>",
    "profilImageUrl" : "<filepath>",
    "inter" : true,
    "language" : "fr",
    "loginTokens" : [ 
        {
            "createdAt" : NumberLong(<timestamp>),
            "hashedToken" : "<hash>",
            "type" : "personalAccessToken",
            "name" : "<external application name>",
            "lastTokenPart" : "d65946"
        }
    ],
    "oceco" : {
        "notificationPush" : true,
        "notificationEmail" : false,
        "notificationAllOrga" : true
    },
    "collection" : "citoyens"
}
```

### organizations
description : 
ctk model :
collection : 
json document : 
### projects
description : 
ctk model :
collection : 
json document : 
### events
description : 
ctk model :
collection : 
json document : 
### actions
description : 
ctk model :
collection : 
json document : 

### classifieds
description : 
ctk model :
collection : 
json document : 
### needs
description : 
ctk model :
collection : 
json document : 

### pois (Point of Interest)
description : 
ctk model :
collection : 
json document : 


### forms
[coforms Tech.md](../08 - COForms/coforms Tech.md)
description : 
ctk model :
collection : 
json document : 
#### inputs
description : 
ctk model :
collection : 
json document : 
### answers
description : 
ctk model :
collection : 
json document : 


### badges
description : 
ctk model :
collection : 
json document : 
### endorsments
description : 
ctk model :
collection : 
json document : 





# Generic Features collections

### cms
description : 
ctk model :
collection : 
json document : 

### costum
description : 
ctk model :
collection : 
json document : 

### newsletter
description : 
ctk model :
collection : 
json document : 

### cities
description : 
ctk model :
collection : 
json document : 

### zones
description : 
ctk model :
collection : 
json document : 

### comments
description : 
ctk model :
collection : 
json document : 

### documents
description : 
ctk model :
collection : 
json document : 

### bookmarks
description : 
ctk model :
collection : 
json document : 

# Structural collections

### slugs
description : 
ctk model :
collection : 
json document : 

### lists
description : 
ctk model :
collection : 
json document : 

### tags
description : 
ctk model :
collection : 
json document : 

### translate
description : 
ctk model :
collection : 
json document : 

### cron
description : 
ctk model :
collection : 
json document : 


### gamesmobile
description : 
ctk model :
collection : 
json document : 

### playersmobile
description : 
ctk model :
collection : 
json document : 

### questmobile
description : 
ctk model :
collection : 
json document : 
