# installation en local de rocketchat (dev)

 *Attention au préalable il faut avoir une installation fonctionnelle de communecter*

installation en local pour dev ou test de rocketchat la version modifié pour communecter

* installation de meteor
* clone du repo modifier
* installation des packages npm

cherché la branch update-* la plus avancé

``` bash
curl https://install.meteor.com/ | sh
git clone https://gitlab.adullact.net/pixelhumain/Rocket-Chat.git
cd Rocket-Chat
git checkout update-3.6.2
meteor npm install
```

copier dans un fichier > settings-dev.json

``` json
{
  "oceco": {
  	"token": "",
  	"endpoint": ""
  },
  "environment": "production",
  "endpoint": "http://localhost:5080",
  "module": "co2",
  "urlimage": "http://localhost:5080",
  "emailAdmin": ""
}
```

Démmarer le chat localement par meteor sur le port 3000

``` bash
meteor run --settings settings-dev.json
```

sur l'installation de communecter
dans code/pixelhumain/ph/protected/paramsconfig.php rajouter ou activer

``` php
'rocketchatEnabled' => true,
'rocketchatURL' => 'http://192.168.1.25:3000', //rocketchat url avce l'ip
'rocketAdmin' => '', //email admin
'rocketAdminPwd' => '', //password admin
"adminLoginToken" => "", //token
"adminRocketUserId" => "", //userID admin
```

pour finir la config il te faudra un compte valide sur ton instance docker de co et te connecter avec sur le chat
et que dans le settings-dev.json la emailAdmin tu mette l'email en question et apres faudra faire un appel pour recup le token admin de ce compte sur rocketchat

``` bash
curl -H "Content-type:application/json" \
      http://localhost:3000/api/v1/login \
      -d '{ "email": "TON_emailAdmin", "password": "TON_PASSWORD" }'
```

et récupérer userId et authToken dans le retour et renseigner adminLoginToken et adminRocketUserId avec

dans la config admin rocketchat dans  > Général > Intégration iframe

* il faut activer l'envoi
* envoyer la cible d'origine : \*
* il faut activer la reception
