# Installation de Communecter via Docker
[Retour au TODO de Fluidlog](https://pad.lescommuns.org/GTQVINGDT2ClF_tlGQJ7KA)

## Installation de Communecter via docker

choose community edition CE [https://docs.docker.com/engine/installation](https://docs.docker.com/engine/installation)
example install docker ubuntu [https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu)

Erreur avec AptGet… OK
docker-ce n’était pas dispo de base, il a fallu ajouter des serveurs
En fait, j’ai du suivre le post:
[https://gist.github.com/Stoakes/a5adb36e9211964dbf00b47d36edd181](https://gist.github.com/Stoakes/a5adb36e9211964dbf00b47d36edd181)

Post-installation steps for Linux [https://docs.docker.com/engine/installation/linux/linux-postinstall](https://docs.docker.com/engine/installation/linux/linux-postinstall)


## Installer docker-compose
[https://docs.docker.com/compose/install](https://docs.docker.com/compose/install)


## Installer Communecter
```shell
git clone https://gitlab.adullact.net/pixelhumain/docker ~/pixelhumain-docker
cd ~/pixelhumain-docker
#pour linux
docker-compose -f docker-compose.yml -f docker-compose.install.yml run ph cotools --install
#ou pour windows/mac utiliser
docker-compose -f docker-compose-no-build.yml -f docker-compose.install-no-build.yml run ph cotools --install
```

## Mise à jour des droits
Modifier les droits pour pouvoir modifier les fichiers :
Pour taper ces commandes, se mettre dans le rep : `cd ~/pixelhumain-docker`
```shell
#modifier le group/user pour pouvoir ouvrir/modifier les fichiers
sudo chown -R ${USER:=$(/usr/bin/id -run)}:$USER code/
sudo chown -R ${USER:=$(/usr/bin/id -run)}:$USER code/pixelhumain/
sudo chown -R ${USER:=$(/usr/bin/id -run)}:$USER code/modules/
sudo chown -R ${USER:=$(/usr/bin/id -run)}:$USER code/log/
```

Voir aussi “Getting start” : [https://docs.docker.com/compose/gettingstarted/#step-3-define-services-in-a-compose-file](https://docs.docker.com/compose/gettingstarted/#step-3-define-services-in-a-compose-file)


## Problème d’accès
Suite à cela, je n’avais pas accès à CO.
Nous avons vérifié si un container tournait (voir paragraphe plus bas) -> non
Nous avons supprimé le répertoire code pour tout recommencer.
```shell
docker-compose -f docker-compose.yml -f docker-compose.install.yml run ph install
```

**Explication**
la il va rebuilder le container front et utiliser le container install pour faire l’install sur le front
c’est pour ça que l’on a 2 fichier :
-   docker-compose.yml
-   docker-compose.install.yml

docker-compose.install.yml ne sert que pour l’install et certaine commande
Suite à ces commandes, il clone tout ce qu’il faut en local, ça prend un certain temps en fonction de votre réseau…
Aller sur [http://localhost:5080](http://localhost:5080)
La première fois, j’ai eu des messages d’erreur d’accès…
Thomas m’a demandé si j’avais le répertoire :
`/pixelhumain-docker/code/pixelhumain/ph/vendor/`
mais je ne l’avais pas…

Nous avons de nouveau supprimé le répertoire code, et modifié le script

`/pixelhumain-docker/docker-install/Dockerfile`

pour y ajouter curl et php


## Voir la présence et lancer un container

Pour savoir si docker tourne, faire "docker ps"
S’il n’y a rien (à part le titre des colonnes du tableau), c’est signe qu’aucun container ne tourne.

Exemple : ici, il n’y a pas de container qui tourne :

```part
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

Là, il y en a :

```part
CONTAINER ID        IMAGE                     COMMAND                  CREATED             STATUS              PORTS                                                                           NAMES
92ed85af6a28        pixelhumaindocker_front   "/usr/bin/supervisord"   4 weeks ago         Up About a minute   9000/tcp, 0.0.0.0:5080->80/tcp, 0.0.0.0:5443->443/tcp, 0.0.0.0:5901->9001/tcp   pixelhumaindocker_front_1
8f16ade3a72b        mongo                     "docker-entrypoint..."   4 weeks ago         Up About a minute   27017/tcp                                                                       pixelhumaindocker_mongo_1

```

Et pour lancer un container :
`docker-compose -f docker-compose.yml up`

## Inscription en local
Une fois sur l’interface d’accueil de CO, il faut remplir le formulaire d’inscription, sauf que le mail d’activation n’est pas envoyé.
Pour passer cette étape, on “triche” avec la commande :
`docker-compose -f docker-compose.yml -f docker-compose.install.yml run ph cotools --emailvalid=email@example.com`

(en modifiant bien sur : [email@example.com](mailto:email@example.com) par l’email qui a servi à l’inscription)
On rafraîchit la page, on se loge, et ça fonctionne, on arrive dans CO !


## Gérer le démarrage auto
[https://docs.docker.com/engine/installation/linux/linux-postinstall/#configure-docker-to-start-on-boot](https://docs.docker.com/engine/installation/linux/linux-postinstall/#configure-docker-to-start-on-boot)

```shell
sudo systemctl enable docker
Synchronizing state of docker.service with SysV init with /lib/systemd/systemd-sysv-install...
Executing /lib/systemd/systemd-sysv-install enable docker
```

Malgré cela, CO ne démarre pas au lancement de ma machine.

## Accès à Communecter
Je n’ai pas encore réussi (pas essayé non plus…) à le lancer au démarrage, donc à chaque fois que je souhaite lancer CO :
-   Je vais dans mon répertoire `~/projets/pixelhumain-docker/`
-   Je lance `docker-compose -f docker-compose.yml up`
-   Je vais sur [http://localhost:5080](http://localhost:5080), et ça tourne !

Tout est noté à la fin du script ci-dessus… (je dis ça, car j’ai cherché un peu car je n’avais pas lu…)


# Ajout de Fluidgraph dans CO
-   [Ajout de Fluidgraph dans CO2](https://hackmd.lescommuns.org/GYVgJgbARgTCCMBaYoAsjUHZgAZEE4BmQgDmShExBgGMb5UBDYIA?view)


## Administration de la base NoSQL (mongoDB)
Pour avoir accès à mongo en dehors du container il faut ouvrir le port
Voir fichier : [https://github.com/pixelhumain/docker/blob/master/docker-compose.yml](https://github.com/pixelhumain/docker/blob/master/docker-compose.yml)

J’ai du ajouter la ligne concernant l’ouverture de port comme l’exemple qui suit :
```part
services:
  mongo:
    image: mongo
    volumes:
      - ./code/data/db:/data/db
    ports:
      - "5017:27017"
```

Cela m’a permis, via l’outil [Robomongo](https://robomongo.org/) d’ajouter une collection :

```JSON
{
    "_id" : ObjectId("59f1920bc30f30536124355d"),
    "name" : "DEV Config",
    "key" : "devParams",
    "mangoPay" : {
        "ClientId" : "communecter",
        "ClientPassword" : "XXXXXXXXXXX",
        "TemporaryFolder" : "../../tmp"
    }
}
```

Je ne sais pas encore à quoi ça correspond, mais on m’a dit de le faire, sinon, j’avais l’erreur :
```part
ERREUR 403
Missing Configs db.applications.key == prodParamsexists 
```

Ce qui m’a permis de faire fonctionner mon instance locale.
