[toc]

# OCECO connecté
par souci d'efficacité, oceco est au centre d'une nouvelle approche de developpment de projet au sein d'un réseau social (communecter)
donc tout organisations dans communecter peux créer sa communauté de personne, bénéficier d'un chat rocket chat, créer des projet et démarer une gestion de projet via oceco 

pour simplifier l'organisation des groupes et communecter et connecté avec la communication (le chat rocket chat) et avec OCECO
## communecter to OCECO
Quand OCECO est activer sur une organisation 
on retrouve dans OCECO tout les projets et tout les evennements de cette orga
on peut regarder la fiche de chaque projet 
on y retourve : 
- sa communauté de contributeurs
- les actions (todo list)
- agenda 

## OCECO et Rocket Chat
Oceco et le chat sont connecté créant une interaction bilatérale hyper efficace 

### un chat par projet
Chaque projet est une entité autonome public ou privé et aura un chat associé 
Quand Oceco est activé chaque projet aura une interface dans oceco.
Un message est posté dans le chat pour informer la communauté de contributeur de l'activité, des changements etc 

### notification des actions oceco dans rocket chat
un message est posté dans oceco dés que : 
- une nouvelle actions est créer 
- une actions change de status (Création, Terminé) 
- une action est assigné à quelqu'un

### slash command
creer des actions directement depuis le rocket chat 
```
/oceco-add-action #leprojet blablabalbl @toto
```
[toute les slash commandes](../10 - OCECO/oceco.md)

## OCECO Form
OCECO et dispo en version mobile ocecoMobi et en version Web OcecoForm 
la version OCECO Form permet un dynamique beaucoup plus personnalisable 
pour différents usages : 
- Gouvernance horizontale 
- Gestion de projet, priorisation 
- Transparence des financements
	+ distribution de Financement multisource
- coRémunéaration

### plein d'outil de gestion
- observatoire : voir toute la dynamique de votre oceco graphique avec des graphes personnalisable 
- kanban : un trello en mode gestion de tache
- calendrier 
- timeline Horizontale et Verticale 
- gantt 
- vue PPT

## COForm connecté à rocket chat
dans un contexte donnée on peut avoir besoin de recolté des informations au travers d'un questionnaire 
COmmunecter a créer une solution libre de questionnaire COForm totalement
On peut y associer un observatoire pour créer des représentation graphique des réponses
[Pour en savoir plus](../08 - COForms/coforms.md)
Un coform peut etre créer sur une organisation ou un projet 
### un post Rocket chat à chaque réponse au coform
Un formulaire permet : 
- d'aggréger de la connaissance 
- de faire communauté et créer du commun, l'union autour de pratique commune
- de partagé et mutualiser (tableau commun partage d'usage)
- coConstruction (brainstorm)

## Les elements partagés entre oceco et co
Tout élément dans CO peut ouvrir un oceco
il y retrouvera 
--- ses projets
--- ses events
--- sa communauté
--- ses actions
on peut activer pas mal d'option comme 
--- systeme de proposition
