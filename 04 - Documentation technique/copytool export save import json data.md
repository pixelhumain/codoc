# copytool export save import json data
https://docs.google.com/drawings/d/1ACcE-wwXnlHVUvuOh0QrARfRbuImyJC6SkmoZmipJxo/edit
Synchroniseur de data entre systeme 
- Analyse la data et la consomation rattachées à un slug 
- l'Export en JSON
- l'enregistre dans costum/data 
- montre l'état de la data sur la machine locale
- permet d'importer la data sur la machine locale


## LECTURE
http://communecter56-dev/costum/co/export/slug/xxx
basé sur la base de donnée locale
permet de lister les éléments et la consommation rattaché au slug 
les collection analyser sont 
- l'élement : orga, event, project, person
- costums
- forms
- answers


## INSTALL
basé sur le contenu du dossier costum/data/export_".$slug
permet de connaitre l'état de la data de la DB locale concernant le slug 
- différencie les documents à un ou plusieurs elements
- rapport des données installé
- bouton pour lancer insert
- Forcer une install 
http://communecter56-dev/costum/co/export/slug/xxx/install/1
voir l'etat de tout les documents
http://communecter56-dev/costum/co/export/slug/xxx/install/answers.json
http://communecter56-dev/costum/co/export/slug/xxx/install/costum.json
voir l'etat d'un document particulier

## INSERT
http://communecter56-dev/costum/co/export/slug/xxx/mode/insert/insert/costum.json 
params : insert , nom du document collection.json à traiter
import dans la DB locale les data rattaché au slug et contenu dans costum/data/export_".$slug
traiter les collection un par un 

## JSON
http://communecter56-dev/costum/co/export/slug/xxx/mode/json
permet de lire en JSON tout les datas et documents rattaché au slug

## SAVE
créer le dossier costum/data/export_xxx contenant une sauvegarde de tout les documents lié au slug 
avant de sauvegarder vous pouvez les visualiser avec /slug/xxx/mode/json
http://communecter56-dev/costum/co/export/slug/xxx/mode/json/save/1


# wishlist
- [ ] connect to an online DB instance 
- [ ] execute force insert
- [ ] make an interface to choose what is copied 
- [ ] generate a readme.md for each export folder containing steps to follow or project specifics
- [ ] manage copy to prod using existing elements
    + check and use existing element id
- updating a context
- use updated to check for differences and updates 
- on export index , list all costums of the local machine