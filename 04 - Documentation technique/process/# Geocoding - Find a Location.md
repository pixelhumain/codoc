# Find a Location

# Docs
Refactor https://docs.google.com/document/d/12bO0BGOy-YBboe2AXSTwJen7iNp6y4EGDa3fRkfoUk4/edit
Archi https://docs.google.com/drawings/d/12ALV-L50kJonApAJFH54UWuGTzsZLz4ujPe6Z7VDOXE/edit

# Process

http://communecter74-dev/co2/city/autocompletemultiscope
AutocompleteMultiScopeAction
	City::searchCity
		search in DB 
			$city = self::getTranslateLevelCity($city);
			$zonesWithTrad = PHDB::find(Zone::COLLECTION, $where, $fields);
		search in OSM : Nominatim API
			SIG::getGeoByAddressNominatim
				$url = "https://nominatim.openstreetmap.org/search.php?&format=json&addressdetails=1"


## dataMap
"level3Name" => (empty($value["address"]["state"]) ? null : $value["address"]["state"] ),
"level3" => null,
"level4Name" => (empty($value["address"]["county"]) ? null : $value["address"]["county"] ),
"level4" => null,

#### Info utile
level 1 = Pays
level 2 = Canton , région etc en fonction d'un pays , exemple Wallonie , Fallonie pour la belgique
level 3 = Région Province etc en fonction d'un pays Pour la France sa sera les régions 
level 4 = Département, Arrondissement etc en fonction d'un pays Pour la France sa sera les département 
level 5 = EPCI pour la France 

## Quand une ville n'existe pas dans le système
search in nominatim > add to cities and zones 
co2/city/save
	City::insert
		prepare all attributes
		creates zones and levels 
		check if level exists 
			Zone::createLevel
				get zone from Nominatim by name
			Zone::save

## bugs
il manque level3 sur certaines villes Italie 
  - Trieste pas de level4
    https://nominatim.openstreetmap.org/ui/details.html?osmtype=N&osmid=66502648&class=place

et Lublijana ne marche pas 
  https://nominatim.openstreetmap.org/ui/details.html?osmtype=R&osmid=1675898&class=boundary