# Mail 

mails are scheduled and saved in collection cron 
can be tested with 
```
http://127.0.0.1/co2/test/displayMail/id/5f2c0f1fe2f07eb9098b4590
```

### AJax
```
var msg="<span style='white-space: pre-line;'>"+$("#send-mail-admin #message-email").text()+"<br/></span>"+aObj.mailTo.defaultRedirect(elt, type, id);
    
var params={
    tplMail : $("#send-mail-admin #contact-email").val(),
    tpl : "basic",
    tplObject : $("#send-mail-admin #object-email").val(),
    html: msg
};
ajaxPost(
    null,
    baseUrl+"/co2/mailmanagement/createandsend",
    params,
    function(data){ 
        toastr.success("Le mail a été envoyé avec succès");
    }
); 
```

```
var financerId = answerObj.answers.murir.planFinancement[posFinance].financer;
var contactList = {};
contactList[ financerId ] = { type:"<?php echo Organization::COLLECTION?>" };
contactList[ answerObj.user ] = { type:"<?php echo Person::COLLECTION?>" };
mylog.log("answer/sendmail",contactList);
ajaxPost(
    null,
    baseUrl+"/survey/answer/sendmail/id/"+answerId,
    { 
        contactList : contactList, 
        tpl : "default",
        tplObject : "[CTE] Changement de Status Financement : "+answerObj.answers.murir.planFinancement[posFinance].title ,
        generateHTML : "costum.views.custom.mails.notifs", 
        step : "<?php echo @$answer["step"] ?>" },
    function(data){ 
        toastr.success("Un mail a été envoyé à toutes les parties prenantes afin de les notifier");
        prioModal.modal('hide');
        urlCtrl.loadByHash(location.hash);
    }
);
```


### PHP

```
CreateAndSendAction
$res = Mail::createAndSend($_POST);
```

```
$res=self::getCustomMail($res, $fromMail);
Mail::schedule($res);
```

### collection CRON 
