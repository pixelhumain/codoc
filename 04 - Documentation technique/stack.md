# stack

Au centre de COmmunecter nous avons une base de données MongoDB. COmmunecter met àdisposition une API sécurisée et Tokenisé pour ouvrir et rendre disponible les contenus open data à d’autres usages. Nous avons 2 applications principales : 
    - la version WEB, en production depuis 2016, très dynamique et en évolution permanente. Celle-ci est en PHP, HTML5, CSS3, basée sur le framework Yii et utilise fortement du javascript.
    - la version Mobile utilise la même base de donnée mais tourne sur Meteor un framework NodeJs 
L’architecture modulaire de la version Web lui donne beaucoup de souplesse et de flexibilité sans limite dans la création de nouveau module en fonction des nouveaux besoins et objectifs demandés. 
Le module Costum permet de personnaliser totalement COmmunecter en marque blanche en choisissant les fonctionnalités nécessaires en fonction des projets et des contextes. 
Le Module COforms + CObservatoire permet de créer des formulaires simples ou très complexes librement et totalement connectés à COmmunecter. Une vraie alternative aux Google Forms en Logiciel Libre et totalement intégrée au projet.
Ce projet bénéficie de nos meilleurs experts du web et des technologies temps réels qui oeuvrent dans les logiciels libres pour le web  depuis 15 ans.

De Nombreux autres modules sont décrit dans la documentation https://doc.co.tools/books/2---utiliser-loutil/chapter/fonctionnalit%C3%A9s  
