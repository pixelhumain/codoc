# Mediawiki & Communecter

## Introduction
Cette partie du module interop, cherche en cas de mediawiki existant à le connecter avec communecter et de permettre via un bouton a 
créer des éléments du type de la page du wiki, ou en cas d'élément présent de fournir le lien, enfin mais nécessitant un réglage sur le 
wiki existant de marquer le wiki avec le liens de la page co.

## Initialisation du mediawiki
Dans le sub-menu-left d'un élément on clic sur @Mediawiki .

dans MediaWikiController.php
**beforeAction()** instancie **APIMediawiki** (model de relations avec le wiki) qui est enfant de **DB** (model de relations avec la bdd) 

## **interop/MediaWiki/index**


**beforeAction()** du controller instancie **APIMediawiki** (model de relations avec le wiki) qui est enfant de **DB** (model de relations avec la bdd)

le controlleur rend une vue partiel sauf pour edit qui renvoi une string.




### L'action par default du controller **actionIndex()** 
un formulaire s'ouvre lors de la premiére connection ou l'url du wiki et son nom dans communecter son rentré par l'utilisateur.
`$this->renderPartial("interop.views.create.index");`
a la validation du form direction **actionChooseCat()** 

Si wiki deja connecté renvoi vers l'index via:
`$this->renderPartial("interop.views.default.indexMediaWiki");`

### L'action **actionChooseCategory()** 
Insére les données du formulaire (url et name) puis ouvre
`$this->renderPartial("interop.views.create.chooseCat");`

un formulaire préremplie par les données de l'api du wiki sur les catégories filtrer par regex afin de coler aux catégorie du futur menu 
soit (acteurs, ressource, projet)
(les regex son en dur dans le code faire un tableau de comparaison afin d'ajouter ou enlever des mots de trie peu étre une bonne contribution en cas de contributeur volontaire ;o) 
a la validation du form direction **actionInsertCat()**

### L'action **actionInsertCat()** 
Insére les données du formulaire le wiki en db vaut désormais:
```
 "_id" : ObjectId("idmongo"),
    "name" : "name du wiki entré par l'user",
    "parent" : {
        "mongo id du parent" : {
            "type" : "type parent",
            "name" : "name parent"
        }
    },
    "url" : "url entré par l'utilisateur + '/api.php",
    "params" : {
        "actors" : [ 
            "categorie choisit par l'user",
            ...
        ],
        "classifieds" : [ 
            "categorie choisit par l'user",
            ...
        ],
        "projects" : [ 
            "categorie choisit par l'user",
            ...
        ]
    },
    "logo" : ""
}
```
renvoi vers la vue index
`return $this->renderPartial("interop.views.default.indexMediaWiki");`

### L'action **actionMenuLeft()** 
Vas chercher les données pour le menuleft des pages du wiki en fonction du button-catégorie(acteurs,ressources,projet) clicker ou du texte de la search bar.
puis les rend dans la vue `return $this->renderPartial("interop.views.menus.pages");`

### L'action **actionPage()** 
Vas chercher les données pour la page du wiki en fonction du nom de la page.
puis les rend dans la vue `return $this->renderPartial("interop.views.page.index");`

### L'action **actionEdit()** 
Ici via le modéle ApiMediawiki.php on marque la page du wiki avec un lien communecter (!! une propriétées est a créer sur le wiki distant `|pageCo=` voir avec l'administrateur du wiki.)
renvoie une string message sur le status de l'edition.

### L'action **actionDoc()** 
renvoi vers cette page



## Relations wiki et Intégration dans communecter
Dans **A propos** il ya trois boutons qui font les liens:
Un bouton **Page du Wiki** qui redirige vers la page source du wiki.
Un bouton **Site web** qui redirige vers le site de l'élement du wiki.

Le dernier bouton peu avoir deux valeurs :
**Créer dans communecter** qui ouvre un formulaire préremplie et éditable afin de créer un élément communecter.
**Page Communecter** qui redirige vers la page communecter en relation avec l'élément du wiki.

