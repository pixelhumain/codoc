# Outils et bonne pratique

## scripts
qlqs script pour faciliter la vie de dev
au préalable il faut créer un lien symbolique local sur la machine en ligne de commande

```
cd ~
ln -s ~/path/To/Dev/Folder d7
```

### switch de branche by script
executer 
```
. ~/d7/pixelhumain/scripts/gitCheckout.sh master
ou 
. ~/d7/pixelhumain/scripts/gitCheckout.sh qa
```

### up tout les repo
créer un raccourci avec un lien sym
```
cd ~
ln -s . ~/d7/pixelhumain/scripts/gitPull.sh p
```
puis quand vous voulez tout up , executer 
```
. p
```

## Communication
Rocket Chat est notre principale outil de communication directe
#codev est le channel de dev , il est privé et interne à OPEN ATLAS 
On y partage les questions, les problème rencontrés, des histoires.
C'est l'espace de discussion commune entre tout les devs actifs sur CO.

Chaque projet a son propre channel en fonction des contextes


### Scrum Ecrit
tout les lundi et jeudi matin , tout le monde ecrit son scrum sur le channel #coscrum.
On y partage :
- ce qui a été fait 
- ce qui sera fait 

## Test unitaire sont en cours

## Processus de déploiement

### DEV
### QA
### PROD