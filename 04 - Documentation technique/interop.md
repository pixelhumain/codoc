[toc]

# Intéropérer avec Communecter 
Communecter, COmobi, Oceco, Cocolight utilise des apis pour fonctionner indépendement tout en contribuant à une meme BDD mongo commune.
on utilise l'api2 pour tout ce qui concerne le métier de communecter https://gitlab.adullact.net/pixelhumain/api2
Vous trouverez les endpoints documentés dans la collection [postman](https://gitlab.adullact.net/pixelhumain/api2/-/blob/master/API2.postman_collection.json?ref_type=heads) 

# intéropérer avec OCECO 
On doit encore améliorer la documentation mais les endpoints sont là, sont maintenus et fonctionnent
https://gitlab.adullact.net/pixelhumain/codoc/-/blob/master/04%20-%20Documentation%20technique/interop%20oceco.md

https://gitlab.adullact.net/pixelhumain/oceco/-/blob/master/private/doc/rest-oceco.md?ref_type=heads
https://gitlab.adullact.net/pixelhumain/oceco/-/blob/master/private/doc/open-api.json?ref_type=heads


# Interopérabilité avec des sources externes 
## Vue d’ensemble du chantier sur l’interopérabilité
À gauche, la liste des sources extérieurs sur lesquelles on récupère les données :
- Wikidata
- Wikipédia
- OpenStreetMap
- OpenDataSoft (la base SIRENE)
- Data.gouv
- Datanova (les enseignes La Poste)
- Pôle Emploi
- SCANR
- EDF

Au milieu, le processus de Conversion des données (détails sur le prochain schéma)
A droite, l’affichage des données converties sur le site de Communecter ainsi que des exemple d’usage de ces données par des sites extérieurs.

## Conversion des données sémantiques we interoperate with using their API
### Wikidata
For any city, We retreive main information available on Wikidata
The process is the following :
- We choose a geographic scope (a country) to filter
- We call our own semantic convert system (**[doc avaible here](/4 - Documentation technique/api.md)**) :

The convert system will interrogate the Wikidata API to get data in JSON.
The next exemple is the data for the city of Saint-Denis, capital city of Réunion island :
And convert this data in the pivot language named “PH onthology”

```
/ph/api/convert/wikipedia?url=https://www.wikidata.org/wiki/Special:EntityData/Q47045.json
```

**[Exemple Wikidata here](/4 - Documentation technique/api.md)**
Here are the mapping
<table id="bkmrk-source%E2%80%99s-data-ph-o">
<thead><tr>
<th>Source’s data</th>
<th>PH onthology</th>
</tr></thead>
<tbody>
<tr>
<td>itemLabel.value</td>
<td>name</td>
</tr>
<tr>
<td>coor.latitude</td>
<td>geo.latitude</td>
</tr>
<tr>
<td>coor.longitude</td>
<td>geo.longitude</td>
</tr>
<tr>
<td>item.value</td>
<td>url</td>
</tr>
<tr>
<td>itemDescription.value</td>
<td>description</td>
</tr>
</tbody>
</table>

- We’ll want to contributing back any extra data we can offer with COpédia (coming soon)

### DBpedia
- For any city, We retreive main information available on Wikipedia


### OpenStreetMap
For any city, we retreive main information avaible on OSM
The process is the following :
- We choose a geographic scope (a country) to filter
- We call our own semantic convert system (**[doc avaible here](/4 - Documentation technique/api.md)**) :

The next exemple is all the OSM data of the city of Saint-Louis :
```
http://overpass-api.de/api/interpreter?data=[out:json];node[%22name%22](poly:%22-21.303505996763%2055.403919253998%20-21.292626813288%2055.391189163162%20-21.282029142394%2055.381522536523%20-21.256155186265%2055.392395046639%20-21.232012804782%2055.387888015185%20-21.211100938923%2055.390619722192%20-21.199480966855%2055.382654775478%20-21.185882138486%2055.385961778627%20-21.173346518752%2055.389949958731%20-21.16327583783%2055.399563417107%20-21.14709868917%2055.405379688232%20-21.166028899095%2055.414700890276%20-21.184085220909%2055.432085218794%20-21.190290936422%2055.440880800108%20-21.195166490948%2055.462318490892%20-21.237553168259%2055.459769285867%20-21.258726107298%2055.463692709631%20-21.286021128961%2055.455515913879%20-21.294777773557%2055.419916682666%20-21.303505996763%2055.403919253998%22);out%2030;
```
Here are the mapping

<table id="bkmrk-source%E2%80%99s-data-ph-o-0">
<thead><tr>
<th>Source’s data</th>
<th>PH onthology</th>
</tr></thead>
<tbody>
<tr>
<td>tags.name</td>
<td>name</td>
</tr>
<tr>
<td>lat</td>
<td>geo.latitude</td>
</tr>
<tr>
<td>long</td>
<td>geo.longitude</td>
</tr>
<tr>
<td>type</td>
<td>type</td>
</tr>
<tr>
<td>tags.amenity</td>
<td>tags.0</td>
</tr>
</tbody>
</table>

**[Exemple OSM here](https://github.com/pixelhumain/wiki/wiki/Doc-de-l'API#exemple-openstreetmap-)**
- We’ll want to contributin back any extra data we can offer with **[COSM](/4 - Documentation technique/api.md)** (coming soon)


#### Contribuez à OSM
COSM a pour but donner à l'utilisateur une meilleur visibilité des éléments OSM d'un territoire, de pouvoir contribuez a enrichir les tags OSM d'un OSM.
COSM permet à l'utilisateur de pouvoir lister l'intégralité des éléments OSM pour un scope géographique (uniquement les villes pour le moment).
Les éléments sont afficher en bleu s'il ont un type. C'est à dire, soit :
- un tag "amenity"
- un tag "place"
- un tag "office"
- un tag "leisure"
- un tag "shop"

L'utilisateur peut à tout moment cliquer sur le bouton "Voir tous les tags" pour ... voir tous les tags de l'élément OSM.
Dans le cadre listant tous les tags de l'élément l'utiliseur peut en cliquant sur "Modifier/Ajouter un tag" ajouter ou modifier un tag. Pour on ne peut ajouter que les 5 tags cités plus haut.
L'information sera directement pusher vers la page OSM de l'élément en question.


### Data.gouv
For any city, we retreive main information of the organizations placed in this city
The process is the following :
- We choose a geographic scope (a country) to filter
- We call our own semantic convert system (**[doc avaible here](/4 - Documentation technique/api.md)**) :

The module will find all the organizations placed in the geographic scope filter and then extract all the data in the differents datasets available.
The next exemple is all the data of the different structure of Méto-France, meteorological center of France.

`https://www.data.gouv.fr/api/1/datasets/54a12162c751df720a04805a/`

Here are the mapping



<table id="bkmrk-source%E2%80%99s-data-ph-o-1">
<thead><tr>
<th>Source’s data</th>
<th>PH onthology</th>
</tr></thead>
<tbody>
<tr>
<td>slug</td>
<td>name</td>
</tr>
<tr>
<td>page</td>
<td>url</td>
</tr>
<tr>
<td>tags[]</td>
<td>tag[]</td>
</tr>
<tr>
<td>item.value</td>
<td>url</td>
</tr>
<tr>
<td>owner</td>
<td>creator</td>
</tr>
</tbody>
</table>

**[Exemple Data.gouv here](https://github.com/pixelhumain/wiki/wiki/Doc-de-l'API#exemple-datagouv-)**

### Pôle emploi
For any city, we retreive all the job offer. (no exact localisation of the job place)
The process is the following :
- We choose a geographic scope (a country) to filter
- We call our own semantic convert system (**[doc avaible here](/4 - Documentation technique/api.md)**) :

To get data with the Pôle emploi’s API, a token is needed.
The next exemple fetch all the job offer of the city of Saint-Louis.
```
https://api.emploi-store.fr/partenaire/infotravail/v1/datastore_search_sql?sql=SELECT%20%2A%20FROM%20%22421692f5-f342-4223-9c51-72a27dcaf51e%22%20WHERE%20%22CITY_CODE%22=%2797414%27%20LIMIT%2030
```

### OpenDataSoft (SIREN database)
For any city, we retreive all the organizations and the association of the SIREN’s database.
The process is the following :
- We choose a geographic scope (a country) to filter
- We call our own semantic convert system (**[doc avaible here](/4 - Documentation technique/api.md)**) :

The next exemple will fetch all the data in the SIRENE database for the city of Saint-Louis.
```
https://data.opendatasoft.com/api/records/1.0/search/?dataset=sirene%40public&facet=categorie&facet=proden&facet=libapen&facet=siege&facet=libreg_new&facet=saisonat&facet=libtefen&facet=depet&facet=libnj&facet=libtca&facet=liborigine&rows=30&start=0&geofilter.polygon=(-21.303505996763,55.403919253998),(-21.292626813288,55.391189163162),(-21.282029142394,55.381522536523),(-21.256155186265,55.392395046639),(-21.232012804782,55.387888015185),(-21.211100938923,55.390619722192),(-21.199480966855,55.382654775478),(-21.185882138486,55.385961778627),(-21.173346518752,55.389949958731),(-21.16327583783,55.399563417107),(-21.14709868917,55.405379688232),(-21.166028899095,55.414700890276),(-21.184085220909,55.432085218794),(-21.190290936422,55.440880800108),(-21.195166490948,55.462318490892),(-21.237553168259,55.459769285867),(-21.258726107298,55.463692709631),(-21.286021128961,55.455515913879),(-21.294777773557,55.419916682666),(-21.303505996763,55.403919253998)
```

Here are the mapping

<table id="bkmrk-source%E2%80%99s-data-ph-o-2">
<thead><tr>
<th>Source’s data</th>
<th>PH onthology</th>
</tr></thead>
<tbody>
<tr>
<td>fields.l1_declaree</td>
<td>name</td>
</tr>
<tr>
<td>fields.categorie</td>
<td>type</td>
</tr>
<tr>
<td>fields.siret</td>
<td>shortDescription</td>
</tr>
<tr>
<td>fields.coordonnees.0</td>
<td>geo.latitude</td>
</tr>
<tr>
<td>fields.coordonnees.1</td>
<td>geo.longitude</td>
</tr>
<tr>
<td>fields.libapen</td>
<td>tags.0</td>
</tr>
</tbody>
</table>

**[Exemple OpenDataSoft here](/4 - Documentation technique/api.md)**


### ScanR ( National Education )
For any city, we retreive main information from the national education of France
The process is the following :
- We choose a geographic scope (a country) to filter
- We call our own semantic convert system (**[doc avaible here](/4 - Documentation technique/api.md)**) :

The next exemple fetch all the actives research strutures of the city of Bordeaux :
```
https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-etablissements-publics-prives-impliques-recherche-developpement&facet=siren&facet=libelle&facet=date_de_creation&facet=categorie&facet=libelle_ape&facet=tranche_etp&facet=categorie_juridique&facet=wikidata&facet=commune&facet=unite_urbaine&facet=departement&facet=region&facet=pays&facet=badge&facet=region_avant_2016&rows=30&start=0&geofilter.polygon=(44.810795852605,-0.5738778170842),(44.817148298105,-0.57643460444186),(44.823910193873,-0.58695822406613),(44.818476638462,-0.60304723869607),(44.822474304509,-0.61064859861704),(44.824937843733,-0.61415033833008),(44.835177466959,-0.61079419661495),(44.841384923705,-0.62771243191386),(44.860667021743,-0.63833642556746),(44.871658097695,-0.63105127891779),(44.86227970331,-0.61630176568479),(44.854215265872,-0.59460939385687),(44.865671076253,-0.57646019656194),(44.869188961886,-0.57608874140575),(44.909402227434,-0.58088555560083),(44.908480410411,-0.57648917779388),(44.916666965125,-0.54773554113942),(44.889099273803,-0.53553255107571),(44.869138522062,-0.54141014437767),(44.868086689933,-0.53680669655034),(44.861267174723,-0.53784686147751),(44.848134506953,-0.53761462401784),(44.842390488778,-0.5422310311368),(44.836291776079,-0.54665943781219),(44.829021270567,-0.53642317794196),(44.822772234064,-0.53766321563778),(44.813135278103,-0.55606047183132),(44.810795852605,-0.5738778170842)
```

Here are the mapping

<table id="bkmrk-source%E2%80%99s-data-ph-o-3">
<thead><tr>
<th>Source’s data</th>
<th>PH onthology</th>
</tr></thead>
<tbody>
<tr>
<td>fields.libelle</td>
<td>name</td>
</tr>
<tr>
<td>fields.site_web</td>
<td>shortDescription</td>
</tr>
<tr>
<td>fields.geolocalisation.0</td>
<td>geo.latitude</td>
</tr>
<tr>
<td>fields.geolocalisation.1</td>
<td>geo.longitude</td>
</tr>
</tbody>
</table>

**[Exemple ScanR here](/4 - Documentation technique/api.md)**

- Datasets used :
    - Public or private research and development structures
    - Member of the university institute of France


### Datanova (La Poste)
For any city, we retreive the location of all buildings of La Poste
The process is the following :
- We choose a geographic scope (a country) to filter
- We call our own semantic convert system (**[doc avaible here](/4 - Documentation technique/api.md)**) :
The next exemple will fetch all La Poste buildings localised in the city of Saint-Louis.
```
https://datanova.laposte.fr/api/records/1.0/search/?dataset=laposte_poincont&rows=30&start=0&geofilter.polygon=(-21.303505996763,55.403919253998),(-21.292626813288,55.391189163162),(-21.282029142394,55.381522536523),(-21.256155186265,55.392395046639),(-21.232012804782,55.387888015185),(-21.211100938923,55.390619722192),(-21.199480966855,55.382654775478),(-21.185882138486,55.385961778627),(-21.173346518752,55.389949958731),(-21.16327583783,55.399563417107),(-21.14709868917,55.405379688232),(-21.166028899095,55.414700890276),(-21.184085220909,55.432085218794),(-21.190290936422,55.440880800108),(-21.195166490948,55.462318490892),(-21.237553168259,55.459769285867),(-21.258726107298,55.463692709631),(-21.286021128961,55.455515913879),(-21.294777773557,55.419916682666),(-21.303505996763,55.403919253998)
```

Here are the mapping

<table id="bkmrk-source%E2%80%99s-data-ph-o-4">
<thead><tr>
<th>Source’s data</th>
<th>PH onthology</th>
</tr></thead>
<tbody>
<tr>
<td>fields.libelle_du_site</td>
<td>name</td>
</tr>
<tr>
<td>recordid</td>
<td>type</td>
</tr>
<tr>
<td>fields.adresse</td>
<td>address.streetAddress</td>
</tr>
<tr>
<td>fields.latlong.0</td>
<td>geo.latitude</td>
</tr>
<tr>
<td>fields.latlong.1</td>
<td>geo.longitude</td>
</tr>
<tr>
<td>fields.libapen</td>
<td>tags.0</td>
</tr>
</tbody>
</table>
**[Exemple Datanova here](https://github.com/pixelhumain/wiki/wiki/Doc-de-l'API#exemple-datanova-)**

## Smart Citizen (coming soon)
- onclick : we’ll show all SCK kits for a given city

## Umaps (coming soon)
- POI’s of type geoJson, on click we show the content on our map

## WordPress RSS (coming soon)
- any WP blog’s RSS can be pluggued to an elements wall

## using an iframe

## FramaPads
- users can use Framapads from inside CO (simple Iframe)

## Copédia
### Une autre façon de voir Wikipédia ...
Accessible depuis l'url suivante : /co2/#interoperability.copedia

Dans Copédia, le but est de permettre à l'utilisateur d'avoir une autre vision d'une page Wikipédia, de faciliter sa recherche et de la rendre plus intuitive.


### Consulter la données Wikipédia
Copédia permet en selectionnant un scope géographique (uniquement les villes pour le moment) de lister tous les arcticles Wikipédia en liens avec la page Wikipédia de la ville selectionné (tout liens hypertexte renvoyant vers une autre page Wikipédia).

![](/Images/copedia.png)

Les liens Wikipédia listés sont ainsi catégorisés parmis 5 grands types :
- Evenement
- Organisation
- Lieu
- Personne
- Autre (Possède un type mais qui ne rentre pas dans les catégories précedemment citées)
- Indéfini (Ne possède pas de type)

Il est possible à tous moment de pouvoir filtrer parmis ces 6 grands types et ainsi obtenir par exemple uniquement les personnes d'une page Wikipédia.

Copédia met également les dates en relations avec certains éléments, dans une frise chronologique.

Les dates que Copédia affiche sont :
- La date de début d'un évenement (un point) ou alors toute la durée de l'évenement (si possible)
- La date de création d'une organisation
- La date de naissance d'une naissance

Pour tous les éléments listés il est possible de :
- Consulter leur page Wikipédia (en cliquant sur l'icone de Wikipédia)
- Consulter leur page DBPédia (en cliquant sur l'icone de DBPédia)
- Consulter leur page Wikidata (en cliquant sur l'icone de Wikidata)
- Consulter un résumé de la page Wikipédia (avec photo si disponible) de la page Wikipédia de l'élément (en cliquant sur le nom de l'élément)
- Effectuer une "recherche Copédia" listant l'intégralité des liens Wikipédia contenu dans la page Wikipédia de l'élément ciblé et ainsi pouvoir répéter tous le processus décrit plus haut. (en cliquant sur l'icone de Copédia)


### Contribuer à Wikipédia
Si un élément ne possède pas de type, il est possible d'ajouter vous même un type à cet élément en cliquant sur le bouton "Ajouter un type (Wikidata)" qui va permettre à l'utilisateur de pusher lui même un type directement dans la page Wikidata de l'élément parmis ces 4 grands type :
- Personne
- Lieu
- Organisation
- Evenement

## EDF
Ce document décrit les aspects techniques nécessaires pour intégrer les données d'Open Data EDF Réunion dans la plateforme Communecter, permettant ainsi une exploitation optimisée des données énergétiques disponibles.

<b>Objectifs </b>
- Faciliter la récupération des données ouvertes pour les utilisateurs de Communecter.
- Assurer une mise à jour régulière des données énergétiques sur Communecter.
- Fournir des visualisations et des analyses enrichies dans un cadre communautaire.

<b>Sources de données et formats disponibles</b>

Les données disponibles sur le site Open Data EDF Réunion sont principalement accessibles via :

- API REST : Permet de récupérer les données sous format JSON.
- Fichiers téléchargeables : CSV, Excel.
- Cartes interactives : Données géolocalisées.

Données pertinentes pour Communecter :
- Production d’électricité (par filière et en temps réel).
- Consommation énergétique (par commune, secteur, année).
- Part des énergies renouvelables.
- Installations et infrastructures (localisation, type).

<b>Spécifications techniques</b>

API Open Data EDF
- Endpoint de base : https://opendata-reunion.edf.fr/api/records/1.0/search/
- Paramètres utiles :
    - dataset : Nom du dataset (e.g., production-annuelle-delectricite-par-filiere).
    - rows : Nombre de résultats.
    - facet : Facettes pour filtrer (e.g., année, filière).
    - q : Requête pour filtrage avancé.

Exemple de requête :

```
https://opendata-reunion.edf.fr/api/records/1.0/search/?dataset=production-annuelle-delectricite-par-filiere&rows=10&facet=annee_prod&facet=filiere
```
Pipeline de traitement sur Communecter :
- Extraction : Récupération des données brutes via l'API.
- Transformation : Formatage et nettoyage des données.
- Affichage : Affichage des données dans la page filières Energie de La Réunion.

L’affichage des données d’Open Data EDF Réunion dans la plateforme Communecter permettra une valorisation des données énergétiques à La Réunion. Cela offrira aux utilisateurs une meilleure compréhension des enjeux énergétiques locaux, tout en favorisant les initiatives citoyennes et les projets collaboratifs.

