# Fédération des événements

Cette documentation présente la fonctionnalité de fédération des événements ActivityPub que nous avons mise en place sur notre plateforme. 

Objectif
---
L'objectif final de cette documentation est de permettre aux utilisateurs de comprendre le fonctionnement de notre système de fédération des événements ActivityPub, et de faciliter l'interopérabilité entre notre plateforme et d'autres instances ActivityPub. En documentant notre système, nous espérons contribuer à l'expansion de l'écosystème ActivityPub et à la diffusion des événements à travers les réseaux sociaux décentralisés.

## Table des matières

 - [Configuration de la fédération](#configuration-de-la-fédération)
      - [Recherche et suivi d'utilisateurs du Fediverse](#recherche-et-suivi-dutilisateurs-du-fediverse)
      - [Gestion des invitations à rejoindre des groupes sur le Fediverse](#gestion-des-invitations-à-rejoindre-des-groupes-sur-le-fediverse)
- [Fonctionnement de la fédération](#fonctionnement-de-la-fédération)
    - [Types de messages échangés entre les instances](#types-de-messages-échangés-entre-les-instances)
    -  [Limites de la fédération](#limites-de-la-fédération)
    - [Tests de fédération](#tests-de-fédération)
 
## Configuration de la fédération


Cette section explique comment configurer votre compte utilisateur pour la fédération des événements ActivityPub.

### Recherche et suivi d'utilisateurs du Fediverse

Notre plateforme permet aux utilisateurs de rechercher et de suivre les utilisateurs du Fediverse. Les utilisateurs peuvent chercher des utilisateurs du Fediverse en utilisant leur adresse (@nom@adresse) sur le Fediverse. Lorsqu'un utilisateur suit un autre utilisateur du Fediverse, tous les événements publics publiés par cet utilisateur sur leur propre instance ActivityPub apparaîtront sur le fil des evenements de l'utilisateur de notre plateforme. La signature de ces événements sera celle de l'instance ActivityPub de l'utilisateur suivi, avec le logo et le nom de cette instance.

Pour suivre un utilisateur sur le Fediverse, un utilisateur peut :

1. Trouver l'adresse de l'utilisateur sur le Fediverse en accédant à son profil sur leur instance ActivityPub.
2. Copier l'adresse de l'utilisateur sur le Fediverse.
3. Aller sur la page de votre profil dans notre plateforme.
4. Puis dans la section "Reseau Extrene > Abonnement"
5. Coller l'adresse de l'utilisateur sur le Fediverse dans la barre de recherche.
6. Lancer la recherche ne tapant sur "Entrée" de votre clavier.
7. Cliquer sur l'icone de suivre à côté du profil de l'utilisateur recherché.

Une fois que l'utilisateur est suivi, tous les événements publics qu'il publie sur son instance ActivityPub apparaîtront sur le fil des événements de l'utilisateur de notre plateforme.

Cette fonctionnalité offre aux utilisateurs de notre plateforme un accès à des événements publics publiés sur des instances ActivityPub externes, ce qui peut élargir leur portée et leur audience.

### Gestion des invitations à rejoindre des groupes sur le Fediverse

Lorsqu'un utilisateur est invité à rejoindre un groupe sur le Fediverse, les invitations seront affichée dans la section Réseau externe > Abonnées dans le profil de l'utilisateur sur notre plateforme.

L'utilisateur peut :
1. Accéder à l'invitation dans la section Réseau externe > Abonnées de son profil.
2. Accepter ou refuser l'invitation en cliquant sur l'icone correspondant.
4. Une fois que l'utilisateur a rejoint le groupe sur l'instance, il pourra publier des événements publics sur cette instance.
5. Ces événements publics seront alors automatiquement fédérés sur le Fediverse et apparaîtront à la fois sur le fil des événements de l'utilisateur de notre plateforme et sur le groupe pour lequel l'utilisateur a été invité à devenir membre.

Notez que la publication d'événements sur une instance de groupe sur le Fediverse est soumise aux politiques de modération de cette instance.


## Fonctionnement de la fédération

Cette section décrit comment fonctionne la fédération des événements ActivityPub sur notre plateforme.

La fédération des événements ActivityPub implique un échange de messages entre différentes instances ActivityPub. Les messages échangés sont basés sur le protocole ActivityPub et permettent à chaque instance de communiquer avec les autres instances.

Lorsqu'un utilisateur publie un événement sur son instance ActivityPub, cette instance envoie un message ActivityPub à toutes les instances fédérées pour informer les autres instances de la publication de l'événement. Les autres instances peuvent alors décider de récupérer l'événement et de l'afficher sur leur propre instance pour les utilisateurs qui les suivent.

De même, lorsqu'un utilisateur suit un autre utilisateur sur une instance fédérée, cette instance envoie un message ActivityPub à l'instance de l'utilisateur suivi pour l'informer de ce suivi. Cette instance peut alors décider d'envoyer un message ActivityPub à l'instance de l'utilisateur suiveur pour informer cette instance des événements publics publiés par l'utilisateur suivi.

Enfin, lorsqu'un utilisateur accepte une invitation à rejoindre un groupe fédéré sur une autre instance ActivityPub, l'instance de l'utilisateur envoie un message ActivityPub à l'instance du groupe pour informer cette instance de l'adhésion de l'utilisateur. Cette instance peut alors décider d'envoyer un message ActivityPub à l'instance de l'utilisateur pour l'inviter à publier des événements sur le groupe.

La fédération des événements ActivityPub est basée sur un échange de messages entre les différentes instances fédérées. Chaque message ActivityPub est envoyé de manière asynchrone et peut être récupéré et affiché sur les différentes instances des utilisateurs qui suivent les événements publiés.

### Types de messages échangés entre les instances
Les instances d'ActivityPub communiquent entre elles en échangeant différents types de messages. Voici les principaux types de messages échangés :

* **Objets** : Ce sont les messages qui contiennent l'information, tels que les événements, les notes, les profils d'utilisateur, les groupes, etc. Les objets sont créés sur une instance, puis envoyés à d'autres instances pour être affichés sur leur plateforme.
* **Activités** : Les activités sont des actions qui peuvent être effectuées sur les objets, telles que la publication d'un nouvel événement, la mise à jour d'un profil utilisateur, ou la participation à un groupe. Les activités sont envoyées à l'instance concernée pour que l'action soit effectuée. 
* **Suivis** : Les suivis sont des messages envoyés par une instance pour informer une autre instance qu'un utilisateur a commencé à suivre un autre utilisateur ou un objet sur leur plateforme.
* **Notifications** : Les notifications sont des messages envoyés à un utilisateur pour le informer qu'une activité a eu lieu sur leur plateforme. Par exemple, lorsqu'un utilisateur est mentionné dans un événement ou lorsqu'un utilisateur rejoint un groupe.

Pour plus d'informations sur les différents types de messages échangés entre les instances, vous pouvez consulter la documentation officielle du protocole ActivityPub et ActivityStreams :

ActivityPub : [https://www.w3.org/TR/activitypub/](https://www.w3.org/TR/activitypub/)
ActivityStreams :[ https://www.w3.org/TR/activitystreams-core/]( https://www.w3.org/TR/activitystreams-core/)
### Règles de validation et de sécurité
Les règles de validation et de sécurité sont importantes pour assurer l'interopérabilité des instances ActivityPub et garantir la sécurité des utilisateurs.

Le protocole ActivityPub utilise des règles de validation pour vérifier que les messages échangés entre les instances sont conformes aux normes établies. Les instances qui ne respectent pas ces normes peuvent être bloquées ou signalées par d'autres instances, ce qui peut entraîner une perte de visibilité sur le réseau.

Pour garantir la sécurité des utilisateurs, ActivityPub prend en compte plusieurs mesures de sécurité. Les messages sont signés numériquement pour s'assurer que les informations échangées sont authentiques et n'ont pas été modifiées en cours de route. De plus, les utilisateurs peuvent bloquer ou signaler des instances ou des utilisateurs individuels qui ne respectent pas les normes de conduite appropriées.

Il est important que les administrateurs d'instances ActivityPub prennent en compte ces règles de validation et de sécurité pour assurer le bon fonctionnement de leur instance et la sécurité de leurs utilisateurs. Pour plus d'informations, consultez la documentation officielle du protocole ActivityPub et ActivityStreams.

### Limites de la fédération
La fédération est un processus en constante évolution et peut présenter certaines limites et défis. Voici quelques-unes des limites de la fédération  :
* Les événements récurrents ne sont pas pris en charge par la fédération.
* La mise à jour en temps réel des événements fédérés peut prendre un certain temps en raison des différences de mise en cache des instances.
* Les invitations à des événements peuvent ne pas être correctement fédérées en raison de différences dans la façon dont les instances gèrent les données d'invitation.
* Les images et les fichiers joints peuvent ne pas être correctement fédérés en raison de restrictions de taille ou de type de fichier.
* Certaines fonctionnalités spécifiques à une instance, telles que les extensions personnalisées ou les fonctionnalités expérimentales, peuvent ne pas être disponibles sur d'autres instances fédérées.

## Tests de fédération

Cette section décrit les tests que nous avons réalisés pour évaluer l'interopérabilité de notre plateforme avec d'autres instances ActivityPub du Fediverse.
#### Mobilizon
Nous avons testé l'interopérabilité de Mobilizon avec différentes fonctionnalités sur ces différentes versions. De plus, nous avons également effectué des tests sur différentes plateformes telles que Mobilizon.fr, Mobilizon.quebec, Mobilizon.asso.fr et d'autres instances auto-hébergées.
Voici un tableau récapitulatif des tests que nous avons effectués :

| Fonctionnalités / Versions | 1.1.3 | 1.2.3-dirty | 2.0.0 | 2.0.2 | 2.1.0 | 3.0.0 | 3.0.1 | 3.0.2 | 3.0.3 | 3.0.4 (mobilizon.fr) |
|----------------------------|-------|-------------|-------|-------|-------|-------|-------|-------|-------|------------------------|
| Suivre un utilisateur ou groupe Mobilizon | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ |
| Interception de la création d'un événement | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ |
| Interception de la modification d'un événement | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ |
| Interception de la suppression d'un événement | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ |
| Interception de la participation à un événement | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ |
| Accepter l'invitation par un groupe Mobilizon | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ❌ |
| Refuser l'invitation par un groupe Mobilizon | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ❌ |
| Fédérer la création d'un événement sur Mobilizon | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ❌ |
| Fédérer la mise à jour d'un événement sur Mobilizon | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ❌ |
| Fédérer la suppression d'un événement sur Mobilizon | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ❌ |
