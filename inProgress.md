# COProgress

- suivi encaissement https://docs.google.com/spreadsheets/d/1eXFMYnlK0oBtFqRkNIhMZ6VPVILqlfJZ/edit#gid=1532840417



:::info
# Suite Priorité 1

### interview evaluation individuelle
### AAP ANCT
150K€
@Anatole @Gova
#### France connect
https://api.gouv.fr/guides/api-franceconnectees

### XD : Extreme Ademe #presta
40K€
@MIRA @Christon @Nicoss

### Coco Light #better
> optimisation load 
> optim SEO
> interop AP 
> mini version standalone et modulaire
> instance light easy deploy
@yorre @armelin

### continuer OFR-TL #better
@flo @christon

### suite CMS #better
> garder une equipe CORE
@IFA  @sitraka @Francki @bouboule  

### Refactor AAP #better
> bugs refactor mini amélio 
> redesign Klee
@Anatole @Gova @Rinelfi @Schumman @tib 

### clean up CO #better
> bugs deprecations refactor mini amélio 
#### [COBugs](https://docs.google.com/presentation/d/19nu7Fb414EuHTP4lZDgBSDSeSGDacM1bXx5wQFaTe1Q/edit#slide=id.p)
#### container docker de déploiement optimisé 
#### clean up costum 
> costum est trop lourd aujourd'hui 
> analyser ce qu'on peut virer dans les vieux projets 
> avec @Thomas
- deprecated costum
- les assets non optimiser 
- sortir les projets de costum 
    - clean up les assets 
#### [Better Tags](https://codimd.communecter.org/s-QLmGelQnedOMnqgCmEDQ?both)
- analyses des tags doublons et replace 
#### BETTER search engine CTE
#### Refactor Links
cdp : @MIRA @Christon @Nicoss
exe : @Manda @Mahefa @Dinah

### deploiement et usage des badges #usage
:::



:::spoiler **Suite Priorités 2**
# Suite Priorité 2
### suppression et fusion un element 
### homepage 2023 : 10ans aprés
- documentation (bird eye view) 
- kicker de potentiels
- s'ouvrir à une communauté ouverte 
### aggregated object api
  - discussion avec flo 
  - retourner un object searchable contenant tout 
    - element@openatlas@co.tools
    - answers@openatlas@co.tools
    - events@openatlas@co.tools 
    - projects@openatlas@co.tools
### better gallery
> interop Next Cloud ??
### better Annonces + oceco
[connecté Petites et Oceco, Needs wants](https://codimd.communecter.org/RZl-dPclQS-ijl83B8on5Q#)
### better DDA + Oceco
### update la BD Address 
https://mail.google.com/mail/u/0/#search/couprie/FMfcgzGkbDbTgTfrkcDxnnlkkbgBLTdD
En complément ces bases devraient répondre à la question :
- Table d'appartenance des communes, on y retrouve le code commune->code EPCI-> code département -> code région : https://www.insee.fr/fr/information/2028028
- Table des départements : https://www.insee.fr/fr/information/5057840
- Table des régions : https://www.insee.fr/fr/information/5057840
- La liste des EPCI et leur composition communale au 01/01/21 : https://www.insee.fr/fr/information/2510634
- La composition communale des EPCI 2021, avec la population : https://www.collectivites-locales.gouv.fr/institutions/liste-et-composition-des-epci-fiscalite-propre
- Le référentiel des CRTE au 19/07/21 : https://www.data.gouv.fr/fr/datasets/contrat-de-relance-et-de-transition-ecologique/#_
# interop
- pluggin mediawiki
- pluggin nextcloud
- pluggin moodle
- pluggin decidim
:::



:::spoiler **Suite Priorités 3**
## Priorités 3 
### RGAA
https://www.numerique.gouv.fr/publications/rgaa-accessibilite/obligations/
### observatoir de consomation d'espace dans CO et trans outils pour CO, les costums
### graph geo
### logs précis et historique des changements
### coform block for detail page
:::



:::danger
### 1er trimestre -> 03.2023
:::
### Fulltime
- Thomas
- Tib
- Clement
- Florent

- Anatole 
- Nicoss
- yorre
- Christon 
- Ifaliana 
- Jean 
- Mirana
- Schumman
- Rinelfi
- RADAMANIRINA Sitraka **Philippe** **@Senju007**
- ANDRIANIRINARISOA Paul Antenaina Louis **Francki** **@francki06**
- MANDATIANA Gerson **Elvestino** **@gersonelvestino**
- FANILONTSOA Santatriniaina **Dinah** **@fanilontsoadinah**
- RAOELIMAHEFA **Charly** **@Mahefa** 
- Armellin

- Agatha 
- Ambre
- Caroline


### Yii2 sur DEV
@Thomas
#### Test
CMS team en test complet 
cdp : @MIRA @Christon @Nicoss
exe : @Manda @Mahefa @Dinah


### Refactor CMS
Gros Refonte du CMS pour tout nos COstum (IFA, Mira, Clem)
sortira à la prochaine 
@IFA @MIRA @Nicoss @sitraka @Francki @Manda @Mahefa @Dinah


### CODATE inside calendar
@Schumman 


### Calendrier Fédéré
@Rinelfi 

### AAP ANCT
- Déploiement automatique
@gova @anatole @thomas

### EVENT fédéré activity PUB from Mobilizon
@Armellin 


### OCECO CAC : interface de lecture publique
@Gova


### IRIS Quartier injecté
@Tib


### CoRému
@Anatole


### OFR-TL
5 reseaux
@flo @christon

---


:::danger
### 4eme trimestre -> 12.2022
:::
### Fulltime
- Thomas
- Tib
- Clement
- Florent
- Caroline
- Anatole 
- Nicoss
- yorre
- Christon 
- Ifaliana 
- Jean 
- Mirana
- Schumman
- Rinelfi
- RADAMANIRINA Sitraka **Philippe** **@Senju007**
- ANDRIANIRINARISOA Paul Antenaina Louis **Francki** **@francki06**
- MANDATIANA Gerson **Elvestino** **@gersonelvestino**
### MiTemps
- FANILONTSOA Santatriniaina **Dinah** **@fanilontsoadinah**
- RAOELIMAHEFA **Charly** **@Mahefa** 


# NEWS LETTER BUILDER
- Toutes vos dernieres actus envoyer et automatiser en Newsletter communautairex²x²
- Envoyer des **newsletter recurrente**
- Personnaliser et designer sa NewsLetter Communautaire

### Comment
sur votre élément, clicker sur le Bouton +
ajouter NEWSLETTER
![](https://codimd.communecter.org/uploads/upload_6943478faf7437fc8501a9ea4468dd58.png)
pour découvrir
![](https://codimd.communecter.org/uploads/upload_6f73ab5317bd2a1f3048d8b7d8e7aee8.png)

### TUTO Vidéo
Pour tout comprendre

# COFORM par copier coller de texte
construisez votre formulaire dans un editeur de texte 
et créez votre Formulaire par simple copir coller

# COFORM better builder, drag and drop inputs
L'ajout de question et la prévisulisation
![](https://codimd.communecter.org/uploads/upload_879834546fe794725c65978a9d08e12d.png)


# COEVENT : Costum générique pour les évennements
- Co construire le programme de votre evennement
- page de présentation des acteurs, du programme, et carto des lieux

# OFR-TL : Outils pour Faire Réseau de Tiers Lieux
un générateur de plateforme social pour les réseaux de Tiers Lieux. une offre complète et vivante d'outil déployable pour vos Tiers Lieux. 

# CTL : Les Communs des Tiers Lieux
Un méga Observatoire, basé sur la donnée ouverte de France Tiers Lieux pour promouvoir le cofinancement d'outil pour les Tiers Lieux.


# OCECO 
## Gestion de Projet en Kanban (DISCUSS, TODO, EN COURS, TOTEST, TERMINÉ) 
## LECTURE en SlideShow / Power Point
- assis confortablement pour glisser d'une proposition à l'autre 
## Système de communication et Notification par status  de dossier et mail type envoyé
## Menu Verticale
## Usage de AAP en BUDGET PARTICPATIF
Créez des campagnes de vote sur vos propositions de projets 
Voter , commenter, évaluer, prioriser et Suivre 

# CALCULATEUR DE BONHEUR
![](https://codimd.communecter.org/uploads/upload_f92e9b70fb877c1dbc3f6bdb8b505300.png)

# CO FORM : input sondage de date
Rajouter des sondage de date à vos questionnaires
Choix de dates et générer un évennement associé
![](https://codimd.communecter.org/uploads/upload_88f2d4f58ebadbcd122fb7458376b241.png)

# OCECO TOOLS
toute une suite d'outil pratique et en lancement rapide 
- Choix de Date pour vos sprints
- Calculateur de bonheur et de gestion du temps
- Vote Majoritaire 
- Creer un Nouveau formulaire 
- Ouvrir un OCECO
    - Appel à Projet 
    - Outil de gouvernance horizontale
- Tableau Decisionnel à 2 dimensions


---


:::danger
### 3eme trimestre -> 07.2022
:::
### Fulltime
- Thomas
- Tib
- Florent
- Caroline
- Anatole 
- Nicoss
- yorre
- Christon 
- Ifaliana 
- Jean 
- Mirana
- Schumman
- Rinelfi
### Stagiaire
- Rafael Dubois
- FANILONTSOA Santatriniaina **Dinah** **@fanilontsoadinah**
- RADAMANIRINA Sitraka **Philippe** **@Senju007**
- ANDRIANIRINARISOA Paul Antenaina Louis **Francki** **@francki06**
- MANDATIANA Gerson **Elvestino** **@gersonelvestino**
- RAOELIMAHEFA **Charly** **@Mahefa** 



:::danger
### 2eme trimestre -> 04.2022
:::

:::info
## RH
### Fulltime
- Thomas
- Tib
- Florent
- Caroline
- Anatole 
- Nicoss
- yorre
- Christon 
- Ifaliana 
- Jean 
- Mirana
### Stagiaire
- Schumman
- Rinelfi
- Rafael Dubois
::: 

## Observatoire FTL 
## Animation réseau MEIR 
## AAP CAC
## gestion subvention cosindni smarterre 
## Documentation CODOC basé sur gitlab avec un template Hugo
# CTE5 Amelioration design et nouvelle sitemap  

## Chantier Stagiaire 
### [x] GRAPH / navigation data 
https://www.communecter.org/graph/co/finder/slug/ctenat
- Finder utilisant les elements de CO (jaona et Tib)
    à propos 
      information 
    projects
      stage 
      experience
    events 
      etude 
    communauté
    journal
#### [ ] intégrer dans un blockcms

### [x] CODate
Framadate avec Coform
voir framadate : listé une liste de date et recolter celle qui correspondent

### [x] input coform idem mieux voter
voir mieux voter https://app.mieuxvoter.fr/

### [ ] REfactor la page answers de co.coforms
https://www.communecter.org/#@granddir.view.forms.dir.observatory.6221c15cdb837461570491cc
- [x] faire une vu excel, configurable par les colonne https://docs.google.com/presentation/d/1SP3F8VE3twEUNX9QuxqV6hP3TKcGpjdQK3HnjYlYnQ0/edit#slide=id.gfb4ca34504_0_49
- [ ] ajouter les 3 vues de ocecoforms 
- [ ] connecté l'obs CMS avec Graphique

#### [x] intégré avec le voteMajoritaire
- si on active un input mieux voter > ca pourrait activer automatiquement un graph mieu voter

#### [x] design excel des answers + choix des colonnes
#### [x] finder dans coform
#### [x] finder avec ocecoform

#### [ ] finder dans ocecoform

#### [/] Ajouter une question , drag drop jquery  formbuilder
voir avec le menu de @ifa 
#### save as template

--- 

## Stagiaire @Riza
### [x] Repair COEvents ( avec yorre )
- [x] reparer 
- [x] passer en cms 
- [x] template générique 
- [x] open event

### [x] tester coevent sur lartevolution
- [x] creer des events, avec le costum coevent
- [ ] voir si communEvent marche tjrs 

### [x] export Excel answers
### [/] coform > export (PDF , EXcel )
### [/] export pdf ocecoform


:::danger
### 1er trimestre -> 01.2022
:::
:::info
## RH
### Fulltime
- Thomas
- Tib
- Florent
- Caroline
- Anatole 
- Nicoss
- yorre
- Christon 
- Ifaliana 
- Jean 
- Mirana
### Stagiaire
- Schumman
- Rinelfi
::: 

### Yii1 2 Yii2
- Optim DB 
- intégré les actions listés par @thomas partout 
- Unifier les repos en ph 
- structurer les tests unitaires

## Open Badge
## Activity Pub
## COSTUM MEIR
## FINDER navigation

## MEP 26/01/2022
### COForm > Quadrant Ariane
>Edition tps reel socket io
Le 1er Navigator Ariane , 1ere Plongez dans l'envirronement Ariane
### COLLAB EDITING INPUT
> Collaborer à plusieur sur un formulaire coform est possible à présent
### COForm TL974
> le premier formulaire de récolte d'information, c'est avec le réseau [La Réunion des Tiers Lieux](https://www.communecter.org/costum/co/index/slug/LaReunionDesTiersLieux#) que commence l'expérimentation et l'étude des tiers lieux du territoire réunionais, pour decouvrir, suivre et rester informer
### CMS Observatory
Aprés une belle année d'évolution du CMS, encore une belle nouveauté avec l'apparition des blockCMS (chart, d3js, ) on peut construire en autonomie un observatoire ou simplement afficher des des graphes à l'interieur des COstums CMS
### nouveaux BlockCMS
- chart 
- D3js
- 

:::danger
### 4eme trimestre -> 12.2021
:::
## COFORM TEMPLATES
possible to copy any given template 
- as a duplicate changeable by the parent 
- as an independent modifiable copy

## Better NEWS
https://codimd.communecter.org/IIE_wBJETzeK3buMvpgLJQ#

## OCECOFORM
@anatole @gova @nicoss
- Oceco Prez
- Oceco Proposition Prez 
- PROPOSITIoN DECISION FINANCE SUIVRE

## Appel à projet
@anatole @gova @nicoss

## Cocity et un notre monde

## COForm FabEn

## AD2R innovation social ???

## CRESS amélioration

:::danger
### 3eme trimestre -> 09.2021
:::

# Communication
- [ ] Enregistrer des fonctionalités pour mieux les présenter 

# FEATURES
- [x] redesign de l'app Agenda
- [x] OPTIM NOTIFICATION https://codimd.communecter.org/5Ph1nhyrR9qcX8CN_awZkQ?both
 
:::info
:::
# COSTUMS

## costum network comaps
@yorre 

## LILA
@Mira @Ifa

## optim schema indexation
@yorre

## HomePage
@nicoss

## TL974
@christon @flo 

## notragora rebranché
@flo

## en PROD

## CTE4
@clem @tib @anatole @nicoss @mira

## RIES
@christon @ifaliana

## costum artiste : Idriss KAFMARON
@Jean @Nicoss
https://chat.communecter.org/channel/costumGenerqueArtiste

## costum HER
@Nicoss
https://chat.communecter.org/channel/nemeton

## costum nemeton
@Nicoss
https://chat.communecter.org/channel/nemeton

## PRDR : PIR : Plateforme de l'innovation Réunionaise 6K€
- https://www.communecter.org/#@prdr.view.directory.dir.members
-https://docs.google.com/presentation/d/1iGj8Sun020Yu84B3y28PJ6X0xMVl6JBoJgqwcHk8Di0/edit#slide=id.g4f648fe76e_0_624

## Innover et Observatoire sexy France Tiers @florent
- export pdf des graph avce Christon  
- [x] observatory panels
- [ ] sexy observatory panels
- [/] fiche costumisation générique réseau 

## amélioration de la CRESS @flo
https://chat.communecter.org/channel/cressDeLaReunion1
- [x] coform economie circulaire 
- [ ] observatroire  
- [ ] better nav , filtre accueil par tagguing (script)
- [ ] envoie de mail coform pour data amelioration , pour préremplir et connecter à un element 
- [ ] formulaire inscription event , limitation participant

## sommom #2
https://chat.communecter.org/channel/sommom
- [90%] amélioration 

## openatlas association @Christon
https://www.communecter.org/costum/co/index/slug/pixelhumain 
- [90%] sociétaire 

## Costum filere Generique : @Christon
- [?%] filiere Prez
- [?%] generation de filiere 
 
### features
- [ ] coform rejoindre critère
- [ ] faire un observatoire
- [ ] importer les datas du fichier mapping 

### filieres
- [50%] Université @IFA
- [ ] Innovation [communauté PRDR] @jaona 
    - [80%] circle packing 
    - [90%] navigation dans les graphs
    - [ ] costum filiere innovation 
    - [ ] intégré les visualisation graphs (https://www.communecter.org/#@prdr.view.directory.dir.members) 
    - graph carto 

### coevents @JEAN
https://docs.google.com/presentation/d/1rZpyxlMoPlhRtCDEfXjsbhImjfPa-9wJ8qT7TxZ5zbw/edit
- [0%] repair costum

- [Culture ](https://docs.google.com/presentation/d/1TE8v2oicBqdDQb-ppZgeQiq_D9Z_t1A1GEhqGz7uQnA/edit#slide=id.g3802b17b5_110)
    - [Artistes auteurs](https://docs.google.com/presentation/d/1TE8v2oicBqdDQb-ppZgeQiq_D9Z_t1A1GEhqGz7uQnA/edit#slide=id.g4f648fe76e_0_624)

- [coeur Numérique](https://docs.google.com/presentation/d/1V_Ka7F4sLlKY0hS20yyb_5234Tcc4OaqhqPbJzIksQ8/edit#slide=id.g4fd68111b6_0_679)

### Cocity et CObservacity : Mirana
- Init all Cocity for existing CO people, orgas, projects
    - vision regional
    - Cocity Observatory : observacity 
- pour chaque commune du pacte demande au sentinelle citoyen
- de remplir les actions qui sont faites dans le sens des 32 mesures
- observatoire locale des 32 mesures locale

### costum candidat regional : Ericka et Vanessa

:::danger
### 2eme trimestre 06.2021
:::

:::info

## RH
### Fulltime
- Thomas
- Tib
- Florent
- Caroline
- Anatole 
- Nicoss
- yorre
- Christon 
- Ifaliana 
- Jean 
- Mirana
### Break
- Bouboule
:::

:::success
# NEW STUFF
:::

## PASSAGE PHP7 Yii2

## Appel à Projet Sindni 93K€
https://docs.google.com/presentation/d/1qWjFI0x4jWXb-D9Ky2s14ZnjJIxArId9SCF9xZThKbI/edit#slide=id.g503e9e3bd2_0_257


:::info
# Features
:::
## Enregistrement de Filtres Favoris
innovatoin qui permet d'enregistrer les critères de recherche ou 

:::info
# COSTUMS
:::

## EYWA
## GLAZ BIJOUX
## KOSASA
## METEOLAMER 

## Cocity
- Init all Cocity for existing CO people, orgas, projects
- vision regional
- Cocity Observatory : observacity 
    - pour chaque commune du pacte demande au sentinelle citoyen
    - de remplir les actions qui sont faites dans le sens des 32 mesures
    - observatoire locale des 32 mesures locale

## CODECO : générateur de DEVIS à base de screenshot
- checkbox les besoins fonctionnels 
- calcul automatique de la préstation
- proposition de costum existant 
    - vitrine de communauté 
    - costum générique filiere 

## CMS : Analyse consommation, activité et activation de costum générique
- analyser les costum générique fait en solo pour un contribution au commun

## open atlas cooperative
https://www.communecter.org/costum/co/index/slug/openAtlas#


:::danger
## 1er trimestre 03.2021
:::

### Full Time
1. Thomas
2. Tib
3. Clem
4. Karine
5. Anatole 
6. Nicoss
7. Yorre
8. Christon 
9. Ifaliane 
10. Jean 
11. Mirana 
### par mission
1. Florent
2. Bea

:::success
# dossier en cours
:::
## dossier BPI
https://docs.google.com/document/d/1AowOtBdbqNVDhFphkIKBT_AJhMb4iYut00JfnUsAIrQ/edit
avec Germain Merlin

## PIR : Plateforme de l'innovation Réunionaise
Collaboration avec la technopole Reunion 
pour transformer le mapping actuellement statique
en COSTUM plateforme filiere et faire suite à la demo [PRDR](https://www.communecter.org/#@prdr.view.directory.dir.members)
Etapes : 
- Mapping+Livré des incubés to Excel to CO
- CoForms pour généré des parcours Innovation pour tout les projet 
- Cobservatoire
- Veille et Documentation Dynamique 
- Annuaire
- Agenda
- News


## coSindni
https://docs.google.com/presentation/d/1Lf4CqN2WD20HfFmA4R9Xk99qH5KMLLZB4JbreZ23wlQ/edit#slide=id.g9a69f2df26_0_211
## MTES : suite CTE ou CRTE
discussion en cours 
## MTES : Deb , biodiversité
discussion en cours 
## Cohésion des territoires

:::success
# REPAIR REFACTOR OPTIM
:::
## refactor et optim searchObj
:bouboule:
## optimisation des search+maps
:yorre:
## codeco
:Nicoss:
https://www.communecter.org/costum/co/index/id/costumDesCostums#welcome

## network
:yorre: map optim
## observatoire coeur num
:christon:
## smarterre :christon: 
## smarterritoire :christon:
## documenation CMS 
:Nicoss:

:::success
# NEW STUFF
:::
:::info
# Features
:::
## costum builder | cmsEngine 
:JEAN:Mirana:Ifa:Nicoss:tib:bouboule:
https://chat.communecter.org/channel/cms
### coforms
:Anatole:
generateur de form data 
coform send to : textarea list of email sends a generic 
## cobserv generique
:Anatole: 
element observatoire

## ocecomobi
:thomas:tib: 
amélioration et debug
tutoriel https://codimd.communecter.org/TRVeo099SPyzZx_9bG4ARQ?both#

## fact oceco
:bea:flo:tib:Anatole:
chat https://chat.communecter.org/channel/factOceco
Diagnostique https://codimd.communecter.org/2R__nWdSTeWvyBZBzaUzWA?both
ppt https://docs.google.com/presentation/d/1tXM5WJK2PsWIvBNCpCwNJiTKwij40X3_LFnvg_8Hxn8/edit#slide=id.gb44acf559e_3_0

## copytool costum consume
:tib:
  data struct
  costum.weight and usage 
doc
:::info
# COSTUMS
:::
## Amélioration de la CRESS

## eywa
:jean:
chat https://chat.communecter.org/channel/eywa

## kosasa
:Mirana:
chat https://chat.communecter.org/channel/costumAssoKosasa

## glazbijou
:jean: 
chat https://chat.communecter.org/channel/siteGlazBijoux

## lila
:Ifa:
chat https://chat.communecter.org/channel/numerisationLILA

## homme et environnement
:nicoss:
chat https://chat.communecter.org/channel/costumHommeEnvironnement

## fabrique des Energies
:christon: 
chat https://chat.communecter.org/channel/fabEnergies
url https://www.communecter.org/#@FabEnergies.view.directory.dir.contributors
## appel projet generique
:Mirana: 
doc https://docs.google.com/document/d/1IuRzUuOfheoqtLHVkL_M372iO_9JpuCCTGpTQySou8o/edit
## cocity builder
:Mirana: 
chat https://chat.communecter.org/channel/cocity
doc https://codimd.communecter.org/pswmskt0Rcew4uu5MwZ0qQ#co
ppt https://drive.google.com/drive/folders/1bbXTycPdjEwsp3elYeVo0TX5f8xiAQZx
cocityPrez http://dev.communecter.org/costum/co/index/slug/cocityPrez
cocity http://dev.communecter.org/costum/co/index/slug/fianarantsoa

### filiere builder :Mirana:

### DEAL AH
:flo:Anatole:tib:clem:
chat https://chat.communecter.org/group/dealdev
url http://qa.communecter.org/costum/co/index/slug/dealAH

### Meteolamer
:yorre:
http://dev.communecter.org/costum/co/index/slug/meteolamer#

### France Tiers Lieux
:florent:
https://www.communecter.org/costum/co/index/id/franceTierslieux


:::danger
## 12.2020
:::

## Ressource Humaine 
### Fulltime
- Thomas
- Tib
- Clem
- Karine
- Anatole 
- Nicoss
- yorre
- Christon 
### Stagiaire
- Ifaliane 
- Jean 
- Mirana
### Quitté
- Rapha

# Projet CTE3
( !@oceatoon !@Bouboule !@RaphaelRiviere !@Anatole !@Nicoss !@GbxPierre )
https://docs.google.com/presentation/d/1jL4VDkipsyJZfctXO6qiG-cjc0P4lIOEBRtpcKtkikE/edit?pli=1#slide=id.g8057040f93_0_113

# Projet Pacte 3
!@bouboule !@Nicoss

# Projet CMCAS Haute Bretagne
!@GbxPierre

# CMS
(!@GbxPierre !@oceatoon !@Mirana !@Ifaliana)
- templatisation des COSTUMs
- block de tpls

# CObservatory
(!@oceatoon !@Anatole)
creation de graph dynamique 

# DEAL
( !@oceatoon !@Bouboule !@RaphaelRiviere !@florentBenameur )
- usage de COForm sur 4 étape 

# New Design 2.0.2.0
(!@Nicoss !@Bouboule)

# COstum
- cocity
- coEvent
- COCV
- COBrainstorm

# Dossiers
- SCIC Open Atlas 
- Smarterre en cours de finalisation 
    - 
- Plateforme des territoires et des citoyens
    + AO defense
    + envoyé au SGAR Reunion
    + traduit pas Nassima Mecheri
    + https://docs.google.com/presentation/d/1bZel1-3CmZJFkObvK87CNQpKgLm-Sogti_iu_hcAy4U/edit#slide=id.g71e6b3cf0e_11_43
- COScience (depot raté)
    - https://docs.google.com/presentation/d/1BF3rBnyfMHKjTjn53ShyT_tvEFe6D0iUD3BhqJ1irtI/edit#slide=id.g737fb454ef_0_91
- FACT OCECO  (en cours)
    - https://docs.google.com/presentation/d/1ZesGOCO15vuIaW1MSpO852PhSGEbQ6NWlrV2kAWH7eM/edit#slide=id.g80669dd5c9_17_1
- Dossier TL Reunion 

## Communauté
- Miks, secteur de la kiltir Réunion https://chat.communecter.org/channel/miksGouvernanceOutilsCollaboratifs
- Kiltir Relancé 
- Pacte 0.3 
- CTE 3.0 
- Numérique relancé 
- asso Energie Partagée



--- 
:::danger
## 09.2020
:::
## Dossiers
- AFNIC validé

## Design
- COvid
- COcity
- Baniere Générique
- Gif Baniere

## COTools
(!@thomascraipeau)
- https://peertube.communecter.org/login : PEERTUBE dans communecter
- meet.communecter.org : Jistsi dans communecter 

## COForms
(!@oceatoon !@Bouboule !@RaphaelRiviere)
- integration dans CO génériquement 
- appliqué 

## CObservatory
(!@oceatoon !@Anatole)

## OCECO Mobi
https://oce.co.tools/
https://play.google.com/store/apps/details?id=org.communecter.oceco&hl=fr&ah=lVN3mXqHKQjIOg3qHn0YzhiUebc


## OCECO FACT
( !@oceatoon !@BeaPotier !@florentBenameur )
[PPT](https://docs.google.com/presentation/d/1di2j3etdu2CeUJvk6Rq7J2RbO3Qr2evvaKKm9xDWZG8/edit#slide=id.g7f2715dafa_5_0)
- usage de COForm sur 4 étape : Proposer, Decider, Financer, Suivre
- dépot de dossier FACT à ARACT ANACT avec une communauté de testeur

## COstum
- CRESS https://www.communecter.org/costum/co/index/slug/sommom#welcome
- Hinaura mednum 
- ultranum mednum

## Projet CMCAS
(!@GbxPierre !@oceatoon)
http://co.cmcashb.fr

## Projet Sommom : Questionnaire pour les observatoires de cétacés
(!@Anatole)
https://www.communecter.org/costum/co/index/slug/sommom#welcome

## [Projet AFNIC] Interop (!@lotik !@florentBenameur !@Lamyne !@catalyst !@anis !@opteos)
https://codimd.communecter.org/m1TM7HxkS3y3AbhFF25RrQ#

### Interop Mediawiki
- lecture et structuration du contenu

### Interop Markdown externe
- ouverture des MD de 
    + codimd
    + github MD 

### Interop documentation Gitlab en MD
- utilsation de l'api GraphQL de gitLab 


--- 
## COstum
- mednum
- smarterre https://www.communecter.org/costum/co/index/slug/smarterre
- smarterritoire
- costum générique candidat
- open atlas https://www.communecter.org/costum/co/index/slug/openAtlas#
- pixelhumain https://www.communecter.org/costum/co/index/slug/pixelhumain
- coeurNum https://www.communecter.org/costum/co/index/id/coeurNumerique
