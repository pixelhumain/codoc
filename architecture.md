# Architecture
[following this suggestion](https://matklad.github.io//2021/02/06/ARCHITECTURE.md.html)
everything is centered around a MongoDB Database

## web instance

### Techno
#### PHP framework Yii
WE are still using php 5.6 
php7 will be set up first semester 2021
We still need to switch to Yii 2 it's in progress 

#### javascriptJ
we still use the good old [Jquery](https://jquery.com/) 
et des centaines de pluggins web /pixelhumain/ph/plugins

#### MongoDB
We chose a No SQL database for it's simplicity et mutability
our schemas evolve permenantly and almost dynamically 
the whole system is in the end à huge collection of well organised JSON documents 
We try to follow as much as possible common object standards from [schema.org](https://schema.org/docs/full.html)
 
## hébergement
OVH 
instance type de serveur 

## stockage BDD
instance type de serveur 

## schema de BDD


## costum : personnalisde integrated instance
> json configs, specific designs 

use CO and personnalize freely 
connect it to your own DNS
show only the data of your community 
while continuing to contribute to a common Database

## COmobi
> NodeJs, Meteor, CO.api, MongoDB
https://play.google.com/store/apps/details?id=org.communecter.mobile&hl=en_US

## Oceco Mobi
> NodeJs, Meteor, CO.api, MongoDB
https://play.google.com/store/apps/details?id=org.communecter.oceco&hl=fr

## Usage de systeme externe et interopérabilité
nominatim > overpass API
api-addresse.data.gouv.fr 
mediawiki
gitlab

## Features Technical documentation 
### [COstum](./07 - COstum/costum.md) 
### [CMS](./04 - Documentation technique/cmsEngine.md) 
[autre](./07 - COstum/CMS - Templates Blocks.md)
### [COforms](./08 - COForms/coforms Tech.md) 
### [SearchObj](./04 - Documentation technique/process/search.md) 
### [mapObj](./04 - Documentation technique/process/mapObj.md) 

## Hebergement des COTOOLS


