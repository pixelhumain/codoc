# Plateforme de partage des connaissances

## Objectifs
- Mener une étude des acteurs d’un secteur, d’une filière, d’un territoire à travers un formulaire dynamique
- Relier un formulaire avec votre plate-forme personnalisée qui modélisent les dynamiques sociales et sociétales de votre communauté
- Gagner du temps sur le traitement des données issues d’un formulaire et leurs mises à jour
- Disposer d’une base de connaissance actualisée interactive et évolutive
- Donner la possibilité de valoriser cette connaissance à travers des outils d’analyse en temps réel (observatoire)
- Décloisonner la connaissance et la rendre accessible à d’autres systèmes d’informations

## Principes

Nous proposons un module de formulaire totalement personnalisé qui est directement liés aux acteurs et à leurs activités au sein de votre COstum : chaque champ renseigné de votre formulaire peut être interprété. Vous pouvez ainsi récolter ou mettre à jour dynamiquement l’ensemble des informations relatives aux membres de votre communauté (changement d’adresse/horaires, évolution de l’offre de services, …), faire remonter le bilan (quantitatif, qualitatif, financier) d’une action pour évaluer son impact ([observatoire](https://codimd.communecter.org/pxZ0E0dJQLO-rUOwc1zH1A#observatoire)).


## Exemple
...
