# Formations de formateurs

## Administrateur - Animation du réseau
### <span style="color:#e06666;">Objectifs</span>
* Maîtriser l'utilisation globale et les fonctionnalités poussées d'une plateforme spécifique (COstum)
* Maîtriser l'espace d'administration de la plateforme (inviter des membres, importation/exportation de données, ...)
* Connaître les leviers d'une animation de réseau efficace à partir des outils à disposition.

### <span style="color:#e06666;">Public</span>
Administrateur.rices actuel.les ou futur\.es d'une plateforme dédiée basée sur COmmunecter (COstum)

### <span style="color:#e06666;">Pré-requis</span>
Connaître l'utilisation transversale de sa plateforme dédiée

### <span style="color:#e06666;">Modalités</span>
* Lieu : Visio-conférence
* Approche pédagogique : 
  - Mise en pratique directement sur votre plateforme (COstum)
  - Documentation
* Durée : 2h
* Prix : <span style="color:#e06666;">400 Euros</span> par session => <span style="color:#e06666;">Cofinancé</span> (divisé) par les participants

## Formation administrateur -Système de gestion de contenu (CMS)
### <span style="color:#e06666;">Objectifs</span>
Maîtriser la création et le paramétrage de la technologie CMS de COmmunecter, sur votre plateforme dédiée.

### <span style="color:#e06666;">Public</span>
Administrateur.rices actuel.les ou futur.es  d'une plateforme dédiée basée sur COmmunecter (COstum)

### <span style="color:#e06666;">Pré-requis</span>
Avoir quelques notions en css (mise en page d'un site web)

### <span style="color:#e06666;">Modalités</span>
* Lieu : visio-conférence
* Approche pédagogique : 
  - Mise en pratique directement sur votre plateforme (COstum)
  - Documentation
* Durée : 2h
* Prix : <span style="color:#e06666;">250 Euros</span> par session => <span style="color:#e06666;">Co-financé</span> par les participants


## Formation formulaire de connaissance (COForm)
### <span style="color:#e06666;">Objectifs</span>
* Maîtriser la création, la modification et le paramétrage des formulaires intégrés de COmmunecter
* Choisir les bons champs pour rendre votre étude dynamique

### <span style="color:#e06666;">Publics</span>
* Administrateur.rices actuel.les ou futur.es d'une plateforme dédiée basée sur COmmunecter (COstum)
* Porteur.euses / Animateur.rices d'une enquête, d'un recensement qualitatif et/ou quantitatif (guidé ou libre d'accès)
* Personnes souhaitant constituer une base de connaissance territoriale et thématique

### <span style="color:#e06666;">Pré-requis</span>
Avoir quelques notions en css (mise en page d'un site web)

### <span style="color:#e06666;">Modalités</span>
* Lieu : Visio-conférence
* Approche pédagogique : 
  - Mise en pratique directement sur votre plateforme (COstum)
  - Documentation
* Durée : 2h
* Prix : <span style="color:#e06666;">250 Euros</span> par session => <span style="color:#e06666;">Cofinancé</span> (divisé) par les participants






## Formation cartographie dédiée
### <span style="color:#e06666;">Objectifs</span>
* Mettre en place une cartographie basée sur la donnée de votre plateforme spécifique
* Intégrer votre cartographie dans un outil de communication externe (site internet, écran d'accueil, ...)

### <span style="color:#e06666;">Public</span>
Administrateur.rices actuel.les ou futur.es d'une plateforme dédiée basée sur COmmunecter (COstum)

### <span style="color:#e06666;">Pré-requis</span>
Maîtrise le modèle et la structuration de données de votre plateforme spécifique

### <span style="color:#e06666;">modalités</span>
* Lieu : Visio-conférence
* Approche pédagogique : 
  - Mise en pratique directement sur votre plateforme (COstum)
  - Documentation
* Durée : 2h
* Prix : <span style="color:#e06666;">200 Euros</span> par session => <span style="color:#e06666;">Cofinancé</span> (divisé) par les participants


## Formation observatoire dynamique
### <span style="color:#e06666;">Objectifs</span>
* Savoir créer une page observatoire
* Maîtriser la mise en page de son observatoire dédié
* Connaître les différents graphiques
* Valoriser graphiquement les réponses d'un questionnaire

### <span style="color:#e06666;">Publics</span>
* Administrateur.rices actuel.les ou futur.es  d'une plateforme dédiée basée sur COmmunecter (COstum)
* Porteur.euses / Animateur.rices d'une enquête, d'un recensement qualitatif et/ou quantitatif (guidé ou libre d'accès)
* Personnes souhaitant constituer une base de connaissance territoriale et thématique

### <span style="color:#e06666;">Pré-requis</span>
* [Formation Coform](#Formation-Coform)
* [Formation CMS](#Formation-système-de-gestion-de-contenu-CMS)

### <span style="color:#e06666;">modalités</span>
* Lieu : visio-conférence
* Approche pédagogique : 
  - Mise en pratique directement sur votre plateforme (COstum)
  - Documentation
* Durée : 1 j
* Prix : <span style="color:#e06666;">500 Euros</span> par session => <span style="color:#e06666;">Co-financé</span> par les participants


## Formation administrateur - Paramétrage avancé d'une plateforme

### <span style="color:#e06666;">Objectifs</span>
* Savoir charger/enregistrer/modifier un modèle (template) de plateforme
* Maîtriser les fonctionnalités avancées de paramétrages d'une plateforme spécifique (COstum)

### <span style="color:#e06666;">Public</span>
Administrateur.rices actuel.les ou futur.es d'une plateforme dédiée basée sur COmmunecter (COstum)

### <span style="color:#e06666;">Pré-requis</span>
* [Formation administrateur - Animation du réseau](#Formation-administrateur---Animation-du-réseau)
* [Formation système de gestion de contenu (CMS)](#Formation-système-de-gestion-de-contenu-CMS)

### <span style="color:#e06666;">modalités</span>
* Lieu : Visio-conférence
* Approche pédagogique : Mise en pratique directement sur votre plateforme (COstum)
  - Mise en pratique directement sur votre plateforme (COstum)
  - Documentation
* Durée : 1j
* Prix : <span style="color:#e06666;">500 Euros</span> par session => <span style="color:#e06666;">Cofinancé</span> (divisé) par les participants



## Récap


| Services                                                                                   | Prix                   | Durée|Prix par structure (5) | 
|:------------------------------------------------------------------------------------------ |-----| ---------------------- | ---------------------- |
| Plateforme territoriale (locales) et/ou thématiques d'animation d'un réseau de tiers-lieux | 1000 Euros / structure |2 x 2h |1000                   |
| Administrateur - Animation du réseau (x2 min)                                              | 400 Euros              | 2 x 2h|160                    |
| Formation administrateur -Système de gestion de contenu (CMS) (x2 min)                     | 250  Euros             | 2 x 2h |100                    |
| Formation formulaire de connaissance (COForm) (x2 min)                                     | 250 Euros              | 2 x 2h | 100                    |
| Formation observatoire dynamique (x2 min)                                                  | 500 Euros              | 1 j | 200                    |
| Formation administrateur - Paramétrage avancé d'une plateforme (x2 min)                    | 500 Euros              | 1 j| 200                    |
| Formation cartographie dédiée                                                              | 200 Euros              | 2h |80                     |
|                                                                       Fonds d'amélioration                     |         500 Euros          |                   500 |
|                                                                                            |                TOTAL        |           2340             |


