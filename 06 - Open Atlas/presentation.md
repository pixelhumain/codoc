
# PRÉSENTATION D'OPEN ATLAS


L'association OPEN-ATLAS, implantée à La Réunion depuis 2009, a pour objet d'être un lieu multidisciplinaire d'échanges permettant de développer des projets œuvrant dans des domaines de l’intérêt général, notamment autour de l’économie de l’information et de la communication. D’intérêt social et solidaire, l’association s’intéresse à tous les territoires, du local à l’international. 

Elle œuvre, en toute diversité, dans les champs du développement humanitaire, culturel, artistique, de la communication de services public ou privé, de la cartographie virtuelle, de la connectivité et la mise à disposition de l'information, de la communication (au sein d'un groupe). 

L’association se donne également pour objectif d’être un laboratoire et une cellule active de recherche et de développement, en toute transversalité, défendant le décloisonnement et le partage des compétences, ainsi que la liberté de penser et d’agir. Open Atlas promeut l’action et l’intelligence collective et l’initiative citoyenne, militant pour la conjugaison de l’éthique et de l’efficacité, autour de l’économie numérique, œuvrant dans le principe de développement soutenable, durable et solidaire.

Notre équipe est composée d’experts reconnus du logiciel libre, nous développons et adaptons des solutions dans notre cœur de  métier :  la structuration et la valorisation de l’information/donnée au cœur des territoires.

Là où certains proposent une approche “Smart” au travers de capteurs ou de programmes intelligents, nos solutions proposent une approche sociétale innovante et complémentaire, basée sur l’humain​, où le fruit de la collaboration des acteurs, leur intelligence collective et les différentes initiatives entreprises sont accessibles, centralisées, partagées et mises en valeur pour tendre vers des territoires plus intelligents, plus ouverts et interconnectés​. 


#innovation sociétale	     #besoins sociaux     #territoire connecté      #intelligence collective      #société de demain      #système d’information territorial     #base de connaissance localisée     #open-source     #gratuit     #données protégées     #informations partagées     #linked data     #objets connectés      #interopérabilité     #bien commun      #humain     #participation citoyenne     #citoyenneté numérique     #smart city      #territoire intelligent     #expérimentation      #systémique     #collaboratif     #co-construction     #cofinancement


### Solutions numériques
Nos solutions numériques, disponibles sur internet et applications mobiles, sont CO-construites, open-sources, COllaboratives, innovantes,  avec une approche locale et systémique, suivant une méthode agile, où les données d’un territoire sont gérées comme un bien COmmun​.
Nos valeurs 


### Notre devise
“1 + 1 = 3 | L'intelligence collective au service du territoire”

### Intention
L’open source ! Des épices et des recettes et un code source libres d’utilisation et d’amélioration

### Libre
Parce qu’une communauté qui partage est une communauté qui innove plus que les autres, Open Atlas développe que des logiciels libres. Vous êtes autonomes vis-à-vis d'Open Atlas et bénéficiez, comme chacun de nos clients, de développements déjà réalisés pour les autres. Le code est réellement libre, il peut être téléchargé par tous, sans condition.  

### Sobre
Les fonctionnalités que nous créons ou adaptons répondent uniquement à vos besoins.  

### Protégé
Bien commun : intelligence collective, systémique, Agile 
Notre méthode projet et nos développements sont itéractifs, pour tester avec vous chaque détail et  faire participer l’usager final.  

### Universel
Un respect des standards de développement et de sécurité… pour que nos solutions soient  opérationnelles et pleinement interopérables. 

### Vous !
Nos solutions se modulent et s'adaptent en continu à vos méthodes, vos services, vos applications pour en simplifier les tâches, sans changer votre vision de votre métier.  

### Nos solutions
Nos solutions sont destinées aux différents acteurs qui composent un territoire : aux collectivités, aux associations, aux entreprises et au grand public. 
Nos solutions sont en cohérence avec les politiques publiques actuelles : innovation sociétale, mieux vivre ensemble, enjeux économiques actuels, le développement de modes de production et de consommation responsables, l'ouverture des données publiques aux citoyens, protection de l’environnement, participation citoyenne, les Projets de Rénovation Urbaine, les Programmes Investissement d’Avenir, la Politique de la Ville…

Dans ce contexte, nous bénéficions d’une grande diversité de clients et de projets à l’échelle internationale. Nous avons l’habitude de nous voir confier des chantiers de grande ampleur.  

### L'ÉQUIPE
OPEN-ATLAS promeut une gouvernance horizontale avec un organigramme à plat, même si sur chaque projet, il y a des leaders techniques et  fonctionnels, clairement identifiés. En cela, notre équipe fonctionne comme fonctionnent les  communautés du logiciel libre. Il s'agit d'une organisation particulièrement efficace pour produire  du logiciel libre. 
OPEN-ATLAS est composé de partenaires freelance très expérimentés dans le domaine du logiciel libre qui travaillent essentiellement sur le développement et le suivi de nos produits où toute l’équipe partage les mêmes habitudes de travail. Après avoir expérimenté l’emploi au sein de notre structure, nous avons opté pour une solution de ressources en free lance. Ce modèle présente plusieurs avantages : 

Il  responsabilise les personnes avec qui nous sous-traitons notamment  le développement et le design. 
Il permet une organisation du travail plus souple en accord avec notre gouvernance horizontale.
Il respecte la volonté de liberté de nos collaborateurs, qui peuvent contribuer au commun tout en continuant parallèlement leur projet d'activité professionnelle.
Il représente moins de charges pour la structure. 
Notre équipe travaille avec/autour des logiciels libres depuis plus d’une quinzaine d’années mais aussi dans l’accompagnement et l’animation des projets et connaît les  besoins des entreprises et des administrations.  


La partie R&D de notre structure est prépondérante : 
elle comporte toute la partie innovation de nos développements
elle assure la généricité de nos différents modules
elle nous permet d’assurer du conseil territorial pour des projets complexes  
Des équipes projets sont constituées pour chacun de nos clients, les fonctions des différents salariés  sont, dans une certaine mesure, susceptibles de varier d'un projet à l'autre.

Composition de l’équipe OPEN-ATLAS : 

Types de ressources/compétences 
Explications
Nombre
Développeur sénior 
développeur autonome  sur le développement 
développeur encadrant et formateur d’un junior
développeur en capacité de gérer la partie R&D de nos développements ***
15
Développeur junior 
développeur apprenant ou en capacité de développer mais nécessitant un encadrement 
4
Sysadmin
gestion de l'hébergement des données, sécurité système...
1
Administrateur plateforme
gestion des données sur la plateforme
modération des données 
1
Designer
création de design et ergonomie plateforme
2
Animateur (terrain Et/ou CO)
animateur terrain et/ou accompagnateur de partenaires 
2
Communial (Commercial)
communication sur les communs et valeurs du projets en contact avec les futurs clients et partenaires. 
suivi des projets. 
Interface avec l’équipe de développement et le client. 
2
Assistant administratif
ressources humaines
tâches administratives
gestion des salaires, dépenses et recettes 
2
Comptable
comptabilité de la structure 
1
Stagiaire
Toute ressource précédemment citée souhaitant découvrir et contribuer au projet, et partager ses valeurs 
3
Bénévole
les valeurs portées par le projet facilitent l’apport de ressources bénévoles non négligeables au développement du projet : 
recherche de financement
production documentaire
experts pouvant être sollicités sur des compétences particulières 
25



Il nous paraît important de détailler les compétences, savoir faire et l’expérience de l’équipe actuelle du projet : 

Tibor KATELBACH : 15 ans d’expérience en tant que développeur web, certifié par le ministère de la Recherche et de l’éducation comme travailleur effectuant des travaux de recherche et développement pouvant s'appliquer au Crédit impôt recherche. Spécialiste Système d’information : conception, architecture technique et système, infrastructure, développement, interopérabilité, innovation, suivi projet. formateur informatique. Expertise sur les logiciels libres, sur les développements open-source et projets collaboratifs. Chef de projet : cadrage et gestion de projet, définition des objectifs du système d'informations, chef d’équipe...Tibor a contribué à de nombreux projets innovants
Clément DAMIENS , Développeur Senior 
Thomas CRAIPEAUX, Développeur Senior et administrateur système
Florent BENAMEUR, accompagnateur aux changements et aux dynamiques de transitions territoriales et organisationnelles. Coordinateur des projets médiation numérique, laboratoire de recherche-actions.
Nicolas,, Développeur Senior 
Anatole, Développeur Senior 
Yorre, Développeur Senior 
Jean , Développeur Senior 
Mirana, Développeur Confirmé
Ifaliana, Développeur Confirmé
Christon, Développeur Confirmé
Jaona, Développeur Junior
Christianie, Community Manager
Christelle, Administration
Caroline Paret, Assistante administrative
Béatrice POTIER, accompagnatrice sur la méthodologie ARIANE d’animation des territoires
Jérôme GONTIER (bénévole) : ingénieur hospitalier au CHU Réunion mène des projets d’envergure d’organisation et de performance (dictée numérique à reconnaissance vocale, plateforme SMS de rappel de RDV, projet de zéro papier, intégration automatisée des bilans de labos de ville, gestion des lits….) 

La structure est impliquée dans tous les réseaux du numérique. Elle est reconnue en tant que contributrice active et innovante à l’international. Localement elle est reconnue par la Technopole de La Réunion comme acteur majeur du numérique. 

Elle contribue au fonctionnement et au développement du web depuis plus de 15 ans. L’équipe a acquis une bonne expérience dans le montage de projets innovants et collaboratifs mais aussi dans la contribution bénévole à des outils servant la connaissance des territoires. 
L’équipe est à l’origine d’un collectif d’experts séniors informel CORD : Collaborative Open R&D. Elle participe à un groupe de réflexion au sein de la W3C. 
A l’échelle internationale, l’équipe contribue à l'open App ECosystem ; un collectif œuvrant pour améliorer, standardiser et développer des systèmes inter-opérants. 
Notre philosophie nous amène à promouvoir des  systèmes et des bases de connaissance ouvertes et sécurisées, développant ainsi la visibilité, la transparence et la création de lien. 

Notre approche se veut toujours innovante, ouverte et adaptée aux technologies actuelles. Nous développons nous même, depuis 9 ans, les outils nécessaires pour améliorer la connaissance des territoires 

L’équipe est en lien avec différents acteurs du numérique à La Réunion, en métropole et à l’international :

Zone géographique 
Réseaux et partenaires
Réunion
Orika, TEEO, Esiroi, LIM, Logicells, Technopole, Red Samurai, Digitale Réunion, Zeop, SRI, Nexa, Pôle emploi, Réseau des Jeunes Entrepreneurs Réunionnais, Smart City La Possession, Association WebCup ...
Métropole
Adullact, Framasoft, La Myne (connecté French Tech + pôle emploi), Écosystème des Tiers Lieux Ouvert et Libre Francophone 
mais aussi : 
Toulouse : Nacelles 0.2 
Lille : http://lacoroutine.org
Paris : http://www.opensourcepolitics.eu
St Etienne : http://www.openfactory42.org
Brest : https://www.telecom-bretagne.eu


International
W3C, FabLab de Barcelone, Open App Ecosystem, Collaborative Technology Alliance, Fuzzy Frequency, Transformap, Assemblée des communs, Livin Coop 


L’équipe rassemble ainsi toutes les compétences pour développer des fonctionnalités de qualité mais aussi pour accompagner l’ensemble des parties prenantes pour appréhender les enjeux, formuler les besoins et travailler en mode agile dans une véritable démarche de co-développement. 


NOS PROJETS / ACTIONS
OPEN-ATLAS met en place et conduit des projets d’envergure :
COmmunecter est un projet d’innovation sociétale ouvert à tous et à tous les territoires, collaboratif, libre et open-source. Un bien commun qui se construit dans le temps, suivant une méthode Agile, pour et en concertation avec tous les acteurs d’un territoire (habitants, associations, entreprises et collectivités), afin de co-construire les “territoires de demain", plus intelligents et interconnectés. 
COmmunecter a été incubé à la Technopole de la Réunion. Il est également référencé à la Civic Tech en tant que plateforme citoyenne de référence.
Smarterre est une démarche globale portée par Open Atlas, déployable sur n’importe quel territoire pour favoriser l’innovation collaborative et la transition écologique.
Divers projets déclinés à partir de la plateforme mère COmmunecter (COSTUM) que nous vous détaillerons dans le document références OPEN-ATLAS
CoBus : un projet de réduction de la fracture du numérique auprès des quartiers prioritaires avec une approche structurante à la fois pour les bénéficiaires et le territoire : proximité, concertation, formation adaptée au numérique, citoyenneté numérique avec le cas pratique COmmunecter + réseau d’antennes pour alimenter en internet des zones non couvertes et des quartiers prioritaires  + interconnection des ⅓ lieux de La Réunion.

Les différentes activités de notre structure  :
Développement web :
Développement d’outils numériques informatiques pour favoriser le développement des territoires, valoriser les acteurs locaux et encourager l’économie locale
Gestion de communecter.org et développement des projets liées à COmmunecter
Vente de services liés à l’utilisation de la plateforme COmmunecter
Système d’acquisition et visualisation de données
Production d’API (Applications Programming Interface) et d’open data sémantique
Hébergement de données et d’application


Animation : 
Outils d’animation
Gestion et/ou accompagnement d’équipes liées à l’animation des projets menés 
Organisation d’événements (hackathon, rencontre citoyenne, forum des communs…)
Animation d’événement (cartoparties, utilisation de Communecter pour la communication autour d’un événement…)
Animation éditoriale

Formation : 
Formation initiation au numérique, smart city, outils collaboratifs, efficience de territoire, Système d’information Territorial…
Ecole du libre 

Conseil, recherche et développement : 
Dynamiser l’activité et le partage interrégional
Analyser, mesurer et améliorer les process d’un territoire
Promouvoir et développer des filières locales (Culture, Economie Sociale Solidaire, Economie Circulaire, Laboratoire, Recherche & Développement, Energie, Mobilité…) d’un territoire
Conseil et implémentation en système d’information territorial
Améliorer l’efficience des organisations
Proposer de nouveaux outils juridiques autour des communs
Recherche et développement technologique software, hardware et sociétale
Concevoir des solutions pour décentraliser et distribuer le Web

 De plus l’expérience acquise durant ces 11 années nous a permis de développer :
une philosophie et une éthique de travail : nos réalisations sont portées par une structure à but non lucratif, nos développements sont open-source
une nouvelle approche de travail en commun, de collaboration avec différentes structures, de travail d’équipe et de gestion de projet en mettant en place des outils numériques performants de travail collaboratif :
Éditeurs de texte collaboratif (Framapad, HackMD, CryptPad)
Gestionnaire de fichiers (Nextcloud)
Gestionnaire de tâches (Wekan)
Système de votes (Communecter, Loomio)
Messagerie instantanée (Rocket.chat)
Plateforme de débat (Dialoguea)
Documentation participative (Wiki)
Hébergement de code open source (GitHub)
Réseaux sociaux libres (Communecter, Mastodon, Diaspora*)
Système d’exploitation (Ubuntu)
Visioconférences (Appaer.in, Framatalk)
une méthodologie Agile 
la mise en oeuvre de l’intelligence collective au quotidien ainsi que l’application d’une gouvernance horizontale

Nous participons, à notre échelle, à la création du monde numérique de demain et nous croyons à sa capacité à répondre aux défis des transformations de la société.


NOS PRODUITS

Fort d’une expérience de plus de 15 ans dans le développement et l’innovation numérique et plus de 6 ans sur le terrain de l’expérimentation du développement territorial, notre équipe est en mesure de proposer les services suivants : 

développement web 
Animation et accompagnement 
Costum
Animation locale sur le territoire de la Réunion *


SmarTerre
Accompagnement auprès des acteurs locaux et des partenaires sur d’autres territoires (méthode, tutoriels, formation, mise en place de projets...)
Développement de filières


Développement spécifiques adaptés aux besoins des territoires 


Version mobile 





NOS PRIX

Notre philosophie de plan de financement n’est pas celle classique d’une entreprise qui va vendre x fois un produit fini. Nous encourageons le partage des coûts de développement. Dès qu’un service/module est développé ou finalisé il rentre dans le domaine du bien commun et peut être utilisé par les bénéficiaires de manière libre. 

Au niveau du développement web, en dehors de toute animation et accompagnement, ce ne sont que les nouvelles fonctionnalités, les costums ou autres développements spécifiques qui font l’objet d’un financement. Cette originalité permet à nos clients de bénéficier à la carte de modules gratuits mais surtout d’une plateforme de développement de territoire de plusieurs centaines de milliers d’euros (suivant leur besoin) pour un coût bien inférieur.
C’est ce qui fait la force d’un modèle win/win et explique une adhésion croissante à nos services. 

De plus nous avons un fonctionnement agile permettant de nous adapter aux besoins de nos clients que ce soit sur la forme du contrat (one shot, abonnement, subvention…) que sur le suivi et l’accompagnement. 

Nos prix prennent en compte les éléments suivants : 

Fixe 
Au besoin 
développement spécifique
maintenance
hébergement des données 
admin système
gestion de projet 
animation
accompagnement
administration plateforme
accompagnement des partenaires sur leur territoire


DONNÉES ADMINISTRATIVES
Structure 

Nom : OPEN-ATLAS
Forme juridique : association
Date de création : 31/01/2009
Nationalité : française  
Adresse siège social : Association OPEN-ATLAS 56 rue Andy, 97422 La Saline Réunion
Tel : +262 (0)6 93 91 85 32  
SIRET/SIREN : 51338183000019
Code activité (NAF) : 9499Z
RNA : W9R2002319
Instance dirigeante

 Rôle
Nom
Date de Naissance
Adresse
Téléphone
 Profession
 
Président
 Luc Lavielle 
15/11/77
56 rue Andy, 97422 La Saline
0693918532
 Médecin
Secrétaire
 Gilles Guillon
19/10/2021 
 13 rue de la balance 97436 Piton Saint Leu
0692728317
Instituteur
Trésorier
 Pierre Magnin 
 14/06/78
 allee des coriandres 97460 Eperon
0693918532
 Médecin


Coordonnées bancaires  

Recettes 

Notre modèle économique se construit en cofinancement et partage des coûts avec différentes sources de revenu :
Prestations de services auprès de nos partenaires
Appels à projets
Financement public : le projet étant d’intérêt général et porté par une association, nous faisons appel à des subventions et financements régionaux, nationaux et européens. 
Financement participatif
Les cotisations des adhérents à l’association OPEN-­ATLAS
Des dons publics et privés défiscalisables
Licence de réciprocité : une contribution définie dans le code social permettra à tous les partenaires d’être rétribué pour sa contribution, ou inversement de rétribuer la plateforme pour son usage.
Bénévolat 

Notre structure a reçu un soutien financier de la part de partenaires publics et privés pour un total de 967 971 € : 

Année
projets 
Total
2014
4000€ Région Réunion +50000€ Incubateur Technopole Réunion,Europe
54 000€
2015
20000€ Région Réunion + 51000 € Association des Camélias + 4000 € Association Granddir
75 000€
2016
22000€ crowdfunding + 10000€ CGET + 15000 € association Hors cadre Lille
47 000€
2017
50000€ AFNIC + 30 000 € Cyberun + 15000€ Notragora + 8000€ i-viatic 


103 000€
2018
 HVA 3500 €, Hors Cadre 800 €,1000 € Le Monde + Le Port 14 300€ + 25 000€ TCO
151 124 € 


2019
Budget citoyen du 62 16 K€, CTE National 1 24,5 K€, CTE National 2 MTES 96,6 K€, CTC 7 K€,Subventions Lush 9,5 K€,  La Possession 5K€, CRESS 6 K€ , GAL Meuse 12,5 K€,  La Raffinerie 12,5 K€, Notragora 3,2 K€,, Filière numérique 19,8 K€, autres 20 K€, Hinauro 12 K€, Solidarnum 12 K€, Wello 4 K€, 
312 821 € 


2020
SARL EVOLUTIO 4 K€, CRESS 3,5 K€, ASSOCIATION TRANSITION CITOYENNE 7,5 K€, CMCAS DE HAUTE BRETAGNE 8 K€, DEAL 39 K€, AGENCE ALPINE DES 12 K€,  ASSOCIATION TRANSITION CITOYENNE 1 K€, LA MEUSE 5 K€, Costum Kosasa 3 K€ , Costum Maia Show Co Sylvette 5 K€, AFNIC 14 K€, CTC 13 K€, FRANCE TIERS LIEUX 5,5 K€, Coexiscience 14K€, ONG 4K€, 
Nb : progression freinée par la crise COVID
225 026 €




TOTAL
967 971 €

Notre expérience des 4 dernières années dont celle assez révélatrice de 2019 nous amène à effectuer un découpage en 5 grands types de contrats : 

Types de contrats 
Explications
Contrat < 10K€
contrats de développement actés généralement auprès d’associations. 
10K€ < contrat < 25K€
contrats avec des collectivités inférieurs au seuil de déclenchement d’un appel d’offre. 
25 K€ < contrat < 50 K€

contrats pluriannuels de développement + animation et/ou accompagnement
50 K€ < contrat < 100 K€
contrat > 100 K€




NOS RÉFÉRENCES 

Contrats réalisés ces 4 dernières années  : 


contrats 
Description
CTE National 2 MTES
Le contrat de transition écologique illustre la méthode souhaitée par le Gouvernement pour accompagner les territoires : une coconstruction avec les élus, les entreprises et les citoyens qui font le pari d’une transition écologique génératrice d’activités économiques et d’opportunités sociales
CTC
plateforme du Pacte pour la transition : 50+ organisations et des milliers de citoyen·nes
s’engagent pour des communes plus écologiques et plus justes
La Possession
Une plateforme de mise en relation et de synergie entre le tissu associatif et les habitants de la Possession 
CRESS
Structuration de la filière de l’ESS à La Réunion 


La Raffinerie
Plateforme de gestion de tiers lieu collaboratif 
Notragora
NotrAgora est une plateforme internet contributive qui se construit pas à pas, en relation avec ses usagers, ses partenaires et les habitants des Hauts-de-France.
Elle est un espace de production et de diffusion des productions mises en œuvre dans le cadre de projets participatifs menés par l'association Hors Cadre et ses partenaires.
Filière numérique 
Structuration et animation de la filière numérique à La Réunion 
DEAL : Amerlior out Kaz
Système de demande de Financement d’amélioration de l’habitat
France Tiers Lieux 
Cartographie et caractérisation des tiers lieux au national
Fabrique de l'énergie
Structuration et animation de la filière énergie en Bretagne 
OCECO
expérimentation avec l’ANACT , nouveaux outils structurant l’organisation interne 



ECO-RESPONSABILITÉ NUMÉRIQUE

Plateforme Open-Source 

Nos épices, nos recettes et notre code source sont libres d’utilisation et d’amélioration.
L’Open Source : une méthodologie de développement encadré juridiquement par des licences qui contribuent à un cercle vertueux d’amélioration d’une technologie.
L’Open Source repose sur :
une vision profondément humaniste,
de l’innovation collaborative,
la reconnaissance par le partage des savoirs.

Richard Stallman, contributeur au mouvement du logiciel libre, définissait le logiciel comme suit : “le logiciel est, à la manière de la connaissance scientifique, une forme de patrimoine de l’humanité, un bien commun que nous enrichissons collectivement, pour le bien-être de tous”.
 
Pratiques dans la structure 

Notre mode de fonctionnement limite les dépenses énergétiques et polluantes. 
Chaque entité travaille à son domicile avec ses propres équipements légers : un portable et une connexion internet. 
Les rencontres se font par visioconférences, chat ou téléphone, en présentiel qu’en cas de nécessité. 
Nous utilisons à majorité nos propres outils de développement, très peu d’achats de composants nécessaires. 
Les différentes formes de rencontres peuvent faire l’objet de réunions, SPRINT, SCRUM et méthode Agile. 
Les outils utilisés sont open-source. 


