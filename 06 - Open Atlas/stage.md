# Informations globales sur les stages
## Bienvenue !
Nous sommes heureux de vous accueillir au sein de notre association. Vous venez d’intégrer l’association OPEN ATLAS.

Ce livret a été mis en place pour vous guider et vous accompagner dans les grandes étapes de votre intégration. Vous y trouverez des informations pratiques et des éléments de présentation de l’association.

Cette page vous est spécialement destiné et vous sera utile pour comprendre les activités, le fonctionnement ainsi que l’organisation de l’association que vous intégrez. Il a pour objet de vous donner les informations utiles dans votre vie professionnelle quotidienne.

Nous vous en souhaitons bonne lecture et bon usage.


## L'association
Nous vous invitons à lire attentivement ces pages :
- [Présentation de l'association](/1 - Le Projet/openatlas.md)
- [Historique du projet](/1 - Le Projet/historique.md)


## Votre arrivée
Avant toute chose assurez d'avoir connaissance de :
- Votre référent : c’est votre contact principal. Il a pour mission de vous accueillir, de vous aider, de vous informer et de vous guider dès votre arrivée.
- L'intitulé et la nature de votre poste.
- Votre convention : notre association a signé avec votre Centre de formation une convention. En signant votre convention, vous vous êtes engagé⋅e pour une durée déterminée.
- Vos horaires de travail : les bureaux sont ouverts de 8h30 à 17h.
- [Decouvrir l'outil](/02 - Utiliser l'outil/par usages.md)


## Vos obligations
L’association dépend de la Convention Collective de l’Animation et des règles qui la régissent.
- **Sécurité** : il incombe à chaque salarié de prendre soin de sa sécurité et de sa santé ainsi que de celle des autres personnes concernées du fait de ses actes ou de ses omissions au travail.
- **Environnement** :
    - La gestion des déchets : l’activité de l’association génère peu de déchets. Toutefois, le tri sélectif est pratiqué dans le respect des jours de collecte.
    - La préservation des ressources : nous sensibilisons les membres de l’association au respect des règles environnementales.
- **Absences** :
    - Toute absence doit être justifiée. Qu’elle qu’en soit la cause, vous devez en informer l’association dans les meilleurs délais. Toute absence injustifiée peut faire l’objet d’une sanction disciplinaire.
    - En cas de maladie, vous devez prévenir ou faire prévenir votre responsable. En cas d’interruption de votre travail, votre médecin vous délivre un certificat d’arrêt de travail qu’il vous faut envoyer à votre employeur ainsi qu’à votre Caisse d’Assurance Maladie.
    - Certains évènements familiaux donnent droit à des journées d’absences exceptionnelles (justificatifs à fournir).


## Vos aides et démarches
Selon votre statut et vos revenus, vous pouvez prétendre à certaines aides. Ci-dessous quelques des démarches que vous pouvez effectuer afin d’améliorer votre situation professionnelle.


### CAF
Si vous payez un loyer ou remboursez un prêt signé avant le 1er février 2018 pour votre résidence principale, et si vos ressources sont modestes, vous pouvez bénéficier de l’une des deux **aides au logement** suivantes (elles ne sont pas cumulables !).
- L’allocation de logement familiale (ALF) si vous avez des personnes à charge dans votre foyer ou formez un ménage
- L’allocation de logement sociale (ALS) si vous ne bénéficiez pas de l’ALF.

Le montant de l’allocation est soumis à plusieurs critères. Afin de connaître précisément vos droits, faire la simulation sur le site de la Caf.fr.


### Devenir indépendant, professions libérales
Le travailleur indépendant (ou Freelance en secteur informatique) est un professionnel qui exerce une activité économique indépendante. Il est autonome et propriétaire de ses moyens de production. Il est décisionnaire et garant de l’évolution de son entreprise.
L’activité peut, sous certaines conditions, être exercée au local d’habitation de l’entrepreneur.
La rémunération dépend de la facturation de prestations. Une prestation se facture à l’heure, à la journée ou au forfait en fonction de ses missions.

L’entrepreneur est assujetti à l’impôt sur le montant brut de ses revenus annuels (l’IR) après abattement de ses frais professionnels, dans la catégorie des bénéfices non commerciaux (BNC). Il cotise également à diverses caisses d’assurances. Les cotisations sont calculées et payées chaque mois ou chaque trimestre en fonction des recettes encaissées le mois ou le trimestre précédent.

Les Indépendants peuvent adhérer à des associations agréées et bénéficier des avantages fiscaux liés à cette adhésion :
- Agessa
- Maison des Artistes

Pour toute information liée à la création d’activité indépendante :
Tel : 01 53 35 83 63
[artistes-auteurs.urssaf.fr](https://www.artistes-auteurs.urssaf.fr/accueil)


### La formation professionnelle des entrepreneurs
Vous avez droit à la formation car vous réglez la CFP (Contribution pour la Formation Professionnelle). Votre attestation est à retirer sur le site de l’URSSAF (via la déclaration micro-entrepeneur) rubrique "mes échanges avec l'URSSAF". Ce montant indiqué sur l’attestation ne correspond pas à l’enveloppe à laquelle vous avez droit, mais au montant de CFP que vous avez versé au cours de l’année d’activité écoulée.


##### 1 - Se renseigner auprès de votre Fonds d’Assurance Formation
Afin de savoir à combien vous pouvez prétendre, rendez-vous muni de votre code APE sur le site du Fonds d’Assurance Formation correspondant à votre activité. FIFPL pour les professions libérales et AGEFICE pour les prestataires de services.


##### 2 - Choisissez votre formation
Vous n’êtes pas limité à ce que propose votre FAF ! Vous êtes libre de choisir ce à quoi vous voulez être formé avant de faire une demande de financement. Vérifier que la formation choisie soit éligible à un financement auprès de votre FAF.


##### 3 - Constituez votre dossier de demande de financement
Le dossier doit être déposé au moins un mois avant le début de la formation pour les prestataires de services. Les libéraux peuvent faire leur demande en ligne sur le site du FIFPL dans un délai maximum de 10 jours après le début de la formation.


##### 4 - Suivez votre formation et transmettez vos justificatifs
Pendant votre formation, vous devrez signer les feuilles d’émargement et conserver vos justificatifs de présence. Vous transmettrez l’ensemble de ces derniers et la facture payée de la formation à votre organisme référent pour vous faire rembourser.
Le remboursement, partiel ou total, intervient généralement dans un délai de deux à trois mois après la fin de la formation.


## Interlocuteurs
Mail : contact@communecter.org


## Évaluation / Diagnostique
- Visions sur les missions ?
- appréciation du code ?
- sensation d'autonomie ?
- est ce que vous avez senti une progressé ? 
- apprit des nouvelles choses ?
- Propositions a faire 
    + Processus du stage
    + Technique 
    + Design 
    + Fonctionnement d'Open Atlas 
- Comment c'est passé l'intégration avec les CDP ? 
- Qu'est ce que vous avez aimer chez Open Atlas ?
- Est ce que tu veux continuer avec nous ? 
- Quel serait votre Job de reve ? 
- Présentation des conditions financières ?

### Diagnostique par le suiveur du stage
- capacité et qualité de 1 à 5
    + posé des questions ? à demander de l'aide ?
    + rapidité de codage ?
    + qualité du code ?
    + revenir sur son travail ? 
    + prendre des choix et orientation ( plutot simple ou complexe ) 
    + capacité critique 
    + capacité d'amélioration et proposition
    + motivation
    + niveau technique 
    + capacité d'expression 
    + creativité innovation 
    + design
    + architecture systeme
    + documentation 
    + degré d'intégration dans le code ? 
    + degré d'autonomie ? 
    + degré d'intégration dans l'équipe ?


