# Contrats obtenus par Open Atlas

https://codimd.communecter.org/RD3z75u4Tca2kuze3mbyrw?both#
## Année 2022
|      Date     |           Projet            | resp |  realisa  |  Montant TTC (en €)   |
|:-------------:|:---------------------------:|:----:|:---------:|:---------------------:|
|02/04/2022		| COMMUNE ST DENIS			  |  tka | Anatole   |						 |
				|							  |		 | Gova      |                       |
|02/16/2022		| SCBCM / MTES 				  |  tka |  Anato    |    --- €              |
|02/17/2022		| SGAR						  |  tka |  flo      |						 |
|				|							  |		 | christon  |    --- €              |
|03/28/2022		| TECHNOPOLE REUNION		  |  tka |  mirana   |                       |
|				|							  |		 |	nico     |                       |
|				|							  |		 |	christon |    --- €              |
|04/22/2022		| ANCT						  |  tka |  anatole  |                       |
|				|                             |		 | gova      |                       |
|				|                             |		 | schumman  |                       |
|				|                             |		 | rinelfi   |    --- €              |
|09/09/2022		| GRANDDIR					  |  tka |  christon |    --- €              |
|11/03/2022		| MAKE SENS					  |  tka |  nico     |                       |
|				|                             |		 | christon  |    --- €              |
|11/29/2022		| LA ROSEE					  |  tka |  flo      |                       |
|				|                             |		 | christ    |    --- €              |
|12/07/2022		| REDLAB					  |  tka |  flo      |                       |
|				|                             |		 | christ    |    --- €              |
|:---------------------------:|:-------------:|:----:|:---------:|:---------------------:| 


## Année 2021
|      Date     |           Projet            | resp |  realisa  |  Montant TTC (en €)   |
|:-------------:|:---------------------------:|:----:|:---------:|:---------------------:|
| 11/6/2021     |  ARCS-ARCI  Projet RIES     |  tka |  christ   |    3000 €             |
|               |  Asso Préfiguration Nat.    |  flo |  flo,clem |    5500 €             |
| 6/1/2021      |  ASSOCIATION KOSASA         |  tka |  mira     |    4000 €             |
| 11/6/2021     |  CEDTM  Projet Sommom       |  tka |  anatole  |    6000 €             |
| 25/11/2021    |  CIE des TIERS-LIEUX        |  flo |  flo      |    2500 €             |
|               |  CRESS                      |  tka |  clem     |    6000 €             |
|               |  DEMORUN                    |  tka |  nico     |    2530,32 €          |
|               |  DRFIP REUNION              |  TFB |tka,ana,gov|    22800 €            |
| 11/6/2021     |  HER                        |  tka |  nico     |    3200 €             |
| 11/6/2021     |  HER  Projet HER - audit    |  tka |  flo      |    800 €              |
| 6/12/2021     |  HORS CADRE   Notragora     |  old |  flo      |    3081,11 €          |
| 6/12/2021     |  HORS CADRE  Notragora      |  old |  flo      |    2764,98 €          |
| 24/8/2021     |  HVA   Maintenance          |  old |           |    400 €              |
|               |  LA RAFFINERIE              |  tka |  rapha    |    4000 €             |
|               |  LE DIVELLEC Mona           |  old |           |    1500 €             |
|               |  NEREE    Meteolamer        |  tka |  yorre    |    4000 €             |
| 17/6/2021     |  OXALIS SCOP SA  Faben      |  tka |  christon |    5000 €             |
| 8/3/2021      |  SCBCM / MTES Héberg CTE    |  tka |  tib      |    6000 €             |
| 9/7/2021      |  SCBCM / MTES Héberg CTE    |  tka |  tib      |    14000 €            |
| 15/7/2021     |  SCBCM / MTES Plateforme    |  tka |  tib      |    25806,45 €         |
|               |  SCBCM ECOLOGIE             |  tka |           |    6000 €             |
| 5/11/2021     |  SGAR   Tiers-Lieux         |  tka |  multi    |    35500 €            |
|               |  SHOW-CO ARTS               |  tka |  gova     |    3200 €             |
|               |  SOLIDARNUM                 |  tka |  flo      |    8000 €             |
| 5/11/2021     |  TECHNOPOLE  MEIR - 30%     |  tka |  nic,mir  |    11058 €            |
| 28/12/2021    |  TECHNOPOLE  MEIR - 20%     |  tka |           |    7373,27 €          |
| 29/12/2021    |  TECHNOPOLE  MEIR - Vidéo   |  tka |           |    1400 €             |
|:---------------------------:|:-------------:|:----:|:---------:|:---------------------:| 

## Année 2020
|           Projet            |      Date     | resp |  Montant TTC (en €)   |
|:---------------------------:|:-------------:|:----:|:---------------------:|
|  CMCAS Haute Bretagne       |  juillet. 2020|   tka|  8 000                |
|  Sommom                     |  avril. 2020  |   tka|  2 500                |
|  Pacte Transition 3         |  avril. 2020  |   TCR|  12 500               |
|  CTE National 3             |  juin. 2020   |   tka|  40 000               |
|  CRESS                      |  Aout 2020    |   tka|  3 500                |
|  France Tiers Lieux         |  Sept 2020    |      |                       |
|  Afnic                      |  Aout 2020    |   TJ | 14 000                |

## Année 2019
|           Projet            |      Date     |  Montant TTC (en €)   |
|:---------------------------:|:-------------:|:---------------------:|
|  CTE National 2             |  Mai. 2019    |  100 000              |
|  Pacte pour la Transition  1|  Jan. 2019    |  7 000                |
|  CRESS                      |  Mars 2019    |  4 500                |
|:---------------------------:|:-------------:|:---------------------:|

## Année 2018
|           Projet            |      Date     |  Montant TTC (en €)   |
|:---------------------------:|:-------------:|:---------------------:|
|  Le Port                    |  Août 2018    |  14 300               |
|  Centres sociaux connectés  |  Nov. 2018    |  5 000                |
|  CTE                        |  Juill. 2018  |  24 700               |
|:---------------------------:|:-------------:|:---------------------:|

## Année 2017
|           Projet            |      Date     |  Montant TTC (en €)   |
|:---------------------------:|:-------------:|:---------------------:|
|  NotrAgora                  |  2017         |  15 000               |
|  KGougle.nc                 |  2017         |  0                    |
|  TOTAL                      |               |  70  500              |
