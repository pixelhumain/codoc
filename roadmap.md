
# Bonne Année 2024 !!
On vous souhaites beaucoup d'innovation, de créativité et de collaboration pour cette nouvelle année !!
Voici une 

# roadmap T1 2023
- **Refactor** Total du moteur et interface du **CMS** 
- **Calendrier Fédéré** grace à activity PUB, intéropérant dans les 2 sens avec les events de Mobilizon https://fedidevs.org/projects/server-apps/communecter/objects/event/
- **Projet : OCECO CAC** : plateforme de récolte de proposition de citoyens pour les Conseils d'Actions Citoyens de Saint Denis https://www.communecter.org/costum/co/index/slug/coSinDni#oceco
- Geodata : Data **IRIS Quartier injecté** et utilisable 
- **Groupe de travail OFR-TL** (Outil pour Faire Réseau en TL) https://www.communecter.org/costum/co/index/slug/portailDesReseauxDeTierslieux#
- **OPTIMisation** du temps de chargement des pages 
- **Projet : MouvOutremer** https://www.communecter.org/costum/co/index/slug/mouvoutremer
- Projet : **MEIR MAPPING DE L'INNOVATION DE LA TECHNOPOLE** https://www.mapping.technopole-reunion.com/
- **intégration de la COREMU dans OCECO** (outil d'appel à commun), permettant une gestion transparente des budget et de leur distribution 
- refonte du **Costum de la Raffinerie** avec le monteur de CMS
- Formulaire du **RENCENSEMENT et cartographie National 2023 pour France Tiers Lieux** https://cartographie.francetierslieux.fr/
- intégration de **Matomo** (OPEN SOURCE) option pour les costums, data analyse des visites https://communecter.matomo.cloud/
- **PROJET : Appel à Projet Saint Denis** 2023
- **CODate inside** : Outil de choix de date 

# roadmap T2 2023
- **CODATE inside calendar** via le calendrier 
- **CORNERDEV** : un btn disponnible sur tout les contextes CO lié à oceco pour faire des remarques collective et des remontés connecté avec la plateforme de QA 
- **CODoc : documentation de CO** https://www.communecter.org/#codoc construit directement avec le repository Gitlab de codoc 
- **meilleure installation Docker** https://gitlab.adullact.net/pixelhumain/codoc/-/blob/master/04%20-%20Documentation%20technique/Installer%20Communecter/docker.md
- **V0.2 CMS EDITOR** : La suite des amélioration du moteur de CMS permettant l'autonomie sur la gestion des costums 
- **graph geo bar, pie chart et heatmap** : de vrai **observatoire géographique** avec l'intégration de graph D3JS directement sur les cartos LeafLet 
- **Unit Test** : nos fonctionnalités principales en test unitaire pour renforcer nos déploiement 
- **Tableau de bord COform**, analyser comment se déploiement vos questionnaires 

# roadmap T3 2023

## JUILLET
- **Projet fédérés** : vos Projects sont partageable sur le Fediverse, connectez des contributeurs du Fediverse
- générer les **OFRTLs Region**
- Ecrivez des **scenario de PPT** en reveal js contenant des graphs 
- **Projet pré analyse FTL** : outils de structuration de la donnée et d'export pour France Tiers Lieux
- ancien observatoire rétabli
- **statistique FTL** et analyse de la plateforme France Tiers Lieux
- **Template de Costum COEvent** : pour organiser et gérer vos evennements ouvertement 

## AOUT
- COFORMs **Answer Navigator** : naviguer dans les reponses de votre questionnaire, créer des interface valorisant la data
- **NAVIGATOR des Tiers Lieux** : naviguer dans le monde et la data des TLs, aggragation de plusieurs source de formualaire
- **L'IA des Tiers Lieux** : on a questionné et expérimenté chatGPT à propos des Tiers Lieux
- **Tableau de bord fédéré**
	- usage 
	- flux de news
	- events
	- personne ont AP activé
	- projects
- **classer vos flux d'actualités du fediverse** avec liste de lecture en tagguant les sources
- amélioration des links et des roles
- **Agenda Fédéré** : ajouter vos open agenda et vos sources iCal pour les voir sur une meme interface

## SEPTEMBRE
- **Gestion des spams** : Interface Super Admin pour effacer et gérer les Spams  https://codimd.communecter.org/RikdgsJ7SCOZF2VJQeWXaA#
- COFORM Data : **Export excel handsontable**
- Search Rajouter **les ODD en filtre générique** 
- Block CMS pour faire des **slide PPT dans vos costums** 

# roadmap T4 2023

## OCT
- Projet : Commun Lundi avec son Coevent https://www.communecter.org/costum/co/index/slug/communLundi#

## NOV
- **AAP 0.3** : l'Appel à projet **fait peau neuve** avec le nouveau design de l'agence klee 
	- socketio 
	- Versionnage coform
	- mobile kanban
- **Block CMS KANBAN** pour suivre vos projets dans vos COstums
- **Open Badges Fédérés** : partager vos Open Badges avec le Fediverse 
- interface ergonomique en **Kanban de distribution et Classification des roles** d'une Communauté
- **Librairie de Block CMS de type graph**, mettez des graphes dans vos costums en toute autonomie

## DEC
- **AAP COSindni 2024**, intégration des **données INSEE**, statistiques décisionnelles et croisement de donnée pour avoir un **Observatoire de Quartier**
- interface ergonomique en **Kanban gestion des Open Badge** pour une Communauté



# roadmap 2022
# roadmap 2021

# roadmap 2020.1

- Mongo 3.6 
- PHP7
- CDN OVH
- CO FORMS Générique 
- CObserv
- New Design
- Dashboard Personnel
- new Filter refactor
- Costum Generique template 

# roadmap 2020.2

- passage Yii2
- refonte APi
- refactor js conforme avec JSLint
- refactor SEO 
- CI : integration continue
- new design et Homepage 2020
- system Maintenance
- Collabathon trimestriel
- costum Ecoute à projet 
- Fediverse
- COP2P : COPI et COPIPI
- Costum Light
- interop RSS
- interop WEKAN
- interop Loomio
- Costum GogoCarto
- Costum Transiscope
