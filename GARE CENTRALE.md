GARE CENTRALE
===

###### tags: `comprendre` `holoptisme`

[version Mind Map](https://atlas.mindmup.com/openatlas/bird_eye_view/index.html) 

## [OPEN ATLAS ](https://codimd.communecter.org/UTyzDSYZRGyPNo1PSAtiGg#)

- [REFLEXION](https://docs.google.com/presentation/d/1P_4ghDdCrHCJENmFPNDTYwD9qrUxH6yM/edit#slide=id.g5dbf6fd585_0_171)
- [LISTE DE CONTACT SOCIÉtaire](https://codimd.communecter.org/GANo4kEbQuq2PqY4GuBX3w?both#)
- [SCE coopérative STATUT](https://docs.google.com/document/d/1-YxU4KCUWc6qKb6mnNlNLet-tSPfu2sRjUsORQLVaSE/edit#)
- [Observatoire](https://www.communecter.org/costum/co/index/slug/proceco#oceco.slug.openAtlas.formid.618b7b4abc572b35b4505da3.aappage.list?view=detailed&inproject=false)
---

## VISION D'ENSEMBLE
[Presentation Globale](https://codimd.communecter.org/nkgfbJTmRBW5nDOtUseZLw#)
[Présentation Open Atlas(jerome)](https://docs.google.com/document/d/1x957wzUAri5YZxkSMaNYhercBFbeK814EnUqvRaLK7Y/edit#)
[VISION PRODUIT](https://codimd.communecter.org/0YJPdApXQl2yFqTz1VHUDg#) 
[CO PREZ](https://codimd.communecter.org/YBGlT9H3Q7aoEBf6ueMt9w#)
[WHY CO](https://docs.google.com/presentation/d/1KdqpJ1T9Edb1RYQyhdJvtevfjA-FAU6xJmG8L7DS-04/edit#slide=id.g4b60dbcc9d_0_72)
[CO CITY](https://docs.google.com/presentation/d/1e-ozTx2dV3QXTiqwWk83xnZqNdHSm_N4quUAUj6DEtk/edit#slide=id.p)
[schema](https://excalidraw.com/#room=891d83c2e25e58cb3c73,qkB83fCGox1n4JEm-iND1w)

### [CODOC](https://gitlab.adullact.net/pixelhumain/codoc)
---

## JOURNAL ACTIVITÉS et NOUVEAUTÉS
doc interne de fonctionnement
#### [OCECO OPEN ATLAS](https://oce.co.tools/organizations/555eba56c655675cdd65bf19/organizations/detail/555eba56c655675cdd65bf19)
anciennement on utilisait [coteam Work](https://codimd.communecter.org/GkgTw4FjSQehXQkRo6clSA?both)
#### [COPROGRESS AVANCEMENT TRIMESTRE](https://codimd.communecter.org/_DLdjhrtTVG9b2dXb-kZdw?both#)
#### [POST DE COMM](https://codimd.communecter.org/uRoBVCGZTdCyNiauK-bVkg?both)
#### [COTeam worCO](https://codimd.communecter.org/GkgTw4FjSQehXQkRo6clSA) 
---

## GESTION DE PROJETS
[Kanban suivi de projet](https://www.communecter.org/costum/co/index/slug/proceco#welcome.slug.openAtlas.formid.618b7b4abc572b35b4505da3.page.list?view=project_kanban)

### Conception Architecture
- Coùt et Devis
    - Financement
        - découpage 
            - communs 
            - investissement 
            - prestation
            - innovation technique
            - innovation d'usage
- estimation de temps
- évaluation des ressources
    - assignation
    
### DEV
- Agilité 
- Assignation
- Résultat Atteint et non atteint
 
### [TEST Qualité assurance](https://docs.google.com/presentation/d/18eXXGdOnvebbkfIR5QSrKw7AkFGOBTUheJH64AzRsag/edit#slide=id.p)

### BILAN ÉVALUATION

--- 

## FEATURES
### COSTUM (doc todo)
### CMS 
##### Templates

### [COFORM](https://codimd.communecter.org/mlc2qxtGStCNb5MnUJwfiA#)
##### [oceco Form](https://)
##### Coform Templates 
##### CObservatoire
#### co-city, foss, smarterritoire,smarterre ? (luc: obsolète ?)

### [OCECO](https://codimd.communecter.org/4773mbh1TryRriorF79r3Q#)
- DOC https://codimd.communecter.org/Jrz7nr29QxisbCNVtcB3Sw#
##### [AAP](https://codimd.communecter.org/hzSqXC2lSAa6H0ewd1DI_g)
- [TEST](https://codimd.communecter.org/MDzBUpyISf2QMJoubheirQ)
- [PREZ](https://www.communecter.org/costum/co/index/slug/aap#welcome)
- [PREZ PPT](https://docs.google.com/presentation/d/1L6OLB3JAVi5UXYxExJ3hjq2KJRaQjoFaawvRiQiNN-s/edit#slide=id.g1577897bff5_1_207)
- [TODO](https://oce.co.tools/organizations/555eba56c655675cdd65bf19/projects/contributors/6119fb7bde0a5568e14ed59d)
- [BUGS](https://docs.google.com/presentation/d/1-qIeMdQg0IIJO_AYDAv-DM3QYwbp1G3IB_4L3DnQr5E/edit#slide=id.g16b67e5d538_2_63)
- [TEST FIELDS](https://codimd.communecter.org/MDzBUpyISf2QMJoubheirQ#)
- [OLD TODO BUGS](https://docs.google.com/presentation/d/1SP3F8VE3twEUNX9QuxqV6hP3TKcGpjdQK3HnjYlYnQ0/edit#slide=id.g127567aecb0_0_4)
- [TODO codi}(https://codimd.communecter.org/_TwBjzT6SqGgAiJX3yxnIA?both#Test-d%E2%80%99une-d%C3%A9mo-AAP-R%C3%A9silience)
##### Gouvernace et Gestion de Projet et Proposition 
##### [CORému](https://codimd.communecter.org/EvGX7xrHQtmr5n7T5o9oQQ?both#)
##### BUDGET PARTICPATIF

### [OPEN BADGES](https://codimd.communecter.org/_tD_BGxjRduMSK35C8QPLw)

### COMMUNECTER
Réseau globale

### API
api
api2
api oceco

### [Interop](https://docs.google.com/presentation/d/1pJ4wMV89Dac0YXL6qOA1OT6MzVSikGoR3nQQhYxwplA/edit)
https://gitlab.adullact.net/pixelhumain/codoc/-/blob/master/04%20-%20Documentation%20technique/interop.md

---

## [USAGES](https://codimd.communecter.org/p/Zv--E_yJV#/16)

- [codoc](https://gitlab.adullact.net/pixelhumain/codoc/-/blob/master/02%20-%20Utiliser%20l'outil/par%20usages.md)
- [CO par cas d'usages](https://codimd.communecter.org/pxZ0E0dJQLO-rUOwc1zH1A#) 
- OUTILS DE PROPOSITION / Evaluation / Financement / Suivi
    - Outil et process de Gouvernance 
    - D'holoptisme et de stygmergie 
    - de bilan et observatoire dynamqiue 
- [OFR-TL](https://codimd.communecter.org/0id6Cpb5R3CK0Ba7cEOoBA)
    - Outil pour Faire Réseau de Tiers Lieux 
- [COMMUNITY GESTION DE COMMUNAUTÉ](https://codimd.communecter.org/3_rr0JdTSfmescP_Z5i0Gg#)
    - [PROCESS](https://docs.google.com/presentation/d/1orYK1ceCmASaIldCXWkvKiLxw72payb4Zm0G73NpN4U/edit#slide=id.g16982062a7a_5_16)
    - [COEUR](https://docs.google.com/presentation/d/1NZr1DMFeS3I-JYuXrGvecijvgWW4zLaVvUyaevK-jHM/edit#slide=id.p) 
    - [COSTEAM](https://codimd.communecter.org/0YJPdApXQl2yFqTz1VHUDg#)
- COEVENT : Costum générique pour les évennements 
    - ex : https://lasemaine.tierslieux.re/
- CROWDFUNDING
    - ex : https://adopteuncommun.communecter.org/
- QUESTIONNAIRE 
    - USAGE NUMÉRIQUE EN TIERS LIEUX
    - CODATE : sondage de date
- FAIRE DES OBSERVATOIRES DYNAMIQUES
    - LIER à un ou plusieurs formulaire
- SMARTERRE : TERRITOIRE ET Filières
    - [SMARTERRE : Volet 1 Territoire Intelligent et Interconnecté](https://docs.google.com/document/d/1KC0TFna4i0m-hKTAxKj-3aSI3AwKY0CiqMcsn14AXg0/edit#)
    - [SMARTERRE : Volet 2 Territoire Apprenant](https://docs.google.com/document/d/1gGg9dSJ7cZwxNo1NGt2AWDU2KY7gKARPfTYZ4Af9-kA/edit)
    - [FILIÈRE](https://docs.google.com/document/d/1l6d1MoYjXlKTYIGDqTKGCgneQ28JnPkjldocCkOELC4/edit)
- [NEEDS AND WANTS](https://codimd.communecter.org/RZl-dPclQS-ijl83B8on5Q?both)
    - [Covoiturage](https://codimd.communecter.org/rzASXOexRdiqKDcW19xcBg#) 
    - Partage d'outil

---

## R&D 
#### [DOCUMENTATION](https://codimd.communecter.org/rcC-GvqmQheu9P-OprBFLw#)
- [costum de documentation](https://www.communecter.org/costum/co/index/slug/productprez#)
- [costum prez ex : aap prez](https://www.communecter.org/costum/co/index/slug/aap#welcome)
- [CODESIGN](https://codimd.communecter.org/_rhtFLDsS_upOpghM-IuyQ#)
- [CODOC repo](https://gitlab.adullact.net/pixelhumain/codoc)
- [CODOC yorre](https://rajaonariveloyorre.gitlab.io/codoc-page/)
#### [ACTIVITY PUB](https://codimd.communecter.org/ZatCWItFR-GJ0VQY-R3m0Q?edit#) 
#### Calendrier agrégeant et intéropérable 
#### [COCOLIGHT](https://codimd.communecter.org/vIe7Av-ERdKEMaOykeMomA?both#)
#### DATA JOURNALISME
#### [ARCHITECTURE](https://docs.google.com/presentation/d/1YpjSqWYd285QyikzY7-CcqfQDibhu1XxGD8IClRoew8/edit#slide=id.g503e9e3bd2_0_403)

--- 

## FORMATIONS

---

## ADMIN
- document des Virements 
- Previsionnel 

#### RH

# Réunion
- Caroline (Admin)
- Agatha (Graphiste)
- Thomas (Senior)
- Tib (Senior)
- Florent (Senior)

# GAP
- Clement (Senior)

# Bordeaux
- Ambre (Graphiste)

# Madagascar
- RADAMANIRINA Sitraka Philippe @Senju007
- Rijaniaina Élie Fidèle @Rinelfi
- Jean Dieu Donné (Gova) @Ramiandrison
- FANILONTSOA Santatriniaina Dinah @fanilontsoadinah
- RAOELIMAHEFA Charly @Mahefa
- ANDRIANIRINARISOA Paul Antenaina Louis Francki @francki06
- Nambinintsoa Nicolas @Nicoss
- Mirana Sylvany @Mirana
- Hajavololona Armellin @ArmelWanes
- Ifaliana Arimanana @Ifaliana
- RAKOTOSON Anatole @Anatole-Rakotoson
- Schumann @schumann
- RAJAONARIVELO Yorre @yorre
- MANDATIANA Gerson Elvestino @gersonelvestino
- ANDRIATAHINA Dady Christon @Dev-christon

#### Doc onboarding
- Stagiaire 
- Simple experience découverte

---
## Communication
tout à faire 
(luc : quid de faire un liste d'actions en mode coRem et de choper des "growth hacker" engagé sur ce budget ?)
- Animation podcast audio+video (youtube, spotify&autres)
- linkedIn
- réseaux sociaux tradi
- ateliers et tables rondes virtuels
- webinaires
- sites vitrines propres
    - openatlas
    - pixelhumain
    - ...

---

## FINANCEMENT
### Où
- 80% Presta
    - missions one shot
    - récurrents
        - 15k/an ministère ecologie
        - forfait hosting (trou dans la raquette)
        - dons ?
- 20% Subvention 
### Ingénieurie Finaciere [TODO]
- CII 
- Europe

### Dépenses 
#### presta
- dev
    - chef de projet (dev)
        - clem tib flo 
        - à monter : yorre, nico, anatole, Mira
    - dev Mobile Meteor(nodeJs) (temps reel) 
        - Thomas
    - devOps 
        - Thomas
    - exe 
        - fianAtlas 15 personnes
- graphiste 
- admin 
#### fonctionnement
- server 
    - map box
    - mail chimp 
    - OVH
    - coûts iOS
    - achats DNS
        - communecter.org
        - open-atlas.org
        - ?
    - trademarks EUIPO
        - ? à faire ?

## Stratégie Commerciale

### déploiement et commissions
à discuter 
- 5% apport d'affaire 
- 10% chef de projet 
- 5% documentation / capitalisation
- 5% innovation 

### monter en charge douce et progressive 
### objectifs 2023 
- AAP 3-15 territoires supplémentaires
### domaine de coeur
- collectivité
- monde associatif 
- Tiers Lieux 
    - Communs des Tiers Lieux
- Ademe 
- ANCT
- Ministere de l'ecologie

## Ex de Devis
[Sgar Tiers Lieux](https://docs.google.com/presentation/d/1RthbdQw4ksZA_ogab_UoFmW-q_lrtjEJ_uMhqw_Brdo/edit#slide=id.g7f2715dafa_5_0)
[RFFLabs](https://docs.google.com/presentation/d/1Up1HLUEwDXLxu2BSCe6J_pdONX74oekO28oJ08anepU/edit#slide=id.g71e6b3cf0e_13_83)

---

## [Projets Références Clients](https://codimd.communecter.org/1Pa4QXtrRyeca8ZloNNjag#)
[contrats](https://gitlab.adullact.net/pixelhumain/codoc/-/blob/master/06%20-%20Open%20Atlas/contrats.md) 

---

### Ce qui nous rassemble