CoInput
===
###### tags: `coform`
CoInput est un librairie qui permet de construire de forme complexe avec un minimum de code.

## Utilisation de base
Pour l'utiliser il faut juste instancier l'objet CoInput en specifiant les options.

```javascript=
var coInput = new CoInput({
    //options
    container:"#co-inputs",
    inputs:[
        {
            type:"inputSimple",
            options:{
                label:"Nom",
                name:"firstName"
            }
        },
        {
            type:"inputSimple",
            options:{
                label:"Prénom",
                name:"lastName"
            }
        }
    ],
    onchange:function(name, value){
        console.log(name, value)
    }
})
```
![](https://codimd.communecter.org/uploads/upload_7ea3b4d88ab4dae02ad89671e74139b2.png)

## Options
### - container *(obligatoire)*
C'est une chaine de caractère (**string**) specifiant le selecteur de l'element qui va contenir les inputs.

----

### - inputs *(obligatoire)*
C'est un tableau d'objet ou de chaine de caractère (**[object|string]**) specifiant la configuration des inputs.
* ***Cas 1: utilisation d'objet***
```javascript=
var options = {
    /*...*/
    inputs:[
        {
            type:"inputSimple",
            options:{
                label:"Input simple",
                name:"input-simple"
            }
        },
        {
            type:"colorPicker",
            options:{
                label:"Color",
                name:"color"
            }
        },
    ]
    /*...*/
}
```
* ***Cas 2: utilisation de chaine de caractère***
```javascript=
var options = {
    /*...*/
    inputs:["inputSimple", "color"]
    /*...*/
}
```
* ***Cas 3: utilisation des deux***
```javascript=
var options = {
    /*...*/
    inputs:[
        {
            type:"inputSimple",
            options:{
                label:"Input simple",
                name:"input-simple"
            }
        },
        "color"
    ]
    /*...*/
}
```
***Remarque:***
Pour avoir des inputs sur la même ligne, faut juste les regrouper dans un tableau.
- Exemple:
```javascript=
var options = {
    /*...*/
    inputs:[
        [
            {
                type:"inputSimple",
                options:{
                    label:"Même ligne",
                    name:"input-1"
                }
            },
            {
                type:"inputSimple",
                options:{
                    label:"Même ligne",
                    name:"input-2"
                }
            }
        ],
        {
            type:"inputSimple",
            options:{
                label:"Autre ligne",
                name:"input-3"
            }
        }
    ]
    /*...*/
}
```
![](https://codimd.communecter.org/uploads/upload_d1a23d5c290177eb0dbe85c27c8152f7.png)

----

### - customInputType *(optionnel)*
C'est objet qu'on peut utiliser pour créer de type personnalisé.
```javascript=
var options = {
    /*...*/
    inputs:[
        {
            type:"myInput",
            options:{
                label:"Mon input"
            }
        }
    ],
    customInputType:{
        /**
         * Pour créer un type personnaliser, faut suivre ces 4 étapes:
        */
        myInput:function(options){
            //- Etape 1: créer la structure html de votre input
            var $html = $(`
                <div>${options.label}: <input type='text'/></div>
            `)

            //- Etape 2: attacher l'evenement "valueChange" à cet element au moment de changement de valeur de votre input
            $html.find("input").change(function(){
                $html.trigger("valueChange", $(this).val())
            })

            //- Etape 3: Faut ajouter et definir la propriéré "val" à votre element qui est une fonction retournant la valeur de votre input
            $html.val = function(){
                return $html.find("input").val()
            }

            //Etape 4: faut retourner l'element en question à la fin
            return $html;
        }
    }
    /*...*/
}
```
![](https://codimd.communecter.org/uploads/upload_5812f6b4ab4f7412fedde3fb813ae973.png)

----
### - payload *(optionnel)*
C'est un objet qu'on peut utiliser pour passer des paramètres qui va être envoyé au gestionnaire d'événement au moment de changement.
```javascript=
var options = {
    /*...*/
    payload:{
        //...parametres
    },
    onchange:function(name, value, payload){
        //on peut recuperer le payload ici.
    }
    /*...*/
}
```
*Remarque*:
On peut surcharger ce payload dans chaque option des inputs.
```javascript=
var options = {
    /*...*/
    payload:{
        key:"global-key"
    },
    inputs:{
        {
            type:"colorPicker",
            options:{
                //on surcharge la payload ici
                payload:{
                    key:"specific-key"
                }
            }
        }
    }
    /*...*/
}
```

----
### - onchange *(optionnel)*
C'est une fonction qui permet d'ecouter le changement de valuer des inputs. C'est une fonction qui prend trois paramètres (name, value, payload)
```javascript=
var options = {
    /*...*/
    onchange:function(name, value, payload){
        //on peut recuperer le payload ici.
    }
    /*...*/
}
```
----
### - i18n *(optionnel)*
C'est un objet qu'on peut utiliser pour traduire le label des inputs en fonction de leur nom (name).
```javascript=
var options = {
    /*...*/
    inputs:[
        "color",
        {
            type:"inputSimple",
            options:{
                name:"firstName"
            }
        }
    ],
    i18n:{
        color:"Couleur",
        firstName:"Nom"
    }
    /*...*/
}
```
![](https://codimd.communecter.org/uploads/upload_d942817c431df45f49b3f7e85f28abda.png)

---

## Les inputs de base
La syntaxe de base pour declarer de type est:
```
{
    type:"nom_du_type",
    options:{
        //options du type
    }
}
```
### - Input simple
C'est un champ de type HTML input
* Type : **inputSimple**
* Options :

| Option       | Type                           | Description                  | Valeur par defaut |
| ------------ | ------------------------------ | ---------------------------- | ----------------- |
| type         | string (Valid HTML input type) | Specifier le type d'input.   | "text"            |
| name         | string                         | Nom attribuer à l'input.     | inputSimple       |
| label        | string                         | Label de l'input             | null              |
| defaultValue | any                            | Valeur par defaut de l'input | null              |

### - Input group
C'est un champ qu'on peut rassembler plusieurs input simple
* Type: **inputGroup**
* Options:

| Option       | Type                   | Description                    | Valeur par defaut |
| ------------ | ---------------------- | ------------------------------ | ----------------- |
| name         | string                 | Nom du champ                   | inputGroup        |
| label        | string                 | Label du champ                 | null              |
| inputs       | [inputSimple_options]  | Liste des inputs dans le group | []                |
| defaultValue | {inputName:inputValue} | Valeur par defaut              | null              |


### - Select
C'est un champ de type HTML select
* Type: **select**
* Options:

| Option       | Type             | Description            | Valeur par defaut |
| ------------ | ---------------- | ---------------------- | ----------------- |
| name         | string           | Nom attribuer au champ | select            |
| label        | string           | Label du champ         | null              |
| options      | [{label, value}] | Les options du champ   | []                |
| defaultValue | string           | Valeur par defaut      | null              |

### - Color picker
C'est un champ pour sélectionner un couleur
* Type: **colorPicker**
* Options:

| Option       | Type                 | Description       | Valeur par defaut |
| ------------ | -------------------- | ----------------- | ----------------- |
| name         | string               | Nom du champ      | colorPicker       |
| label        | string               | Label du champ    | null              |
| defaultValue | string (valid color) | Valeur par defaut | null              |

### - Textarea
C'est un champ de type HTML textarea
* Type: **textarea**
* Options:

| Option       | Type   | Description       | Valeur par defaut |
| ------------ | ------ | ----------------- | ----------------- |
| name         | string | Nom du champ      | textarea          |
| label        | string | Label du champ    | null              |
| defaultValue | string | Valeur par defaut | ""                |

### - Input file
C'est un champ de type HTML input file
* Type: **inputFile**
* Options:

| Option | Type   | Description    | Valeur par defaut |
| ------ | ------ | -------------- | ----------------- |
| name   | string | Nom du champ   | inputFile         |
| label  | string | Label du champ | null              |

### - Input group buttons
C'est un champ qui comporte plusieurs boutons qu'on peut cliquer pour selectionner une valeur.
* Type: **groupButtons**
* Options:

| Option       | Type                     | Description           | Valeur par defaut |
| ------------ | ------------------------ | --------------------- | ----------------- |
| name         | string                   | Nom du champ          | groupButtons      |
| label        | string                   | Label du champ        | null              |
| options      | [{ label, value, icon }] | Les options du champ. | []                |
| defaultValue | string                   | Valeur par defaut     | null              |

### - Input multiple
C'est champ qui permet d'ajouter plusieurs inputs dynamiquement.
* Type: **inputMultiple**
* Options:

| Option       | Type                     | Description                             | Valeur par defaut |
| ------------ | ------------------------ | --------------------------------------- | ----------------- |
| name         | string                   | Nom du champ                            | inputMultiple     |
| label        | string                   | Label du champ                          | null              |
| inputs       | [valid_input_params]     | Les inputs à ajouter pour chaque ligne. | []                |
| defaultValue | [{inputName:inputValue}] | Valeur par defaut                       | []                |

### - Input dynamic
C'est champ permet de rendre un input dynamic c'est-à-dire de pouvoir selectionner un valeur au lieu de saisir manuellement.
* Type: **inputDynamic**
* Options:

| Option       | Type            | Description       | Valeur par defaut |
| ------------ | --------------- | ----------------- | ----------------- |
| name         | string          | Nom du champ      | inputDynamic      |
| label        | string          | Label du champ    | null              |
| options      | [{label,value}] | Liste des options | []                |
| defaultValue | string          | Valeur par defaut | null              |

### - Input section
C'est champ permet de regrouper des inputs.
* Type: **section**
* Options:

| Option | Type                 | Description     | Valeur par defaut |
| ------ | -------------------- | --------------- | ----------------- |
| name   | string               | Nom du champ    | "section"         |
| label  | string               | Label du champ  | "Section"         |
| inputs | [valid_input_params] | Liste de inputs | []                |

### - Input css
L'input css c'est une extension de CoInput qui contient tout les propriétés css existant.
* Type: **cssPropertyCamelCase**
* Options:

| Option | Type   | Description    | Valeur par defaut                |
| ------ | ------ | -------------- | -------------------------------- |
| name   | string | Nom du champ   | Nom du propriété css en question |
| label  | string | Label du champ |                                  |
| defaultValue       | string        |  Valeur par defaut             | null                                  |
### - Input tags
Un champ d'input qui permet de saisir des tags (mots clés)
* Type: **tags**
* Options:

| Option       | Type   | Description           | Valeur par defaut |
| ------------ | ------ |:--------------------- | ----------------- |
| name         | string | Nom du champ          | "select"          |
| label        | string | Label du champ        |                   |
| defaultValue | string | Valeur par défaut     | ""                |
| options      | array  | Valeur à séléctionner | [""]              |

### - Input font select
Un champ d'input qui permet de séléctionner des polices (font family)
* Type: **selectFont**
* Options:

| Option | Type   | Description    | Valeur par defaut |
| ------ | ------ | -------------- | ----------------- |
| name   | string | Nom du champ   | "select"          |
| label  | string | Label du champ |                   |

### - Input box shadow
Un groupe de champ composé d'input text et d'input switch pour configurer l'ombre de l'élément 
* Type: **boxShadow**
* Options:

| Option       | Type                      | Description       | Valeur par defaut |
| ------------ | ------------------------- | ----------------- | ----------------- |
| name         | string                    | Nom du champ      | "boxShadow"       |
| label        | string                    | Label du champ    |                   |
| defaultValue | string (valid box shadow) | Valeur par defaut |                   |

### - Input border
Un groupe d'input qui permet de définir le border d'un élément avec sa couleur et son type
* Type: **inputBorderSingle**
* Options:

| Option | Type   | Description    | Valeur par defaut |
| ------ | ------ | -------------- | ----------------- |
| name   | string | Nom du champ   | "border"          |
| label  | string | Label du champ |                   |

### - Input Icon
Un input qui permet de choisir un icône de font awesome sur un élément
* Type: **inputIcon**
* Options:

| Option       | Type   | Description       | Valeur par defaut                           |
| ------------ | ------ | ----------------- | ------------------------------------------- |
| name         | string | Nom du champ      | "inputIcon"                                 |
| label        | string | Label du champ    |                                             |
| defaultValue | string | Valeur par défaut | "fa fa-"+icon ou "Pas d'icône sélectionnée" |


### Les inputs de mise en forme css prédéfinie (style css)
Les input css à déclarer juste dans un input [""]:
La syntaxe de base pour declarer de type est:
```
{
    inputs: [
        "inputCss1", "inputCss2"
    ]
}
```
| name                      | property                   | type              | description                                                                        |
| ------------------------- | -------------------------- | ----------------- |:---------------------------------------------------------------------------------- |
| "accentColor"             | accent-color               | inputSimple       | la couleur d'accentuation                                                          |
| "alignItems"              | align-items                | groupButtons      | alignement d'un objet au sein de son conteneur                                     |
| "alignSelf"               | align-self                 | groupButtons      | alignement des objets flexibles d'une ligne flexible                               |
| "all"                     | all                        | select            | réinitialiser toutes les propriétés d'un élément                                   |
| "animationDelay"          | animation-delay            | inputRange        | durée d'attente avant de démarrer une animation                                    |
| "animationDirection"      | animation-direction        | select            | sens de l'animation                                                                |
| "animationDuration"       | animation-duration         | inputRange        | durée d'une animation pour parcourir un cycle.                                     |
| "animationFillMode"       | animation-fill-mode        | select            | la façon dont une animation applique les styles avant et après exécution.          |
| "animationIterationCount" | animation-iteration-count  | inputRange        | le nombre de cycles utilisés pour répéter une animation                            |
| "animationName"           | animation-name             | inputSimple       | le nom de l'animation qui doivent appliquées à l'élément                           |
| "animationTimingFunction" | animation-timing-function  | select            | la façon dont une animation CSS se déroule au fur et à mesure de chaque cycle      |
| "backdropFilter"          | backdrop-filter            | inputSimple       | diffusion de la couleur sur la zone derrière l'élément                             |
| "backfaceVisibility"      | backface-visibility        | select            | face arrière visible lorsqu'elle est orientée vers l'utilisateur.                  |
| "backgroundType"          | background                 | inputGroup        | [background-color, background-image]: Couleur et image d'un arrière-plan d'un elt  |
| "backgroundAttachment"    | background-attachment      | select            | position de l'image d'arrière-plan fixée dans la zone d'affichage                  |
| "backgroundBlendMode"     | background-blend-mode      | select            | images d'arrière-plan fusionnées entre elles et avec l'arrière-plan.               |
| "backgroundClip"          | background-clip            | select            | l'arrière-plan d'un élément s'étend sous la boîte de contenu                       |
| "background"              | background                 | colorPicker       | arrière-plans d'un élément                                                         |
| "backgroundColor"         | background-color           | colorPicker       | la couleur utilisée pour l'arrière-plan d'un élément                               |
| "backgroundImage"         | background-image           | inputFile         | définir image comme arrière-plan                                                   |
| "backgroundOrigin"        | background-origin          | select            | l'origine de l'arrière-plan background-image                                       |
| "backgroundPosition"      | background-position        | inputSimple       | la position initiale pour chaque image arrière-plan                                |
| "backgroundRepeat"        | background-repeat          | select            | la façon d'image utilisé en arrière-plan sont répétées                             |
| "backgroundSize"          | background-size            | select            | la taille des images d'arrière-plan pour l'élément                                 |
| "border"                  | border                     | inputBorderSingle | les propriétés liées à la bordure                                                  |
| "borderWidth"             | border-width               | inputSimple       | largeur de la bordure d'un élément                                                 |
| "borderColor"             | border-color               | colorPicker       | couleur de la bordure d'un élément                                                 |
| "borderStyle"             | border-style               | select            | le style des lignes utilisées pour les bordures des 4 côtés d'un élément           |
| "borderRadius"            | border-radius              | inputGroup        | les coins arrondis pour la bordure d'un élément                                    |
| "borderBottomLeftRadius"  | border-bottom-left-radius  | inputNumber       | le rayon de courbure de la bordure pour le coin en bas à gauche de l'élément       |
| "borderBottomRightRadius" | border-bottom-right-radius | inputNumber       | le rayon de courbure de la bordure pour le coin en bas à droite de l'élément       |
| "borderTopLeftRadius"     | border-top-left-radius     | inputNumber       | le rayon de courbure de la bordure pour le coin en haut à gauche de l'élément      |
| "borderTopRightRadius"    | border-top-right-radius    | inputNumber       | le rayon de courbure de la bordure pour le coin en haut à droite de l'élément      |
| "bottom"                  | bottom                     | inputSimple       | définition de l'emplacement vertical des éléments positionnés                      |
| "boxShadow"               | box-shadow                 | inputShadow       | ajout des ombres à la boîte d'un élément                                           |
| "boxSizing"               | box-sizing                 | select            | la façon dont la hauteur et la largeur totale d'un élément est calculée            |
| "breakAfter"              | break-after                | select            | la façon dont la page, la colonne ou la région se fragmente après la boîte générée |
| "breakBefore"             | break-before               | select            | la façon dont la page, la colonne ou la région se fragmente avant la boîte générée |
| "breakInside"             | break-inside               | select            | comment la page, la colonne ou la région se fragmente au sein de la boîte générée  |
| "captionSide"             | caption-side               | select            | choisir l'emplacement de la légende d'un tableau                                   |
| "caretColor"              | caret-color                | colorPicker       | la couleur du curseur visible à l'endroit où l'utilisateur peut ajouter du contenu |
| "clear"                   | clear                      | select            | indique si un élément peut situé à côté d'éléments flottants qui le précèdent      |
| "color"                   | color                      | colorPicker       | la couleur de premier plan d'un élément texte et de ses éventuelles décorations    |
| "clipPath"                |                            | inputSimple       | empêche une portion d'un élément d'être affichée                                   |
| "colLg"                   |                            | select            | grid system de bootstrap col-lg                                                    |
| "colMd"                   |                            | select            | grid system de bootstrap col-md                                                    |
| "cursor"                  | cursor                     | select            | la forme du curseur lorsque le pointeur est au-dessus de l'élément                 |
| "direction"               | direction                  | select            | paramétrée afin de correspondre à la direction du texte                            |
| "display"                 | display                    | select            | type d'affichage utilisée pour le rendu d'un élément                               |
| "filter"                  | filter                     | filterCssFunction | retourne un nouveau tableau contenant tous les éléments du tableau d'origine       |
| "float"                   | float                      | select            | indique qu'un élément doit être deplacé du flux normal                             |
| "flexDirection"           | flex-direction             | select            | a façon dont les éléments flexibles sont placés dans un conteneur flexible         |
| "font"                    | font                       | inputGroup        | Selectionner ou charger une police pour la mise en forme de texte                  |
| "fontFamily"              | font-family                | inputGroup        | Selectionner ou charger une police pour la mise en forme de texte                  |
| "fontSize"                | font-size                  | inputNumber       | définit la taille de police utilisée pour le texte                                 |
| "fontStyle"               | font-style                 | select            | définit la style de police utilisée pour le texte                                  |
| "fontVariant"             | font-variant               | select            | définit tous les paramètres typographiques pour une police de caractères           |
| "fontWeight"              | font-weight                | select            | définit la graisse utilisée pour le texte                                          |
| "height"                  | height                     | inputNumber       | la hauteur de la boîte de contenu d'un élément                                     |
| "left"                    | left                       | inputNumber       | définit une partie de la position des éléments positionnés                         |
| "letterSpacing"           | letter-spacing             | inputNumber       | définit l'interlettre utilisée pour les caractères qui composent le texte          |
| "lineHeight"              | line-height                | inputNumber       | indique la hauteur minimale des lignes au sein de l'élément                        |
| "margin"                  | margin                     | inputGroup        | la taille des marges sur les quatre côtés de l'élément                             |
| "marginBottom"            | margin-bottom              | inputNumber       | la marge basse appliquée à un élément                                              |
| "marginLeft"              | margin-left                | inputNumber       | la marge gauche appliquée à un élément                                             |
| "marginRight"             | margin-right               | inputNumber       | la marge right appliquée à un élément                                              |
| "marginTop"               | margin-top                 | inputNumber       | la marge haut appliquée à un élément                                               |
| "maxHeight"               | max-height                 | inputNumber       | la hauteur maximale d'un élément donné.                                            |
| "minHeight"               | min-height                 | inputNumber       | la hauteur minimale d'un élément donné.                                            |
| "minWidth"                | min-width                  | inputNumber       | la largeur minimale d'un élément donné                                             |
| "maxWidth"                | max-width                  | inputNumber       | la largeur maximale d'un élément donné                                             |
| "objectFit"               | object-fit                 | select            | la façon dont le contenu d'un élément remplacé doit s'adapter à son conteneur      |
| "padding"                 | padding                    | inputGroup        | définir les différents écarts de remplissage sur les quatre côtés d'un élément     |
| "paddingBottom"           | padding-bottom             | inputNumber       | ajuste la hauteur de la boîte de remplissage en bas de l'élément                   |
| "paddingLeft"             | padding-left               | inputNumber       | ajuste la hauteur de la boîte de remplissage à gauche de l'élément                 |
| "paddingRight"            | padding-right              | inputNumber       | ajuste la hauteur de la boîte de remplissage à droite de l'élément                 |
| "paddingTop"              | padding-top                | inputNumber       | ajuste la hauteur de la boîte de remplissage en haut de l'élément                  |
| "overflowX"               | overflow-x                 | select            | les mécanismes à utiliser si le contenu dépasse des bords droit-gauche de la boîte |
| "overflowY"               | overflow-y                 | select            | les mécanismes à utiliser si le contenu dépasse des bords haut et bas de la boîte  |
| "position"                | position                   | select            | définit la façon dont un élément est positionné dans un document                   |
| "right"                   | right                      | inputNumber       | renvoie la valeur de la coordonnée droite de l'élément                             |
| "textAlign"               | text-align                 | groupButtons      | alignement horizontal d'un élément de bloc ou de la boîte d'une cellule de tableau |
| "textDecorationStyle"     | text-decoration-style      | select            | définit le style appliqué sur les lignes visées par text-decoration-line           |
| "textDecorationLine"      | text-decoration-line       | select            | définit la façon dont les décorations linéaires sont ajoutées à un élément         |
| "loaderUrl"               |                            | select            | url d'un chargement (spinner)                                                      |
| "hideOnDesktop"           |                            | checkbox          | définit que l'élément n'apparaise pas en mode Desktop (résolution)                 |
| "hideOnTablet"            |                            | checkbox          | définit que l'élément n'apparaise pas en mode Tablet (résolution)                  |
| "hideOnMobil"             |                            | checkbox          | définit que l'élément n'apparaise pas en mode Mobile (résolution)                  |
| "textDecoration"          | text-decoration            | select            | décorer le texte en ajoutant une ligne sous, sur ou à travers le texte             |
| "textDecorationColor"     | text-decoration-color      | colorPicker       | définit la couleur utilisée pour dessiner les lignes décorant le texte             |
| "textJustify"             | text-justify               | select            | définit le type de justification à appliquer au texte justifié                     |
| "textTransform"           | text-transform             | select            | définit la façon d'utiliser les lettres capitales pour le texte d'un élément       |
| "textShadow"              | text-shadow                | inputShadow       | ajouter des ombres au texte                                                        |
| "transitionDelay"         | transition-delay           | inputNumber       | indique la durée à attendre avant de débuter la transition                         |
| "transitionDuration"      | transition-duration        | inputNumber       | définit le nombre de secondes ou de millisecondes que doit durer une animation     |
| "transitionProperty"      | transition-property        | inputNumber       | désigne les propriétés CSS dont un effet de transition devrait être appliqué       |
| "transitionTimingF"       | transition-timing-function | select            | la façon dont les propriétés CSS affectées par un transition sont calculées        |
| "top"                     | top                        | inputNumber       | définit une partie de la position des éléments positionnés                         |
| "verticalAlign"           | vertical-align             | select            | définit l'alignement vertical d'une boîte en ligne/bloc ou une cellule de tableau  |
| "visibility"              | visibility                 | select            | utilisée afin de cacher un élément                                                 |
| "width"                   | width                      | inputNumber       | définir la largeur de la boîte du contenu d'un élément                             |
| "whiteSpace"              | white-space                | select            | utilisée pour décrire la façon dont les blancs sont gérés au sein de l'élément     | 