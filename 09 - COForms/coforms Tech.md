# coforms Tech
[documentation](~d/modules/costum/views/tpls/forms/README.md]
[schema](https://docs.google.com/presentation/d/1jwi3pYEzL_r8IH1AEpzlJe4jTt2kG7BZS5Hyq1kZHeE/edit#slide=id.p)

# json
[[~d/modules/costum/data/freeform.json]]
[[~d/modules/costum/data/forms/openForm.json]]

# templates : can be used anywhere in any costumForm
[[~d/modules/costum/views/custom/co/freeform.php]] 
  [[~d/modules/costum/views/custom/co/formSection.php]]
    [[~d/modules/costum/views/custom/co/formbuilder.php]]
      ** basic form tpls
      [[~d/modules/costum/views/tpls/forms/text.php]]
      [[~d/modules/costum/views/tpls/forms/select.php]]
      [[~d/modules/costum/views/tpls/forms/textarea.php]]
      [[~d/modules/costum/views/tpls/forms/checkbox.php]]
      ** complexe tpls forms
      [[~d/modules/costum/views/tpls/forms/calendar.php]]
  [[~d/modules/costum/views/custom/co/answerList.php]]

# OPEN FORM WITH WIZARD
[[~d/modules/costum/views/custom/co/formWizard.php]] 
  [[~d/modules/costum/views/tpls/forms/wizard.php]]
    [[~d/modules/costum/views/custom/co/formSection.php]]
      [[~d/modules/costum/views/custom/co/formbuilder.php]]


# TECHNICALLY , ARCHITECTURE
## parent Forms and subForms
### PARENT FORM 
are the master piece of a coform session 
it presents and holds 
- the parent Context
- the list of subForms 
- possible parameters 
- 
```
{
    "_id" : ObjectId("5f3e5d62275640c499c2e11a"),
    "id" : "ctenatForm",
    "parent" : {
        "5ca1b2bb40bb4e9352ba351b" : {
            "type" : "organizations",
            "name" : "CTE National"
        }
    },
    "homeTpl" : "survey.views.custom.ctenat.explainRow",
    "subForms" : [ 
        "action", 
        "caracter", 
        "murir", 
        "contractualiser", 
        "suivre"
    ],
    "source" : {
        "key" : "ctenat",
        "keys" : [ 
            "ctenat"
        ],
        "insertOrign" : "costum"
    },
    "active" : true,
    "params" : {
        "period" : {
            "from" : 2018,
            "to" : 2023
        },
        "elementelement1" : {
            "type" : "organizations",
            "limit" : ""
        },
        "elementelement3" : {
            "type" : "projects",
            "limit" : ""
        },
        "elementporteur" : {
            "type" : "organizations",
            "limit" : ""
        },
        "elementprojet" : {
            "type" : "projects",
            "limit" : ""
        },
        "elementaction" : {
            "type" : "projects",
            "limit" : ""
        },
        "elementparents" : {
            "type" : "organizations",
            "limit" : ""
        },
        "elementproject" : {
            "type" : "projects",
            "limit" : ""
        },
        "calendaropenForm19" : {
            "sectionTitles" : [ 
                "1er<br/>Sem<br/>2023", 
                "2ème<br/>Sem<br/>2023"
            ],
            "dateSections" : [ 
                "01/01/2024"
            ]
        },
        "budgetbudget" : {
            "nature" : {
                "investissement" : "Investissement",
                "fonctionnement" : "Fonctionnement"
            },
            "amounts" : {
                "price" : "Montant",
                "amount2019" : "2019 (euros HT)",
                "amount2020" : "2020 (euros HT)",
                "amount2021" : "2021 (euros HT)",
                "amount2022" : "2022 (euros HT)",
                "amount2023" : "2023 (euros HT)"
            },
            "estimate" : "true"
        }
    },
    "updated" : NumberLong(1597679109),
    "graph" : {
        "budgetbudget_1" : {
            "label" : "test",
            "type" : "amount"
        },
        "budgetbudget_2" : {
            "label" : "",
            "type" : "poste"
        }
    }
}
```
### SUBFORM
## formTpl
  is the definition of a form, saved inside db.forms
  looks like this 
  ```
   "_id" : ObjectId("5e0d010dbf4d14b1f08dcaf7"),
      "id" : "openForm1",
      "type" : "openForm",
      "name" : "just a form",
      "inputs" : {
          "element" : {
              "label" : "L'auteur",
              "placeholder" : "Auteur",
              "type" : "tpls.pods.author",
              "info" : "si tu ne sais pas ou tu vas, regarde d'ou tu viens"
          },
          "element1" : {
              "label" : "Structure porteuse",
              "type" : "tpls.forms.cplx.element",
              "info" : "La simplicité est l'ultime sophistication Léonard de Vinci 1515"
          },
          "element2" : {
              "label" : "Structures associées",
              "type" : "tpls.forms.cplx.element",
              "info" : "La simplicité est l'ultime sophistication Léonard de Vinci 1515"
          },
          ...
  ```

## step rules 
## explain Tpl
## rendering order 

### OPEN FORM WITH WIZARD
[[~d/modules/costum/views/custom/co/formWizard.php]] 
  [[~d/modules/costum/views/tpls/forms/wizard.php]]
    [[~d/modules/costum/views/custom/co/formSection.php]]
      [[~d/modules/costum/views/custom/co/formbuilder.php]]
        individual inputs ased on configs

## links activity (historic, answered, voted....)



## FORM PARAMS

### anyOnewithLinkCanAnswer
### canReadOtherAnswers
### startDate
### endDate 
block editing if End date passed 
### oneAnswerPerPerson
if exist on va dessus 
existe par on génere si F5 
else  
### canModify 
### canVote : false
- add action 
- show voters count
- [ ] show voters list
-   [ ] color action I voted

### canFund : false
*   [X] costum.cms.fundMsg
*   [X] add action 
*   [X] show funders count and ampount
*   [ ] show funders list

### step rules
  - any step can have rules to manage how it is shown
  - required : is a path for a given value in the answer
```
rules.required : ["priorisation"]

or 
"rules.required" : [ 
    "answers.action.parents", 
    "answers.action.project"
]
```
  - checkValue : is a path and a value 
```
"rules" : {
        "checkValue" : {
            "priorisation" : [ 
                "Passage en réunion de finalisation", 
                "Action validée"
            ]
        }
    }
```

### BUG affichage de la liste des contributeur
- [ ] color action I funded
- [ ] costum.form.fundPlan : 4400€
-   [ ] equilibre Budget [[~d/modules/survey/views/custom/ctenat/resultsRow.php]]
- [X] costum.form.showAnswers
- [X] costum.form.params.calendar
- parameters needed to make this complexe input work 
- [X] costum.form.params.sumColumns
- list of column to sumup and show in the answers list


## Text indenté
https://codimd.communecter.org/yWHBvYY_TUaVZE65Z9440g
Construisez votre formulaire dans un fichier texte en local et copier coller le en prod
Une fois que vous etes familier avec les formulaires 
chaque input à une clef de correspondance ex input test [text] in put textarea [textarea]

### Liste des inputs

#### Nouvelle étape
```
- [step] Modèle Culturel
```

#### Input Text
> Taper un petit texte court
- `text pour placeholder`: on utilise le cote  ``` ` ` ``` pour le texte dans l'input (placeholder)
- ``info``: on utilise double cote ``` `` `` ``` pour le notice en bas de l'input
```
- [text] Imaginaire, sources d'inspiration `saisir un texte` ``on peut saisir ce qu'on veut``
```

#### Input textarea
> Input pour remplir un grand texte éventuellement struturé en Markdown
```
- [textarea] Questions ouvertes
```

#### Input Nombre
> Input permet de saisir des nombres
```
- [number] Saisir un nombre
```

#### Input Email
> Input permet de saisir un adresse email
```
- [email] Saisir votre adresse email
```

#### Input Date
> Input permet de choisir une date
```
- [date] Séléctionner une date
```

#### Input Tag
> Input permet de saisir des tags
```
- [tag] Saisir des mots clés 
```

#### Input radio
> Liste de choix unique
```
- [radioNew] Liste de choix unique
  - Choix 1
  - Choix 2
  - Choix 3
```

#### Input Evaluation
> #todo
```
- [evaluation] Ceci est une evaluation
  - Quadrant un
  - Quadrant deux
  - Quadrant trois
  - Quadrant quatre
```

#### Input Checkbox
> Liste de Check box
```
- [checkboxNew] Quelles sont les valeurs qui sont au coeur de votre projet ?
   - Open-source
   - Collaboratif
   - Peer-to-peer
   - Bienveillance
   - Autonomie
```

#### Input Checkbox avec ajouter Nouveau
> Liste de Check box, l'attribut permet de choisir plusieurs options et en ajouter plus d'information sur le choix qu'on a fait
```
- [multiCheckboxPlus] Vos objectifs
  - [cplx] Court terme : 
  - [cplx] Moyen terme : 
  - [cplx] Long terme `placeholder` : 
```

#### Input radio avec ajouter Nouveau
> Liste de radio bouton, l'attribut permet de choisir un seul options et en ajouter plus d'information sur le choix qu'on a fait
```
- [multiRadio] Vos objectifs
  - [cplx] Court terme : 
  - [cplx] Moyen terme : 
  - [cplx] Long terme : 
```

#### Input Address
> Input de Geolocalisation 
```
- [address] Zone(s) géographique(s) d'action(s)
```

#### Input simpleTable
> C'est un tableau à deux entrées simple (basé sur des lignes et colonnes)
- [rows] : pour les lignes
- [columns] : pour les colonnes
- (nombre) : type de données pour les colonnes, et on a aussi le type (text)
- pour avoir plusieurs lignes, il suffit d'ajouter plusieurs [rows]
```
- [simpleTable] Quantités traitées
  - [rows] Quantités traitées
  - [columns] 2020 (nombre)
  - [columns] 2021 (nombre)
  - [columns] 2022 (nombre)
```

#### Input repondants
> C'est un tableau qui liste le noms et les prénoms des personnes qui ont complété le formulaire
```
- [repondants] Liste des répondants
```


### Exemple
Pour faire plusieurs étapes ajouter simplement plusieurs block step
```
- [section] Modèle Culturel
- [text] Nom de votre structure `saisir nom de votre structure` ``le nom de structure est composé des alphas numeriques``
- [text] Type de structure
- [radioNew] Est-ce qu’une conférence de presse commune serait utile ?
  - Oui
  - Non
- [checkboxNew] Types de déchet(s)
  - Papier
  - Electro-ménager
  - Numérique
  - Biodéchets
- [multiCheckboxPlus] Types d'action(s) réalisé(es)
  - Assainissement
  - Réparation (Recyclage)
  - Revalorisation (Recyclage)
  - Réutilisation (Recyclage
  - Transformation (Recyclage)
  - Ressourcerie (Recyclage)
  - Compost (Recyclage)
  - [cplx] Autres (Recyclage):
  - [cplx] Autre(s) :
- [address] Zone(s) géographique(s) d'action(s)
- [multiCheckboxPlus] Types de déchet(s)
  - Papier
  - Electro-ménager
  - Numérique
  - Biodéchets
  - [cplx] Autre(s) :
- [multiRadio] Types de déchet(s)
  - Papier
  - Electro-ménager
  - Numérique
  - Biodéchets
  - [cplx] Autre(s) `A Préciser`:
- [simpleTable] Quantités traitées
  - [rows] Quantités traitées
  - [rows] Quantités ramassées
  - [columns] 2020 (nombre)
  - [columns] 2021 (nombre)
  - [columns] 2022 (nombre)
  - [columns] Commentaires (text)
- [textarea] Questions ouvertes
- [repondants] Liste des répondants
```