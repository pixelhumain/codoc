### Thomas Craipeau (!@thomascraipeau) !#dev !#sysAdmin
### Tibor Katelbach (!@oceatoon) !#dev !#projectManager !#dossier !#animation !#administration
15 ans d’expérience en tant que développeur web, certifié par le ministère de la Recherche et de l’éducation comme travailleur effectuant des travaux de recherche et développement pouvant s'appliquer au Crédit impôt recherche. Spécialiste Système d’information : conception, architecture technique et système, infrastructure, développement, interopérabilité, innovation, suivi projet. formateur informatique. Expertise sur les logiciels libres, sur les développements open-source et projets collaboratifs. Chef de projet : cadrage et gestion de projet, définition des objectifs du système d'informations, chef d’équipe...Tibor a contribué à de nombreux projets innovants.
### Florent Benameur (!@florentBenameur) !#dev !#projectManager !#animation !#dossier !#CDP !#COstum

## AAP
### RAKOTOSON Anatole (!@Anatole-Rakotoson) !#dev !#AAP !#COForm !#Oceco !#Observatoire !#CDP
### Gova Jean Dieu Donné (!@Ramiandrison) !#dev !#AAP !#COForm !#Navigator !#Observatoire !#CDP
### Schumman (!@...)!#dev !#AAP !#COForm !#CoForm Editeur !#CODate 
### Rija (!@...) !#dev !#AAP !#COEvent !#CornerDev !#Oceco !#Kanban
### Moise (!@...) !#dev !#AAP !#Observatoire

## CMS
### Nambinintsoa Nicolas (!@Nicoss) !#dev !#CMS !#DesignCSS !#COstum !#CDP
### Ifaliana Arimanana (!@Ifaliana) !#dev !#CMS
### Manda (!@...) !#dev !#CMS
### Sitraka (!@...) !#dev !#CMS

## COSTUM 
### Mirana Sylvany (!@Mirana) !#dev !##COstum !#CMS !#CDP
### ANDRIATAHINA Dady Christon (!@Dev-christon) !#dev !#COstum !#Observatoire !#Graph !#CDP
### Francki !#dev !#COstum !#Graph !#Observatoire 
### Dinah (!@...) !#dev !#interopOSM
### Mahefa (!@...) !#dev !#Navigator 
### Judi (!@...) !#dev !#Kanban
### Peterson (!@...) !#dev !#CMS !#COCity !#interopWikipedia

## COCOLIGHT
### Armel (!@...) !#dev !#ActivityPub !#cocoLight
### Leonid (!@...) !#dev !#cocoLight !#unitTest
### Elie (!@...) !#dev !#cocoLight !#unitTest

## ADMIN
### Caroline Paret (!@CarolineP) !#administration

## COEUR 
### Jerome Gonthier (!@PsychoTesteur) !#dossier
### Beatrice Potier (!@Beapotier) !#dossier !#animation

## CVs
https://docs.google.com/presentation/d/1IdVNgRVn7RT6TfnLOo78Ajwaf9LRNwaBpCsKA7GhXZI/edit#slide=id.g59e29e1223_1_8

## Comment Rejoindre L'equipe
- juste prendre contact 
    + mail : contact@communecter.org
    + chat : https://chat.communecter.org/channel/codev_open

## Pourquoi etre COFreelance (freelance sociétaire d'Open Atlas)
- garder son indépendance et pouvoir faire plusieurs chose en meme 
- pas de lien hierarchique 
- pas de lieu de travail obligatoire 
- etre freelance oui , mais pas tout seul 
    * bénéficier d'un collectif de developpeur 
- OPEN ATLAS gère tout l'administratif 
- OCECO un outil : 
    + de récolte de proposition ouverte
    + de priorisation des propositions
    + de partage et de suivi des taches
    + de gouvernance horizontale
    + de cofinancement 
    + de coRémunération
- AGESSA ou Auto Entrepreneur ou autre...

## Ancien CObattant
### Raphael Riviere (!@raphaelRiviere) !#dev !#projectManager 
### Tristan Guoguet (!@alphaTango) !#dev !#projectManager
### Sylvain Barbot (!@sylvainB) !#dev !#projectManager 
### Rajaomaria Jaona (!@rajaomariajaona) !#dev 
### RAJAONARIVELO Yorre (!@yorre) !#dev
### Clément Damien (!@Bouboule) !#dev !#projectManager !#dossier