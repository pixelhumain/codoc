# OCECO : Appel à projet pour le monde associatif de la Ville de Sin Dni de la Réunion

### présentation en PPT
https://docs.google.com/presentation/d/1qWjFI0x4jWXb-D9Ky2s14ZnjJIxArId9SCF9xZThKbI/edit?usp=sharing

Le contrat de ville de Saint-Denis est un dispositif qui permet de mener des actions spécifiques au sein des quartiers prioritaires de la ville pour atteindre des objectifs ciblés par quartier.

Il convient de rendre plus efficient le processus de consultation et d'évaluation allant des objectifs ciblés  à l’appel à projet et permettant le suivi des actions et des résultats grâce à des outils, des formulaires et des visualisations.


Cela passe par :
La simplification de la récolte de projets y intégrer directement 
la qualification 
la caractérisation 
le planning
L’évaluation selon les critères avec 2 comités d’évaluation
La simplification du financement avec un seul dossier et pièces correspondantes
Une communication simplifiée avec les porteurs de projet
Le suivi et la validation des impacts selon les indicateurs et les objectifs fixés
La communication relative aux actions et à la visibilité des projets lauréats et leurs réalisations


1ere étape : Analyses des processus existants


Diagnostic et analyse des pratiques relatives au dispositif du contrat de ville (services municipaux et maillage partenarial impliqués),
On pourra alors adapter nos processus digitaux pour obtenir un systeme simplifier, centralisé en un seul dossier, permettant une remonter et donc un suivi permanent des engagements signés entre la mairie et les acteurs du territoires.

Approche méthodologique :
Observations, entretiens individuels, ateliers collectifs, analyses des processus et des ressources
→ Participation à une réunion de services pour impliquer les services impactés - 2 demi-journées
→ Entretiens individuels - 5 demi-journées

Portail projet de la ville
Les services de la ville concernés, les coordinateurs, les financeurs, opérateurs….etc voit une seule et unique interface pour tous les projets et peuvent agir selon leur rôle et leur droit.

Un territoire (des quartiers) avec des projets

Possibilité de :
Consulter des projets sur une double interface (annuaire / cartographie)
Chaque Changement informe tout partie prenant
Contacter les porteurs et les équipes
Mesurer et Visualiser l’impact de chaque projet
Définir et Visualiser les enjeux et les objectifs QPV
Pour chaque quartier QPV, une interface ou portail projet associatif permet de voir tous les objectifs d’un territoire et montrer les appels à projets associés. 
Des Filtres permettront de visualiser les projets par Objectif


Un Appel à projets par objectifs
Chaque objectif a son contexte et ses enjeux pour lesquels on lance un appel à projets 

Les 5 étapes du nouveaux système d'appel à projet et 
Récolte des propositions - Évaluer / Traiter - Financer - Contractualiser - Suivre Réaliser 


> Dépôt de proposition de projets
Les porteurs de projet se présentent et répondent à l’appel à projets
Détail  projet
- Présentation du Porteur ( Réutilisable pour plusieurs AO )
- Caractéristiques du projet (description, objectifs/impacts prévisionnels, choix des critères d’évaluation des résultats)
- Calendrier 
- budget prévisionnels
- Interface d’instruction privée accessible au comité technique (commentaires, notifications, …)
- Remplir les indicateurs d'impacts répondant aux objectifs initiaux des l'AAP


> Évaluer les propositions
Les étapes de sélection par critères prédéfinis permettent aux comités d’analyser et de départager  les projets de façon dynamique et interactive.
Évaluation par 10 critères
Avec tri automatique des réponses 

> Contractualiser
Rendre disponibles les décisions et documents contractuels, permettre plusieurs acteurs coordonnateurs de valider les décisions
Validation par les comités technique et centrale
Possibilité d’automatiser les notifications favorables ou défavorables par mail de façon automatisée

> Suivi de projet
Expérimenter une approche dynamique de suivi de projets 
Assurer le passage de la promesse à l’objectif concret (et/ou sa réévaluation)
Déterminer les jalons de validation
Définir des tâches pour tester et valider  
Assigner des badges de progression et des filtres pour aller à l’essentiel
Faciliter la communication directe avec les porteurs

L’étape de suivi est essentielle pour s’assurer du bon déroulement du projet et permet de vérifier si les objectifs sont atteints
Jalon : Plan d’action
TODO - ÉTAT AVANCEMENT
Évaluer Agilité

> Observatoire 
Suivi des projets et de leurs impacts sur le territoire selon des indicateurs et critères d’évaluation 


> Visualiser et Communiquer 
On voit facilement toutes les réponses et leur statut. L’ensemble des personnes impliquées (porteurs, techniciens, instructeurs, …) est notifié par mail .
Accès et actions par rôle des coordonateurs, financeurs, opérateurs....

> Listing et cartographie des projets 
Consultation / Modification

> Listing des projets avec les actions admin

> Un observatoire local
Un Tableau de bord et de gestion global permettant d’analyser, de décider et d’agir sur la base des réalisations, des indicateurs et des avancements de tous les projets réalisés.
Valoriser ce qui fonctionne, revoir ce qui ne marche pas
Co-construire les enjeux et objectifs de l’année suivante


> Un projet Open Source, mutualisé et en amélioration permanente
Tous ces travaux viennent contribuer au projet global Open Source et seront des fonctionnalités mises à disposition d’autres organisations et territoires. 
La contribution au commun permet de rémunérer le travail et la maintenance de la plateforme existante sur laquelle le projet se base.



*****************************************

Nous avons rencontrez l'equipe de Martine Noury pour une présentation de nos produits territoriaux, d'efficience, d'observatoire et de récolte et visualisation de la connaissance. Nous voulions partager une vision ambitieuse pour SINDNI que nous avons appelé COSindni, Collaboratif et93 Connecté. 
Un projet commun a émergé sur la manière de traiter et d'organiser les appels à projet pour le monde associatif