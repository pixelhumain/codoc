# CoRémunération

schema https://nuage.tierslieux.re/s/YidbroJyJm3CDKe/preview
usage RAFF https://www.communecter.org/costum/co/index/slug/proceco#welcome.slug.laRaffinerie3.formid.61ada77e853ba97cfc6e58b2.page.list
video https://peertube.communecter.org/videos/watch/6eda2cb8-0df8-43cc-836d-576a6f545c0d

# Définition
La CoRémunération permet de consommer en autoassignation de budget 
en pleine confiance et transparence un budget alloué à un contexte
- d'assurer un minimum de qualité assurance et de gestion dans un fonctionnement en coRému
- d'accepter des candidatures de participation sur des actions
- d'assurer la qualité des travaux et le resultat
- d'assurer et de distribuer la bonne repartition des budgets
- créer un contexte de transparence et de compréhension pour mettre en confiance 
- générer les pieces justificatives
- produire des bilans d'activités (globaux, par projet, par personnes, par type de taches)


# Contexte d'experimentation
- Open Atlas 
- La Raffinerie

### Fonctionalités

#### Version actuelle
- dans un projet / Proposition 
- un action declaré 
- un montant (budget sur une durée )
- des candidats / contributeur / participant
    - avec montant (presta) 
    - sans montant (division du montant dispo)
    - sans montant et ajout du montant aprés réalisation dans les limites du budget
- realisation du travail (finalisation) 
- validation du travail 
- rémunération 
- generer justificatif 

#### Autre Vision
- une action / projet declarée
- declaration de taches et sous taches
- un montant (budget sur une durée )
- des candidats / contributeurs / participants
    - avec montant (presta) 
    - sans montant (division du montant dispo)
    - sans montant et ajout du montant aprés réalisation dans les limites du budget
- realisation du travail (finalisation) 
- validation du travail 
- rémunération 
- generer justificatif 

- [TODO] écran coRému simplifiant les ecrans


# Proposition
![](https://codimd.communecter.org/uploads/upload_aab5f8e67b733eec8b505d3e43eae45b.png)
- associer liste de référents

## LDD
- [TODO] budget / durée : ex : 800€/mois
- [DONE] remettre le btn candidat / postuler / bénévole

# passage en projet
[TODO] btn généré un event oceco (recurant ou pas)
sur chaque occurence, bienveillant et benevole different
les bénévoles > rentrent par les events oceco 
- tous les mercredi 
- ou sur une date spontanée

# Benevole postule dans oceco
## [TODO] LDD sous condition de badge 
- [ENCOURS] soit on a le badge 
    - recoit les notifs
- [ENCOURS] jai pas le badge mais interésere: mode decouverte 
    - chapoté par un titulaire de badge 
    - eventuellement aquisition de badge post action
![](https://codimd.communecter.org/uploads/upload_58b892cdd49a64a405769cd8190ab7ce.png)

- action qui credite un badge > une fois valider 
- calendrier general
    - d'activité 
- calendrier personnel 
    - indiquer les jours dispo pas dispo
        - comment le synchroniser avec un calendrier externe pour le pas dispo
- intégrer le diag des besoins financier

### facturation
- bloqué > LDD > à la validation de la coRému (envoi de facture)

### comment lié une proposition à un event / projet
[QUID] : faire le lien entre les events oceco RAFF et les projet OCECOFORM ???

# Chantier ponctuel
- Annuaire de presta , badge
    - badge presta , btp, beton, terrasement
- [TODO] Ne pas pouvoir terminer avant la fin 
- et pouvoir annuler le statut terminer
- [TODO] Limit la liste des acteurs au postulant selectionné
- [TODO] Rajouter une raison de paiement 
    - ex : accompte 
    - paiement intermediaire 
- [TODO] Postuler sur ligne de financement 
    - pourra taper dans le budget d'une ligne 
    - limiter au budget 
- [TODO] Date dans les exports
- [TODO] Suivi > pouvoir creer un event (appel à benevole) sur une action donner 
    - faire lien avec les events ocecoMobi Raff
 
- ![](https://codimd.communecter.org/uploads/upload_060d26ac6234f6c34e8f8eeb4e681149.png)

# Candidater
- [EN COURS] sur une ligne de depense ou action 
    - soit candidater avec estimation 
        - devra etre assigné pour etre confirmé
    - soit simple participation 
        - coRému (montant divisé)
        - si non divisé alros estimation
-  participation et le prix

# Event : Apero du mercredi
[REFLEXION]
Cron process automatique générant des propositions à la création d'un event > génère template de proposition
- selection du type d'event (apero , concert, ciné)
la creation d'un evennemnt de type "apéro mercredi"
- [TODO] posssibilité de généré 3 propositions auquel vont repondre
    - proposition BAR 
    - GENERAL
    - RESTO 
    - option
        - cine plein air
        - concert

# usage possible actuel 08/2022
- declarer une proposition 
- lister les actions ou dépenses
    - un budget peut etre ajouter sur chaque ligne 
        1 - division par le nombre de participant
        2 - les candidat remplissent le cout de leur contribution
    - une ligne sans budget est possible (signifie que les prestataire devront l'estimer)
        - les candidats ou prestataire pourront estimer
            - construisant le coup final au fur et à mesure
- validation des actions
    - chaque action peut etre decoupé en sous action si necessaire
- paiement des prestataire 



# écran coRému simplifiant les ecrans



