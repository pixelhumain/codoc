Appel à Projet
| Tâche                      | Admin | Evaluateur | Financeur | Agent Mailer |   | Visiteur |
| ----------------------------- | ------- | :----------: | ----------- | -------------- | --- | ---------- |
| **FICHE ACTION COMPLETE**   | ----- |   -----   | -----     | -----        |   | -----    |
| *Lire*                      | x     |     x     | x         |              |   |          |
| *Modifier*                  | x     |     x     | x         |              |   |          |
| *Ajouter des documents*     | x     |     x     | x         |              |   |          |
| *Export en PDF*             | x     |     x     | x         |              |   |          |
| **OBSERVATOIRE**            | ----- |   -----   | -----     | -----        |   | -----    |
| *Lire*                      | x     |     x     | x         |              |   |          |
| **EVALUATION (Sélection)** | ----- |   -----   | -----     | -----        |   | -----    |
| *Lire*                      | x     |     x     | x         |              |   |          |
| *Noter*                     | x     |     x     | x         |              |   |          |
| *Changer l'admissibilité*  | x     |     x     | x         |              |   |          |
| **FINANCEMENT**             | ----- |   -----   | -----     | -----        |   | -----    |
| *Lire*                      | x     |     x     | x         |              |   |          |
| *Editer*                    | x     |           | x         |              |   |          |
| *Ajouter des documents*     | x     |     x     | x         |              |   |          |
| **SUIVI**                   | ----- |   -----   | -----     | -----        |   | -----    |
| *Lire*                      | x     |     x     |           |              |   |          |
| *Editer*                    | x     |           |           |              |   |          |
| **AUTRE ACTION**            | ----- |   -----   | -----     | -----        |   | -----    |
| *Supprimer une proposition* | x     |           |           |              |   |          |
| *Envoi email*               | x     |           |           |    x          |   |          |

