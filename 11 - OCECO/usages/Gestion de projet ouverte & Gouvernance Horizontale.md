# Gestion de projet ouverte, de ressource, et de gouvernance horizontale

## contexte d'expériementation

### Open Atlas
Chez Open Atlas dans l'approche coopérative de son développement à mis en place son outil de gestion du projet et de la coopérative. La volonté d'une gestion ouverte et d'un système de proposition libre et ouvert que propose OCECO est parfaitement adapté et en amélioration permanente

#### Fonctionalités
- Système (Personnalisable) de Proposition Ouvert
	+ Proposition
		* Description en Détail d'une idée, d'un concept, d'une fonctionalité
			- Défini les Ligne de dépense et leur cout 
			- On assigne ou des candidat peuvent participer 
			- on peut associer un badge necessaire à la réalisation d'une tache
		* Personnalisation du Formulaire de Proposition
		* Vote sur les propositions
		* possibilité de rejoindre
			- devenir contributeur d'une proposition
			- on peut estimer les lignes de dépenses 
			- 
	+ Evaluation 
		* Décision Collaborative sur les priorités
		* L'évaluation est configurable selon plusieurs modèle d'évaluation
		* Donne une note et donc priorise les propositions
	+ Fiancement 
		* assurer le financement selon plusieurs modèles possible 
			- coFinancement 
			- prestation 
			- crowdfunding (financement participatif)
			- investissement sur fonds propres 
		* Transformation en projet
	+ Suivi 
		* Fonctionnement déclaratif
		* Assignation des Taches et des sous taches
		* Validation des taches
		* Gestion des Paiements
- Bilan et Observatoire 
	+ Globale de l'activité
	+ Par Projet 
- Mode de Visualisation 
	+ Liste Complète, Compacte
	+ Excel
	+ PDF
	+ Kanban
		* Par Personne
		* Par Projet 
