# user Story

## Role
- Admin
- Porteur de dossier 
- option : Financeur
- option : Évaluateur

## En tant qu'admin

#### Modification du formualaire:

#### Gestion des utilisateurs:
Les administrateurs sont chargés de créer et de gérer les comptes des utilisateurs du système d'appel à projet. Ils peuvent attribuer des rôles et des autorisations appropriés à chaque utilisateur.

#### Configuration et personnalisation
Les administrateurs ont la responsabilité de configurer et de personnaliser le système d'appel à projet en fonction des besoins spécifiques de l'organisation. Cela peut inclure la personnalisation des formulaires, des flux de travail et des paramètres du système.

#### Sécurité et confidentialité
Les administrateurs doivent veiller à la sécurité et à la confidentialité des données du système d'appel à projet. Ils peuvent mettre en place des mesures de sécurité, gérer les sauvegardes, et s'assurer que seules les personnes autorisées ont accès aux informations sensibles.

#### Suivi et gestion des projets
 Les administrateurs peuvent suivre et gérer l'ensemble des projets soumis via le système d'appel à projet. Ils peuvent consulter les soumissions, gérer les étapes de l'évaluation, et générer des rapports ou des statistiques sur l'avancement des projets.

#### Support technique
Les administrateurs sont souvent chargés de fournir un support technique aux utilisateurs du système d'appel à projet. Ils peuvent répondre aux questions, résoudre les problèmes techniques et fournir une assistance générale aux utilisateurs.

## En tant que porteur de proposition