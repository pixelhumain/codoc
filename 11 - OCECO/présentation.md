# Présentation

OCECOForm
===
###### tags: `coform`
[PRESENTATION](https://docs.google.com/presentation/d/1tXM5WJK2PsWIvBNCpCwNJiTKwij40X3_LFnvg_8Hxn8/edit#slide=id.gb44acf559e_3_0)
[SCHEMA](https://docs.google.com/drawings/d/1vYq4fLmKjZxgHB51-L2pm65Y-fO4PVFnYzqj8R39Stw/edit)
[CHAT](https://chat.communecter.org/channel/ocecoform)
[PROJET]()
[DOCUMENTATION](https://codimd.communecter.org/Jrz7nr29QxisbCNVtcB3Sw)
[TEST QUALITé](https://codimd.communecter.org/v-CdwIIHSF2O41WKQjgrTw)


# V0.1
> **Objectif** : pouvoir utiliser toutes les etapes et sections
> commencer à remplir des propositions
> imaginer et participer à DEcider et Financer
> s'assurer que ocecoform et ocecomobi interagissent parfaitement
- [x]  Faire des propositions
    - remplir des actions
        - [ ] TESTED
    - POuvoir s'engager sur des actions
        - [ ] TESTED
    - Estimer une action
        - [ ] TESTED
    - [ ] TESTED 
        - https://oce.co.tools/organizations/555eba56c655675cdd65bf19/projects/rooms/6119fb7bde0a5568e14ed59d/room/611f3ce7eaac45616c3352b1/action/6336992fbb5f1818eb43dc98
        - https://oce.co.tools/organizations/555eba56c655675cdd65bf19/projects/rooms/6119fb7bde0a5568e14ed59d/room/611f3ce7eaac45616c3352b1/action/6336992fbb5f1818eb43dc98
        - https://oce.co.tools/organizations/555eba56c655675cdd65bf19/projects/rooms/6119fb7bde0a5568e14ed59d/room/611f3ce7eaac45616c3352b1/action/6336992fbb5f1818eb43dc98
        - https://oce.co.tools/organizations/555eba56c655675cdd65bf19/projects/rooms/6119fb7bde0a5568e14ed59d/room/611f3ce7eaac45616c3352b1/action/6336992fbb5f1818eb43dc98 
- [x] Décider/Discuter mode simple
- [x] Financer mode simple
- [x] Passage en projet
- [x] Etat d'avancement 
- [x] Suivre 
    - ajouter des taches
    - régler des taches
    - assurer le lien avec ocecomobi
- [x] Dashboard project simple

# V0.2
- [x] avoir un bread crumb partout pour se repérer
- [x] form proposition 
    - ajouter le contexte de la proposition grace au champs thématique
        - lister les projet ou sous orga
- [x] avoir plusieur ocecoform sur une orga 
    - sur differente sous orga / projet 
    - affichage dans l'observatoire globale de l'orga parent
- [x] bouton 5eme etape dashboard projet 
- [x] texte expliquant le passage en prpojet 
- [x] Dashboard globale de l'organisation simple
    - chiffre et information des projet de l'orga
    - graph des dynamique projet 
    - usages et disponibilité des ressource
- [x] Etat d'avancement
- [x] Decision complete
- [x] Dashboard project complet
- [x] Page Liste des propositions en tableau (voir les propositions sans forcément scroller)
- [x] Ajouter un filtre thématique des propositions (Liste du filtre thématique à configurer depuis l'oceco form spécifique)
- [x] Dupliquer l'input évaluation à double entrée sur 2 processus différents avec des critères (colonnes) différents à paramétrer : 
    - [x] Priorisation des propositions
    - [x] Evaluation des projets

# V0.3
- [x] export csv personnalisable 
- [x] vue Excel avec handsontable
- [x] export PDF
- [x] AAP : outil de communication  notification et contractualisation pour des dossier selectionné 

# V0.4
> **Objectif** : Implémentation des spécificités des acteurs
- [x] filtre observatoire par thematique
- [x]  filtre observatoire par localité
- [x] menu Vertical
- [x] candidature sur les dépense avec contrainte de badges
- [x] coRémunération
- [x] écran unique recap de la CORému simplifiant la gestion 
- [x] écran de recap en tableau de toute l'activité globale ou par projet
    - aficher le nb de nouvelle action (comme count NEW)
    - nom du projet 
    - nom de la personne qui a fait l'action
    - quelle action 
        - des valeur ou des params
    - filtre par projet 
- [ ] Dashboard perso user simple
    - a voir : endroit ou centrale de communication  
- [x] Vue temporelle
    - [x] Calendrier
    - [x] Gantt
    - [x] Timeline
- [x] View PPT
- [x] drag drop COForm Editor


# V0.5
- [x] filter Builder : construire des btn filtre configuré
- [ ] observatoire Builder : configurer soi meme des graphs et le connecter à la data
- [x] Etude et Amelio ergonomique
- [ ] ocecoTools
    - [x] Codate
    - [x] Evaluation Ariane
    - [x] Evaluation Bib
    - [ ] ....
- [ ] Financement : Systeme de paiement 
- [ ] Financement : crowdfunding
- [ ] Distribution de de budget , enveloppe nominative


# LISTE DES Réponse
- [ ] afficher en liste horizontale par default 
- [ ] pouvoir voter (thumb up down) depuis la ligne des réponses
- [ ] Titre doit etre celui du Titre de la proposition 

## etat d'avancement
- Nouvelle Proposition
- En decision
- En Financement 
- Passer en projet 
- En cours 
- Appel à participation 
- Terminé
- Abandonné

- [ ] Certains états qui appel à l'action peuvent notifier la communauté (notif, chat, mail)
- [ ] déplacer les bouton 'etat d'avancement dans le dashboard projet en mode badge en dessous des tache au dessus des graphs d'etape

# 4 étapes

## PROPOSER
- [x] Pouvoir envoyer le contextId et contextData des orgas qui utilise ocecoform depuis le dash des forms dans l'etape proposer
- [x] ajouter un bouton "Ajouter une question Specific" visible que par les admins de l'element qui utilise oceco
- [x] les admins de l'element qui utilise oceco ne pourront pas modifier oceco la base 
- [x] **Ajouter une question specific** specific s'enregistre dans la **collection inputs**
    - [ ] les inputs.specific.true seront mergé à la liste form.inputs de l'etape dans formBuilder


## DECIDER
- [ ] Pouvoir switcher et configurer le coform par proposition
    - il doit etre possible d'utilise des mode de gouvernance différent en fonction des types de proposition

![](https://codimd.communecter.org/uploads/upload_3fb4cf83dfe2c8fbf8ae143c1d4fbaa4.png)

## FINANCER
- [ ] Pouvoir configurer le ou les type de financement disponible sur l'étape 

- [ ] si crowdfunding > ajouter un lien vers une page publique de crowdfunding utilisant coform

## SUIVRE
- [ ] Transformer une proposition en projet, il existera alors dans oceco 
- [ ] connecte la communauté 
    - engagé
    - votante 
    - follower 
- [ ] créer les lignes actions correspondant aux dépenses
- [ ] Connecter la todo au tasks d'une action
- [ ] Afficher un btn, pour aller voir la fiche projet dans CO 

## DASH BOARD ORGA GLOBALE
## DASH BOARD PROJECTS
- pour visualiser :
    - les projets mourant dont il faut s'occuper ou decider d'abandonner 
    - les projets success et qu'on resoliciter 
## DASH BOARD PAR PROJET
- visualiser les pics de traffic, demander au client de les expliquer 
## DASH BOARD PAR PERSONNE

# bug
etat d'avancement reload



# Lexique 
## Vie d'un projet 
> **Proposition** 
Une idée, un contrat, un fonctionnalité, tout ce qui nous passe par la tete dans un contexte vivant 

> **Evaluation**
En fonction de la Gouvernance de l'organisation , une proposition passera par des étapes 
> Financement *

> Suivi

> Projet

> Actions 
    > Taches

