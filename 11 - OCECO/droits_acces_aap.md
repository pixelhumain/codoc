## I- Liste des droits d'accès
 **Agent mailer :** Seule la personne ayant le rôle "Agent mailer" est autorisée à accéder à l'interface d'envoi d'e-mails. Cette personne peut saisir le contenu et l'objet de l'e-mail, ainsi que sélectionner un modèle préétabli.
 
**Evaluateur :** L'accès à l'interface d'évaluation (étape 2) est réservé  aux personnes ayant le rôle d'"Evaluateur". Ces personnes ont le pouvoir de voter et de décider de l'admissibilité d'un dossier.
 
**Financeur :** Les financeurs ont un rôle crucial dans le système d'appel à projet. Ils sont responsables de fournir des ressources financières pour soutenir les projets sélectionnés. Leur rôle implique souvent l'évaluation des propositions de projet, la prise de décisions concernant l'octroi des financements et le suivi financier des projets financés. Les financeurs jouent un rôle clé dans la réalisation des objectifs du système d'appel à projet en contribuant au financement et à la réussite des projets sélectionnés.

**Administrateur :** Les administrateurs d'un système d'appel à projet ont des responsabilités essentielles pour gérer et superviser le système. Leurs rôles peuvent inclure :

- **Gestion des utilisateurs:** Les administrateurs sont chargés de créer et de gérer les comptes des utilisateurs du système d'appel à projet. Ils peuvent attribuer des rôles et des autorisations appropriés à chaque utilisateur.
- **Configuration et personnalisation:** Les administrateurs ont la responsabilité de configurer et de personnaliser le système d'appel à projet en fonction des besoins spécifiques de l'organisation. Cela peut inclure la personnalisation des formulaires, des flux de travail et des paramètres du système.
- **Sécurité et confidentialité:** Les administrateurs doivent veiller à la sécurité et à la confidentialité des données du système d'appel à projet. Ils peuvent mettre en place des mesures de sécurité, gérer les sauvegardes, et s'assurer que seules les personnes autorisées ont accès aux informations sensibles.
- **Suivi et gestion des projets:** Les administrateurs peuvent suivre et gérer l'ensemble des projets soumis via le système d'appel à projet. Ils peuvent consulter les soumissions, gérer les étapes de l'évaluation, et générer des rapports ou des statistiques sur l'avancement des projets.
- **Support technique:** Les administrateurs sont souvent chargés de fournir un support technique aux utilisateurs du système d'appel à projet. Ils peuvent répondre aux questions, résoudre les problèmes techniques et fournir une assistance générale aux utilisateurs.
