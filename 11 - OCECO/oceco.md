# Tutoriel Oceco
Open source Projet Management , 
Active and interactive Community Management 

# Doc
[Schema](https://docs.google.com/drawings/d/1Pa1u0u4A64GTvIt22BPNxiaz7RXWUblY8VY4JYZzCw4/edit)

# Installer
dans le navigateur : https://oce.co.tools
sur Android : https://play.google.com/store/apps/details?id=org.communecter.oceco&hl=fr&ah=lVN3mXqHKQjIOg3qHn0YzhiUebc
IPhone : 
Desktop : 
- sur chrome ouvrir https://oce.co.tools
- clicker installer oceco en PWA (Progressive Web App)

# Ecrans

## Menu
- Changer d'organisation : liste toutes les organisations au
- Crédit : liste toutes les acquisitions de crédits
    - Ajouter une Ligne de crédit 
        - TODO : expliquer 
## Organisation Porteuse
- Detail : Fiche de présentation de l'organisation
    - TAbleau de bord 
    - Bouton de configuration
- Action
- Notification
- Projet

## Profile Personnel
- Détail : à propos 
- Mes Organisations
- Actions Perso
- Ajouter une action
- Btn Config
    - confidentialité
    - oceco
        - notification
        - temps pomodoro

## Accueil
- Liste de tout les projets actif de l'orga porteuse
- Filtres dans le champs recherche (applicable partout)
    - **#** : filtrer par tags
    - **?** : filtrer par utilisateur
    - **:** : filtrer par utilisateur
    - à gauche trier 

## Todo
- Liste de toutes les actions qui me sont assignées
- Filtre 
    - par projet 

## Admin
- Validation des actions Terminés
- Configuration de l'outil
- Onglet projet 
    - Ajout de projet
- Onglet évennement
- Onglet actions : historiue de toutes les actions
- Onglet Membres : liste les membres de l'organisation

## Fiche Projet
- Detail
    - Présentation du proejt 
    - Tableau de bord des actions du projet
        - état de contributions par contributeur
        - 
- Actions
    - Ajouter une action
- Notifs
- contributeurs
    - [ ] TODO : pouvoir agir sur les contributeur 
- Events
    - Ajouter un event
- S'abonner pour suivre 
- Inviter quelqu'un
- Envoyer un message à tous les contributeurs
- Parametrer le projet


# Oceco Depuis Rocket Chat
OcecoMobi est totalement connecté à rocket Chat grace aux slash commande , pour comprendre , taper `/oceco` dans la zone de message, vous verez s'afficher tout les commandes disponibles 

#### /oceco-add-action
params : #projectSlug actionDesc @personAssigned
ajouter une action à un projet 

#### /oceco-element-dashboard-action
Synthèse des actions du channel projet en params ou dans lequel on est 

#### /oceco-element-list-action
Lister toute les taches du channel projet en params ou dans lequel on est 

#### /oceco-me-search-action
Chercher une action parmis mes actions

#### /oceco-me-list-action
Liste toute mes actions

#### /oceco-add-comment-action-id
params : actionId
ajouter un commentaire sur une action

#### /oceco-add-task-action
params : actionId
Ajouter une tache ou sous action sur une action

#### /oceco-element-dashboard-action
Retourne la synthese des action d'un projet

#### /oceco-list-comment-action
params : actionId
Liste tout les commentaire d'une action

#### /oceco-me-finish-action
params : actionId
Cloture Termine l'action

#### /oceco-me-list-action
Liste toute mes actions

