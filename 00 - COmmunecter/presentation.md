English version

# Présentation de Communecter
## C'est quoi COmmunecter ?
Notre équipe travaille depuis 2012 sur le développement d’une `plateforme collaborative COmmunecter`, projet d’`innovation sociétale et collaborative, ouvert à tous, libre et disponible sur internet` et applications mobiles. Elle permet de réunir les acteurs d’un territoire sur un même espace participatif et collaboratif permettant de créer une `synergie de territoire` au travers d’un large panel de fonctionnalités :
`dédiées à la connaissance et au partage` :  cartographie, géolocalisation, base de connaissances territoriale, la définition des besoins des uns, des offres des autres...
`dédiées à la collaboration et à l’animation` : outils collaboratifs, module de recherche territorial, réseau sociétal, la création de lien, événements, projets….
        
Notre `plateforme d’innovation sociétale`, déclinée en `COSTUM`, plateformes modélisées `sur mesure` en fonction des besoins, avec une approche systémique et ouverte : 
- Capacité à répondre aux défis des transformations de la société.
- Construire des solutions transversales pour répondre notamment aux enjeux économiques et environnementaux de tous les acteurs du territoire.
- Favoriser la participation et la collaboration des acteurs.
- Le caractère innovant et open-source de la plateforme sont également des atouts indéniables d’attraction pour un plus large public sensible à l’éthique d’une démarche. 

L’utilisation d’une plateforme de territoire comme support à une structuration et synergie de territoire est un réel avantage pour l’ensemble des acteurs :

#Numérique                   #Filière                  #Centralisé
#Innovation                  #Participation            #Collaboration 
#Co-construction             #Retombées économiques    #Simplicité 
#Gratuité d’utilisation      #Création de lien         #Dynamique
#Interaction                 #Gain de temps            #Efficacité
#Crowdsourcing 

COmmunecter est un projet sorti d’incubation à la Technopole de La Réunion en 2015. Open-Atlas  possède une bonne connaissance de la dynamique et du parcours vécu par les porteurs de projets et la plateforme est adaptée pour améliorer l'efficacité de nombreuses filières.

Nous sommes convaincus de l'importance d’amener les filière vers un réel réseau social et territorial dédié. 
Notre expertise en ce domaine nous a amené à créer plusieurs observatoires thématiques et communautaires  qui font leur preuve aujourd’hui et qui continuent à évoluer : 
- CTE : porté par le MTES ministère de la transition écologique et solidaire   au niveau national pour la thématique écologie, 107 agglomérations, 1250 projets de territoire sur le thème de l’écologie et bientôt 830 agglomérations participent au projet. 
- Pacte pour la transition : actions citoyennes avec mise en relation de 3000 communautés sur autant de communes, projet porté par le CTC et 62 organisations citoyennes
- Sommom : Observatoire trans DOM TOM et Métropole des cétacés marins porté par le CEDTM et Kelonia
- RIES : Réseau Italien de l’ESS porté par un grand collectif italien.
- ARESS : Réseau Réunionnais de l’ESS porté par la CRESS. 

Notre approche systémique, nous permet de regarder ou traiter une ou plusieurs thématiques séparément ou comme un ensemble. Avec sa forte approche territoriale, COmmunecter permet de se focaliser/zoomer sur un ou plusieurs territoires ciblés (communes, agglomération, région).

COmmunecter est une plateforme complète, modulaire avec de nombreuses fonctionnalités disponibles : 
- annuaire 
- agenda 
- géolocalisation  : tout est localisable
- CO FORMS : Formulaires permettant la récolte de données
- observatoire : Rendre l’information récoltée visuelle, exploitable, analysable et capitalisable.
- Tags, Open Badges et Parcours : caractériser l’information au sein d’une thématique pour mieux trier, filtrer, organiser l’information selon les usages et les personas
- Chat dédié à chaque thématique.
- CO Tools : serie d'outil open source connecté (Rocket Chat, Wekan, Next Cloud, Peertube, Activity Pub, CodeMD )

Avec une vision partenariale à long terme, la plateforme open source, construite comme un commun numérique, fait profiter, à chaque évolution, à tous ses utilisateurs de toutes les nouveautés, une sorte de cofinancement permanent avec lequel vous pourrez profiter d’innovations venant d’autres écosystèmes (solution alternative). 

Exemples :
- Outils d’appel à projet : Utiliser les fonctionnalités en développement pour la ville de Saint Denis et pour l’ANCT pour la gestion de propositions de nouveaux projets déposés à la Technopole avec un processus complet de : Proposition, évaluation, Financement, et suivi (pour en savoir plus)
- Outils de Gestion de projet innovant , OCECO porté par ANACT pour la Qualité de vie au travail
- Outils de Benchmark.
- Mutualisation de ressources (partage de serveur, de camions, de salles, imprimantes 3D...)
- Découvrir les savoirs faires intéressants pour mon projet  

En travaillant avec Open Atlas, vous investissez dans l’innovation à l’échelle nationale, tout ce qui est fait sur COmmunecter est générique et mutualisable, vous participez au développement d’un “vrai produit d’excellence national 100% Péi" ( dixit : Abel Hiol de la DRARI ), il est important de soutenir le développement d’outils territoriaux et tout cela en logiciel libre et ouvert, un vrai commun numérique. 


## Philosophie de COmmunecter
Pixel Humain ne désigne plus la plateforme mais la communauté participant à ce projet.

En savoir + sur l'historique et la raison d'être du projet : [code social](codesocial.md)
<iframe src="//player.vimeo.com/video/73771864?title=0&amp;amp;byline=0" width="800" height="450" allowfullscreen="allowfullscreen"></iframe>

## Quelles sont les possibilités de COmmunecter ?
Liste complète des fonctionnalités : [documentation](/2 - Utiliser l'outil/Fonctionnalités/liste.md)

<iframe src="//player.vimeo.com/video/133636468?title=0&amp;amp;byline=0" width="800" height="450" allowfullscreen="allowfullscreen"></iframe>

