# Vidéos de communecter

## Présentations publiques
<table id="bkmrk-date-visio-23%2F08%2F18-">
<thead><tr>
<th>Date</th>
<th>Visio</th>
</tr></thead>
<tbody>
<tr>
<td>23/08/18</td>
<td><a href="https://videos.lescommuns.org/videos/watch/b211d45b-3c7a-42fc-afab-cb0fd1560a0d">Université d'été solidaire et rebelle des mouvement sociaux et citoyens</a></td>
</tr>
<tr>
<td>14/08/18</td>
<td><a href="https://videos.lescommuns.org/videos/watch/a66dd3b5-544a-422c-8fa6-9bc361f59dfa"> Communecter : un réseau social en bien commun</a></td>
</tr>
<tr>
<td>16/11/17</td>
<td><a href="https://youtu.be/jLh6k7VpDwA" target="_blank" rel="noopener">Intéropérabilité (RGR2017)</a></td>
</tr>
<tr>
<td>20/09/17</td>
<td><a href="https://www.youtube.com/watch?v=QJ4BdSwDnsg" target="_blank" rel="noopener">Communecter aux biennales</a></td>
</tr>
<tr>
<td>17/12/16</td>
<td><a href="https://www.youtube.com/watch?v=kWcW1dJKEHI" target="_blank" rel="noopener">Communecter sur un écran tactile</a></td>
</tr>
<tr>
<td>01/10/16</td>
<td><a href="https://vimeo.com/161594058" target="_blank" rel="noopener">Et toi, tes Communecté ?</a></td>
</tr>
<tr>
<td>04/04/16</td>
<td><a href="https://www.youtube.com/watch?v=7-SBXj5VrZ8" target="_blank" rel="noopener">DO-it Yourself Démocratie</a></td>
</tr>
<tr>
<td>24/08/15</td>
<td><a href="https://www.youtube.com/watch?v=7c_fiFnuRHE" target="_blank" rel="noopener">Territoire connecté et Tourisme Collaboratif</a></td>
</tr>
<tr>
<td>16/07/15</td>
<td><a href="https://vimeo.com/133636468">Communecter - réseau sociétal libre</a></td>
</tr>
<tr>
<td>04/09/13</td>
<td>
<a href="https://vimeo.com/73771864" target="_blank" rel="noopener">Pixel Humain : réseau sociétal libre</a> (<a href="https://vimeo.com/74212373" target="_blank" rel="noopener">version courte</a>)</td>
</tr>
</tbody>
</table>

## Interview
<table id="bkmrk-24%2F07%2F18-radio-pikan" style="width: 326px;"><tbody>
<tr>
<td style="width: 78px;">24/07/18</td>
<td style="width: 248px;"><a href="https://youtu.be/f_PCESNMx6s" target="_blank" rel="noopener">Radio Pikan</a></td>
</tr>
<tr>
<td style="width: 78px;">07/04/16</td>
<td style="width: 248px;"><a href="https://www.youtube.com/watch?v=Ymqd53YP6DM&amp;t=2s" target="_blank" rel="noopener">Interview Accropolis</a></td>
</tr>
<tr>
<td style="width: 78px;">30/10/15</td>
<td style="width: 248px;"><a href="https://vimeo.com/147290335" target="_blank" rel="noopener">Interview Loca'Terre</a></td>
</tr>
<tr>
<td style="width: 78px;">15/09/15</td>
<td style="width: 248px;"><a href="https://www.youtube.com/watch?v=SvK7B3UL71Y" target="_blank" rel="noopener">Interview technopole de la réunion</a></td>
</tr>
</tbody></table>

## Réunions Pixel Humain
<table id="bkmrk-date-visio-24%2F09%2F18-">
<thead><tr>
<th>Date</th>
<th>Visio</th>
</tr></thead>
<tbody>
<tr>
<td>24/09/18</td>
<td><a href="https://videos.lescommuns.org/videos/watch/36bbef9e-680b-4dd1-b4c2-ff1a4221c417">Rendez-vous des pixels actifs #1</a></td>
</tr>
<tr>
<td>30/01/18</td>
<td><a href="https://youtu.be/MC2cJe6y0lo" target="_blank" rel="noopener">Campagne de dons récurrents #2</a></td>
</tr>
<tr>
<td>23/01/18</td>
<td><a href="https://www.youtube.com/watch?v=Kf7FxuG-N18" target="_blank" rel="noopener">Campagne de dons récurrents</a></td>
</tr>
<tr>
<td>19/10/17</td>
<td>
<a href="https://youtu.be/K6oKpFTQQ1o" target="_blank" rel="noopener">Évolution des sites internets de l'écosystème pixel humain</a> (<a href="https://youtu.be/BYa_d-HO-PE" target="_blank" rel="noopener">version courte</a>)</td>
</tr>
<tr>
<td>02/11/17</td>
<td>
<a href="https://www.youtube.com/watch?v=P9h_hyJrg0o" target="_blank" rel="noopener">Rencontre avec Sebastian de Gogocarto</a> (<a href="https://youtu.be/IPD-euec9uI" target="_blank" rel="noopener">version courte</a>)</td>
</tr>
<tr>
<td>19/10/17</td>
<td><a href="https://www.youtube.com/watch?v=jGgZj3h0lrw" target="_blank" rel="noopener">Point sur la communication de Communecter</a></td>
</tr>
<tr>
<td>26/06/17</td>
<td><a href="https://www.youtube.com/watch?v=yoKz3fppfII" target="_blank" rel="noopener">Trouvons le WHY de Communecter</a></td>
</tr>
</tbody>
</table>

## Rencontres
<table id="bkmrk-date-visio-14%2F07%2F17-">
<thead><tr>
<th>Date</th>
<th>Visio</th>
</tr></thead>
<tbody>
<tr>
<td>14/07/17</td>
<td><a href="https://www.youtube.com/watch?v=KFAazejZLeY" target="_blank" rel="noopener">Rencontre avec l’Assemblée des communs de Lille</a></td>
</tr>
<tr>
<td>27/07/16</td>
<td><a href="https://www.youtube.com/watch?v=DZUF6Ej7GTI" target="_blank" rel="noopener">Construire un commun</a></td>
</tr>
<tr>
<td>05/07/16</td>
<td><a href="https://www.youtube.com/watch?v=OLyOo1Snm18" target="_blank" rel="noopener">Nova Ideo – Open Source</a></td>
</tr>
<tr>
<td>28/06/16</td>
<td><a href="https://www.youtube.com/watch?v=nkR3KUYCHYI" target="_blank" rel="noopener">Synergie entre plateformes #2</a></td>
</tr>
<tr>
<td>19/02/16</td>
<td><a href="https://www.youtube.com/watch?v=US9R4drRCys" target="_blank" rel="noopener">Apéro Zéro #1 - Lancement du crowdfunding</a></td>
</tr>
<tr>
<td>19/10/15</td>
<td><a href="https://www.youtube.com/watch?v=US9R4drRCys" target="_blank" rel="noopener">Rencontre avec Michel Bauwens au sujet des licence P2P</a></td>
</tr>
<tr>
<td>06/10/15</td>
<td><a href="https://www.youtube.com/watch?v=rrafwjxukwg" target="_blank" rel="noopener">Rencontre avec TERA</a></td>
</tr>
<tr>
<td>22/09/15</td>
<td><a href="https://www.youtube.com/watch?v=TzoSPA5W5Cc" target="_blank" rel="noopener">Synergie internationale #3</a></td>
</tr>
<tr>
<td>08/01/15</td>
<td><a href="https://www.youtube.com/watch?v=BovYpSRO5as" target="_blank" rel="noopener">Metamaps</a></td>
</tr>
<tr>
<td>09/10/14</td>
<td><a href="https://www.youtube.com/watch?v=6fW8SXRENhc" target="_blank" rel="noopener">Synergie internationale #2</a></td>
</tr>
<tr>
<td>09/10/14</td>
<td><a href="https://www.youtube.com/watch?v=cmiezuJTTKs" target="_blank" rel="noopener">Discussion Jurique Cabinet Herbin </a></td>
</tr>
<tr>
<td>29/09/14</td>
<td><a href="https://www.youtube.com/watch?v=R91kOkNgEqY" target="_blank" rel="noopener">Synergie internationale</a></td>
</tr>
<tr>
<td>01/09/14</td>
<td><a href="https://www.youtube.com/watch?v=QOcozvASs9U" target="_blank" rel="noopener">Sharitories &amp; Pixel humain</a></td>
</tr>
<tr>
<td>28/08/14</td>
<td><a href="https://www.youtube.com/watch?v=_o7mgs0IyxA" target="_blank" rel="noopener">Synergie entre plateformes</a></td>
</tr>
</tbody>
</table>

## Technique
<table id="bkmrk-date-visio-01%2F08%2F17-">
<thead><tr>
<th>Date</th>
<th>Visio</th>
</tr></thead>
<tbody>
<tr>
<td>01/08/17</td>
<td><a href="https://www.youtube.com/watch?v=9Yb5bomm-QE" target="_blank" rel="noopener">Vocabulary, translation and interoperabilty, OCDB et COPI</a></td>
</tr>
<tr>
<td>29/07/17</td>
<td><a href="https://www.youtube.com/watch?v=2rkE69--LxM" target="_blank" rel="noopener">Présentation pour nouveaux développeurs</a></td>
</tr>
<tr>
<td>10/08/15</td>
<td><a href="https://www.youtube.com/watch?v=LiX9NjM4bbc" target="_blank" rel="noopener">Dev Team Weekly #2</a></td>
</tr>
<tr>
<td>24/07/15</td>
<td><a href="https://www.youtube.com/watch?v=6cN5maryHGU" target="_blank" rel="noopener">Dev Team Weekly</a></td>
</tr>
</tbody>
</table>

## Tutoriels
<table id="bkmrk-date-visio-05%2F05%2F17-">
<thead><tr>
<th>Date</th>
<th>Visio</th>
</tr></thead>
<tbody>
<tr>
<td>05/05/17</td>
<td><a href="https://www.youtube.com/watch?v=OOnEnxjhr-I" target="_blank" rel="noopener">Créer une carte</a></td>
</tr>
<tr>
<td>24/02/17</td>
<td><a href="https://www.youtube.com/watch?v=fqt_fkV0Mfo" target="_blank" rel="noopener">Tuto carte (modifier une fiche)</a></td>
</tr>
<tr>
<td>24/02/17</td>
<td><a href="https://www.youtube.com/watch?v=DTuzy7bWo-0" target="_blank" rel="noopener">Tuto carte (ajout d'une organisation)</a></td>
</tr>
<tr>
<td>24/02/17</td>
<td><a href="https://www.youtube.com/watch?v=hK5JaYYj0KA" target="_blank" rel="noopener">Tuto carte (création d'un compte)</a></td>
</tr>
<tr>
<td>24/02/17</td>
<td><a href="https://www.youtube.com/watch?v=XcuYgWg5zj0" target="_blank" rel="noopener">Tuto carte (description de l'interface)</a></td>
</tr>
<tr>
<td>08/07/16</td>
<td><a href="https://www.youtube.com/watch?v=ik5swBa5qco&amp;t=3s" target="_blank" rel="noopener">Prise en main de Communecter</a></td>
</tr>
<tr>
<td>21/06/16</td>
<td><a href="https://www.youtube.com/watch?v=66m1hm3A2ic&amp;t=25s" target="_blank" rel="noopener">Service d'Aide Communecter (SAC)</a></td>
</tr>
<tr>
<td>03/05/16</td>
<td><a href="https://www.youtube.com/watch?v=0VKR0oCLV2U" target="_blank" rel="noopener">Démo du site</a></td>
</tr>
</tbody>
</table>
