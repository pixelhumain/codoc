# Abécédaire 

## **Communecter**

Construit à partir de l’expression _“se connecter à sa commune_”, parfois raccourci en _CO_, _Communecter_ est le nom donné à l’outil technique.

> La plateforme Communecter est un logiciel libre, je contribue à son code source.

_Se communecter_ est un verbe d’action. Se dit d’une personne souhaitant **s’informer sur les actions portées dans la localité où l’on se trouve**.

> Je viens d'emménager à Poitiers, je me communecte dans mon quartier.
> Va te communecter ailleurs !

On parle parfois de _communexion_ pour parler de la vision localisée disponible sur l’outil.
L'adjectif communecter peut désigner un·e citoyen·ne ou un groupe d’individus partageant la connaissance de son écosystème.

> Mon association est communectée : j’ai référencé tous les projets qu’elle porte et indiqué tous les partenaires avec qui elle travaille


## Pixel Humain
Pixel Humain (ou simplement PH) est un **mouvement citoyen** souhaitant potentialiser la réflexion et l’action individuelle par l’intelligence collective que permet l’appropriation, l’usage, l’amélioration et le partage d’outils numériques libres.

Le Pixel Humain était aussi le nom de la première version de la plateforme technologique au service des individus pour la création, l’usage et la promotion d’outils numériques libres et révolutionnaires, devenue Communecter.

_Pixels humains_ est le surnom que nous donnons aux _Humains_ faisant partie de ce mouvement coopératif.

Etymologie : construit sur la métaphore de l’être humain comme l’un des pixels constituants l’image de la société et par analogie à son homonyme utilisé en chimie : le potentiel hydrogène (pH) en référence au potentiel humain déterminé et déterminants de son écosystème.

## Open Atlas
_Open Atlas_ (parfois raccourci en _opal_) est l’entité juridique (association loi 1901) portant le projet Communecter. Elle est garante du respect de l’éthique du projet. [En savoir +](openatlas.md).


## Écosystème
Ensemble des liens qu’une personne ou un groupe d’individus entretien avec son environnement social. Pour un individu l’écosystème désigne, par exemple, les associations dans lesquelles il est investi, les projets qu’il porte, ainsi que les événements auxquels il a participé. L’écosystème d’une association peut contenir les membres, mais aussi les partenaires, ou encore les financeurs s’il y en a.


## Territoire connecté
Terre de tradition adaptée aux échanges par les flux immatériel numériques et/ou matériels (aéroports, ports, gares, réseaux routiers) apportant à toute personne habitant ce territoire, même isolé, une capacité à établir des liens internationaux...

## Open data
Domaine de la liberté de l’information au service du bien commun comprenant des publications ou dossiers d’intérêt général en libre accès et réutilisables par tous.

## Open source
Domaine des sources logicielles en accès libre permettant, d’une part, la libre utilisation du logiciel concerné et ses dérivés, d’autre part, les échanges amenant à l’optimisation ou le développement commun du produit utilisé avec ses concepteurs toujours mentionnés.

## Code logiciel
Appelé aussi Code Source, c’est une suite d’instructions sous forme de texte exploité/transformé en langage de programmation permettant l’utilisation du logiciel.

## Cartographie de réseau
Logiciel de mise en liens graphiques d’une complexité de données d’origines diverses permettant d’appréhender un ensemble par la visualisation, de faire apparaître des données cachées, mais aussi de procéder à des analyses fines, ou de compléter des statistiques.

## Proxité
Capacité à créer du lien
Développer des services de proximité par liens d’affinités, de compétences, de complémentarités, d’intérêts ou de valeurs communs.

## Acteurs locaux
Institutions, organisations, entreprises, associations, citoyens dont le rôle social, économique ou politique leur confère une responsabilité pour intervenir dans la gestion du territoire.

## Vivre ensemble
Espace virtuel/matériel de rencontre incluant un comportement éthique appelant le respect mutuel, la tolérance, l’inclusion, l’ouverture, permettant l’expression d’un lien social riche et harmonieux projetant ceux qui y adhèrent vers une construction positive.

## Économie collaborative
Activité citoyenne permettant de produire de la valeur, des produits éco-conçus, des richesses immatérielles et réalisées dans le cadre de nouvelles méthodes transversales et en réseaux avec une mutualisation des biens, espaces et outils.

## Transition
Période s’inscrivant dans un temps de changement accompagnée la méthode pour passer d’un système/d’un concept à un autre et permettant sa mise en œuvre opérationnelle.

## Biens communs
Les communs sont des ressources partagées entre une communauté d’utilisateurs qui déterminent eux-mêmes le cadre et les normes régulant la gestion et l’usage de leur ressource.

> Il n’y a pas de commun sans « commoners ». (…) Il n’y a pas de commun sans agir en commun.
> New to the Commons ? David Bollier.

## Citoyens
Personne jouissant des droits civils et politiques du lieu où il vit et décidé à s’impliquer dans la vie de sa cité.

## Association
Convention par laquelle deux ou plusieurs personnes mettent en commun, d'une façon permanente, leurs connaissances, talents, activités dans un but utile à un objectif louable ou à la société, et non commercial.

## Charte collaborative des partenaires
Ensemble des règles que des partenaires de la même organisation collaborative doivent observer pour garantir la pérennité de leur projet dans un respect mutuel et au mieux de leurs intérêts communs et personnels.

## Réseau sociétal
Maillage de toutes les informations concernant la vie publique, cela concerne tous les participants de la vie civile du citoyen jusqu’à l’organisation de l’État.

## Citizen Tool Kit
Ensemble des outils mis à la disposition des citoyens pour aider à construire/utiliser les réseaux.

## Code social
Ensemble des règles à observer pour permettre une vie sociale respectant les droits civiques.

## Glocal
Vision globale de l’action locale / Action simultanée sur un territoire global et local.
