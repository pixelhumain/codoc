# Qui sommes-nous ?

## Pixels actifs
##### Contributeur·ice·s hyper-actifs
Ces personnes ont une forte implication au sein du projet. Salarié ou non d'[Open Atlas](openatlas.md) ils s'investissent parfois depuis plusieurs années pour développer le projet. 

Ces contributeur·ice·s historiques à COmmunecter sont ravi·e·s d'accueillir de nouvelles personnes. Ce n'est pas un cercle fermé dans lequel vous devez faire vos preuves mais bel et bien une communauté ouverte très sympathique :p.

Open Atlas embauche régulièrement des stagiaires et des services civique. Pour les découvrir et voir les rôles de chacun ça se passe [ici](https://communecter.org/#@openAtlas.view.directory.dir.members).

##### Ambassadeur·ice·s
Il existe également une belle [communauté d'ambassadeurs](https://www.communecter.org/#@coambassadeurs.view.directory.dir.members) qui développe l'usage de COmmunecter sur le territoire ou dans la communauté ou ielles sont.

##### Donateur·ice·s
Grâce à eux le projet se pérennise ! Cliquez sur le rôle "Donateur" dans la [communauté Pixel Humain](https://communecter.org/#@pixelhumain.view.directory.dir.members).

##### Pixels actifs
Ils contribuent régulièrement au projet de manière très diverse (cf . [Contribuer](/3 - Contribuer/accueilcontributeur.md)). Par exemple Stephan nous aide à traduire le site et Jean-Batiste nous fait parfois des suggestions d'améliorations.

Les ambassadeurs font bien sûr également partie de cette _ensemble_.

##### Collectifs et communautés amis
Nous sommes en relation avec de nombreux collectifs. On sent particulièrement proche et on échange régulièrement avec La Myne, Chez Nous et La Raffinerie.

Nos échanges avec d'autres grosses communautés (OpenStreetMap, Zéro Déchet, ...) sont documentés [ici](/1 - Le Projet/Nos amis/).
