-   [Transiscope](https://transiscope.org/presentation-du-projet) permet de **localiser** des alternatives citoyennes. Le but est de connecter différentes bases de données existantes et de les visualiser au même endroit.
-   [Communecter](https://www.communecter.org/) est un **réseau social** permettant de communiquer sur les territoires selectionné par les utilisateur·ice·s. C’est également un outil d’organisation permettant de discuter et de prendre des décisions de manière inclusive.

On nous demande régulièrement pourquoi nous ne travaillons pas avec le projet Transiscope. Cet article montre nos approches respectives et comment nous pouvons créer des ponts.

#### Transiscope agrège, Communecter permet de faire réseau

Le bus sémantique de Transiscope rend un très grand service aux utilisateur·ice·s qui n’ont pas à visiter 10 sites différends pour avoir une information exhaustive. Ça rend également un très grand service aux producteurs de données qui n’ont pas à se préoccuper de la manière dont ils doivent structurer leur base (framacalc, google sheets, API, …).
La force de Communecter est de proposer un espace commun où il peut y avoir de très nombreuses interactions entre les utilisateur·ice·s.

#### Communecter décrit le réel, Transiscope valorise les alternatives

Sur la base d’une charte le comité de pilotage de Transiscope décide en amont des sources pouvant se retrouver sur la carte partagée.
La modération de Communecter se fait à posteriori. Elle se base pour l’instant sur le cadre législatif global.

#### Intéropérabilité

-   Communecter a prévu d’ajouter Transiscope à la liste des sources de données du moteur de recherche
-   Transiscope intégrera les données de Communecter quand l’API permettra de faire ressortir exclusivement les éléments respectant la charte Transiscope (proposition de créer un tag `#transiscope` dans CO)
