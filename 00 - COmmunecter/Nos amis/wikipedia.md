Pierre-Antoine (membre du CA de Wikimédia France) pousse le projet **[Horizon 1%](https://meta.wikimedia.org/wiki/Horizon_1%25)** qui pourrait donner lieu a une belle collaboration. [Tom](https://communecter.org/#page.type.citoyens.id.57e5256640bb4eff07c4c9d6) a commenté ci-dessous les points de convergences.

Je pense qu'il pourrait y avoir beaucoup plus de liens entre Open Atlas (l'association portant Communecter) et Wikimédia France.

-   Nous avons clairement envie de cette synergie entre communs (numérique) que vous mentionner dans ce projet. Nous sommes déjà acteur de ce réseau : nous travaillons avec l'assemblée des communs de Lille, nous participons aux rencontres d'autres communs ([Zéro Déchet](/1 - Le Projet/Nos amis/zerodechet.md), [OpenStreetMap](http://open-atlas.org/sotm/), ...) ou en venant dans des lieux comme celui-ci :).
-   Nous développons des projets concrets allant dans ce sens. Vous concernant le projet CoPedia est quasiment opérationnel. Il permet de visualiser **et de contribuer à wikidata** (en typant des éléments) directement via Communecter. Nous serions ravi de discuter avec vous de son implémentation dans notre outil.
-   Nous serions ravi de participer à ce "tour de france des projets libre" et à la création du WikiLab.
-   Nous réfléchissons également à un système de crowdfunding interne qui ressemblerai à celui de [Ma Voix](https://collecte.mavoix.info/) mais adapté au financement de fonctionnalités/projets.

Puisque vous parlez "d'Expérimenter et adapter des solutions logicielles libres" je me permet de vous exposer comment Communecter pourrait outiller Horizon 1% à atteindre ces objectifs :

-   Le principe de "transparence par défaut et de confidentialité si besoin" est le fonctionnement de base de Communecter. Quoi que vous référencer (Évènement, Projet, Organisation, ...) il est en édition libre par défaut, puis en édition restreinte si quelqu'un le souhaite.
-   Communecter est prévu pour les communautés fédérées en groupes locaux comme Wikimédia :

1.  Au sein d’un groupe local Communecter permet de s’organiser grâce à ces différents outils de gouvernance ouverte ([en savoir + sur l'espace collaboratif](/2 - Utiliser l'outil/Fonctionnalités/espaceco.md) et [la messagerie](/2 - Utiliser l'outil/Fonctionnalités/chat.md)). N'importe qui peut lancer son projet (rattaché au groupe local ou à l'instance nationale) pour avoir son espace et le faire évolué de façon stimergique.
2.  Communecter rend plus efficace les échanges entre les groupes locaux. Grâce à l’actualité fédérée, vos membres et sympathisants restent informées de ce qui se passe au sein du réseau dans sa globalité.
3.  Globalement, vous positionnez wikimédia comme un acteur des communs de la connaissance ouverte

-   Vous pouvez lancer des [sondages publics](/2 - Utiliser l'outil/Fonctionnalités/sondages.md) pour recueillir le point de vue de vos utilisateur·ice·s et contributeur·ice·s.

Bref, si vous souhaitez ouvrir votre organisation à la communauté et être plus inclusif dans votre fonctionnement, Communecter me semble être une bonne piste.
