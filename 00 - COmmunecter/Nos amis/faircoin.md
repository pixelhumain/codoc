En mars 2018 nous avons participé au concours 200 FairCoin pour les organisations à but non lucratif" : [forum.fair.coop/t/candidature-communecter/389/7](https://forum.fair.coop/t/candidature-communecter/389/7)

Il est possible de nous faire des dons en Faircoin : [open-atlas.org/don](http://open-atlas.org/don/)

Faircoin est présent sur COmmunecter : [communecter.org/#@faircoop](https://www.communecter.org/#@faircoop)

Une visioconférence a eu lieu avec Florence de Faircoin le 15 novembre.
