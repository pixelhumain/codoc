[[_TOC_]]

# Bienvenue !

Communecter.org est une boîte à outils citoyenne collaborative, ouverte à tous, un réseau sociétal innovant, open source et libre, de développement de territoire avec une approche locale (quartier, arrondissement, commune, ville, ...). La plateforme offre des outils adaptés aux échanges, aux rencontres, aux débats, au montage de projets , à l'expression des besoins des uns et aux services offerts par les autres.

![](/Images/communecter-title.png)

## Quelle plateforme souhaitez vous ?

Nous vous proposons de créer votre propre plateforme personnalisée (appelée [COstum](https://doc.communecter.org/books/2---utiliser-loutil/page/costum)). Grâce à notre ingénieux système vous bénéficierez des informations présentes dans communecter.org, et ce qui sera publié sur votre plateforme sera également visible sur communecter.org.

-   Holoptisme et stigmergie de votre Organisation, Projet, Evenenement
    [Système d'appel à projets](https://doc.communecter.org/books/6---costum/page/systeme-d%E2%80%99appels-%C3%A0-projet)
-   [Observatoire de secteur](https://doc.communecter.org/books/6---costum/page/observatoire-de-secteur)
-   [Plateforme d'action citoyenne](https://doc.communecter.org/books/6---costum/page/plateforme-daction-citoyenne)
-   [Gestion de communauté et valorisation de ressources](https://doc.communecter.org/books/6---costum/page/gestion-de-communaut%C3%A9-et-valorisation-de-ressources)
-   [Réseau social](https://doc.communecter.org/books/6---costum/page/r%C3%A9seau-social-personnalis%C3%A9)
-   [Récolte de connaissances](https://doc.communecter.org/books/6---costum/page/r%C3%A9colte-de-connaissances)
-   [Gestion du temps bénévole & banque du temps](https://doc.communecter.org/books/6---costum/page/gestion-du-temps-b%C3%A9n%C3%A9volat-et-banque-de-temps)
    

## Vous êtes une organisation ?

Communecter répond au besoin de différent contexte et s’adapte à vos objectifs, si vous connaissez les objectifs que vous aimeriez atteindre , nous vous aiderons à les atteindre, et si nécessaire nous pouvons créer des outils sur mesure et mutualiser .

-   un group de citoyens ou une [Association](https://doc.communecter.org/books/2---utiliser-loutil/page/communecter-mon-association)
-   une Coopérative ou une [Fédération](https://doc.communecter.org/books/2---utiliser-loutil/page/communecter-ma-f%C3%A9d%C3%A9ration)
-   [Entreprise](https://doc.communecter.org/books/2---utiliser-loutil/page/communecter-mon-entreprise)
-   un secteur ou une [Filière](https://doc.communecter.org/books/2---utiliser-loutil/page/communecter-une-fili%C3%A8re) de société
-   un Territoire : Un région, un département, une agglomération, une [Commune](https://doc.communecter.org/books/2---utiliser-loutil/page/communecter-ma-commune)
-   [Espace Public Numérique](https://doc.communecter.org/books/2---utiliser-loutil/page/communecter-mon-epn)
-   [Média](https://doc.communecter.org/books/2---utiliser-loutil/page/communecter-mon-m%C3%A9dia)
    

## Faite le vous meme ?

-   Communecter un espace personnalisé
-   Créer un costum basé sur des template
-   Créer un questionnaire en tout autonomie
-   Connectez votre commune basé sur le COstum COcity
-   Connectez des secteur et filière locale et déployez un Territoire intelligent : SmarTerritoire
-   Envoyez nous votre cahier des rèves
    

## Quelques réalisations

Toute l’année nous développons des plateformes participatives, libres et locales.

[CARNET DE DEV](https://docs.google.com/presentation/d/e/2PACX-1vSeXLMiQTkCM7iKE17D_TfH3VUy-K5tOWCpEuPqT-IcbzqyZY37aixCvvpxogK2CMblgNfpHmRpwz-B/pub?start=false&loop=false&delayms=3000)
 
  

## Roadmap, Ligne de vie du projet  
Où sommes nous et va-t-on ?

-   vision 2020
    
-   en cours de développement en ce moment
    
-   Milestone et Réalisation mise en prod
    
-   Projets Réalisés et livrés
    

## Listes des modules et fonctionnalités

-     
    

## Comprendre, Participation , Implication

-   Comprendre le fonctionnement pour embarqué
    
-   Organigramme : qui fait quoi
    
-   Suivi de projet : Qu’est qu’il y aurait à faire en ce moment
    
-   Comprendre la Gouvernance Ouverte
    
-   J’aimerais participer , mais par où je commences
    
-   d’où on vient

